<?php
/**
 * Created by PhpStorm.
 * User: ayder
 * Date: 8/19/17
 * Time: 6:27 PM
 */

namespace app\controllers;

use app\models\CaptchaForm;
use app\models\Mark;
use app\models\Message;
use app\models\Notification;
use app\modules\user\models\Answer;
use app\modules\user\models\Task;
use app\modules\user\models\User;
use app\modules\user\models\Voting;
use Yii;
use yii\data\Pagination;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\HttpException;
use yii\web\UploadedFile;

/**
 * Class MessageController
 * @package app\modules\user\controllers
 * Actions for messaging between admins and users
 */
class MessageController extends AppController
{
    public $layout = 'user';

    /**
     * tasks for users
     * and list of users for admins
     * @return $this|string|HttpException
     */
    public function actionIndex()
    {
        $this->setMeta('Суперсушка');
        if (Yii::$app->user->isGuest) {
            return Yii::$app->response->redirect(['/user/auth/login']);
        }
        if (!Yii::$app->user->isGuest) {

            return $this->render('index');
        }
        return new HttpException(401);
    }

    public function actionDelete($id = false)
    {
        $this->setMeta('Суперсушка');
        if (Yii::$app->user->isGuest) {
            return $this->redirect('/user/auth/logout');
        }
        $this->setMeta('Суперсушка');
        if (!Yii::$app->user->isGuest) {

            if (isset($id)) {
                $answer = Answer::findOne(['id' => $id]);
                $answer->is_readed = 1;
                $answer->update();
                $this->redirect(['/user/help/feedback']);
            }
            return new HttpException(401);
        }
    }


    public
    function actionAjaxMarks()
    {
        if (Yii::$app->request->isAjax) {
            $marks = Mark::find()->where(['user_id' => Yii::$app->user->getId()])->asArray()->all();
//                return date("H:i:s");
            $result = json_encode($marks);
//            echo $tasks;
            return $result;
        }
    }

    public
    function actionAjaxTasks($week)
    {
        if (Yii::$app->request->isAjax) {
            $tasks = Task::find()->where(['week' => $week])->asArray()->all();
//                return date("H:i:s");
            $result = json_encode($tasks);
//            echo $tasks;
            return $result;
        }
    }

    /**
     * task_content for user and admin
     * @return $this|string|HttpException
     */
    public
    function actionTasks($week = false)
    {
        $this->setMeta('Суперсушка');

        if (Yii::$app->user->isGuest) {
            return Yii::$app->response->redirect(['/user/user/auth_error']);
        }

        if (!Yii::$app->user->isGuest) {

            //$id = Yii::$app->user->getId();

            if (isset($week) && $week == null) {
                $week = 1;
                $tasks = Task::find()->where(['week' => $week])->all();
                if (!isset($tasks))
                    return $this->render('tasks', compact('week'));
                return $this->render('tasks', compact('tasks', 'week'));
            } elseif (isset($week)) {
                $tasks = Task::find()->where(['week' => $week])->all();
                if (!isset($tasks))
                    return $this->render('tasks', compact('week'));
                return $this->render('tasks', compact('tasks', 'week'));
            }

//            return $this->render('tasks');
        }
        return new HttpException(401);

    }

    public
    function actionAjax()
    {
        if (Yii::$app->request->isAjax) {
            $data = Yii::$app->request->post();
            $message = explode(":", $data['message']);
            if ($message != null || $message != '') {
                $task_id = explode(":", $data['task_id']);
                $message = $message[0];
                $task_id = $task_id [0];
                //my code here
                $newMessage = new Message();

                $newMessage->text = Html::encode($message);
                $newMessage->user_id = Yii::$app->user->getId();
                $newMessage->task_id = $task_id;
                $newMessage->admin_id = Yii::$app->user->identity['parent_id'];

                \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
                if ($newMessage->validate()) {
                    $newMessage->save();
                    return $message;
                }
            }
            if ($data['message'] == null || $data['message'] == '')
                return 'error!! empty message -' . $message;
        }
    }

    public
    function actionCurrentTask($task_id = false, $admin_id = false)
    {
        $this->setMeta('Суперсушка');
        if (Yii::$app->user->isGuest) {
            return $this->redirect('/user/auth/logout');
        }
        if (!Yii::$app->user->isGuest) {

            if (!isset($admin_id) || $admin_id == null)
                $admin_id = Yii::$app->user->identity['parent_id'];
            $this->setMeta('Суперсушка');
            $task = Task::find()->where(['id' => $task_id])->one();
            $admin = User::find()->where(['id' => $admin_id])->one();

            $message = new Message();
            //chance using
            $voting = Voting::findOne(['user_id' => Yii::$app->user->getId()]);
            //
            if ($message->load(Yii::$app->request->post())) {
                $message->imageFiles = UploadedFile::getInstances($message, 'imageFiles');

                if ($message->messageUserToDB($message, $task_id)) {
                    if ($message->upload())
                        return $this->refresh();

                    //notification
                    $notification = new Notification();
                    $notification->task_id = $task_id;
                    $notification->type = 'messageAdmin';
                    //user id
                    $notification->user_id = Yii::$app->user->getId();
                    $notification->admin_id = $admin->id;
                    $notification->seen = 0;
                    //date
                    date_default_timezone_set('Etc/GMT-3');
                    $notification->created_at = date('Y-m-d H:i:s');
                    $notification->flashed = 0;
                    //user id
                    $notification->admin_name = $admin->name;
                    if ($notification->validate())
                        $notification->save();
                    //notification
                    return $this->refresh();
                } else {
                    return $this->render('current-task', compact('task', 'admin', 'message', 'voting'));
                }
            }

            if (isset($voting) && $voting->load(Yii::$app->request->post())) {
                return $this->immuneChanging($voting, $task, $admin, $message);
            }

            return $this->render('current-task', compact('task', 'admin', 'message', 'voting'));
        }
        return new HttpException(401);
    }

    private
    function findAnswer($id)
    {
        return Answer::find()->where(['id' => $id])->one();
    }

    public
    function actionResults()
    {
        $this->setMeta('Суперсушка');

        if (Yii::$app->user->isGuest) {
            return Yii::$app->response->redirect(['/user/user/auth_error']);
        }

        if (!Yii::$app->user->isGuest) {
            $voting = Voting::findOne(['user_id' => Yii::$app->user->getId()]);
            return $this->render('results', compact('voting'));
        }
        return new HttpException(401);

    }

    public
    function actionVoting()
    {
        $this->setMeta('Суперсушка');

        if (Yii::$app->user->isGuest) {
            return Yii::$app->response->redirect(['/user/auth/login']);
        }

        if (!Yii::$app->user->isGuest) {

            return $this->render('voting');
        }
        return new HttpException(401);

    }

    public
    function actionVotingSecond()
    {
        $this->setMeta('Суперсушка');

        if (Yii::$app->user->isGuest) {
            return Yii::$app->response->redirect(['/user/auth/login']);
        }

        if (!Yii::$app->user->isGuest) {
            return $this->render('voting-second');
        }
        return new HttpException(401);

    }

    public
    function actionVotingThird()
    {
        $this->setMeta('Суперсушка');

        if (Yii::$app->user->isGuest) {
            return Yii::$app->response->redirect(['/user/auth/login']);
        }

        if (!Yii::$app->user->isGuest) {
            return $this->render('voting-third');
        }
        return new HttpException(401);

    }

    /**
     * @param $voting
     * @param $task
     * @param $admin
     * @param $message
     * @return string|\yii\web\Response
     */
    private
    function immuneChanging(Voting $voting, Task $task, $admin, $message)
    {
        if ($voting->changeChance && $voting->chance == 2) {
            $voting->chance -= 1;
            $voting->task_id = $task->id;
            $voting->update();
            $user = User::findOne(['id' => Yii::$app->user->getId()]);
            $user->role_id = 2;
            $user->update();
            Yii::$app->session->setFlash('successImmune');
            return $this->refresh();
        } else {
            Yii::$app->session->setFlash('errorImmune');
            return $this->render('current-task', compact('task', 'admin', 'message', 'voting'));
        }
    }


    public
    function actionCurrent()
    {
        $this->setMeta('Сообщения', 'СуперСушка', 'СуперСушка');
        $messages = Message::find()
            ->where(['user_id' => Yii::$app->user->getId()])
            ->all();
        $admin = User::findOne(['id' => Yii::$app->user->identity['parent_id']]);

        $newMessage = new Message();

        if ($newMessage->load(Yii::$app->request->post())) {
            $newMessage->imageFiles = UploadedFile::getInstances($newMessage, 'imageFiles');

            if ($newMessage->messageUserToDB($newMessage, $newMessage->task_id)) {
                Yii::$app->session->setFlash('success');
                if ($newMessage->upload()) {
                    Yii::$app->session->setFlash('success');
                    return $this->refresh();
                } else {
                    Yii::$app->session->setFlash('error');
                    return $this->refresh();
                }
            } else {
                Yii::$app->session->setFlash('error');
                return $this->render('current', compact('messages', 'admin', 'newMessage'));
            }
        }
        return $this->render('current', compact('messages', 'admin', 'newMessage'));
    }


    public
    function actionUpload()
    {
        if (Yii::$app->request->isAjax) {
            if (is_array($_FILES)) {
                if (is_uploaded_file($_FILES['userImage']['tmp_name'])) {
                    $sourcePath = $_FILES['userImage']['tmp_name'];
                    $targetPath = "images/" . $_FILES['userImage']['name'];
                    if (move_uploaded_file($sourcePath, $targetPath)) {
                        $string = "<img class='image-preview' src='/$targetPath' 
                            width='275' height='200' />";
                        return $string;
                    }
                }
            }
        }
    }

}