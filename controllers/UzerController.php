<?php
/**
 * supersushka - UzerController.php * Initial version by: Tom
 * Initial created on: 22.09.2017 11:47
 */

namespace app\controllers;


use app\models\Comment;
use app\models\Follow;
use app\models\Mark;
use app\models\Message;
use app\models\Notification;
use app\models\Post;
use app\models\PostUpdateForm;
use app\models\Report;
use app\models\SearchForm;
use app\models\VotesNewsUser;
use app\modules\user\models\Answer;
use app\modules\user\models\Image;
use app\modules\user\models\Task;
use app\modules\user\models\User;
use app\modules\user\models\UserQuestion;
use Yii;
use yii\data\Pagination;
use yii\db\StaleObjectException;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\HttpException;

class UzerController extends AppController
{
    public $layout = 'user';
    public $defaultAction = 'view';

    public function beforeAction($action)
    {
        if ($action->id == 'view') {
//            $this->enableCsrfValidation = false;
        }
        return parent::beforeAction($action);
    }

    public function actionView($post_id = false, $user_id = false)
    {
        $this->setMeta('Суперсушка');
        if (Yii::$app->user->isGuest) {
            return $this->redirect('/user/auth/login');
        }

        if (!Yii::$app->user->isGuest) {
            $user = User::findOne(['id' => $user_id]);
            $post = new Post();
            $comment = new Comment();

            if ($post->load(Yii::$app->request->post()) &&
                $post->publishPost(Yii::$app->user->getId(), Yii::$app->user->identity['role_id'])) {
                return $this->refresh();
            }
            if ($comment->load(Yii::$app->request->post())) {
                $comment->user_id = Yii::$app->user->getId();
                $comment->post_id = $post_id;
                $comment->text = Html::encode($comment->text);
                $comment->save();
                return $this->refresh();
            }
            if (Yii::$app->user->identity['role_id'] < 5 || $user_id != null) {
                if (isset($user)) {
                    return $this->render('view', compact('post', 'comment', 'user'));
                } else
                    return $this->render('view', compact('post', 'comment'));
            } elseif (Yii::$app->user->identity['role_id'] >= 5 && $user_id == null) {
                $adminPost = $post;
                if (isset($user)) {
                    return $this->render('view-admin', compact('adminPost', 'comment', 'user'));
                } else
                    return $this->render('view-admin', compact('adminPost', 'comment'));
            }
            return new HttpException(401);
        }
    }

    public
    function actionUpdate($id, $view = false)
    {
        $this->setMeta('Изменить пост', 'Суперсушка', 'Суперсушка');
        if (isset($id)):
            $modelPost = Post::findOne(['id' => $id]);

            if ($modelPost->load(Yii::$app->request->post())) {
                if ($modelPost->update()) {
                    Yii::$app->session->setFlash('successPostUpdate');
                    switch ($view):
                        case  'view':
                            return $this->redirect(['/uzer/view']);
                            break;
                        case  'news':
                            return $this->redirect(['/uzer/news']);
                            break;
                        case  'followers':
                            return $this->redirect(['/uzer/followers']);
                            break;
                    endswitch;
                    return $this->redirect(['/uzer/view']);
                } else {
                    //$er = 'Error!@';
                    Yii::$app->session->setFlash('errorPostUpdate');
                    switch ($view):
                        case  'view':
                            return $this->redirect(['/uzer/view']);
                            break;
                        case  'news':
                            return $this->redirect(['/uzer/news']);
                            break;
                        case  'followers':
                            return $this->redirect(['/uzer/followers']);
                            break;
                    endswitch;
                    return $this->redirect(['/uzer/view']);
                }
            }
            if (isset($view))
                return $this->render('update', compact('modelPost', 'view'));
            return $this->render('update', compact('modelPost', 'view'));
        endif;
        return new HttpException(401);
    }

    public
    function actionReport($user_id = false, $post_id = false, $type = false, $route = false)
    {
        //СПРОСИТЬ У Сергея - возможно лучше будет передавать только пользователя, а не причину - для удаления его из системы.
        $report = new Report();
        $report->user_id = $user_id;
        $report->type = $type;
        if (isset($post_id)):

            $report->post_id = $post_id;
            if ($report->validate()) {
                //notification
                $notification = new Notification();
                //task id
                $notification->type = 'report';
                $notification->task_id = $report->id;
                //user id
                $admin = User::findOne(['id' => Yii::$app->user->identity['parent_id']]);
                $notification->user_id = Yii::$app->user->getId();
                $notification->admin_id = $admin->id;
                $notification->seen = 0;
                //date
                date_default_timezone_set('Etc/GMT-3');
                $notification->created_at = date('Y-m-d H:i:s');
                $notification->flashed = 0;
                //user id
                $notification->admin_name = $admin->name;
                if ($notification->validate())
                    $notification->save();
                //notification

                $report->save();
                Yii::$app->session->setFlash('successPostReport');
                if (isset($route)) {
                    if ($route == 'followers')
                        return $this->redirect('/uzer/followers');
                }
                return $this->redirect('/uzer/news');
            } else {
                Yii::$app->session->setFlash('errorPostReport');
                if (isset($route)) {
                    if ($route == 'followers')
                        return $this->redirect('/uzer/followers');
                }
                return $this->redirect('/uzer/news');
            }

        endif;

        return new HttpException(401);
    }

    public
    function actionReportComment($user_id = false, $comment_id = false, $type = false, $route = false)
    {
        //СПРОСИТЬ У Сергея - возможно лучше будет передавать только пользователя, а не причину - для удаления его из системы.
        $report = new Report();
        $report->user_id = $user_id;
        $report->type = $type;

        if (isset($comment_id)):
            $report->comment_id = $comment_id;
            if ($report->validate()) {
                //notification
                $notification = new Notification();
                //task id
                $notification->type = 'report';
                $notification->task_id = $report->id;
                //user id
                $admin = User::findOne(['id' => Yii::$app->user->identity['parent_id']]);
                $notification->user_id = Yii::$app->user->getId();
                $notification->admin_id = $admin->id;
                $notification->seen = 0;
                //date
                date_default_timezone_set('Etc/GMT-3');
                $notification->created_at = date('Y-m-d H:i:s');
                $notification->flashed = 0;
                //user id
                $notification->admin_name = $admin->name;
                if ($notification->validate())
                    $notification->save();
                //notification
                $report->save();
                Yii::$app->session->setFlash('successCommentReport');
                if (isset($route)) {
                    if ($route == 'followers')
                        return $this->redirect('/uzer/followers');
                    if ($route == 'view')
                        return $this->redirect('/uzer/view');
                }
                return $this->redirect('/uzer/news');
            } else {
                Yii::$app->session->setFlash('errorCommentReport');
                if (isset($route)) {
                    if ($route == 'followers')
                        return $this->redirect('/uzer/followers');
                    if ($route == 'view')
                        return $this->redirect('/uzer/view');
                }
                return $this->redirect('/uzer/news');
            }
        endif;

        return new HttpException(401);
    }

    public
    function actionDelete($id = false, $report_id = false, $type = false)
    {
        $this->setMeta('Суперсушка');
        if (Yii::$app->user->isGuest) {
            return $this->redirect('/user/auth/error');
        }

        if (!Yii::$app->user->isGuest) {
            if (isset($id) && $id != null) {
                if (Post::deleteAll(['in', 'id', $id])) {
                    $comments = Comment::find()->where(['post_id' => $id])->all();
                    if (isset($comments)) {
                        Comment::deleteAll(['in', 'post_id', $id]);
                    }
                    Yii::$app->session->setFlash('successPostDelete');
                    $this->refresh();
                } else {
                    Yii::$app->session->setFlash('errorPostDelete');
                    $this->refresh();
                }
            }

            //REPORT PART

            if (isset($report_id) && $report_id != null) {
                if (Report::deleteAll(['in', 'id', $report_id])) {

                    Yii::$app->session->setFlash('successReportDelete');
                    $this->redirect(['/uzer/view']);
                }
            } else {
                Yii::$app->session->setFlash('errorReportDelete');
                $this->redirect(['/uzer/view']);
            }
        }
        return new HttpException(401);
    }

    public
    function actionDeleteNews($id = false, $report_id = false)
    {
        $this->setMeta('Суперсушка');
        if (Yii::$app->user->isGuest) {
            return $this->redirect('/user/auth/error');
        }
        if (!Yii::$app->user->isGuest) {

            if (isset($id)) {
                if (Post::deleteAll(['in', 'id', $id])) {
                    $comment = Comment::find()->where(['post_id' => $id])->all();
                    if (isset($comment)) {
                        Comment::deleteAll(['in', 'post_id', $id]);
                    }
                    Yii::$app->session->setFlash('successPostDelete');
                    if (isset($report_id)) {
                        if (Report::deleteAll(['in', 'id', $report_id])) {

                            Yii::$app->session->setFlash('successReportDelete');
                            $this->redirect(['/uzer/view']);
                        } else {
                            Yii::$app->session->setFlash('errorReportDelete');
                            $this->redirect(['/user/view']);
                        }
                    }
                    $this->redirect(['view']);
                }
            } else {
                Yii::$app->session->setFlash('errorPostDelete');
                $this->redirect(['view']);
            }
        }
        return new HttpException(401);
    }

    public
    function actionDeleteFol($id = false)
    {
        $this->setMeta('Суперсушка');
        if (Yii::$app->user->isGuest) {
            return $this->redirect('/user/auth/error');
        }
        if (!Yii::$app->user->isGuest) {

            if (isset($id)) {
                if (Post::deleteAll(['in', 'id', $id])) {
                    $comment = Comment::find()->where(['post_id' => $id])->all();
                    if (isset($comment)) {
                        Comment::deleteAll(['in', 'post_id', $id]);
                    }
                    Yii::$app->session->setFlash('successPostDelete');
                    $this->redirect(['followers']);
                }
            } else {
                Yii::$app->session->setFlash('errorPostDelete');
                $this->redirect(['followers']);
            }
        }
        return new HttpException(401);
    }


    public
    function actionDeleteCommentView($id = false, $user_id = false)
    {
        $this->setMeta('Суперсушка');
        if (Yii::$app->user->isGuest) {
            return $this->redirect('/user/auth/error');
        }
        if (!Yii::$app->user->isGuest) {

            if (isset($id)) {
                if (Comment::deleteAll(['in', 'id', $id])) {
                    Yii::$app->session->setFlash('successCommentDelete');
                    $user = User::findOne(['id' => $user_id]);
                    $post = new Post();
                    $comment = new Comment();

                    return $this->render('view', compact('post', 'comment', 'user'));
                }
            } else {
                $user = User::findOne(['id' => $user_id]);
                $post = new Post();
                $comment = new Comment();
                Yii::$app->session->setFlash('errorCommentDelete');
                return $this->render('view', compact('post', 'comment', 'user'));
            }
        }
        return new HttpException(401);
    }

    public
    function actionDeleteCommentNews($id = false, $report_id = false, $route = false)
    {
        $this->setMeta('Суперсушка');
        if (Yii::$app->user->isGuest) {
            return $this->redirect('/user/auth/error');
        }
        if (!Yii::$app->user->isGuest) {

            if (isset($id)) {
                if (Comment::deleteAll(['in', 'id', $id])) {
                    Yii::$app->session->setFlash('successCommentDelete');
                    if (isset($report_id)) {
                        if (Report::deleteAll(['in', 'id', $report_id])) {
                            Yii::$app->session->setFlash('successReportDelete');
//                            $this->redirect(['/uzer/view']);
                        } else {
                            Yii::$app->session->setFlash('errorReportDelete');
//                        $this->redirect(['/user/help/report']);
                        }
                    }
                    if (isset($route) && $route == 'view')
                        $this->redirect(['view']);
                    $this->redirect(['news']);
                }
            } else {
                Yii::$app->session->setFlash('errorCommentDelete');
                if (isset($route) && $route == 'view')
                    $this->redirect(['view']);
                $this->redirect(['news']);
            }
        }
        return new HttpException(401);
    }

    public
    function actionDeleteCommentFol($id = false)
    {
        $this->setMeta('Суперсушка');
        if (Yii::$app->user->isGuest) {
            return $this->redirect('/user/auth/error');
        }
        if (!Yii::$app->user->isGuest) {

            if (isset($id)) {
                if (Comment::deleteAll(['in', 'id', $id])) {
                    Yii::$app->session->setFlash('successCommentDelete');
                    $this->redirect(['followers']);
                }
            } else {
                Yii::$app->session->setFlash('errorCommentDelete');
                $this->redirect(['followers']);
            }
        }
        return new HttpException(401);
    }

    public
    function actionDeleteUserNews($id = false)
    {
        $this->setMeta('Суперсушка');
        if (Yii::$app->user->isGuest) {
            return $this->redirect('/user/auth/error');
        }
        if (!Yii::$app->user->isGuest) {

            if (isset($id)) {
                if (User::deleteAll(['in', 'id', $id])) {
                    $img = Image::findOne(['user_id' => $id]);
                    if (isset($img))
                        Image::deleteAll(['in', 'user_id', $id]);

                    $posts = Post::findAll(['user_id' => $id]);
                    if (isset($posts))
                        Post::deleteAll(['in', 'user_id', $id]);

                    $marks = Mark::findAll(['user_id' => $id]);
                    if (isset($marks))
                        Mark::deleteAll(['in', 'user_id', $id]);

                    $messages = Message::findAll(['user_id' => $id]);
                    if (isset($messages))
                        Message::deleteAll(['in', 'user_id', $id]);
                    $this->redirect(['/uzer/news']);
                }
            } else {
                $this->redirect(['/uzer/news']);
            }
        }
        return new HttpException(401);
    }

    public
    function actionDeleteUserFol($id = false)
    {
        $this->setMeta('Суперсушка');
        if (Yii::$app->user->isGuest) {
            return $this->redirect('/user/auth/error');
        }
        if (!Yii::$app->user->isGuest) {

            if (isset($id)) {
                if (User::deleteAll(['in', 'id', $id])) {
                    $this->redirect(['/uzer/followers']);
                }
            } else {
                $this->redirect(['/uzer/followers']);
            }
        }
        return new HttpException(401);
    }

    /**
     * posts from all users
     * @return $this|string|HttpException
     */
    public
    function actionNews($post_id = false)
    {
        $this->setMeta('Суперсушка');

        if (Yii::$app->user->isGuest) {
            return Yii::$app->response->redirect(['/user/user/error']);
        }
        if (!Yii::$app->user->isGuest) {
            $post = new Post();
            $comment = new Comment();
            if ($post->load(Yii::$app->request->post()) &&
                $post->publishPost(Yii::$app->user->getId(), Yii::$app->user->identity['role_id'])) {
                return $this->refresh();
            }
            if ($comment->load(Yii::$app->request->post())) {
                $comment->user_id = Yii::$app->user->getId();
                $comment->post_id = $post_id;
                $comment->text = Html::encode($comment->text);
                $comment->save();
                return $this->refresh();
            }
//            if($comment)
            return $this->render('news', compact('post', 'comment'));
        }
        return new HttpException(401);
    }

    public
    function actionFollow($user_id)
    {
        $followUser = User::findOne(['id' => $user_id]);
        $follow = new Follow();
        //follow data
        $follow->follow_id = $user_id;
        $follow->follow_name = $followUser->name;
        $follow->follow_surname = $followUser->surname;
        //follower data
        $follow->follower_id = Yii::$app->user->getId();
        $follow->follower_id = Yii::$app->user->getId();
        $follow->follower_name = Yii::$app->user->identity['name'];
        $follow->follower_surname = Yii::$app->user->identity['surname'];
        $follow->save();

        return $this->redirect('news');
    }

    public
    function actionUnfollow($user_id)
    {
        $unFollow = Follow::find()->where(['follow_id' => $user_id, 'follower_id' => Yii::$app->user->getId()])->one();
        if (isset($unFollow)) {
            try {
                $unFollow->delete();
            } catch (StaleObjectException $e) {
            } catch (\Exception $e) {
            } catch (\Throwable $e) {
            }
            return $this->refresh('news');
        } else
            return $this->redirect('news');
    }

    public
    function actionUnfollowFol($user_id)
    {
        $unFollow = Follow::find()->where(['follow_id' => $user_id, 'follower_id' => Yii::$app->user->getId()])->one();
        if (isset($unFollow)) {
            try {
                $unFollow->delete();
            } catch (StaleObjectException $e) {
            } catch (\Exception $e) {
            } catch (\Throwable $e) {
            }
            return $this->refresh('followers');
        } else
            return $this->redirect('followers');
    }

    public
    function actionUnfollowView($user_id)
    {
        $unFollow = Follow::find()->where(['follow_id' => $user_id, 'follower_id' => Yii::$app->user->getId()])->one();
        $user = User::findOne(['id' => $user_id]);
        $post = new Post();
        $comment = new Comment();
        if (isset($unFollow)) {
            try {
                $unFollow->delete();
            } catch (StaleObjectException $e) {
            } catch (\Exception $e) {
            } catch (\Throwable $e) {
            }
            return $this->render('view', compact('post', 'comment', 'user'));
        } else
            return $this->render('view', compact('post', 'comment', 'user'));
    }

    public
    function actionFollowView($user_id)
    {
        $followUser = User::findOne(['id' => $user_id]);
        $follow = new Follow();
        //follow data
        $follow->follow_id = $user_id;
        $follow->follow_name = $followUser->name;
        $follow->follow_surname = $followUser->surname;
        //follower data
        $follow->follower_id = Yii::$app->user->getId();
        $follow->follower_name = Yii::$app->user->identity['name'];
        $follow->follower_surname = Yii::$app->user->identity['surname'];
        $follow->save();
        $user = User::findOne(['id' => $user_id]);
        $post = new Post();
        $comment = new Comment();
        return $this->render('view', compact('post', 'comment', 'user'));
    }


    /**
     * posts from all user follows
     * @return $this|string|HttpException
     */
    public
    function actionFollowers($post_id = false)
    {
        $this->setMeta('Суперсушка');

        if (Yii::$app->user->isGuest) {
            return Yii::$app->response->redirect(['/user/user/error']);
        }
        if (!Yii::$app->user->isGuest) {
            $post = new Post();
            $comment = new Comment();
            if ($post->load(Yii::$app->request->post()) &&
                $post->publishPost(Yii::$app->user->getId(), Yii::$app->user->identity['role_id'])) {
                return $this->refresh();
            }
            if ($comment->load(Yii::$app->request->post())) {
                $comment->user_id = Yii::$app->user->getId();
                $comment->post_id = $post_id;
                $comment->text = Html::encode($comment->text);
                $comment->save();
                return $this->refresh();
            }
            return $this->render('followers', compact('post', 'comment'));
        }
        return new HttpException(401);
    }

    public
    function actionSearch()
    {
        $this->setMeta('Суперсушка');

        if (Yii::$app->user->isGuest) {
            return Yii::$app->response->redirect(['/user/user/error']);
        }
        if (!Yii::$app->user->isGuest) {
            $result = '';
            $search = new SearchForm();
            if ($search->load(Yii::$app->request->post())) {
                $result = $search->findUsers();
                if ($result != null) {
                    Yii::$app->session->setFlash('successSearch');
                    return $this->render('search', compact('search', 'result'));
                } else {
                    $result = null;
                    Yii::$app->session->setFlash('errorSearch');
                    return $this->render('search', compact('search', 'result'));
                }


            }
//            $post = new Post();
//            $comment = new Comment();
//            if ($post->load(Yii::$app->request->post()) && $post->publishPost(Yii::$app->user->getId())) {
//                return $this->refresh();
//            }
//            if ($comment->load(Yii::$app->request->post())) {
//                $comment->user_id = Yii::$app->user->getId();
//                $comment->post_id = $post_id;
//                $comment->save();
//                return $this->refresh();
//            }
            return $this->render('search', compact('search', 'result'));
        }
        return new HttpException(401);
    }

    public function actionNotifyAll()
    {
        //ajax
        if (Yii::$app->request->isAjax) {
            $data = Yii::$app->request->post();
            $user_id = explode(":", $data['user_id']);
            if ($user_id != null) {
                $notifications = Notification::find()
                    ->where(['user_id' => $user_id])
                    ->all();
                foreach ($notifications as $notification) {
                    $notification->seen = 1;
                    if ($notification->validate()) {
                        try {
                            $notification->update();
                        } catch (StaleObjectException $e) {
                        } catch (\Exception $e) {
                        } catch (\Throwable $e) {
                        }
                    }
                }
                \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
                return "success";
            }
            if ($data['user_id'] == null || $data['user_id'] == '')
                return 'error!! empty message -' . $user_id;
            //ajax
        }
    }

    public function actionNotify()
    {
        $this->setMeta('Суперсушка', "СуперСушка", "СуперСушка - ЗОЖ игра, 
        где Вы получаете деньги за сброшенный вес!");

        if (Yii::$app->user->isGuest) {
            return $this->redirect('/user/auth/login');
        } elseif (!Yii::$app->user->isGuest) {
            if (Yii::$app->user->identity['role_id'] < 5) {
                $notifies = \app\models\Notification::find()
                    ->where(['admin_id' => 0])
//                    ->andWhere(['seen' => 0])
                    ->all();
            } elseif (Yii::$app->user->identity['role_id'] > 4) {
                $notifies = \app\models\Notification::find()
                    ->where(['>', 'admin_id', 0])
//                    ->andWhere(['seen' => 0])
                    ->all();
            }

            return $this->render('notify', compact('notifies'));
        }

        return new HttpException(404);
    }

    /**
     * @return HttpException|\yii\web\Response
     */
    public function actionAjaxLikes()
    {
        $this->setMeta('Суперсушка', "СуперСушка", "СуперСушка - ЗОЖ игра, 
        где Вы получаете деньги за сброшенный вес!");

        if (Yii::$app->user->isGuest) {
            return $this->redirect('/user/auth/login');
        } elseif (!Yii::$app->user->isGuest) {
            if (Yii::$app->request->isAjax) {
                // контейнер для ошибок
                $error = false;
                // получение данных
                $userId = (int)$_POST['id_user'];
                $newsId = (int)$_POST['id_news'];
                $type = $_POST['type'];

                // проверяем, голосовал ранее пользователь за эту новость или нет
                $sql = VotesNewsUser::find()
                    ->where(['id_user' => $userId])
                    ->andWhere(['id_news' => $newsId])
                    ->one();

                // если что-то пришло из запроса, значит уже голосовал
                //var_dump($result);exit;
                if ($sql != null) {
                    $error = 'Вы уже голосовали';
                } else { // если пользователь не голосовал, проголосуем
                    // получем поле для голосования - лайк или дизлайк
                    if ($type == 'like') $fieldName = 'count_like';
                    // делаем запись о том, что пользователь проголосовал
                    $newLike = new VotesNewsUser();
                    $newLike->id_user = $userId;
                    $newLike->id_news = $newsId;
                    $newLike->save();
                }

                // делаем ответ для клиента
                if ($error) {
                    // если есть ошибки то отправляем ошибку и ее текст
                    return json_encode(array('result' => 'error', 'msg' => $error));
                } else {
                    // если нет ошибок сообщаем об успехе
                    return json_encode(array('result' => 'success'));
                }
            }
        }
    }

}