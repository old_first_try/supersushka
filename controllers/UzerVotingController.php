<?php
/**
 * supersushka - UzerVotingController.php
 *
 * Initial version by: Tom
 * Initial created on: 14.10.2017 19:38
 */

namespace app\controllers;


use app\models\VotingIndexArrayChunk;
use app\modules\user\models\FinalVoting;
use app\modules\user\models\Patches;
use app\modules\user\models\User;
use app\modules\user\models\Voting;
use Yii;
use yii\db\StaleObjectException;
use yii\web\HttpException;

class UzerVotingController extends AppController
{
    public $layout = 'user';

    public function actionFirstVoting($count = false, $user_id = false)
    {
        if (Yii::$app->user->isGuest) {
            $this->setMeta(\Titles::$AUTH_ERROR, \Titles::$PROJECT_NAME, \Titles::$PROJECT_NAME);
            return $this->redirect('/user/auth/login');
        }
        if (!Yii::$app->user->isGuest && Yii::$app->user->identity['role_id'] < 5) {
            $this->setMeta(\Titles::$FIRST_VOTING, \Titles::$PROJECT_NAME, \Titles::$PROJECT_NAME);
            if (isset($count) && $count == null)
                $count = 0;

            //grub all votings for next sub_array with patch (where(['status'=>1])) - detecting
            $allVotings = Voting::find()->where(['and', "chance>=1"])->all();
            //current user voting record for unsetting in next grub votings - comment above
            $currentUserVoting = Voting::findOne(['user_id' => Yii::$app->user->getId()]);

            //unset current user
            foreach ($allVotings as $allVoting) {
                if ($allVoting->user_id == $currentUserVoting->id) {
                    unset($allVotings[$allVoting->id]);
//                    echo $allVoting->id;
                }
            }
            //random
            shuffle($allVotings);
            //if divider
            if ((isset($count) && isset($user_id)) // or - look to the next line
                && ($count != null && $user_id != null)) {
                //vote process for determined (chosen) voting record
                $votingRecord = Voting::findOne(['user_id' => $user_id]);
                $votingRecord->balls = $votingRecord->balls + 10;
                try {
                    $votingRecord->update();
                } catch (StaleObjectException $e) {
                } catch (\Exception $e) {
                } catch (\Throwable $e) {
                }

                $twoRandomVotings = array_slice($allVotings, $count, 2);

                //setFlash after first tour voting last page
                if (($count >= 20 && $currentUserVoting->status == 0) || empty($twoRandomVotings)) {
                    $currentUserVoting->status = 1;
                    try {
                        $currentUserVoting->update();
                    } catch (StaleObjectException $e) {
                    } catch (\Exception $e) {
                    } catch (\Throwable $e) {
                    }
                    $count = 20;
                    Yii::$app->session->setFlash('successVoting');
                    return $this->render('first-voting', compact('count', 'twoRandomVotings',
                        'currentUserVoting'));
                }

                //view render divider
                return $this->render('first-voting', compact('count', 'twoRandomVotings',
                    'currentUserVoting'));
            }

            //define current user vote array
            $twoRandomVotings = array_slice($allVotings, 0, 2);

            return $this->render('first-voting', compact('twoRandomVotings', 'count', 'currentUserVoting'));
        }
        return new HttpException(401);
    }

    public function actionSecondVoting($count = false, $user_id = false)
    {
        if (Yii::$app->user->isGuest) {
            $this->setMeta(\Titles::$AUTH_ERROR, \Titles::$PROJECT_NAME, \Titles::$PROJECT_NAME);
            return $this->redirect('/user/auth/login');
        }
        if (!Yii::$app->user->isGuest) {
            $this->setMeta(\Titles::$SECOND_VOTING, \Titles::$PROJECT_NAME, \Titles::$PROJECT_NAME);
            if (isset($count) && $count == null)
                $count = 0;

            //grub all votings for next sub_array with patch (where(['status'=>1])) - detecting
            $allVotings = Voting::find()->where(['and', "chance>=1"])->all();
            //current user voting record for unsetting in next grub votings - comment above
            $currentUserVoting = Voting::findOne(['user_id' => Yii::$app->user->getId()]);

            //unset current user
            foreach ($allVotings as $allVoting) {
                if ($allVoting->user_id == $currentUserVoting->id) {
                    unset($allVotings[$allVoting->id]);
//                    echo $allVoting->id;
                }
            }
            //randomizing for all voting records
            shuffle($allVotings);
            //if divider
            if ((isset($count) && isset($user_id)) // or - look to the next line
                && ($count != null && $user_id != null)) {
                //vote process for determined (chosen) voting record
                $votingRecord = Voting::findOne(['user_id' => $user_id]);
                $votingRecord->balls = $votingRecord->balls + 14;
                try {
                    $votingRecord->update();
                } catch (StaleObjectException $e) {
                } catch (\Exception $e) {
                } catch (\Throwable $e) {
                }

                $twoRandomVotings = array_slice($allVotings, $count, 2);

                //setFlash after first tour voting last page
                if (($count >= 20 && $currentUserVoting->status == 0) || $twoRandomVotings == null) {
                    $currentUserVoting->status = 1;
                    try {
                        $currentUserVoting->update();
                    } catch (StaleObjectException $e) {
                    } catch (\Exception $e) {
                    } catch (\Throwable $e) {
                    }
                    $count = 20;
                    Yii::$app->session->setFlash('successVoting');
                    return $this->render('second-voting', compact('count', 'twoRandomVotings',
                        'currentUserVoting'));
                }

                //view render divider
                return $this->render('second-voting', compact('count', 'twoRandomVotings',
                    'currentUserVoting'));
            }

            //define current user vote array
            $twoRandomVotings = array_slice($allVotings, 0, 2);

            return $this->render('second-voting', compact('twoRandomVotings', 'count', 'currentUserVoting'));

        }
        return new HttpException(401);
    }

    public function actionThirdVoting()
    {
        $this->layout = 'login';
        $this->setMeta('Суперсушка');
        if (Yii::$app->user->isGuest) {
            $finalVoting = new FinalVoting();
            if ($finalVoting->load(Yii::$app->request->post())) {
                if ($finalVoting->validate()) {
                    Yii::$app->session->setFlash('successThirdVoting');
                    $user = User::findOne(['login' => $finalVoting->voting_login]);
                    $voting = Voting::findOne(['user_id' => $user->id]);
                    $voting->balls += 19;
                    try {
                        $voting->update();
                    } catch (StaleObjectException $e) {
                    } catch (\Exception $e) {
                    } catch (\Throwable $e) {
                    }
                    $finalVoting->sendMails();
                    $finalVoting->save();
                }
                return $this->render('third-voting', compact('finalVoting'));
            }
            return $this->render('third-voting', compact('finalVoting'));
        }
        return $this->redirect('/uzer-voting/third');
    }

    public function actionThirdVotingInner($count = false, $user_id = false)
    {
        if (Yii::$app->user->isGuest) {
            $this->setMeta(\Titles::$AUTH_ERROR, \Titles::$PROJECT_NAME, \Titles::$PROJECT_NAME);
            return $this->redirect('/user/auth/login');
        }
        if (!Yii::$app->user->isGuest) {
            $this->setMeta(\Titles::$THIRD_VOTING_INNER, \Titles::$PROJECT_NAME, \Titles::$PROJECT_NAME);
            if (isset($count) && $count == null)
                $count = 0;

            //grub all votings for next sub_array with patch (where(['status'=>1])) - detecting
            $allVotings = Voting::find()->where(['>', "chance", 0])->all();
            //current user voting record for unsetting in next grub votings - comment above
            $currentUserVoting = Voting::findOne(['user_id' => Yii::$app->user->getId()]);

            //unset current user
            foreach ($allVotings as $allVoting) {
                if ($allVoting->user_id == $currentUserVoting->id) {
                    unset($allVotings[$allVoting->id]);
//                    echo $allVoting->id;
                }
            }
            //randomizing for all voting records
            shuffle($allVotings);
            //if divider
            if ((isset($count) && isset($user_id)) // or - look to the next line
                && ($count != null && $user_id != null)) {
                //vote process for determined (chosen) voting record
                $votingRecord = Voting::findOne(['user_id' => $user_id]);
                $votingRecord->balls = $votingRecord->balls + 19;
                try {
                    $votingRecord->update();
                } catch (StaleObjectException $e) {
                } catch (\Exception $e) {
                } catch (\Throwable $e) {
                }

                $twoRandomVotings = array_slice($allVotings, $count, 2);

                //setFlash after first tour voting last page
                if (($count >= 10 && $currentUserVoting->status == 0) || $twoRandomVotings == null) {
                    $currentUserVoting->status = 1;
                    try {
                        $currentUserVoting->update();
                    } catch (StaleObjectException $e) {
                    } catch (\Exception $e) {
                    } catch (\Throwable $e) {
                    }
                    $count = 10;
                    Yii::$app->session->setFlash('successVoting');
                    return $this->render('third-voting-inner', compact('count', 'twoRandomVotings',
                        'currentUserVoting'));
                }

                //view render divider
                return $this->render('third-voting-inner', compact('count', 'twoRandomVotings',
                    'currentUserVoting'));
            }

            //define current user vote array
            $twoRandomVotings = array_slice($allVotings, 0, 2);

            return $this->render('third-voting-inner', compact('twoRandomVotings', 'count', 'currentUserVoting'));
        }
        return new HttpException(401);
    }


    public function actionThird()
    {
        $this->layout = 'login';
        $this->setMeta('Суперсушка');
        if (Yii::$app->user->isGuest) {
            return $this->redirect('/user/auth/login');
        }
        if (!Yii::$app->user->isGuest) {
            return $this->render('third');
        }
        return $this->render('third');
    }

}