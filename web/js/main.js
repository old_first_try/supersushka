function showFollows() {
    $('#follows').modal();
}

$('.view-follows').on('click', function () {
    showFollows();
});

function showFollowers() {
    $('#followers').modal();
}


$('.view-followers').on('click', function () {
    showFollowers();
});

function postUpdate() {
    $('#postUpdate').modal();
}

$('.postUpdate').on('click', function () {
    postUpdate();
});

//report post
function viewReportPost() {
    $('#report-post').modal();
}

$('.view-report-post').on('click', function () {
    viewReportPost();
});
//report comment
function viewReportComment() {
    $('#report-comment').modal();
}

$('.view-report-comment').on('click', function () {
    viewReportComment();
});

//admin message
function adminMessage() {
    $('#answer').modal();
}

$('.view-answer').on('click', function () {
    adminMessage();
});

//user answer
function userAnswer() {
    $('#report-task').modal();
}

$('.view-report-task').on('click', function () {
    userAnswer();
});

//view password symbols
jQuery('#reveal-password').
change(function(){jQuery('#loginform-password').attr('type',this.checked?'text':'password');});
