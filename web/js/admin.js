function ShowNotification(url, selector) {
    this._selector = selector;
    this._url = url;
    this.count = '0';
    this.init();
}

ShowNotification.prototype = {
    init: function () {
        var self = this;

        this.showNotification();
        this._interval = setInterval(function () {
            self.showNotification();
        }, 10000);
    },

    showNotification() {
        var self = this;

        this.getData()
            .done(function (response) {
                // console.log(response);
                self.drow(response);
            })
            .fail(function (err) {
                console.log(err);
            });
    },

    getData: function (data) {
        return $.ajax({
            url: this._url,
            dataType: "json",
        });
    },

    drow: function (response) {

        let count = document.getElementById("count");
        let count_string = "0";
        let count_notify = document.getElementById("count-notify");
        let user_notify = document.getElementById(this._selector);
        let user_string_notify = "";
        let count_string_notify = 0;
        if (response !== 'empty') {
            count_string = response.length;
            this._count = response.length;
            count_string_notify = "<span class='text-center text-nav'>" +
                "<a href='http://sypersushka.ru/uzer/notify'>" +
                "У вас " +
                +response.length +
                " уведомлений." +
                " Просмотреть все." +
                "</a>" +
                "</span>";
            var rs = "";
            for (let i = 0; i < response.length; ++i) {
                rs = response[i];
                if (rs.type === 'messageUser') {
                    var notifyDate = new Date(Date.parse(rs.created_at)); // Start of notify date.

                    var month = ["Января", "Февраля", "Марта", "Апреля", "Мая", "Июня",
                        "Июля", "Августа", "Сентября", "Октября", "Ноября", "Декабря"][notifyDate.getMonth()];
                    date = notifyDate.getDay() + " " + month + " в " + notifyDate.getHours() + " часа " + notifyDate.getMinutes() + " минут.";
                    console.log(date);
                    user_string_notify += "<li style='float: none;' class='check-notify'" +
                        "data-id='" + response[i].id + "'>" +
                        "<a style='background: #ffdb53;margin-bottom: 5px;'" +
                        "class='border_radius border-green'" +
                        "href='http://sypersushka.ru/message/current-task?user_id=" + user_id_not + "&task_id=" + response[i].task_id + "'>" +
                        "<span>" +
                        "<strong> " +
                        "Новое сообщение от " + response[i].admin_name +
                        "</strong> :" +
                        "<br/>" + date +
                        "" +
                        "</a>";
                    console.log(response[i].id)
                }
                if (rs.type === 'answer') {
                    var notifyDate = new Date(Date.parse(rs.created_at)); // Start of notify date.

                    var month = ["Января", "Февраля", "Марта", "Апреля", "Мая", "Июня",
                        "Июля", "Августа", "Сентября", "Октября", "Ноября", "Декабря"][notifyDate.getMonth()];
                    date = notifyDate.getDay() + " " + month + " в " + notifyDate.getHours() + " часа " + notifyDate.getMinutes() + " минут.";
                    console.log(date);
                    user_string_notify += "<li style='float: none;' class='check-notify'" +
                        "data-id='" + response[i].id + "'>" +
                        "<a style='background: #ffdb53;margin-bottom: 5px;'" +
                        "class='border_radius border-green'" +
                        "href='http://sypersushka.ru/user/help/feedback'>" +
                        "<span>" +
                        "<strong> " +
                        " Ответ на Ваш вопрос от " + response[i].admin_name +
                        "</strong> :" +
                        "<br/>" + date +
                        "" +
                        "</a>";
                    console.log(response[i].id)
                }

                if (rs.type === 'messageAdmin') {
                    var notifyDate = new Date(Date.parse(rs.created_at)); // Start of notify date.

                    var month = ["Января", "Февраля", "Марта", "Апреля", "Мая", "Июня",
                        "Июля", "Августа", "Сентября", "Октября", "Ноября", "Декабря"][notifyDate.getMonth()];
                    date = notifyDate.getDay() + " " + month + " в " + notifyDate.getHours() + " часа " + notifyDate.getMinutes() + " минут.";
                    console.log(date);
                    user_string_notify += "<li style='float: none;' class='check-notify'" +
                        "data-id='" + response[i].id + "'>" +
                        "<a style='background: #ffdb53;margin-bottom: 5px;'" +
                        "class='border_radius border-green'" +
                        "href='http://sypersushka.ru/user/admin/message-user'>" +
                        "<span>" +
                        "<strong> " +
                        "У вас новое сообщение" +
                        "</strong> :" +
                        "<br/>" + date +
                        "" +
                        "</a>";
                    console.log(response[i].id)
                }

                if (rs.type === 'report') {
                    var notifyDate = new Date(Date.parse(rs.created_at)); // Start of notify date.

                    var month = ["Января", "Февраля", "Марта", "Апреля", "Мая", "Июня",
                        "Июля", "Августа", "Сентября", "Октября", "Ноября", "Декабря"][notifyDate.getMonth()];
                    date = notifyDate.getDay() + " " + month + " в " + notifyDate.getHours() + " часа " + notifyDate.getMinutes() + " минут.";
                    console.log(date);
                    user_string_notify += "<li style='float: none;' class='check-notify'" +
                        "data-id='" + response[i].id + "'>" +
                        "<a style='background: #ffdb53;margin-bottom: 5px;'" +
                        "class='border_radius border-green'" +
                        "href='http://sypersushka.ru/uzer/view'>" +
                        "<span>" +
                        "<strong> " +
                        "У вас новая жалоба:" +
                        "</strong> :" +
                        "<br/>" + date +
                        "" +
                        "</a>";
                    console.log(response[i].id)
                }

                if (rs.type === 'question') {
                    var notifyDate = new Date(Date.parse(rs.created_at)); // Start of notify date.

                    var month = ["Января", "Февраля", "Марта", "Апреля", "Мая", "Июня",
                        "Июля", "Августа", "Сентября", "Октября", "Ноября", "Декабря"][notifyDate.getMonth()];
                    date = notifyDate.getDay() + " " + month + " в " + notifyDate.getHours() + " часа " + notifyDate.getMinutes() + " минут.";
                    console.log(date);
                    user_string_notify += "<li style='float: none;' class='check-notify'" +
                        "data-id='" + response[i].id + "'>" +
                        "<a style='background: #ffdb53;margin-bottom: 5px;'" +
                        "class='border_radius border-green'" +
                        "href='http://sypersushka.ru/uzer/view'>" +
                        "<span>" +
                        "<strong> " +
                        "У вас новый вопрос:" +
                        "</strong> :" +
                        "<br/>" + date +
                        "" +
                        "</a>";
                    console.log(response[i].id)
                }

            }
            count.innerHTML = count_string;
            console.log(this._count);
            count_notify.innerHTML = count_string_notify;
            user_notify.innerHTML = user_string_notify;
            console.log('notify - update');
        } else {
            count.innerHTML = '0';
            count_string_notify = "<span class='text-center border_radius_margin text-nav'>" +
                "<a href='http://sypersushka.ru/uzer/notify'>" +
                "<h3 align='center' class='text-post'>" +
                'У вас нет уведомлений.' +
                " Просмотреть все." +
                "</h3>" +
                "</a>" +

                "</span>";
            // console.log(count_string_notify);
            count_notify.innerHTML = count_string_notify;
        }

    },

    destroy: function () {
        clearInterval(this._interval);
    }
};

let countCheck = document.getElementById("count");
if (countCheck != null) {
//в будущем будешь использовать так
    var userNotificationView = new ShowNotification('/user/admin/ajax-draw-notify', 'user-notify');
// var adminNotificationView = new ShowNotification('/user/admin/ajax-draw-notify', 'admin-notify');
}


$(document.body).on(' click', '.check-notify', function () {
// $('.check-notify').on(' click', function () {
    id = $(this).data("id");
    // alert(id);
    $.ajax({
        url: '/user/admin/ajax-delete-notify',
        // dataType: ' json',
        type: "POST",
        data: {
            data: id,
        },
        success: function (response) {
            // notifies = response;
            // alert(response);
        },
        error: function (data) {
            console.log("error in ajax." + data);
        }
    });
});


// $(document).ready(function (e) {
//     // var message = document.getElementById("message_user").value;
//     var message_text = $('#message_user').val();
//     if (message_text !== '') {
//         $(".row:last").after(
//             '<div class="row">' +
//             '<div class="message message-out pull-right">' +
//             '' + message_text + '' +
//             '</div>' +
//             '</div>');
//         // $('#message_text').val('');
//     }
//
//     $("#uploadForm").on('submit', (function (e) {
//         e.preventDefault();
//         $.ajax({
//             url: "/message/upload",
//             type: "POST",
//             data: new FormData(this),
//             contentType: false,
//             cache: false,
//             processData: false,
//             success: function (data) {
//                 alert(data);
//                 if (data != null) {
//                     $(".row:last").after(
//                         '<div class="row">' +
//                         '<div class="message message-out pull-right">' +
//                         '' + data + '' +
//                         '</div>' +
//                         '</div>');
//                     $("#targetLayer").html(data);
//                 }
//             },
//             error: function () {
//                 alert("error in ajax.");
//             }
//         });
//     }));
// });

$(function () {
    // var message = document.getElementById("envoi").onclick = someFunc;

    $('#envoi').on('click', function () {

        var message = document.getElementById("message_text").value;
        var message_text = $('#message_text').val();
        if (message_text !== '') {
            $(".row:last").after(
                '<div class="row">' +
                '<div class="message message-out pull-right">' +
                '' + message_text + '' +
                '</div>' +
                '</div>');
        }
        $.ajax({
            //url я не указываю, потому что отсылаю данные в тот же action
            // dataType: 'json',
            url: '/user/admin/message/',
            type: 'POST',
            data: {
                message: message,
                user_id: user_id,
                task_id: task_id,
                _csrf: tokenCsrf,
            },
            success: function (data) {
                // alert(data); //возвращает объект (так и должно быть)
                // console.log(data);
                $('#message_text').val('');
            },
            error: function () {
                alert('error in ajax ')
            }
        });
    });
});

function notifyAll() {
    $.ajax({
        // dataType: 'json',
        url: '/uzer/notify-all/',
        type: 'POST',
        data: {
            user_id: user_id_notify,
            _csrf: tokenCsrf_notify,
        },
        success: function (data) {
            // alert(data); //возвращает объект (так и должно быть)
            // console.log(data);
        },
        error: function (error) {
            // alert('error in ajax ' + JSON.stringify(error));
        }
    });
}

/*
ACCORDION
 */
$(function () {
    $(".expand").on("click", function () {
        // $(this).next().slideToggle(200);
        $expand = $(this).find(">:first-child");

        if ($expand.text() == "+") {
            $expand.text("-");
        } else {
            $expand.text("+");
        }
    });
});