/*
DO POST
 */

$(document).ready(function () {

    $("[data-toggle=tooltip]").tooltip();
});

/*
WALL
 */

$(function () {
    $('.panel-google-plus > .panel-footer > .input-placeholder, .panel-google-plus > .panel-google-plus-comment > .panel-google-plus-textarea > button[type="reset"]').on('click', function (event) {
        var $panel = $(this).closest('.panel-google-plus');
        $comment = $panel.find('.panel-google-plus-comment');

        $comment.find('.btn:first-child').addClass('disabled');
        $comment.find('textarea').val('');

        $panel.toggleClass('panel-google-plus-show-comment');

        if ($panel.hasClass('panel-google-plus-show-comment')) {
            $comment.find('textarea').focus();
        }
    });
    $('.panel-google-plus-comment > .panel-google-plus-textarea > textarea').on('keyup', function (event) {
        var $comment = $(this).closest('.panel-google-plus-comment');

        $comment.find('button[type="submit"]').addClass('disabled');
        if ($(this).val().length >= 1) {
            $comment.find('button[type="submit"]').removeClass('disabled');
        }
    });
});

/*
WC
 */

function addComment() {
    var userComment = document.getElementById("userComment").value;
    document.getElementById("ui-state-default").innerHTML = userComment;
}

$.material.init();

/*
chating in the task
 */

// var message = document.getElementById("envoi").onclick = someFunc;

$('#send_message').on('click', function () {
    var message = document.getElementById("message_user").value;
    var message_text = $('#message_user').val();
    if (message_text !== '') {
        $(".row:last").after(
            '<div class="row">' +
            '<div class="message message-out pull-right">' +
            '' + message_text + '' +
            '</div>' +
            '</div>');
        // $('#message_text').val('');
    }
    $.ajax({
        //url я не указываю, потому что отсылаю данные в тот же action
        // dataType: 'json',
        url: '/message/ajax/',
        type: 'POST',
        data: {
            message: message,
            task_id: task_id_user,
            _csrf: tokenCsrf_user,
        },
        success: function (data) {
            // alert(data); //возвращает объект (так и должно быть)
            console.log(data);
        },
        error: function () {
            alert('error in ajax ')
        }
    });
});

function showTasks() {

    if (typeof user_week !== 'undefined') {
        var tasks_for_marks;
        $.ajax({
            //url я не указываю, потому что отсылаю данные в тот же action
            url: '/message/ajax-tasks/',
            dataType: 'json',
            type: 'GET',
            data: {
                week: user_week,
                // _csrf: tokenCsrf,
            },
            success: function (tasks) {
                tasks_for_marks = tasks;
                // alert(tasks);
                // alert(week);
                var task_id;
                if (tasks !== null) {
                    var string = "";
                    var task = 0;
                    // Получить ссылку на элемент <div>
                    myDiv = document.getElementById("content");
                    // Добавить содержимое в элемент <div>
                    // today date
                    var now = new Date();

                    var today = new Date(now.getYear(), now.getMonth(), now.getDate());
                    // var month = ["Января", "Февраля", "Марта", "Апреля", "Мая", "Июня",
                    //     "Июля", "Августа", "Сентября", "Октября", "Ноября", "Декабря"][now.getMonth()];
                    var str = now.getDate() + " " + month + " " + now.getFullYear();
                    // alert(now.getTime());
                    for (var i = 0; i < tasks.length; i++) {
                        //task date
                        // comparison
                        task = tasks[i];
                        var d = new Date();
                        //перевод даты
                        d.setTime(Date.parse(task.date));
                        if (d.getTime() <= now.getTime()) {

                            var month = ["Января", "Февраля", "Марта", "Апреля", "Мая", "Июня",
                                "Июля", "Августа", "Сентября", "Октября", "Ноября", "Декабря"][d.getMonth()];
                            date = d.getDay() + " " + month + " " + d.getFullYear();
                            task_id = task.id;
                            console.log(task.title);
                            string +=
                                "<a id='curtask' href='http://sypersushka.ru/message/current-task?task_id=" + task_id
                                + "&admin_id=" + tasks_admin_id
                                + "'>"
                                + "<div class='well taskimg'>"
                                + task.title
                                + " - "
                                + date
                                + " "
                                + "<div id='required-img" + task_id + "'></div>"
                                + "<div class='status" + task_id + "'></div>"
                                // + "<div class='status2" + task_id + "'></div>"
                                + "</div>"
                                + "</a>";
                        }
                    }
                    myDiv.innerHTML = string;

                    let srcEXP = "/images/exclamation_point.png";
                    for (var i = 0; i < tasks.length; i++) {
                        task = tasks[i];
                        task_id = task.id;
                        if (task.status == 1) {
                            var id = "#required-img" + task_id;
                            $(id).append('<img src="' + srcEXP + '" class="required-img" width="35" height="35" alt="" ' +
                                'style="margin-right:45px;" align="right" />');
                        }
                    }
                }
                else {
                    myDiv = document.getElementById("content");
                    myDiv.innerHTML = "<h4 align='center' id='title' class='title'>" +
                        'Пока что на этой неделе нет заданий' +
                        "</h4>";
                }
            },
            error: function () {
                alert('error in ajax ');
            }
        });

        $.ajax({
            url: '/message/ajax-marks/',
            dataType: 'json',
            type: 'GET',
            data: {
                // _csrf: tokenCsrf,
            },
            success: function (marks) {
                // alert(data); //возвращает объект (так и должно быть)
                var srcHP = "/images/happy.svg";
                var srcSD = "/images/sad.svg";
                for (var i = 0; i < tasks_for_marks.length; i++) {
                    if (tasks_for_marks != null) {
                        for (var j = 0; j < marks.length; ++j) {
                            if (tasks_for_marks[i].id == marks[j].task_id) {
                                var id = ".status" + tasks_for_marks[i].id;
                                if (marks[j].status == 1) {
                                    $(id).append('<img src="' + srcHP + '" class="happy" width="35" height="35" alt="" ' +
                                        'style="margin-right:15px;" align="right" />');
                                }
                                if (marks[j].status == 0) {
                                    $(id).append('<img src="' + srcSD + '" class="sad" width="35" height="35" alt="" ' +
                                        'style="margin-right:15px;" align="right" />');
                                }
                            }
                        }
                    }
                }
            },
            error: function () {
                alert('error in ajax ');
            }
        });
    }
}

let content = document.getElementById("content");
if (content != null) {
    showTasks();
    setInterval('showTasks()', 60000);
}

// likes
$(document).ready(function () {
    $('span#like').click(function () {
        setVote('like', $(this));
    });

});

// type - тип голоса. Лайк или дизлайк
// element - кнопка, по которой кликнули
function setVote(type, element) {
    // получение данных из полей
    let id_user = $('#id_user').val();
    let id_news = element.parent().find('#id_news').val();

    $.ajax({
        // метод отправки
        type: "POST",
        // путь до скрипта-обработчика
        url: "/uzer/ajax-likes/",
        // какие данные будут переданы
        data: {
            'id_user': id_user,
            'id_news': id_news,
            'type': type
        },
        // тип передачи данных
        dataType: "json",
        // действие, при ответе с сервера
        success: function (data) {
            // в случае, когда пришло success. Отработало без ошибок
            if (data.result === 'success') {
                // Выводим сообщение
                alert('Голос засчитан');
                // увеличим визуальный счетчик
                let count = parseInt(element.find('b').html());
                element.find('b').html(count + 1);
            } else {
                // вывод сообщения об ошибке
                alert(data.msg);
            }
        },
        error: function (req, status, err) {
            console.log('Something went wrong', status, err);
        }

    });
}

