<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "follow".
 *
 * @property integer $id
 * @property integer $follow_id
 * @property string $follow_name
 * @property string $follow_surname
 * @property integer $follower_id
 * @property string $follower_name
 * @property string $follower_surname
 */
class Follow extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'follow';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['follow_id', 'follower_id'], 'integer'],
            [['follow_name', 'follow_surname', 'follower_name', 'follower_surname'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'follow_id' => 'Follow ID',
            'follow_name' => 'Follow Name',
            'follow_surname' => 'Follow Surname',
            'follower_id' => 'Follower ID',
            'follower_name' => 'Follower Name',
            'follower_surname' => 'Follower Surname',
        ];
    }
}
