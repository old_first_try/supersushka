<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "votes_news2user".
 *
 * @property int $id
 * @property int $id_user
 * @property int $id_news
 */
class VotesNewsUser extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'votes_news2user';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_user', 'id_news'], 'required'],
            [['id_user', 'id_news'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'id_user' => 'Id User',
            'id_news' => 'Id News',
        ];
    }
}
