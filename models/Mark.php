<?php

namespace app\models;

use app\modules\user\models\Voting;
use Yii;

/**
 * This is the model class for table "mark".
 *
 * @property integer $id
 * @property integer $user_id
 * @property integer $task_id
 * @property integer $status
 */
class Mark extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'mark';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id', 'task_id', 'status'], 'integer'],
            [['user_id', 'task_id', 'status'], 'required', 'message'=>'Это поле обязательно!'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_id' => 'User ID',
            'task_id' => 'Task ID',
            'status' => 'Засчитано',
        ];
    }

    public function doMark(Mark $mark)
    {
        if ($mark->validate()) {
            $voting = Voting::findOne(['user_id' => $mark->user_id]);
            if ($this->status == 0 && $voting->chance != 3) {
                $voting->chance -= 1;
                $voting->update();
                $mark->save();
                return true;
            }

            $mark->save();
            return true;
        }
        return false;

    }

    public function doMarkUpdate(Mark $mark)
    {
        if ($mark->validate()) {
            $voting = Voting::findOne(['user_id' => $mark->user_id]);
            if ($this->status == 0 && $voting->chance != 3) {
                $voting->chance -= 1;
                $voting->update();
                $mark->update();
                return true;
            }
            $mark->update();
            return true;
        }
        return false;

    }

}
