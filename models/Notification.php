<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "notification".
 *
 * @property int $id
 * @property int $task_id
 * @property string $type
 * @property int $user_id
 * @property int $admin_id
 * @property int $seen
 * @property string $created_at
 * @property int $flashed
 * @property string $admin_name
 */
class Notification extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'notification';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['admin_id', 'type', 'user_id', 'seen', 'created_at', 'admin_name', 'flashed'], 'required'],
            [['user_id', 'seen', 'task_id', 'flashed', 'admin_id'], 'integer'],
            [['created_at'], 'safe'],
            [['type', 'admin_name'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'admin_id' => 'Admin ID',
            'task_id' => 'Task ID',
            'type' => 'Тип',
            'user_id' => 'ID пользователя',
            'seen' => 'просмотрено',
            'created_at' => 'создано в',
            'flashed' => 'Flashed',
            'admin_name' => 'имя админа',
        ];
    }
}
