<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "report".
 *
 * @property integer $id
 * @property integer $user_id
 * @property string $type
 * @property integer $post_id
 * @property integer $comment_id
 */
class Report extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'report';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [

            [['user_id', 'post_id', 'comment_id'], 'integer'],

            [['post_id'], 'default', 'value' => 0],
            [['comment_id'], 'default', 'value' => 0],

            [['type'], 'string', 'max' => 50],
            [['type'], 'default', 'value' => 'Оскорбление'],
            [['type'], 'required'],

            [['user_id'], 'required'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_id' => 'ID Пользователя',
            'type' => 'Тип оскорбления',
            'post_id' => 'ID Поста',
            'comment_id' => 'ID Комментария',
        ];
    }
}
