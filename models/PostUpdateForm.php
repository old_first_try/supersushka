<?php
/**
 * supersushka - PostUpdateForm.php
 *
 * Initial version by: Tom
 * Initial created on: 25.10.2017 16:55
 */

namespace app\models;


use Yii;
use yii\base\Model;

class PostUpdateForm extends Model
{
    public $id;
    public $date;
    public $content;
    public $user_id;

    /**
     * @var Post
     */
    private $_post;

    public function __construct(Post $post, $config = [])
    {
        $this->_post = $post;
        $this->id = $post->id;
        $this->date = $post->date;
        $this->content = $post->content;
        $this->user_id = $post->user_id;
        parent::__construct($config);
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['date'], 'date', 'format' => 'php:Y-m-d H:i:s'],
            [['date'], 'default', 'value' => date('Y-m-d H:i:s')],

            [['content'], 'required', 'message'=>'Заполните поле для отправки $)'],
            [['content'], 'string'],

            [['user_id'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'date' => 'Date',
            'content' => 'Написать пост:',
            'user_id' => 'User ID',
        ];
    }

    public function update()
    {
        if ($this->validate()) {
            $post = $this->_post;
            $post->id = $this->id;
            $post->date = $this->date;
            $post->content = $this->content;
            $post->user_id = $this->user_id;
            return $post->save();
        } else {
            return false;
        }
    }
}