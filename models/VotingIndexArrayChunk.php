<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "votingIndexArrayChunk".
 *
 * @property int $id
 * @property int $index
 * @property int $user_id
 */
class VotingIndexArrayChunk extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'votingIndexArrayChunk';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['index', 'user_id'], 'required'],
            [['index', 'user_id'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'index' => 'Index',
            'User ID' => 'User ID',
        ];
    }
}
