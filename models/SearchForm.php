<?php
/**
 * supersushka - SearchForm.php
 *
 * Initial version by: Tom
 * Initial created on: 23.09.2017 21:01
 */

namespace app\models;


use app\modules\user\models\User;
use yii\helpers\Html;

class SearchForm extends User
{

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'surname'], 'trim'],
            [['name', 'surname'], 'required'],
            [['name', 'surname'], 'string', 'max' => 255],
            [['name', 'surname'], 'string', 'min' => 3],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'name' => 'Имя',
            'surname' => 'Фамилия',
        ];
    }

    public function findUsers()
    {
        $name = Html::encode($this->name);
        $surname = Html::encode($this->surname);
        $result = User::findOne(['name' => $name, 'surname' => $surname]);
        if ($result != null) {
            if ($result->role_id >= 5)
                return null;
        }
        return $result;
    }


}