<?php

namespace app\models;

use Yii;
use yii\helpers\Html;

/**
 * This is the model class for table "post".
 *
 * @property integer $id
 * @property string $date
 * @property string $content
 * @property integer $user_id
 * @property integer $is_admin
 */
class Post extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'post';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['date'], 'date', 'format' => 'php:Y-m-d H:i:s'],
            [['date'], 'default', 'value' => date('Y-m-d H:i:s')],

            [['content'], 'required', 'message' => 'Напишите что-то, чтобы поделиться'],
            [['content'], 'string'],

            [['user_id', 'is_admin'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'date' => 'Date',
            'content' => '',
            'user_id' => 'User ID',
            'is_admin' => 'Is Admin',
        ];
    }

    public function publishPost($id, $role_id)
    {
        if ($this->validate()) {
            $this->user_id = $id;
            if ($role_id >= 5)
                $this->is_admin = 1;
            elseif ($role_id < 5)
                $this->is_admin = 0;
            $this->content = Html::encode($this->content);
            $this->save();
            return true;
        } else
            return false;

    }

    public function __toString()
    {
        // TODO: Implement __toString() method.
        return $this->id . " " . $this->date . " " . $this->content . " " . $this->user_id;
    }


}
