<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "comment".
 *
 * @property integer $id
 * @property string $date
 * @property string $text
 * @property integer $user_id
 * @property integer $post_id
 */
class Comment extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'comment';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['date'], 'safe'],
            [['date'], 'date', 'format' => 'php:Y-m-d H:i:s'],
            [['date'], 'default', 'value' => date('Y-m-d H:i:s')],

            [['text'], 'required', 'message' => 'Прокомментируйте пост'],
            [['text'], 'string'],
            [['user_id', 'post_id'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'date' => 'Дата',
            'text' => '',
            'user_id' => 'User ID',
            'post_id' => 'Post ID',
        ];
    }

    public function publishComment($user_id/*, $post_id*/)
    {
        if ($this->validate()) {
            $this->user_id = $user_id;/*
            $this->post_id = $post_id;*/
            $this->save();
            return true;
        } else
            return false;

    }

}
