<?php

namespace app\models;

use app\modules\user\models\Voting;
use Yii;
use yii\helpers\Html;
use yii\web\UploadedFile;

/**
 * This is the model class for table "message".
 *
 * @property integer $id
 * @property string $text
 * @property integer $task_id
 * @property integer $user_id
 * @property integer $admin_id
 * @property string $data
 * @property string $path_first
 * @property string $path_second
 * @property string $path_third
 * @property string $path_fourth
 * @property integer $voting
 * @property boolean $is_readed
 */
class Message extends \yii\db\ActiveRecord
{

    /**
     * @var UploadedFile[]
     */
    public $imageFiles;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'message';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['text'], 'string'],
            [['text', 'task_id'], 'required', 'message' => 'Это поле обязательно!'],
            [['task_id', 'user_id', 'admin_id', 'voting'], 'integer'],
            [['data'], 'safe'],

            [['data'], 'date', 'format' => 'php:Y-m-d H:i:s'],
            [['data'], 'default', 'value' => date('Y-m-d H:i:s')],

            ['is_readed', 'default', 'value' => false],

            [['imageFiles'], 'file', 'skipOnEmpty' => true, 'extensions' => 'png, jpg', 'maxFiles' => 4],

            [['path_first', 'path_second', 'path_third', 'path_fourth'], 'string', 'max' => 50],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'text' => 'Сообщение',
            'task_id' => 'Task ID',
            'user_id' => 'User ID',
            'admin_id' => 'Admin ID',
            'data' => 'Дата',
            'imageFiles' => 'Нажмите чтобы загрузить фото в формате png или jpg',
            'path_first' => 'Path First',
            'path_second' => 'Path Second',
            'path_third' => 'Path Third',
            'path_fourth' => 'Path Fourth',
            'voting' => 'Поставьте галочку, если загружаете коллажи для голосования',
            'is_readed' => 'Прочитано или нет',
        ];
    }

    public function messageUserToDB(Message $newMessage, $id)
    {
        $newMessage->task_id = $id;
        if ($newMessage->validate()) {
            $newMessage->text = Html::encode($this->text);
            $newMessage->user_id = Yii::$app->user->getId();
            $newMessage->admin_id = Yii::$app->user->identity['parent_id'];
            //TODO: logic
            $newMessage->save();
            return true;
        }
        return false;
    }

    public function upload()
    {
        //TODO: photos current
        if ($this->imageFiles != null) {
            $newMessage = Message::findOne(['id' => $this->id]);
            $newMessage->voting = $this->voting;

            $i = 1;
            foreach ($this->imageFiles as $file) {
                switch ($i):
                    case 1:
                        $newMessage->path_first = 'user' . Yii::$app->user->identity->getId() . $this->id . $i . "." . $file->extension;
                        $file->saveAs('images/photos/' . $newMessage->path_first);
                        break;
                    case 2:
                        $newMessage->path_second = 'user' . Yii::$app->user->identity->getId() . $this->id . $i . "." . $file->extension;
                        $file->saveAs('images/photos/' . $newMessage->path_second);
                        break;
                    case 3:
                        $newMessage->path_third = 'user' . Yii::$app->user->identity->getId() . $this->id . $i . "." . $file->extension;
                        $file->saveAs('images/photos/' . $newMessage->path_third);
                        break;
                    case 4:
                        $newMessage->path_fourth = 'user' . Yii::$app->user->identity->getId() . $this->id . $i . "." . $file->extension;
                        $file->saveAs('images/photos/' . $newMessage->path_fourth);
                        break;
                endswitch;
                ++$i;
            }
            $newMessage->update();
            if ($this->voting) {
                $pastVoting = Voting::findOne(['user_id' => $newMessage->user_id]);
                if (isset($pastVoting)) {
                    $pastVoting->photoDo = $newMessage->path_first;
                    $pastVoting->photoPosle = $newMessage->path_second;
                    $pastVoting->update();
                    return true;
                } else {
                    return false;
                }
            }
            return true;
        } else {
            return false;
        }
    }

    //TODO: PHOTOS DEPRECATED
    public function messageAdminToDB(Message $newMessage, $id, $user_id)
    {
        if ($this->validate()) {
            $newMessage->text = $this->text;
            $newMessage->task_id = $id;
            $newMessage->user_id = $user_id;
            $newMessage->admin_id = 0;
            $newMessage->save();
            return true;
        }
        return false;
    }

    //TODO: PHOTOS DEPRECATED
//    public function upload()
//    {
//            if (isset($check)) {
//                foreach ($this->imageFiles as $file) {
//                    switch ($i):
//                        case 1:
//                            unlink('images/photos/' . $check->path_first);
//                            $check->path_first = 'user' . Yii::$app->user->identity->getId() . $i . "." . $file->extension;
//                            break;
//                        case 2:
//                            if ($check->path_second != null) {
//                                unlink('images/photos/' . $check->path_second);
//                            }
//                            $check->path_second = 'user' . Yii::$app->user->identity->getId() . $i . "." . $file->extension;
//                            break;
//                        case 3:
//                            if ($check->path_third != null) {
//                                unlink('images/photos/' . $check->path_third);
//                            }
//                            $check->path_third = 'user' . Yii::$app->user->identity->getId() . $i . "." . $file->extension;
//                            break;
//                        case 4:
//                            if ($check->path_fourth != null) {
//                                unlink('images/photos/' . $check->path_fourth);
//                            }
//                            $check->path_fourth = 'user' . Yii::$app->user->identity->getId() . $i . "." . $file->extension;
//                            break;
//                    endswitch;
//                    $file->saveAs('images/photos/user' . Yii::$app->user->identity->getId() . $i . "." . $file->extension);
//                    ++$i;
//                }
//                $check->update();
//                return true;
//            }
//        return false;
//    }
}
