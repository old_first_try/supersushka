<?php
/**
 * supersushka - UploadForm.php
 *
 * Initial version by: Tom
 * Initial created on: 25.09.2017 21:34
 */

namespace app\models;

use Yii;
use yii\base\Model;
use yii\web\UploadedFile;

class UploadForm extends Model
{
    /**
     * @var UploadedFile[]
     */
    public $imageFiles;

    public function rules()
    {
        return [
            [['imageFiles'], 'file', 'skipOnEmpty' => true, 'extensions' => 'png, jpg', 'maxFiles' => 4],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'imageFiles' => 'Фото',
            'text' => 'Содержание'
        ];
    }

    public function upload()
    {
        if ($this->validate()) {
            $i = 1;
            $photo_do = new PhotoBefore();
            $check = PhotoBefore::find()->where(['id' => Yii::$app->user->getId()])->one();
            if (!isset($check)) {
                $photo_do->id = Yii::$app->user->getId();
                foreach ($this->imageFiles as $file) {
                    switch ($i):
                        case 1:
                            $photo_do->path_first = 'user' . Yii::$app->user->identity->getId() . $i . "." . $file->extension;
                            break;
                        case 2:
                            $photo_do->path_second = 'user' . Yii::$app->user->identity->getId() . $i . "." . $file->extension;
                            break;
                        case 3:
                            $photo_do->path_third = 'user' . Yii::$app->user->identity->getId() . $i . "." . $file->extension;
                            break;
                        case 4:
                            $photo_do->path_fourth = 'user' . Yii::$app->user->identity->getId() . $i . "." . $file->extension;
                            break;
                    endswitch;
                    $file->saveAs('images/photos/user' . Yii::$app->user->identity->getId() . $i . "." . $file->extension);
                    ++$i;
                }
            }
            if (isset($check)) {
                $photo_do = $check;
                foreach ($this->imageFiles as $file) {
                    switch ($i):
                        case 1:
                            unlink('images/photos/' . $photo_do->path_first);
                            $photo_do->path_first = 'user' . Yii::$app->user->identity->getId() . $i . "." . $file->extension;
                            break;
                        case 2:
                            unlink('images/photos/' . $photo_do->path_second);
                            $photo_do->path_second = 'user' . Yii::$app->user->identity->getId() . $i . "." . $file->extension;
                            break;
                        case 3:
                            unlink('images/photos/' . $photo_do->path_third);
                            $photo_do->path_third = 'user' . Yii::$app->user->identity->getId() . $i . "." . $file->extension;
                            break;
                        case 4:
                            unlink('images/photos/' . $photo_do->path_fourth);
                            $photo_do->path_fourth = 'user' . Yii::$app->user->identity->getId() . $i . "." . $file->extension;
                            break;
                    endswitch;
                    $file->saveAs('images/photos/user' . Yii::$app->user->identity->getId() . $i . "." . $file->extension);
                    ++$i;
                }
            }
            $photo_do->save();
            return true;
        } else {
            return false;
        }
    }

}