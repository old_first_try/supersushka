<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace app\assets;

use yii\web\AssetBundle;

/**
 * Main application asset bundle.
 *
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class AppAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        'css/main.css',
        'css/login.css',
        'css/menu.css',
        'css/user.css',
        'css/admin.css',
    ];
    public $js = [
        'js/main.js',
        'js/menu.js',
        'js/design.js',
        'js/admin.js',
        'js/user.js',
    ];
    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapPluginAsset',
        'yiister\gentelella\assets\ThemeAsset',
        'yiister\gentelella\assets\ExtensionAsset',
        'exocet\BootstrapMD\MaterialAsset', // include css and js
        'exocet\BootstrapMD\MaterialIconsAsset', // include icons (optional)
        'exocet\BootstrapMD\MaterialInitAsset', // add $.material.init(); js (optional)
        // more dependencies
    ];
}
