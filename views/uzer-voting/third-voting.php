<?php
/**
 * supersushka - third-voting.php
 *
 * Initial version by: Tom
 * Initial created on: 18.10.2017 23:35
 */

use app\modules\user\models\Patches;
use app\modules\user\models\Voting;
use newerton\fancybox\FancyBox;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

?>


<div class="user_container well voting-third" style="border-radius: 15px;">

    <h3 align="center" class="text-post">
        Голосование третий тур!
    </h3>

    <?php if (Yii::$app->session->getFlash('successThirdVoting')): ?>
        <div class="alert alert-success alert-dismissible flash" role="alert" >
            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span
                        aria-hidden="true">&times;</span></button>
            Вы проголосовали в третьем туре голосования! Ваш голос засчитан.
            <br>
            Результат - по окончанию голосования.
        </div>
    <?php endif; ?>
    <?php if (Yii::$app->session->getFlash("Пользователя с таким e-mail нет в системе! Попробуйте снова $)")): ?>
        <div class="alert alert-danger alert-dismissible flash" role="alert" >
            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span
                        aria-hidden="true">&times;</span></button>
            Пользователя с таким e-mail нет в системе! Попробуйте снова $).
        </div>
    <?php endif; ?>


    <?php if (Yii::$app->session->getFlash("Пользователь с таким e-mail не участвует в 3 туре голосования!")): ?>
        <div class="alert alert-danger alert-dismissible flash" role="alert" >
            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span
                        aria-hidden="true">&times;</span></button>
            Пользователь с таким e-mail не участвует в 3 туре голосования!
        </div>
    <?php endif; ?>

    <?php if (Yii::$app->session->getFlash("Этот участик уже набрал 50 голосов! 
            Проголосуйте за другого участника, если хотите $)")): ?>
        <div class="alert alert-danger alert-dismissible flash" role="alert" >
            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span
                        aria-hidden="true">&times;</span></button>
            Этот участик уже набрал 50 голосов!
            Проголосуйте за другого участника, если хотите $)
        </div>
    <?php endif; ?>

    <?php if (Yii::$app->session->getFlash("Вы уже голосовали!")): ?>
        <div class="alert alert-danger alert-dismissible flash" role="alert" >
            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span
                        aria-hidden="true">&times;</span></button>
            Вы уже голосовали!
        </div>
    <?php endif; ?>



    <li class="list-group-item border_radius_margin text-post border-green" >
        <br>
        <?php
        $form = ActiveForm::begin();

        echo $form->field($finalVoting, 'name')->input('text', ['style' => 'width:75%'])->label('Представьтесь (ВАШЕ ИМЯ):');

        echo $form->field($finalVoting, 'user_email')->input('email', ['style' => 'width:75%']);

        echo $form->field($finalVoting, 'voting_login')->input('email', ['style' => 'width:75%'])->label('А теперь укажите e-mail участника игры "СУПЕРСУШКА", за которого Вы голосуете');

        echo "<p align='center'>" .
            Html::submitButton('Проголосовать', ['class' => 'btn btn-success radius'])
            .
            "</p>";

        $form = ActiveForm::end(); ?>
    </li>
</div>
