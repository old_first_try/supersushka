<?php
/**
 * supersushka - first-voting.php
 *
 * Initial version by: Tom
 * Initial created on: 14.10.2017 20:01
 */

use app\modules\user\models\Patches;
use app\modules\user\models\User;
use app\modules\user\models\Voting;
use newerton\fancybox\FancyBox;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;


//TODO:count for two voting Record
if (!isset($i))
    $i = 0;
if (!isset($count))
    $count = 1;
//echo "<br/> this is the count: " . $count . "<br/>";
//echo "<br/> this is the i: " . $i . "<br/>";
//echo count($twoRandomVotings) . " size of current user votings";
//debug($userVotings);
if ($currentUserVoting->status == 1)
    $count = 21;
//echo "<br/> this is the count: " . $count . "<br/>";
?>

<div class="user_container well" style="border-radius: 15px;">

    <h1 align="center" class="text-post">
        Голосование первый тур!
    </h1>

    <?php if ($currentUserVoting->status == 1 && (empty($twoRandomVotings) || $count > 20) ) : ?>
        <h3 align="center" class="text-post">Вы проголосовали в первом туре голосования! Результат будет известен после
            оглашённого в задании времени
            <a href="<?= Url::to('/message/voting') ?>">
                здесь
            </a>!
            <br>
            Или в "Результатах".</h3>
    <?php endif; ?>

    <?php
    if ((!empty($twoRandomVotings) || is_object($twoRandomVotings)) && $currentUserVoting->status == 0 || $count < 20) :

        foreach ($twoRandomVotings
                 as $currentPair) :
            $user = User::findOne(['id' => $currentPair->user_id]);
            echo $currentPair->id; ?>
            <li class="list-group-item border_radius_margin text-post border-green">
                <br>
                <?php
                try {
                    echo FancyBox::widget(['target' => 'a[rel=fancybox]',
                        'helpers' => true,
                        'mouse' => true,
                        'config' => ['maxWidth' => '100%',
                            'maxHeight' => '100%',
                            'playSpeed' => 7000,
                            'padding' => 0,
                            'fitToView' => false,
                            'width' => '70%',
                            'height' => '70%',
                            'autoSize' => false,
                            'closeClick' => false,
                            'openEffect' => 'elastic',
                            'closeEffect' => 'elastic',
                            'prevEffect' => 'elastic',
                            'nextEffect' => 'elastic',
                            'closeBtn' => false,
                            'openOpacity' => true,
                            'helpers' => ['title' => ['type' => 'float'],
                                'buttons' => [],
                                'thumbs' => ['width' => 68, 'height' => 50],
                                'overlay' => ['css' => ['background' => 'rgba(0, 0, 0, 0.8)']]],]]);
                } catch (Exception $e) {
                } ?>

                <?php
                $path = $currentPair->photoDo;
                if (($user->role_id == 4 && $path == null) || $path == null)
                    echo Html::a(Html::img("@web/images/usr/user_default.png", ['class' => 'voting-img-left']),
                        "@web/images/usr/user_default.png", ['rel' => 'fancybox']);
                else
                    echo Html::a(Html::img("@web/images/photos/{$path}", ['class' => 'voting-img-left']),
                        "@web/images/photos/{$path}", ['rel' => 'fancybox']) ?>

                <?php
                $path = $currentPair->photoPosle;
                if (($user->role_id == 4 && $path == null) || $path == null)
                    echo Html::a(Html::img("@web/images/usr/user_default.png", ['class' => 'voting-img-right']),
                        "@web/images/usr/user_default.png", ['rel' => 'fancybox']);
                else
                    echo Html::a(Html::img("@web/images/photos/{$path}", ['class' => 'voting-img-right']),
                        "@web/images/photos/{$path}", ['rel' => 'fancybox']); ?>

                <?php
                $count = $count + 2;
                $i = $i + 1;
                echo "<p align='center'>" .
                    Html::a('Проголосовать!',
                        ['first-voting', 'count' => $count, 'user_id' => $currentPair->user_id],
                        ['class' => 'btn btn-success radius',
                            'data' => ['method' => 'post'],])
                    . "</p>";
                ?>
                <?php
                //TODO: logout -> if 'echo' 2 voting Records
                if ($i == 2):
                    break;
                endif;
                //ONLY 2 RECORDS
                ?>
            </li>
        <?php endforeach;
    endif;
    //    endif;
    ?>
</div>
