<?php
/**
 * supersushka - third.php
 *
 * Initial version by: Tom
 * Initial created on: 19.10.2017 0:15
 */

use yii\helpers\Html;
use yii\helpers\Url;
use \yii\web\HttpException;

$exception = new HttpException(401);
$messageUser = "";
$textColor = $exception->statusCode === 404 ? "text-yellow" : "text-red";

?>

<div class="user_container">
    <div class="text-center text-center text-post">
        <h1><?= Html::encode('Ошибка авторизации!') ?></h1>
        <h1 class="error-number"><?= $exception->statusCode ?></h1>
        <h2><?= nl2br(Html::encode($messageUser)) ?></h2>
        <h3>
            <strong>
                У вас нет доступа к этой форме!
            </strong>
            <br>
            <br>
            Перейдите
            <a href="<?= Url::to('/user/auth/logout') ?>">
                <strong>
                    <u>сюда</u>
                </strong>
            </a>
            для голосования.
        </h3>
    </div>
</div>
