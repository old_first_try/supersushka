<?php
/**
 * supersushka - search.php
 *
 * Initial version by: Tom
 * Initial created on: 23.09.2017 20:50
 */

use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;

?>

<div class="user_container well" style="border-radius: 10px;">

    <ul class="nav nav-tabs nav-justified news">
        <li><a href="<?= Url::to('/uzer/news') ?>"><strong>Новости</strong></a></li>
        <li><a href="<?= Url::to('/uzer/followers') ?>"><strong>Подписки</strong></a></li>
        <li class="active"><a href="#"><strong>Поиск</strong></a></li>
    </ul>

    <div class="list-group-item search-form post-font">

        <?php $form = ActiveForm::begin(); ?>
        <?= $form->field($search, 'name')->textInput(['placeholder' => 'Кого вы ищете?']) ?>
        <?= $form->field($search, 'surname')->textInput(['placeholder' => 'Какая у него/неё фамилия?']) ?>
        <p align="center">
            <?= Html::a('Искать', ['search'], [
                'class' => 'btn btn-success',
                'style' => 'font-size: 16px;',
                'data' => [
                    'method' => 'post',
                ],
            ]) ?>
        </p>
        <?php $form = ActiveForm::end(); ?>
    </div>


    <?php if (Yii::$app->session->hasFlash('successSearch')): ?>
        <div class="alert alert-success alert-dismissible flash border_radius" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span
                        aria-hidden="true">&times;</span>
            </button>
            <strong>Поиск осуществлён!</strong>
        </div>
    <?php endif; ?>

    <?php if (Yii::$app->session->hasFlash('errorSearch')): ?>
        <div class="alert alert-danger alert-dismissible flash border_radius" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span
                        aria-hidden="true">&times;</span>
            </button>
            <strong> Ошибка! Пользователя с заданными именем и фамилией нет в системе.</strong>
        </div>
    <?php endif; ?>

    <?php if (isset($result) & $result!=null) : ?>
            <a href="<?= Url::to(['/uzer/view', 'user_id' => $result->id]) ?>">

                <div class=" well search-form-result">

                    <?= $result->name ?>
                    <?= $result->surname ?>
                </div>
            </a>

    <?php endif; ?>

</div>
