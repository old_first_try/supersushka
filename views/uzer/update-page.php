<?php
/**
 * supersushka - update-page.php
 *
 * Initial version by: Tom
 * Initial created on: 11.11.2017 18:50
 */

use app\modules\user\models\Task;
use yii\helpers\Html;

if (isset($week))
    $tasks = Task::find()->where(['week' => $week])->all();
if (!isset($week))
    $tasks = Task::find()->where(['week' => 1])->all();

?>

<div class="task_container user-tasks">


    <?php if (isset($week)): ?>
        <?php if ($week == 1): ?>
            <ul class="nav nav-pills week">
                <h3 align="left"><strong>Задания распределяются по неделям:</strong></h3>

                <li role="presentation" class="active list-group-item "><a
                        href="<?= Url::to(['/message/tasks/', 'week' => 1]) ?>">1</a></li>
                <li role="presentation" class=" list-group-item "><a
                        href="<?= Url::to(['/message/tasks/', 'week' => 2]) ?>">2</a></li>
                <li role="presentation" class=" list-group-item "><a
                        href="<?= Url::to(['/message/tasks/', 'week' => 3]) ?>">3</a></li>
                <li role="presentation" class="list-group-item "><a
                        href="<?= Url::to(['/message/tasks/', 'week' => 4]) ?>">4</a></li>

            </ul>
        <?php endif; ?>
        <?php if ($week == 2): ?>
            <ul class="nav nav-pills week">
                <h3 align="left"><strong>Задания распределяются по неделям:</strong></h3>

                <li role="presentation" class="list-group-item "><a
                        href="<?= Url::to(['/message/tasks/', 'week' => 1]) ?>">1</a></li>
                <li role="presentation" class="active list-group-item "><a
                        href="<?= Url::to(['/message/tasks/', 'week' => 2]) ?>">2</a></li>
                <li role="presentation" class="list-group-item "><a
                        href="<?= Url::to(['/message/tasks/', 'week' => 3]) ?>">3</a></li>
                <li role="presentation" class="list-group-item "><a
                        href="<?= Url::to(['/message/tasks/', 'week' => 4]) ?>">4</a></li>

            </ul>
        <?php endif; ?>

        <?php if ($week == 3): ?>
            <ul class="nav nav-pills week">
                <h3 align="left"><strong>Задания распределяются по неделям:</strong></h3>

                <li role="presentation" class="list-group-item "><a
                        href="<?= Url::to(['/message/tasks/', 'week' => 1]) ?>">1</a></li>
                <li role="presentation" class=" list-group-item "><a
                        href="<?= Url::to(['/message/tasks/', 'week' => 2]) ?>">2</a></li>
                <li role="presentation" class="active list-group-item "><a
                        href="<?= Url::to(['/message/tasks/', 'week' => 3]) ?>">3</a></li>
                <li role="presentation" class="list-group-item "><a
                        href="<?= Url::to(['/message/tasks/', 'week' => 4]) ?>">4</a></li>

            </ul>
        <?php endif; ?>

        <?php if ($week == 4): ?>
            <ul class="nav nav-pills week">
                <h3 align="left"><strong>Задания распределяются по неделям:</strong></h3>

                <li role="presentation" class="list-group-item "><a
                        href="<?= Url::to(['/message/tasks/', 'week' => 1]) ?>">1</a></li>
                <li role="presentation" class=" list-group-item "><a
                        href="<?= Url::to(['/message/tasks/', 'week' => 2]) ?>">2</a></li>
                <li role="presentation" class=" list-group-item "><a
                        href="<?= Url::to(['/message/tasks/', 'week' => 3]) ?>">3</a></li>
                <li role="presentation" class="active list-group-item "><a
                        href="<?= Url::to(['/message/tasks/', 'week' => 4]) ?>">4</a></li>

            </ul>
        <?php endif; ?>
    <?php endif; ?>
    <?php if (!isset($week) || $week == null): ?>
        <ul class="nav nav-pills week">
            <h3 align="left"><strong>Задания распределяются по неделям:</strong></h3>

            <li role="presentation" class="active list-group-item "><a
                    href="<?= Url::to(['/message/tasks/', 'week' => 1]) ?>">1</a></li>
            <li role="presentation" class=" list-group-item "><a
                    href="<?= Url::to(['/message/tasks/', 'week' => 2]) ?>">2</a></li>
            <li role="presentation" class="list-group-item "><a
                    href="<?= Url::to(['/message/tasks/', 'week' => 3]) ?>">3</a></li>
            <li role="presentation" class="list-group-item "><a
                    href="<?= Url::to(['/message/tasks/', 'week' => 4]) ?>">4</a></li>
        </ul>
        <?php
        $tasks = Task::find()->where(['week' => 1])->all();
    endif;
    ?>

    <?php if (isset($tasks)): ?>
        <?php
        $today = date("Y-m-d");
        foreach ($tasks as $task) :
            $result = ($task->date <= $today); //$result == true
            if ($result):?>
                <a href="<?= Url::to(['/message/current-task', 'task_id' => $task->id,
                    'admin_id' => Yii::$app->user->identity['parent_id']]) ?>">
                    <div class="well">
                        <?= Html::encode($task->title) . " " . Html::encode($task->date) ?>
                        <?php $taskMark = \app\models\Mark::findOne(['user_id' => Yii::$app->user->getId(), 'task_id' => $task->id]) ?>
                        <?php if (isset($taskMark)): ?>
                            <?php if ($taskMark->status == 1): ?>
                                <?= Html::img('@web/images/happy.svg', ['width' => 50, 'height' => 50, 'style' => 'border-radius:15px;',
                                    'align' => 'right']) ?>
                            <?php endif; ?>

                            <?php if ($taskMark->status == 0): ?>
                                <?= Html::img('@web/images/sad.svg', ['width' => 45, 'height' => 45,
                                    'align' => 'right']) ?>
                            <?php endif; ?>
                        <?php endif; ?>

                        <?php if ($task->status == 1): ?>
                            <?= Html::img('@web/images/exclamation_point.png', ['width' => 35, 'height' => 35,
                                'style' => 'margin-right:15px;', 'align' => 'right']) ?>
                        <?php endif; ?>
                    </div>
                </a>
                <?php
            endif;
        endforeach; ?>

        <p align="center">
            <strong>
                **<?= Html::img('@web/images/exclamation_point.png', ['width' => 40, 'height' => 40,
                    'style' => 'margin-right:10px;']) ?> - обязательное задание!
            </strong>
            <br>
            <strong>
                **<?= Html::img('@web/images/happy.svg', ['width' => 40, 'height' => 40,
                    'style' => 'margin-right:15px;']) ?> - задание выполнено!
            </strong>
            <br>
            <strong>
                **<?= Html::img('@web/images/sad.svg', ['width' => 40, 'height' => 40,
                    'style' => 'margin-right:15px;']) ?> - задание не выполнено!
            </strong>

            <?=Html::submitButton('Update', ['id'=>'updatePage'])?>
        </p>
    <?php endif; ?>

</div>
