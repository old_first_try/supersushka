<?php
/**
 * Created by PhpStorm.
 * User: deka
 * Date: 11/27/17
 * Time: 10:12 PM
 */

use app\modules\user\models\User;
use yii\helpers\Html;
use yii\helpers\Url;

?>

<div class="user_container well" style="border-radius: 15px;width: 50%;text-align: center;">

    <?php
    date_default_timezone_set('Etc/GMT-3');
    //    echo "Today is " . date('Y-m-d H:i:s'). "<br>";
    ?>
    <h3 align="center" class="text-post">
        Все уведомления, поступившие когда-либо:
    </h3>

    <?php if (isset($notifies) && $notifies != null): ?>
        <?php foreach ($notifies

                       as $notify): ?>

            <?php if (Yii::$app->user->identity['role_id'] < 5): ?>
                <div class="list-group-item border_radius_margin border-green text-post post-font">
                    <?php if ($notify->type == 'messageUser'): ?>
                    <a style="margin-bottom: 5px;"
                       class="border_radius border-green"
                       href="<?= Url::to(['/message/current-task',
                           'user_id' => Yii::$app->user->getId(), 'task_id' => $notify->task_id]) ?>">
                        <span>
                            <strong>
                                Новое сообщение от
                                <?php elseif ($notify->type == 'answer'): ?>
                                <a style="background: #ffdb53;margin-bottom: 5px;"
                                   class="border_radius border-green"
                                   href="<?= Url::to('/user/help/feedback') ?>">
                                    <span>
                                    <strong>
                                        Ответ на Ваш вопрос от
                                        <?php endif; ?>
                                        <?= Html::encode($notify->admin_name) ?>
                                    </strong>
                                        <?php
                                        $today = date("Y-m-d H:i:s");
                                        $todayDate = new DateTime($today);
                                        $smsDate = new DateTime($notify->created_at);
                                        $interval = $smsDate->diff($todayDate, true);
                                        ?>
                                        <?php if ($interval->format('%a') == 0): ?>
                                            <span class="bold">Сегодня, </span>
                                        <?php endif; ?>
                                        <?php
                                        if ($interval->format('%a') == 0 && $interval->format('%h') == 0 && $interval->format('%i') == 0)
                                            echo $interval->format(' Только что');
                                        if ($interval->format('%a') != 0 && $interval->format('%h') != 0 && $interval->format('%i') != 0)
                                            echo $interval->format(' %a дней, %h часов, %i минут назад');

                                        elseif ($interval->format('%a') == 0 && $interval->format('%h') == 0)
                                            echo $interval->format(' %i минут назад');
                                        elseif ($interval->format('%h') == 0 && $interval->format('%i') == 0)
                                            echo $interval->format(' %a дней назад');
                                        elseif ($interval->format('%a') == 0 && $interval->format('%i') == 0)
                                            echo $interval->format(' %h часов назад');
                                        elseif ($interval->format('%a') == 0)
                                            echo $interval->format(' %h часов, %i минут назад');
                                        elseif ($interval->format('%h') == 0)
                                            echo $interval->format(' %a дней, %i минут назад');
                                        elseif ($interval->format('%i') == 0)
                                            echo $interval->format(' %a дней, %h часов назад');
                                        else echo $messageUser->data;
                                        ?>
                            </span>
                        </a>
                                <?php if ($notify->seen == 1):
                                    echo "Просмотрено";
                                    echo Html::img('@web/images/seen.png');
                                endif; ?>
                </div>
            <?php elseif (Yii::$app->user->identity['role_id'] > 4): ?>

                <?php if ($notify->type == 'question'): ?>
                    <div class="list-group-item border_radius_margin border-green text-post post-font">

                        <a style="background: #ffdb53;margin-bottom: 5px;"
                           class="border_radius border-green"
                           href="<?= Url::to(['/user/admin/answer', 'id' => $notify->task_id]) ?>">
                                        <span>
                                            <span>
                                                <strong>
                                                   У вас новый вопрос:
                                                </strong>
                                                <?php
                                                $today = date("Y-m-d H:i:s");
                                                $todayDate = new DateTime($today);
                                                $smsDate = new DateTime($notify->created_at);
                                                $interval = $smsDate->diff($todayDate, true);
                                                ?>
                                                <?php if ($interval->format('%a') == 0): ?>
                                                    <span class="bold">Сегодня, </span>
                                                <?php endif; ?>
                                                <?php
                                                if ($interval->format('%a') == 0 && $interval->format('%h') == 0 && $interval->format('%i') == 0)
                                                    echo $interval->format(' Только что');
                                                if ($interval->format('%a') != 0 && $interval->format('%h') != 0 && $interval->format('%i') != 0)
                                                    echo $interval->format(' %a дней, %h часов, %i минут назад');

                                                elseif ($interval->format('%a') == 0 && $interval->format('%h') == 0)
                                                    echo $interval->format(' %i минут назад');
                                                elseif ($interval->format('%h') == 0 && $interval->format('%i') == 0)
                                                    echo $interval->format(' %a дней назад');
                                                elseif ($interval->format('%a') == 0 && $interval->format('%i') == 0)
                                                    echo $interval->format(' %h часов назад');
                                                elseif ($interval->format('%a') == 0)
                                                    echo $interval->format(' %h часов, %i минут назад');
                                                elseif ($interval->format('%h') == 0)
                                                    echo $interval->format(' %a дней, %i минут назад');
                                                elseif ($interval->format('%i') == 0)
                                                    echo $interval->format(' %a дней, %h часов назад');
                                                else echo $messageUser->data;
                                                ?>
                                            </span>
                                        </span>
                        </a>
                        <?php if ($notify->seen == 1):
                            echo "Просмотрено";
                            echo Html::img('@web/images/seen.png');
                        endif; ?>
                    </div>

                <?php elseif
                ($notify->type = 'messageAdmin'): ?>

                    <div class="list-group-item border_radius_margin border-green text-post post-font">

                        <a style="background: #ffdb53;margin-bottom: 5px;"
                           class="border_radius border-green"
                           href="<?= Url::to('/user/admin/message-user') ?>">
                                        <span>
                                            <span>
                                                <strong>
                                                   У вас новое сообщение от:
                                                    <?php
                                                    $user = User::findOne(['id' => $notify->user_id]);
                                                    echo Html::encode($user->name) ?>
                                                </strong>
                                                <?php
                                                $today = date("Y-m-d H:i:s");
                                                $todayDate = new DateTime($today);
                                                $smsDate = new DateTime($notify->created_at);
                                                $interval = $smsDate->diff($todayDate, true);
                                                ?>
                                                <?php if ($interval->format('%a') == 0): ?>
                                                    <span class="bold">Сегодня, </span>
                                                <?php endif; ?>
                                                <?php
                                                if ($interval->format('%a') == 0 && $interval->format('%h') == 0 && $interval->format('%i') == 0)
                                                    echo $interval->format(' Только что');
                                                if ($interval->format('%a') != 0 && $interval->format('%h') != 0 && $interval->format('%i') != 0)
                                                    echo $interval->format(' %a дней, %h часов, %i минут назад');

                                                elseif ($interval->format('%a') == 0 && $interval->format('%h') == 0)
                                                    echo $interval->format(' %i минут назад');
                                                elseif ($interval->format('%h') == 0 && $interval->format('%i') == 0)
                                                    echo $interval->format(' %a дней назад');
                                                elseif ($interval->format('%a') == 0 && $interval->format('%i') == 0)
                                                    echo $interval->format(' %h часов назад');
                                                elseif ($interval->format('%a') == 0)
                                                    echo $interval->format(' %h часов, %i минут назад');
                                                elseif ($interval->format('%h') == 0)
                                                    echo $interval->format(' %a дней, %i минут назад');
                                                elseif ($interval->format('%i') == 0)
                                                    echo $interval->format(' %a дней, %h часов назад');
                                                else echo $messageUser->data;
                                                ?>
                                            </span>
                                        </span>
                        </a>
                        <?php if ($notify->seen == 1):
                            echo "Просмотрено";
                            echo Html::img('@web/images/seen.png');
                        endif; ?>
                    </div>
                <?php endif; ?>
                <?php if
                ($notify->type = 'report'): ?>

                    <div class="list-group-item border_radius_margin border-green text-post post-font">

                        <a style="background: #ffdb53;margin-bottom: 5px;"
                           class="border_radius border-green"
                           href="<?= Url::to('/uzer/view') ?>">
                                        <span>
                                            <span>
                                                <strong>
                                                   У вас новая жалоба:
                                                </strong>
                                                <?php
                                                $today = date("Y-m-d H:i:s");
                                                $todayDate = new DateTime($today);
                                                $smsDate = new DateTime($notify->created_at);
                                                $interval = $smsDate->diff($todayDate, true);
                                                ?>
                                                <?php if ($interval->format('%a') == 0): ?>
                                                    <span class="bold">Сегодня, </span>
                                                <?php endif; ?>
                                                <?php
                                                if ($interval->format('%a') == 0 && $interval->format('%h') == 0 && $interval->format('%i') == 0)
                                                    echo $interval->format(' Только что');
                                                if ($interval->format('%a') != 0 && $interval->format('%h') != 0 && $interval->format('%i') != 0)
                                                    echo $interval->format(' %a дней, %h часов, %i минут назад');

                                                elseif ($interval->format('%a') == 0 && $interval->format('%h') == 0)
                                                    echo $interval->format(' %i минут назад');
                                                elseif ($interval->format('%h') == 0 && $interval->format('%i') == 0)
                                                    echo $interval->format(' %a дней назад');
                                                elseif ($interval->format('%a') == 0 && $interval->format('%i') == 0)
                                                    echo $interval->format(' %h часов назад');
                                                elseif ($interval->format('%a') == 0)
                                                    echo $interval->format(' %h часов, %i минут назад');
                                                elseif ($interval->format('%h') == 0)
                                                    echo $interval->format(' %a дней, %i минут назад');
                                                elseif ($interval->format('%i') == 0)
                                                    echo $interval->format(' %a дней, %h часов назад');
                                                else echo $messageUser->data;
                                                ?>
                                            </span>
                                        </span>
                        </a>
                        <?php if ($notify->seen == 1):
                            echo "Просмотрено";
                            echo Html::img('@web/images/seen.png');
                        endif; ?>
                    </div>
                <?php endif; ?>


            <?php endif; ?>
        <?php endforeach; ?>
    <?php elseif ($notifies == null): ?>
        <h3 align="center" class="text-post">
            Уведомления не поступали.
        </h3>
    <?php endif; ?>

</div>
