<?php

use app\components\Notification;
use app\models\Comment;
use app\models\Follow;
use app\models\Post;
use app\models\Report;
use app\modules\user\models\Image;
use app\modules\user\models\Task;
use app\modules\user\models\User;
use machour\yii2\notifications\widgets\NotificationsWidget;
use yii\bootstrap\Modal;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\Url;
use yii\widgets\LinkPager;

//Yii::$app->getSecurity()->generatePasswordHash('newParticipant@gmail.com')
if (!isset($user))
    $user = Yii::$app->user->identity;
$image = Image::findOne(['profile_id' => $user->id]);
if (!isset($image)) {
    $img = 'user_default.png';
} else {
    $img = $image->avatar;
}
$posts = array_reverse(Post::find()->where(['user_id' => $user->id])->all());
$this->title = 'Страница участника';
$user = User::findOne(6);

?>

<div class="user_container">

    <?php
    date_default_timezone_set('Etc/GMT-3');
    //    echo "Today is " . date('Y-m-d H:i:s'). "<br>";
    ?>

    <div class="row well" style="border-radius: 10px;">

        <div class="col-sm-7 col-md-5">
            <a href="<?= Url::to('/user/help/avatar') ?>">
                <small class="avatar-change"><cite title="Поменять фото">
                        <i class="glyphicon glyphicon-picture">
                        </i>
                    </cite>
                </small>
            </a>
            <?= Html::img("@web/images/usr/{$img}", ['alt' => 'Avatar', 'class' => 'img-rounded img-responsive']) ?>

            <div class="btn-group" id="title" style="text-align: center;">
                <?php
                $follows = \app\models\Follow::findOne(['follower_id' => Yii::$app->user->getId(), 'follow_id' => $user->id]);
                if (isset($follows) && Yii::$app->user->getId() != $user->id) :
                    echo
                        "<p style='margin-right:0;'>" .
                        Html::a('Отписаться', ['unfollow-view', 'user_id' => $user->id], [
                            'class' => 'btn btn-danger text-post',
                            'style' => 'margin-left:0;margin-right:auto;font-size:14px;',
                            'data' => [
                                'confirm' => 'Вы уверены, что хотите отписаться?',
                                'method' => 'post',
                            ],
                        ]) . "</p>";
                    ?>
                <?php endif; ?>

                <?php if (!isset($follows) && Yii::$app->user->getId() != $user->id) :
                    echo "<p style='margin-right:0;'>" .
                        Html::a('Подписаться', ['follow-view', 'user_id' => $user->id], [
                            'class' => 'btn btn-success text-post',
                            'style' => 'margin-left:auto;margin-right:0;font-size:14px;',
                            'data' => [
                                'confirm' => 'Вы уверены, что хотите подписаться?',
                                'method' => 'post',
                            ],
                        ]) . "</p>";
                    ?>
                <?php endif; ?>

                <!--follows-->
                <?php
                $followsModal = Follow::find()->where(['follower_id' => $user->id])->all();
                $count = 0;
                if (isset($followsModal) && $followsModal != null) {
                    echo Html::button('Подписки', [
                        'class' => 'btn btn-primary view-follows',
                        'style' => 'font-size:14px;margin=left:0;margin-right:auto;
                        color:#444;'
                    ]);

                    //modal
                    Modal::begin([
                        'header' => '<h2 align="center">Подписки</h2>',
                        'id' => 'follows',
                    ]);
                    ?>
                    <div class="row" align="center" style="position: relative;margin:auto;width: 100%;">
                        <?php foreach ($followsModal as $follow): ?>
                            <?php if ($count % 4 == 0 && $count > 0): ?>
                                <br>
                                <br>
                                <br>
                                <br>
                                <br>
                            <?php endif; ?>
                            <div class="col-sm-3">
                                <?php
                                $count++;
                                $image = Image::findOne(['id' => $follow->follow_id]);
                                $avatar = $image->avatar;
                                if ($avatar == null)
                                    $avatar = 'user_default.png'; ?>
                                <?= Html::img("@web/images/usr/{$avatar}", ['width' => 75, 'height' => 75,
                                    'style' => 'border-radius:50%']) ?>
                                <a href="<?= Url::to(['/uzer/view', 'user_id' => $follow->follow_id]) ?>">
                                    <p style="color: #000;font:italic 18px bold ">
                                        <strong>
                                            <?= $follow->follow_name ?>
                                            <?= $follow->follow_surname ?>
                                        </strong>
                                    </p>
                                </a>
                            </div>
                        <?php endforeach; ?>
                    </div>
                    <?php Modal::end();
                    //endModal
                }
                if (isset($followsModal) && $followsModal == null) {
                    echo Html::button('Подписки', [
                        'class' => 'btn brown view-follows',
                        'style' => 'font-size:14px;',
                        "disabled" => "true",

                    ]);
                }
                ?>
                <!--follows-->
                <!--followers-->
                <?php
                $followers = Follow::find()->where(['follow_id' => $user->id])->all();
                $count = 0;
                if (isset($followers) && $followers != null) {
                    echo Html::button('Подписчики', [
                        'class' => 'btn btn-primary view-followers',
                        'style' => 'font-size:14px;color:#444;'

                    ]);

                }
                //modal
                Modal::begin([
                    'header' => '<h2 align="center">Подписчики</h2>',
                    'id' => 'followers',
                ]);
                ?>
                <div class="row" align="center" style="position: relative;margin:auto;width: 100%;">
                    <?php foreach ($followers as $follower): ?>
                        <?php if ($count % 3 == 0 && $count > 0): ?>
                            <br>
                            <br>
                            <br>
                            <br>
                            <br>
                        <?php endif; ?>
                        <div class="col-sm-3">
                            <?php
                            $count++;
                            $image = Image::findOne(['id' => $follower->follower_id]);
                            $avatar = $image->avatar;
                            if ($avatar == null)
                                $avatar = 'user_default.png'; ?>
                            <?= Html::img("@web/images/usr/{$avatar}", ['width' => 75, 'height' => 75,
                                'style' => 'border-radius:50%']) ?>
                            <a href="<?= Url::to(['/uzer/view', 'user_id' => $follower->follower_id]) ?>">
                                <p style="color: #000;font:italic 18px bold ">
                                    <strong>
                                        <?= $follower->follower_name ?>
                                        <?= $follower->follower_surname ?>
                                    </strong>
                                </p>
                            </a>
                        </div>
                    <?php endforeach; ?>
                </div>

                <?php Modal::end();
                //endModal

                if (isset($followers) && $followers == null) {
                    echo Html::button('Подписчики', [
                        'class' => 'btn brown view-followers',
                        'style' => 'font-size:14px;color:#444;',
                        "disabled" => "true",

                    ]);
                }
                ?>
                <!--followers-->
            </div>
        </div>
        <div class="col-md-7">
            <a href="<?= Url::to('/user/help/profile') ?>">

                <small><cite title="Редактировать">
                        <i class="glyphicon glyphicon-pencil">
                        </i>
                    </cite></small>
            </a>
            <h3 id="title">
                <?= $user->name . " " . $user->surname ?>
            </h3>

            <small><cite title="<?php
                if ($user->city != null) {
                    echo $user->city . ", ";
                }
                if ($user->country != null) {
                    echo $user->country;
                } ?>"><?php
                    if ($user->city != null) {
                        echo $user->city . ", ";
                    }
                    if ($user->country != null) {
                        echo $user->country;
                    } ?>
                    <i class="glyphicon glyphicon-map-marker">
                    </i>
                </cite></small>
            <p style="text-align: center">
                <!--<i class="glyphicon glyphicon-envelope"></i>email@example.com
                <br/>
                <i class="glyphicon glyphicon-globe"></i><a href="http://www.jquery2dotnet.com">www.jquery2dotnet.com</a>
                <br/>-->
                <?php if ($user->birthday != null): ?>
                    <i class="glyphicon glyphicon-gift">
                    </i>
                    <span class="text-post">
                        <?= " " . $user->birthday ?>

                    </span>
                    <br>
                <?php endif; ?>

                <?php
                if ($user->sex != null) {
                    echo Html::img('@web/images/gender.svg',
                        ['class' => 'glyphicon i icons']);
                } ?>

                <span class="text-post">
                    <?= $user->sex; ?>
                </span>

                <br>

                <?php if ($user->role_id == 3): ?>
                    <?= Html::img('@web/images/immune.png',
                        ['class' => 'glyphicon i']); ?>
                    Иммунитет
                <?php endif; ?>

                <?php if ($user->role_id == 4): ?>
                    <?= Html::img('@web/images/immune.png',
                        ['class' => 'glyphicon i']); ?>
                    Полный Иммунитет
                <?php endif; ?>

                <br>

                <?php if ($user->instagram == null): ?>
                    <?= Html::img('@web/images/instagram.svg', [
                        'class' => 'social',
                        "disabled" => "true",
                    ]); ?>
                <?php endif; ?>
                <?php if ($user->instagram != null): ?>
                    <a href="<?= Url::to("{$user->instagram}") ?>">
                        <?= Html::img('@web/images/instagram.svg',
                            ['class' => 'social',
                                'onmouseover' => "this.src='../images/instagram-logo.svg';",
                                "onmouseout" => "this.src='/../images/instagram.svg';"]); ?>
                    </a>
                <?php endif; ?>

                <?php if ($user->vk == null): ?>
                    <?= Html::img('@web/images/vk.svg', [
                        'class' => 'social',
                        "disabled" => "true",
                    ]); ?>
                <?php endif; ?>

                <?php if ($user->vk != null): ?>
                    <a href="<?= Url::to("{$user->vk}") ?>">
                        <?= Html::img('@web/images/vk.svg',
                            ['class' => 'social',
                                'onmouseover' => "this.src='../images/vk-logo.svg';",
                                "onmouseout" => "this.src='/../images/vk.svg ';"]); ?>
                    </a>
                <?php endif; ?>

            </p>
        </div>

        <?php
        if ($user->id == Yii::$app->user->identity['id']): ?>
            <div class="row user-post well">

                <?php $form = ActiveForm::begin(); ?>

                <?= $form->field($post, 'content')->textarea(['placeholder' => 'Что у тебя нового?']) ?>

                <button type="submit" class="btn btn-success"><i class="fa fa-share"></i> Поделиться
                </button>
                <?php $form = ActiveForm::end(); ?>
            </div><!-- Widget Area -->
        <?php endif; ?>

        <?php if (Yii::$app->session->getFlash('successPostDelete')): ?>
            <div class="alert alert-success alert-dismissible flash user-post" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span
                            aria-hidden="true">&times;</span></button>
                <strong>Сделано!</strong> Пост удалён!
            </div>
        <?php endif; ?>

        <?php if (Yii::$app->session->getFlash('errorPostDelete') || Yii::$app->session->getFlash('errorPostUpdate')): ?>
            <div class="alert alert-danger alert-dismissible flash user-post" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span
                            aria-hidden="true">&times;</span></button>
                <strong>Ошибка!</strong> Попробуйте снова!
            </div>
        <?php endif; ?>

        <?php if (Yii::$app->session->getFlash('successPostUpdate')): ?>
            <div class="alert alert-success alert-dismissible flash user-post" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span
                            aria-hidden="true">&times;</span></button>
                <strong>Сделано!</strong> Пост изменён!
            </div>
        <?php endif; ?>

        <?php if (Yii::$app->session->getFlash('successPostReport') || Yii::$app->session->getFlash('successCommentReport')): ?>
            <div class="alert alert-success alert-dismissible flash user-post" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span
                            aria-hidden="true">&times;</span></button>
                <strong>Сделано!</strong> Жалоба отправлена!
            </div>
        <?php endif; ?>

        <?php if (Yii::$app->session->getFlash('errorPostReport') ||
            Yii::$app->session->getFlash('errorCommentReport' ||
                Yii::$app->session->getFlash('errorCommentDelete'))): ?>
            <div class="alert alert-danger alert-dismissible flash user-post" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span
                            aria-hidden="true">&times;</span></button>
                <strong>Ошибка!</strong> Попробуйте снова!
            </div>
        <?php endif; ?>

        <?php if (Yii::$app->session->getFlash('successCommentDelete')): ?>
            <div class="alert alert-success alert-dismissible flash user-post" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span
                            aria-hidden="true">&times;</span></button>
                <strong>Сделано!</strong> Комментарий удалён!
            </div>
        <?php endif; ?>

        <?php if ($posts == null && $user->id != Yii::$app->user->getId()): ?>
            <div class="row user-post list-group-item">
                <h3>
                    Этот пользователь не опубликовал ещё ни одного поста.
                </h3>
                <hr>
            </div>
        <?php endif; ?>

        <?php if ($posts == null && $user->id == Yii::$app->user->getId()): ?>
            <div class="user-post list-group-item">
                <h4 align="center">
                    Вы не написали ёще ни одного поста
                </h4>
            </div>
        <?php endif; ?>

        <?php if (isset($posts) && $posts != null): ?>
            <?php foreach ($posts
                           as $item): ?>
                <div class="row user-post-already list-group-item">
                    <div class="media">
                        <a class="pull-left" href="#">
                            <?= Html::img("@web/images/usr/{$img}", ['alt' => 'Avatar', 'class' => '[ img-circle pull-left ]',
                                'width' => '47', 'height' => '47']) ?>
                        </a>

                        <div class="btn-group show-on-hover">
                        <span class="dropdown-toggle" type="button" data-toggle="dropdown">
                            <span class="[ glyphicon glyphicon-chevron-down ]"></span>
                        </span>

                            <ul class="dropdown-menu" role="menu">
                                <?php if ($user->id == Yii::$app->user->identity['id']): ?>
                                    <li role="presentation"><a role="menuitem" tabindex="-1"
                                                               href="<?= Url::to(['/uzer/delete', 'id' => $item->id]) ?>">Удалить</a>
                                    </li>
                                <?php endif; ?>

                                <?php if ($item->user_id == Yii::$app->user->getId()): ?>
                                    <li role="presentation">
                                        <a role="menuitem" tabindex="-1"
                                           href="<?= Url::to(['/uzer/update', 'id' => $item->id, 'view' => 'view']) ?>">
                                            Изменить</a>
                                    </li>
                                <?php endif; ?>


                                <?php if ($item->user_id != Yii::$app->user->getId() /*&& Yii::$app->user->identity['role_id'] < 6*/): ?>

                                    <a href="<?= Url::to(['/uzer/report-comment/',
                                        'user_id' => $item->user_id, 'post_id' =>
                                            $item->id, 'type' => 'Оскорбление', 'route' => 'view']) ?>">
                                        <span class="label label-info">Пожаловаться</span>
                                    </a>


                                <?php endif; ?>
                            </ul>
                        </div>

                        <div class="media-body">
                            <!--                                                    <h4 class="media-heading">Receta 1</h4>-->
                            <p class="text-left post-font text-post"><?= $item->content; ?></p>
                            <p class="text-right text-post">By <?= $user->name . " " . $user->surname ?></p>
                            <ul class="list-inline list-unstyled">
                                <li>
                                    <span><i class="glyphicon glyphicon-calendar"></i>
                                        <?php
                                        $today = date("Y-m-d H:i:s");
                                        $todayDate = new DateTime($today);
                                        $postDate = new DateTime($item->date);
                                        $interval = $postDate->diff($todayDate, true); ?>

                                        <span class="text-post">
                                            <?= $interval->format('%a дней, %h часов'); ?>
                                        </span>
                                    </span></li>
                                <li>|</li>
                                <li>
                                    <input type="hidden" id="id_user" value="<?= Yii::$app->user->getId(); ?>"/>
                                    <div class="one_news">
                                        <?php
                                        $countLikes = count(\app\models\VotesNewsUser::find()
                                            ->where(['id_news' => $item->id])
                                            ->all());
                                        ?>
                                        <span class="glyphicon glyphicon-heart text-post"
                                              id="like">Like (<b><?= $countLikes ?></b>)</span>
                                        <input type="hidden" id="id_news" value="<?= $item->id; ?>"/>
                                    </div>
                                </li>
                                <li>|</li>
                                <a href="#comments<?= $item->id ?>" data-toggle="collapse">
                                <span><i class="glyphicon glyphicon-comment"></i>

                                    <?php
                                    $comments = Comment::find()->where(['post_id' => $item->id])->all();
                                    if (isset($comments) && $comments != null)
                                        echo count($comments);
                                    else
                                        echo 0;
                                    ?>
                                    <span class="text-post">
                                        комментариев
                                    </span>
                                </span>
                                    <i class="fa fa-caret-down"></i>
                                </a>

                                <div class="collapse text-post" id="comments<?= $item->id ?>">
                                    <?php foreach ($comments

                                                   as $itemComment): ?>
                                        <?php
                                        if (isset($itemComment) && $itemComment != null)
                                            $picture = Image::findOne(['profile_id' => $itemComment->user_id]);
                                        if (isset($picture) && $picture->avatar != null) {
                                            $imgPic = $picture->avatar;
                                        } else {
                                            $imgPic = 'user_default.png';
                                        }
                                        if (isset($itemComment) && $itemComment != null)
                                            $uzer = User::findOne(['id' => $itemComment->user_id]);
                                        ?>
                                        <hr>
                                        <div class="row">
                                            <div class="col-sm-1" style="margin-left: 5px;">
                                                <div class="thumbnail">
                                                    <?= Html::img("@web/images/usr/{$imgPic}", ['class' => 'img-responsive user-photo']) ?>
                                                </div><!-- /thumbnail -->
                                            </div><!-- /col-sm-1 -->

                                            <div class="col-sm-7">
                                                <div class="panel panel-default">
                                                    <div class="panel-heading">
                                                        <strong><?= $uzer->name . " " . $uzer->surname ?></strong>
                                                        <?php
                                                        $today = date("Y-m-d H:i:s");
                                                        $todayDate = new DateTime($today);
                                                        $commentDate = new DateTime($itemComment->date);
                                                        $interval = $commentDate->diff($todayDate, true);
                                                        ?>
                                                        <span class="text-muted"><?= $interval->format('прокомментировано %a дней, %h часов назад'); ?></span>
                                                        <!--actions-->
                                                        <div class="btn-group show-on-hover">
                                                                <span class="dropdown-toggle" type="button"
                                                                      data-toggle="dropdown">
                                                                    <span class="[ glyphicon glyphicon-chevron-down ]"></span>
                                                                </span>

                                                            <ul class="dropdown-menu" role="menu">

                                                                <?php if ((Yii::$app->user->identity['role_id'] >= 5 &&
                                                                        $itemComment->user_id != Yii::$app->user->getId() && $user->role_id < 5) ||
                                                                    $uzer->id == Yii::$app->user->identity['id']): ?>
                                                                    <li role="presentation">

                                                                        <?= Html::a('Удалить', ['delete-comment-news', 'id' => $itemComment->id], [
                                                                            'class' => 'btn btn-danger',
                                                                            'data' => [
                                                                                'confirm' => 'Вы уверены, что хотите удалить эту комментарий?',
                                                                                'method' => 'post',
                                                                            ],
                                                                        ]) ?>
                                                                    </li>
                                                                <?php endif; ?>


                                                                <?php if ($itemComment->user_id != Yii::$app->user->getId()): ?>
                                                                    <li role="presentation">

                                                                        <a href="<?= Url::to(['/uzer/report-comment/',
                                                                            'user_id' => $itemComment->user_id, 'comment_id' =>
                                                                                $itemComment->id, 'type' => 'Оскорбление']) ?>"
                                                                           class="btn btn-info">
                                                                            Пожаловаться
                                                                        </a>
                                                                    </li>
                                                                <?php endif; ?>
                                                            </ul>
                                                        </div>
                                                        <!--actions-->
                                                    </div>
                                                    <div class="panel-body-comment comment-font text-post">
                                                        <?= $itemComment->text ?>
                                                    </div><!-- /panel-body -->
                                                </div><!-- /panel panel-default -->
                                            </div><!-- /col-sm-5 -->

                                        </div><!-- /row -->

                                    <?php endforeach; ?>

                                    <?php $commentForm = ActiveForm::begin(); ?>
                                    <div class="well">
                                        <h4>Что у тебя на уме?</h4>

                                        <?= $commentForm->field($comment, 'text')->input('text',
                                            ['id' => 'userComment', 'class' => 'form-control input-sm chat-input comment',
                                                'placeholder' => 'Напиши своё сообщение здесь',]) ?>

                                        <span class="input-group-btn" onclick="addComment()">
                                        <?= Html::a(' Добавить комментарий', ['view', 'post_id' => $item->id, 'user_id' => $user->id], [
                                            'class' => 'btn btn-primary btn-sm glyphicon glyphicon-comment',
                                            'data' => [
                                                'method' => 'post',
                                            ],
                                        ]) ?>
                                        <?php
                                        $commentForm = ActiveForm::end(); ?>
                                    </div>
                                    <!--                            <li>|</li>-->
                                    <li>
                                        <!-- Use Font Awesome http://fortawesome.github.io/Font-Awesome/ -->
                                    </li>
                            </ul>
                        </div>
                    </div>
                </div>
            <?php endforeach; ?>
        <?php endif; ?>
    </div>

</div>