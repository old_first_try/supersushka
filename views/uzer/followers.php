<?php


use app\models\Comment;
use app\models\Follow;
use app\models\Post;
use app\modules\user\models\Image;
use app\modules\user\models\User;
use yii\data\Pagination;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use yii\widgets\LinkPager;

?>

<div class="user_container well" style="border-radius: 10px;">

    <ul class="nav nav-tabs nav-justified news">
        <li><a href="<?= Url::to('/uzer/news') ?>"><strong>Новости</strong></a></li>
        <li class="active"><a href="#"><strong>Подписки</strong></a></li>
        <li><a href="<?= Url::to('/uzer/search') ?>"><strong>Поиск</strong></a></li>
    </ul>


    <div class="newspost well">

        <?php $form = ActiveForm::begin(); ?>

        <?= $form->field($post, 'content')->textarea(['placeholder' => 'Что у тебя нового?']) ?>

        <?= Html::submitButton('Поделиться', ['class' => 'btn btn-success fa fa-share']) ?>
        <?php $form = ActiveForm::end(); ?>
    </div>

    <?php if (Yii::$app->session->getFlash('successPostDelete')): ?>
        <div class="alert alert-success alert-dismissible flash" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span
                        aria-hidden="true">&times;</span></button>
            <strong>Сделано!</strong> Пост удалён!
        </div>
    <?php endif; ?>

    <?php if (Yii::$app->session->getFlash('errorPostDelete')): ?>
        <div class="alert alert-danger alert-dismissible flash" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span
                        aria-hidden="true">&times;</span></button>
            <strong>Ошибка!</strong> Попробуйте снова!
        </div>
    <?php endif; ?>

    <?php if (Yii::$app->session->getFlash('successPostReport') || Yii::$app->session->getFlash('successCommentReport')): ?>
        <div class="alert alert-success alert-dismissible flash" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span
                        aria-hidden="true">&times;</span></button>
            <strong>Сделано!</strong> Жалоба отправлена!
        </div>
    <?php endif; ?>

    <?php if (Yii::$app->session->getFlash('errorPostReport') || Yii::$app->session->getFlash('errorCommentReport')): ?>
        <div class="alert alert-danger alert-dismissible flash" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span
                        aria-hidden="true">&times;</span></button>
            <strong>Ошибка!</strong> Попробуйте снова!
        </div>
    <?php endif; ?>

    <?php
    $follows = Follow::find()->where(['follower_id' => Yii::$app->user->getId()])->all();
    if (isset($follows)) :
        foreach ($follows as $follow):?>
            <?php
            $posts = array_reverse(Post::findAll(['user_id' => $follow['follow_id']]));

            ?>
            <?php if (isset($posts)): ?>
                <?php foreach ($posts as $item):
                    $img = Image::findOne(['profile_id' => $item->user_id]);
                    if (isset($img) && $img->avatar != null) {
                        $imgPic = $img->avatar;
                    } else {
                        $imgPic = 'user_default.png';
                    }

                    $usr = User::findOne(['id' => $item->user_id]);
                    ?>

                    <?php if (isset($usr)): ?>

                    <div class="list-group-item news-post-user">
                        <div class="media">
                            <a class="pull-left" href="#">
                                <?= Html::img("@web/images/usr/{$imgPic}", ['alt' => 'Avatar', 'class' => '[ img-circle pull-left ]',
                                    'width' => '47', 'height' => '47']) ?>
                            </a>

                            <div class="btn-group show-on-hover">
                        <span class="dropdown-toggle" type="button" data-toggle="dropdown">
                            <span class="[ glyphicon glyphicon-chevron-down ]"></span>
                        </span>

                                <ul class="dropdown-menu" role="menu">

                                    <?php if (Yii::$app->user->identity['role_id'] >= 5 && $item->user_id != Yii::$app->user->getId() && $usr->role_id < 5): ?>
                                        <?php
//                                        Html::a('Удалить пост', ['delete-news', 'id' => $item->id], [
//                                            'class' => 'btn red',
//                                            'style' => "margin-left: 100px;font-size: 16px;",
//                                            'data' => [
//                                                'confirm' => 'Вы уверены, что хотите удалить эту запись?',
//                                                'method' => 'post',
//                                            ],
//                                        ]) ?>

                                        <li role="presentation"><a role="menuitem" tabindex="-1"
                                                                   href="<?= Url::to(['delete-news', 'id' => $item->id]) ?>">Удалить
                                                пост</a>
                                        </li>
                                    <?php endif; ?>

                                    <?php if ($item->user_id != Yii::$app->user->getId() /*&& Yii::$app->user->identity['role_id'] < 6*/): ?>

                                        <li role="presentation"><a href="<?= Url::to(['/uzer/report/',
                                                'user_id' => $item->user_id, 'post_id' => $item->id, 'type' => 'Оскорбление']) ?>"
                                                                   class="btn btn-info">
                                                Пожаловаться
                                            </a>
                                        </li>

                                    <?php endif; ?>

                                </ul>
                            </div>


                            <div class="media-body">
                                <!--                            <h4 class="media-heading">Receta 1</h4>-->
                                <p class="text-right">
                                    <a class="text-post" href="<?= Url::to(['/uzer/view', 'user_id' => $usr->id]) ?>">
                                        By <?= $usr->name . " " . $usr->surname ?></a>
                                    <?php if ($item->user_id != Yii::$app->user->getId()): ?>

                                        <?php
                                        $follows = \app\models\Follow::findOne(['follower_id' => Yii::$app->user->getId()]);
                                        if (isset($follows) && $follows != null) {
                                            echo Html::a('Отписаться', ['unfollow-fol', 'user_id' => $item->user_id], [
                                                'class' => 'btn btn-danger ',
                                                'style' => 'margin-left:0;margin-right:0;',
                                                'data' => [
                                                    'confirm' => 'Вы уверены, что хотите отписаться?',
                                                    'method' => 'post',
                                                ],
                                            ]);
                                        } ?>
                                    <?php endif; ?>
                                </p>

                                <p class="text-left post-font text-post"><?= $item->content; ?></p>
                                <ul class="list-inline list-unstyled">
                                    <li>
                                    <span><i class="glyphicon glyphicon-calendar"></i>
                                        <?php
                                        $today = date("Y-m-d H:i:s");
                                        $todayDate = new DateTime($today);
                                        $postDate = new DateTime($item->date);
                                        $interval = $postDate->diff($todayDate, true); ?>
                                        <span class="text-post">
                                            <?= $interval->format('%a дней, %h часов'); ?>
                                        </span>
                                    </span></li>
                                    <li>|</li>
                                    <li>
                                        <input type="hidden" id="id_user" value="<?= Yii::$app->user->getId(); ?>"/>
                                        <div class="one_news">
                                            <?php
                                            $countLikes = count(\app\models\VotesNewsUser::find()
                                                ->where(['id_news' => $item->id])
                                                ->all());
                                            ?>
                                            <span class="glyphicon glyphicon-heart text-post"
                                                  id="like">Like (<b><?= $countLikes ?></b>)</span>
                                            <input type="hidden" id="id_news" value="<?= $item->id; ?>"/>
                                        </div>
                                    </li>
                                    <li>|</li>
                                    <a href="#comments<?= $item->id ?>" data-toggle="collapse">
                                <span><i class="glyphicon glyphicon-comment"></i>

                                    <?php
                                    $comments = Comment::find()->where(['post_id' => $item->id])->all();
                                    if (isset($comments) && $comments != null)
                                        echo count($comments);
                                    else
                                        echo 0;
                                    ?>
                                    <span class="text-post">
                                        комментариев
                                    </span>
                                </span>
                                        <i class="fa fa-caret-down"></i>
                                    </a>

                                    <div class="collapse" id="comments<?= $item->id ?>">
                                        <?php foreach ($comments

                                                       as $itemComment): ?>
                                            <?php
                                            if (isset($itemComment) && $itemComment != null)
                                                $picture = Image::findOne(['profile_id' => $itemComment->user_id]);
                                            if (isset($picture) && $picture->avatar != null) {
                                                $imgPic = $picture->avatar;
                                            } else {
                                                $imgPic = 'user_default.png';
                                            }
                                            if (isset($itemComment) && $itemComment != null)
                                                $uzer = User::findOne(['id' => $itemComment->user_id]);
                                            ?>
                                            <hr>
                                            <div class="row">
                                                <div class="col-sm-1" style="margin-left: 5px;">
                                                    <div class="thumbnail">
                                                        <?= Html::img("@web/images/usr/{$imgPic}", ['class' => 'img-responsive user-photo']) ?>
                                                    </div><!-- /thumbnail -->
                                                </div><!-- /col-sm-1 -->

                                                <div class="col-sm-5">
                                                    <div class="panel panel-default">
                                                        <div class="panel-heading">
                                                            <a class="text-post" href="<?= Url::to(['/uzer/view', 'user_id' => $uzer->id]) ?>">
                                                                <strong><?= $uzer->name . " " . $uzer->surname ?></strong>
                                                            </a>
                                                            <?php
                                                            $today = date("Y-m-d H:i:s");
                                                            $todayDate = new DateTime($today);
                                                            $commentDate = new DateTime($itemComment->date);
                                                            $interval = $commentDate->diff($todayDate, true);
                                                            ?>
                                                            <span class="text-muted"><?= $interval->format('прокомментировано %a дней, %h часов назад'); ?></span>
                                                            <!--actions-->
                                                            <div class="btn-group show-on-hover">
                                                                <span class="dropdown-toggle" type="button"
                                                                      data-toggle="dropdown">
                                                                    <span class="[ glyphicon glyphicon-chevron-down ]"></span>
                                                                </span>

                                                                <ul class="dropdown-menu" role="menu">

                                                                    <?php if ((Yii::$app->user->identity['role_id'] >= 5 &&
                                                                            $itemComment->user_id != Yii::$app->user->getId() && $usr->role_id < 5) ||
                                                                        $uzer->id == Yii::$app->user->identity['id']): ?>
                                                                        <li role="presentation">

                                                                            <?= Html::a('Удалить', ['delete-comment-news', 'id' => $itemComment->id], [
                                                                                'class' => 'btn btn-danger',
                                                                                'data' => [
                                                                                    'confirm' => 'Вы уверены, что хотите удалить эту комментарий?',
                                                                                    'method' => 'post',
                                                                                ],
                                                                            ]) ?>
                                                                        </li>
                                                                    <?php endif; ?>


                                                                    <?php if ($itemComment->user_id != Yii::$app->user->getId()): ?>
                                                                        <li role="presentation">

                                                                            <a href="<?= Url::to(['/uzer/report-comment/',
                                                                                'user_id' => $itemComment->user_id, 'comment_id' =>
                                                                                    $itemComment->id, 'type' => 'Оскорбление']) ?>"
                                                                               class="btn btn-danger">
                                                                                Пожаловаться
                                                                            </a>
                                                                        </li>

                                                                    <?php endif; ?>


                                                                </ul>
                                                            </div>
                                                            <!--actions-->
                                                        </div>
                                                        <div class="panel-body-comment comment-font text-post">
                                                            <?= $itemComment->text ?>
                                                            <p class="text-right">


                                                        </div><!-- /panel-body -->
                                                    </div><!-- /panel panel-default -->
                                                </div><!-- /col-sm-5 -->

                                            </div><!-- /row -->

                                        <?php endforeach; ?>

                                        <?php $commentForm = ActiveForm::begin(); ?>
                                        <div class="well">
                                            <h4>Что у тебя на уме?</h4>

                                            <?= $commentForm->field($comment, 'text')->input('text',
                                                ['id' => 'userComment', 'class' => 'form-control input-sm chat-input comment comment-font',
                                                    'placeholder' => 'Напиши своё сообщение здесь',]) ?>

                                            <span class="input-group-btn" onclick="addComment()">
                                        <?= Html::a(' Добавить комментарий', ['news', 'post_id' => $item->id], [
                                            'class' => 'btn btn-primary btn-sm glyphicon glyphicon-comment ',
                                            'data' => [
                                                'method' => 'post',
                                            ],
                                            'style' => 'margin-top:0',
                                        ]) ?>


                                        <?php
                                        $commentForm = ActiveForm::end(); ?>

                                        </div>
                                        <!--                            <li>|</li>-->
                                        <li>
                                            <!-- Use Font Awesome http://fortawesome.github.io/Font-Awesome/ -->
                                        </li>
                                </ul>
                            </div>
                        </div>
                    </div>


                <?php endif; ?>
                <?php endforeach; ?>

            <?php endif; ?>
            <?php
        endforeach; ?>
    <?php endif; ?>

    <?php if ($follows == null): ?>
        <h4 align="center" id="title">У вас пока что нет подписок</h4>
    <?php endif; ?>

</div>
