<?php
/**
 * supersushka - view-admin.php
 *
 * Initial version by: Tom
 * Initial created on: 07.11.2017 2:49
 */

//Yii::$app->getSecurity()->generatePasswordHash('newParticipant@gmail.com')

use app\models\Comment;
use app\models\Post;
use app\models\Report;
use app\modules\user\models\Image;
use app\modules\user\models\Task;
use app\modules\user\models\User;
use app\modules\user\models\UserQuestion;
use yii\bootstrap\Modal;
use yii\data\Pagination;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use yii\widgets\LinkPager;

if (!isset($user))
    $user = Yii::$app->user->identity;
$image = Image::findOne(['profile_id' => $user->id]);
if (!isset($image)) {
    $img = 'user_default.png';
} else {
    $img = $image->avatar;
}
$posts = array_reverse(Post::find()->where(['user_id' => $user->id])->all());
$this->title = 'Страница админа';
//reports
$reports = Report::find()->all();
//reports
//stats
$userCount = sizeof(User::find()->all());
$taskCount = sizeof(Task::find()->all());
$messageCount = sizeof(\app\models\Message::find()->all());
$postCount = sizeof(\app\models\Post::find()->all());
$demoUzer = User::findOne(['login' => 'newParticipant@gmail.com']);
//stats
?>

<div class="admin_container well" style="border-radius: 15px;">


    <div class="list-group-item border_admin ">
        <h3 class="text-post list-group-item">Жалобы пользователей:</h3>
        <!--reports-->
        <?php if (Yii::$app->session->getFlash('successReportDelete')): ?>
            <div class="alert alert-success alert-dismissible flash border_radius" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span
                            aria-hidden="true">&times;</span></button>
                <strong>Сделано!</strong> Жалоба удалёна!
            </div>
        <?php endif; ?>

        <?php if (Yii::$app->session->getFlash('errorReportDelete')): ?>
            <div class="alert alert-success alert-dismissible flash border_radius" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span
                            aria-hidden="true">&times;</span></button>
                <strong>Ошибка!</strong> Попробуйте снова!
            </div>
        <?php endif; ?>

        <?php if (isset($reports) && $reports != null): ?>
    <?php foreach ($reports

                   as $report): ?>
    <?php
    if ($report->post_id != 0):
        $userReport = User::findOne(['id' => $report->user_id]);
        $image = Image::findOne(['profile_id' => $userReport->id]);
        if ($image->avatar == null) {
            $imgReport = 'user_default.png';
        } elseif ($image->avatar != null) {
            $imgReport = $image->avatar;
        }

        $post = \app\models\Post::findOne(['id' => $report->post_id]);
        if (isset($post)):
            ?>
            <div class="panel-group" id="accordion">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h4 data-toggle="collapse" data-parent="#accordion"
                            href="#collapse<?= $report->id ?>" class="panel-title expand">
                            <div class="right-arrow pull-right">+</div>
                            <a href="#"><?= $userReport->name ?></a>
                        </h4>
                    </div>
                    <div id="collapse<?= $report->id ?>" class="panel-collapse collapse">
                        <div class="panel-body">
                            <strong>Имя: </strong>
                            <a href="<?= Url::to(['/uzer/view', 'user_id' => $userReport->id]) ?>">
                                <?= Html::encode($userReport->name . " ") ?><?= Html::encode($userReport->surname) ?>
                            </a>

                            <br>
                            <hr>
                            <strong>Тип жалобы: </strong><?= Html::encode($report->type) ?>
                            <hr>
                            <strong>Время поста: </strong><?= Html::encode($post->date) ?>
                            <hr>
                            <!--reports-->
                            <?= Html::button('Просмотреть пост', [
                                'class' => 'btn btn-primary view-report-post',
                                'style' => 'font-size:16px;margin-left:auto;margin-right:auto;'

                            ]); ?>
                            <?php
                            Modal::begin([
                                'header' => '<h2 align="center">Пост, на который пожаловались:</h2>',
                                'id' => 'report-post',
                                'size' => 'modal-lg',
                            ]);
                            ?>
                            <div class="media">
                                <a class="pull-left" href="#">
                                    <?= Html::img("@web/images/usr/{$imgReport}", ['alt' => 'Avatar', 'class' => '[ img-circle pull-left ]',
                                        'width' => '47', 'height' => '47']) ?>
                                </a>

                                <div class="media-body">
                                    <!--                                                    <h4 class="media-heading">Receta 1</h4>-->
                                    <p class="text-left"><?= $post->content; ?></p>
                                    <p class="text-right">
                                        By <?= $userReport->name . " " . $userReport->surname ?></p>
                                    <ul class="list-inline list-unstyled">
                                        <li>
                                    <span><i class="glyphicon glyphicon-calendar"></i>
                                        <?php
                                        $today = date("Y-m-d H:i:s");
                                        $todayDate = new DateTime($today);
                                        $postDate = new DateTime($post->date);
                                        $interval = $postDate->diff($todayDate, true);
                                        echo $interval->format('%a дней, %h часов');
                                        ?>
                                    </span></li>
                                        <li>|</li>
                                        <li>
                                            <input type="hidden" id="id_user" value="<?= Yii::$app->user->getId(); ?>"/>
                                            <div class="one_news">
                                                <?php
                                                $countLikes = count(\app\models\VotesNewsUser::find()
                                                    ->where(['id_news' => $post->id])
                                                    ->all());
                                                ?>
                                                <span class="glyphicon glyphicon-heart text-post"
                                                      id="like">Like (<b><?= $countLikes ?></b>)</span>
                                                <input type="hidden" id="id_news" value="<?= $post->id; ?>"/>
                                            </div>
                                        </li>
                                        <li>|</li>
                                        <a href="#comments<?= $post->id ?>" data-toggle="collapse">
                                <span><i class="glyphicon glyphicon-comment"></i>

                                    <?php
                                    $comments = Comment::find()->where(['post_id' => $post->id])->all();
                                    if (isset($comments) && $comments != null)
                                        echo count($comments);
                                    else
                                        echo 0;
                                    ?>
                                    комментариев
                                </span>
                                            <i class="fa fa-caret-down"></i>
                                        </a>

                                        <div class="collapse" id="comments<?= $post->id ?>">
                                            <?php foreach ($comments

                                                           as $itemComment): ?>
                                                <?php
                                                if (isset($itemComment) && $itemComment != null)
                                                    $picture = Image::findOne(['profile_id' => $itemComment->user_id]);
                                                if (isset($picture) && $picture->avatar != null) {
                                                    $imgPic = $picture->avatar;
                                                } else {
                                                    $imgPic = 'user_default.png';
                                                }
                                                if (isset($itemComment) && $itemComment != null)
                                                    $uzer = User::findOne(['id' => $itemComment->user_id]);
                                                ?>
                                                <hr>
                                                <div class="row">
                                                    <div class="col-sm-1" style="margin-left: 5px;">
                                                        <div class="thumbnail">
                                                            <?= Html::img("@web/images/usr/{$imgPic}", ['class' => 'img-responsive user-photo']) ?>
                                                        </div><!-- /thumbnail -->
                                                    </div><!-- /col-sm-1 -->

                                                    <div class="col-sm-5">
                                                        <div class="panel panel-default">
                                                            <div class="panel-heading">
                                                                <strong><?= $uzer->name . " " . $uzer->surname ?></strong>
                                                                <?php
                                                                $today = date("Y-m-d H:i:s");
                                                                $todayDate = new DateTime($today);
                                                                $commentDate = new DateTime($itemComment->date);
                                                                $interval = $commentDate->diff($todayDate, true);
                                                                ?>
                                                                <span class="text-muted"><?= $interval->format('прокомментировано %a дней, %h часов назад'); ?></span>
                                                            </div>
                                                            <div class="panel-body-comment"
                                                                 style="color:#55acee;">
                                                                <?= $itemComment->text ?>
                                                            </div><!-- /panel-body -->
                                                        </div><!-- /panel panel-default -->
                                                    </div><!-- /col-sm-5 -->

                                                </div><!-- /row -->

                                            <?php endforeach; ?>

                                            <?php $commentForm = ActiveForm::begin(); ?>
                                            <div class="well">
                                                <h4>Что у тебя на уме?</h4>

                                                <?= $commentForm->field($comment, 'text')->input('text',
                                                    ['id' => 'userComment', 'class' => 'form-control input-sm chat-input comment',
                                                        'placeholder' => 'Напиши своё сообщение здесь',]) ?>

                                                <span class="input-group-btn" onclick="addComment()">
                                        <?= Html::a(' Добавить комментарий', ['view', 'post_id' => $post->id,
                                            'user_id' => $userReport->id], [
                                            'class' => 'btn btn-primary btn-sm glyphicon glyphicon-comment',
                                            'data' => [
                                                'method' => 'post',
                                            ],
                                        ]) ?>


                                        <?php
                                        $commentForm = ActiveForm::end(); ?>

                                            </div>
                                            <!--                            <li>|</li>-->
                                            <li>
                                                <!-- Use Font Awesome http://fortawesome.github.io/Font-Awesome/ -->
                                            </li>
                                    </ul>
                                </div>
                            </div>
                            <hr>
                            <?= Html::a('Удалить пост', ['/uzer/delete-news',
                                'id' => $report->post_id, 'report_id' => $report->id, 'type' => 'help'], [
                                'class' => 'btn btn-danger bun',
                                'data' => [
                                    'confirm' => 'Вы уверены, что хотите удалить пост?',
                                    'method' => 'post',
                                ],
                            ]) ?>

                            <?= Html::a('Удалить жалобу', ['/uzer/delete',
                                'report_id' => $report->id, 'type' => 'report'], [
                                'class' => 'btn btn-danger bun',
                                'data' => [
                                    'confirm' => 'Вы уверены, что хотите удалить жалобу?',
                                    'method' => 'post',
                                ],
                            ]) ?>
                            <?php Modal::end(); ?>
                        </div>
                    </div>
                </div>
            </div>
            <!--reports-->
        <?php endif; ?>
    <?php endif; ?>

    <?php if ($report->comment_id != null):
    $userReportComment = User::findOne(['id' => $report->user_id]);
    $comment = \app\models\Comment::findOne(['id' => $report->comment_id]);
    $post = Post::findOne(['id' => $comment->post_id]);

    $userReport = User::findOne(['id' => $post->user_id]);
    $imgComment = Image::findOne(['profile_id' => $report->user_id]);
    if ($imgComment->avatar == null) {
        $imgRC = 'user_default.png';
    } elseif ($imgComment->avatar != null) {
        $imgRC = $imgComment->avatar;
    }
    ?>
    <?php if (isset($comment)): ?>

        <div class="panel-group" id="accordion">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h4 data-toggle="collapse" data-parent="#accordion"
                        href="#collapse<?= $report->id ?>" class="panel-title expand">
                        <div class="right-arrow pull-right">+</div>
                        <a href="#"><?= $userReportComment->name ?></a>
                    </h4>
                </div>
                <div id="collapse<?= $report->id ?>" class="panel-collapse collapse">
                    <div class="panel-body">
                        <strong>Имя: </strong>
                        <a href="<?= Url::to(['/uzer/view', 'user_id' => $userReportComment->id]) ?>">
                            <?= Html::encode($userReportComment->name . " ") ?><?= Html::encode($userReportComment->surname) ?>
                        </a>

                        <br>
                        <hr>
                        <strong>Тип жалобы: </strong><?= Html::encode($report->type) ?>
                        <hr>
                        <strong>Время комментария: </strong><?= Html::encode($comment->date) ?>
                        <hr>
                        <strong>Комментарий: </strong>
                        <br>
                        <!--reports-->
                        <?= Html::button('Просмотреть комментарий', [
                            'class' => 'btn btn-primary view-report-comment',
                            'style' => 'font-size:16px;margin-left:auto;margin-right:auto;'

                        ]); ?>

                        <?php
                        Modal::begin([
                            'header' => '<h2 align="center">Комментарий, на который пожаловались выделен красным:</h2>',
                            'id' => 'report-comment',
                            'size' => 'modal-lg',
                        ]);
                        ?>
                        <div class="media">
                            <a class="pull-left" href="#">
                                <?= Html::img("@web/images/usr/{$imgRC}", ['alt' => 'Avatar', 'class' => '[ img-circle pull-left ]',
                                    'width' => '47', 'height' => '47']) ?>
                            </a>

                            <div class="media-body">
                                <!--                                                    <h4 class="media-heading">Receta 1</h4>-->
                                <p class="text-left"><?= $comment->text; ?></p>
                                <p class="text-right">
                                    By <?= $userReportComment->name . " " . $userReportComment->surname ?></p>
                                <ul class="list-inline list-unstyled">
                                    <li>
                                    <span><i class="glyphicon glyphicon-calendar"></i>
                                        <?php
                                        $today = date("Y-m-d H:i:s");
                                        $todayDate = new DateTime($today);
                                        $postDate = new DateTime($comment->date);
                                        $interval = $postDate->diff($todayDate, true);
                                        echo $interval->format('%a дней, %h часов');
                                        ?>
                                    </span></li>
                                    <li>|</li>
                                    <li>
                                        <input type="hidden" id="id_user" value="<?= Yii::$app->user->getId(); ?>"/>
                                        <div class="one_news">
                                            <?php
                                            $countLikes = count(\app\models\VotesNewsUser::find()
                                                ->where(['id_news' => $post->id])
                                                ->all());
                                            ?>
                                            <span class="glyphicon glyphicon-heart text-post"
                                                  id="like">Like (<b><?= $countLikes ?></b>)</span>
                                            <input type="hidden" id="id_news" value="<?= $post->id; ?>"/>
                                        </div>
                                    </li>
                                    <li>|</li>
                                    <a href="#comments<?= $comment->id ?>" data-toggle="collapse">
                                <span><i class="glyphicon glyphicon-comment"></i>

                                    <?php
                                    $comments = Comment::find()->where(['post_id' => $post->id])->all();
                                    if (isset($comments) && $comments != null)
                                        echo count($comments);
                                    else
                                        echo 0;
                                    ?>
                                    комментариев
                                </span>
                                        <i class="fa fa-caret-down"></i>
                                    </a>


                                    <?php foreach ($comments

                                                   as $itemComment): ?>
                                        <?php
                                        if (isset($itemComment) && $itemComment != null)
                                            $picture = Image::findOne(['profile_id' => $itemComment->user_id]);
                                        if (isset($picture) && $picture->avatar != null) {
                                            $imgPic = $picture->avatar;
                                        } else {
                                            $imgPic = 'user_default.png';
                                        }
                                        if (isset($itemComment) && $itemComment != null)
                                            $uzer = User::findOne(['id' => $itemComment->user_id]);
                                        ?>
                                        <hr>
                                        <div class="row">
                                            <div class="col-sm-1" style="margin-left: 5px;">
                                                <div class="thumbnail">
                                                    <?= Html::img("@web/images/usr/{$imgPic}", ['class' => 'img-responsive user-photo']) ?>
                                                </div><!-- /thumbnail -->
                                            </div><!-- /col-sm-1 -->

                                            <div class="col-sm-5">
                                                <div class="panel panel-default">
                                                    <div class="panel-heading">
                                                        <strong><?= $uzer->name . " " . $uzer->surname ?></strong>
                                                        <?php
                                                        $today = date("Y-m-d H:i:s");
                                                        $todayDate = new DateTime($today);
                                                        $commentDate = new DateTime($itemComment->date);
                                                        $interval = $commentDate->diff($todayDate, true);
                                                        ?>
                                                        <span class="text-muted"><?= $interval->format('прокомментировано %a дней, %h часов назад'); ?></span>
                                                        <!--actions-->
                                                        <div class="btn-group show-on-hover">
                                                                <span class="dropdown-toggle" type="button"
                                                                      data-toggle="dropdown">
                                                                    <span class="[ glyphicon glyphicon-chevron-down ]"></span>
                                                                </span>

                                                            <ul class="dropdown-menu" role="menu">

                                                                <?php if ((Yii::$app->user->identity['role_id'] >= 5 &&
                                                                        $itemComment->user_id != Yii::$app->user->getId() && $user->role_id < 5) ||
                                                                    $uzer->id == Yii::$app->user->identity['id']): ?>
                                                                    <li role="presentation">

                                                                        <?= Html::a('Удалить', ['delete-comment-news', 'id' => $itemComment->id], [
                                                                            'class' => 'btn btn-danger',
                                                                            'data' => [
                                                                                'confirm' => 'Вы уверены, что хотите удалить эту комментарий?',
                                                                                'method' => 'post',
                                                                            ],
                                                                        ]) ?>
                                                                    </li>
                                                                <?php endif; ?>


                                                                <?php if ($itemComment->user_id != Yii::$app->user->getId()): ?>
                                                                    <li role="presentation">

                                                                        <a href="<?= Url::to(['/uzer/report-comment/',
                                                                            'user_id' => $itemComment->user_id, 'comment_id' =>
                                                                                $itemComment->id, 'type' => 'Оскорбление']) ?>"
                                                                           class="btn btn-info">
                                                                            Пожаловаться
                                                                        </a>
                                                                    </li>

                                                                <?php endif; ?>


                                                            </ul>
                                                        </div>
                                                        <!--actions-->
                                                    </div>
                                                    <div class="panel-body-comment"
                                                         style="color:#55acee;">
                                                        <?php if ($itemComment->id == $comment->id): ?>
                                                            <strong style="color: red;font-size:20px;">
                                                                <?= $itemComment->text ?>
                                                            </strong>
                                                        <?php endif; ?>
                                                        <?php if ($itemComment->id != $comment->id): ?>
                                                            <?= $itemComment->text ?>
                                                        <?php endif; ?>
                                                    </div><!-- /panel-body -->
                                                </div><!-- /panel panel-default -->
                                            </div><!-- /col-sm-5 -->
                                        </div>
                                        <!-- /row -->

                                    <?php endforeach; ?>

                                    <?php $commentForm = ActiveForm::begin(); ?>
                                    <div class="well">
                                        <h4>Что у тебя на уме?</h4>

                                        <?= $commentForm->field($comment, 'text')->input('text',
                                            ['id' => 'userComment', 'class' => 'form-control input-sm chat-input comment',
                                                'placeholder' => 'Напиши своё сообщение здесь',]) ?>

                                        <span class="input-group-btn" onclick="addComment()">
                                        <?= Html::a(' Добавить комментарий', ['view', 'post_id' => $post->id,
                                            'user_id' => $userReport->id], [
                                            'class' => 'btn btn-primary btn-sm glyphicon glyphicon-comment',
                                            'data' => [
                                                'method' => 'post',
                                            ],
                                        ]) ?>


                                        <?php
                                        $commentForm = ActiveForm::end(); ?>

                                    </div>
                                    <!--                            <li>|</li>-->
                                    <li>
                                        <!-- Use Font Awesome http://fortawesome.github.io/Font-Awesome/ -->
                                    </li>
                                </ul>
                            </div>
                            <hr>
                            <?= Html::a('Удалить комментарий', ['/uzer/delete-comment-news',
                                'id' => $report->comment_id, 'report_id' => $report->id, 'route' => 'view'], [
                                'class' => 'btn btn-danger bun',
                                'data' => [
                                    'confirm' => 'Вы уверены, что хотите удалить комментарий?',
                                    'method' => 'post',
                                ],
                            ]) ?>


                            <?= Html::a('Удалить жалобу', ['/uzer/delete',
                                'report_id' => $report->id, 'type' => 'report'], [
                                'class' => 'btn btn-danger bun',
                                'data' => [
                                    'confirm' => 'Вы уверены, что хотите удалить жалобу?',
                                    'method' => 'post',
                                ],
                            ]) ?>

                            <?php Modal::end(); ?>
                            <!--reports-->
                        </div>
                    </div>
                </div>
            </div>
            <?php endif; ?>

            <?php endif; ?>

            <?php endforeach; ?>

            <?php elseif ($reports == null): ?>
                <div class="list-group-item admin-title">
                    Сейчас жалоб нет.
                </div>
            <?php endif; ?>
        </div>
        <!--reports-->

        <div class="list-group-item border_admin">
            <!--questions-->
            <h3 class="text-post list-group-item ">
                Вопросы от пользователей:
            </h3>

            <?php
            $query = UserQuestion::find()->where(['status' => 1]);
            $pages = new Pagination(['totalCount' => $query->count(), 'pageSize' => 7]);

            $questions = $query->offset($pages->offset)
                ->limit($pages->limit)
                //жадная загрузка данных
                ->with()
                //
                ->all();
            if (isset($questions) && $questions != null): ?>
                <?php
                foreach ($questions as $question) :?>

                    <div class="panel-group" id="accordion">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h4 data-toggle="collapse" data-parent="#accordion"
                                    href="#collapse<?= $question->id ?>" class="panel-title expand">
                                    <div class="right-arrow pull-right">+</div>
                                    <a href="#"><?= $question->name ?></a>
                                </h4>
                            </div>
                            <div id="collapse<?= $question->id ?>" class="panel-collapse collapse">
                                <div class="panel-body">
                                    <a href="<?= Url::to(['/user/admin/answer', 'id' => $question->id]) ?>">

                                        <div class="well"
                                             style="font-size: 16px;width: 75%;margin-right: auto;margin-left: auto;">
                                            <p style="margin-bottom: 0;font-size: 20px;">
                                                <strong>
                                                    Имя: <?= Html::encode($question->name); ?>
                                                    <br>

                                                    Тема вопроса: <?= Html::encode($question->theme); ?>
                                                </strong>
                                            </p>
                                        </div>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                <?php endforeach;
                echo LinkPager::widget([
                    'pagination' => $pages,
                ]); ?>
            <?php elseif (isset($questions) && $questions == null): ?>
                <div class="list-group-item admin-title ">
                    Вопросов от участников не поступало.
                </div>
            <?php endif; ?>
            <!--questions-->
            <?php if (Yii::$app->session->getFlash('answerFormSubmitted')): ?>
                <div class="alert alert-success alert-dismissible flash border_radius" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span
                                aria-hidden="true">&times;</span></button>
                    Ваш ответ отправлен.
                </div>
            <?php endif; ?>

            <?php if (Yii::$app->session->getFlash('error')): ?>
                <div class="alert alert-danger alert-dismissible flash border_radius" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span
                                aria-hidden="true">&times;</span></button>
                    Ошибка! Попробуйте снова или обратитесь в тех-поддержку.
                </div>
            <?php endif; ?>


        </div>

        <div class="row">
            <?php if ($user->id == Yii::$app->user->identity['id']): ?>
                <div class="col-md-6">
                    <div class="well" style="margin-top: 15px;margin-left: 15px;">

                        <?php $form = ActiveForm::begin(); ?>

                        <?= $form->field($adminPost, 'content')->textarea(['placeholder' => 'Что ты делаешь прямо сейчас?']) ?>

                        <button type="submit" class="btn btn-success"><i class="fa fa-share"></i> Поделиться
                        </button>
                        <?php $form = ActiveForm::end(); ?>
                    </div><!-- Widget Area -->
                </div>

                <div class="col-md-6">
                    <div class="well border_admin">

                        <h3 align="center"
                            class="text-post"> <?= Html::encode('Данные для входа как пользователь') ?></h3>
                        <div class="row flash" style="width: 100%;">
                            <h4 align="center" class="text-post">
                                <?= Html::encode('Логин: ') ?>
                            </h4>
                            <h4 id="title">
                                <?= $demoUzer->login ?>
                            </h4>
                            <h4 align="center" class="text-post">
                                <?= Html::encode('Пароль: ') ?></h4>
                            <h4 id="title">
                                <?= $demoUzer->login ?>
                            </h4>
                        </div>
                    </div><!-- Widget Area -->
                </div>
            <?php endif; ?>
        </div>

        <?php if (Yii::$app->session->getFlash('successPostDelete')): ?>
            <div class="alert alert-success alert-dismissible flash" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span
                            aria-hidden="true">&times;</span></button>
                <strong>Сделано!</strong> Пост удалён!
            </div>
        <?php endif; ?>

        <?php if (Yii::$app->session->getFlash('errorPostDelete') || Yii::$app->session->getFlash('errorPostUpdate')): ?>
            <div class="alert alert-danger alert-dismissible flash" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span
                            aria-hidden="true">&times;</span></button>
                <strong>Ошибка!</strong> Попробуйте снова!
            </div>
        <?php endif; ?>


        <?php if (Yii::$app->session->getFlash('successPostUpdate')): ?>
            <div class="alert alert-success alert-dismissible flash" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span
                            aria-hidden="true">&times;</span></button>
                <strong>Сделано!</strong> Пост изменён!
            </div>
        <?php endif; ?>

        <?php if (Yii::$app->session->getFlash('successPostReport') || Yii::$app->session->getFlash('successCommentReport')): ?>
            <div class="alert alert-success alert-dismissible flash" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span
                            aria-hidden="true">&times;</span></button>
                <strong>Сделано!</strong> Жалоба отправлена!
            </div>
        <?php endif; ?>

        <?php if (Yii::$app->session->getFlash('errorPostReport') ||
            Yii::$app->session->getFlash('errorCommentReport' ||
                Yii::$app->session->getFlash('errorCommentDelete'))): ?>
            <div class="alert alert-danger alert-dismissible flash" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span
                            aria-hidden="true">&times;</span></button>
                <strong>Ошибка!</strong> Попробуйте снова!
            </div>
        <?php endif; ?>

        <?php if (Yii::$app->session->getFlash('successCommentDelete')): ?>
            <div class="alert alert-success alert-dismissible flash" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span
                            aria-hidden="true">&times;</span></button>
                <strong>Сделано!</strong> Комментарий удалён!
            </div>
        <?php endif; ?>


        <?php if ($posts == null && $user->id != Yii::$app->user->getId()): ?>
            <h4 align="center" id="title">Этот пользователь не опубликовал ёше ни одного поста</h4>
        <?php endif; ?>
        <?php if ($posts == null && $user->id == Yii::$app->user->getId()): ?>
            <h4 align="center" id="title">Вы не написали ёще ни одного поста</h4>
        <?php endif; ?>
        <?php if (isset($posts) && $posts != null): ?>

            <?php foreach ($posts

                           as $item): ?>
                <div class="list-group-item border_admin">
                    <div class="media">
                        <a class="pull-left" href="#">
                            <?= Html::img("@web/images/usr/{$img}", ['alt' => 'Avatar', 'class' => '[ img-circle pull-left ]',
                                'width' => '47', 'height' => '47']) ?>
                        </a>

                        <div class="btn-group show-on-hover">
                        <span class="dropdown-toggle" type="button" data-toggle="dropdown">
                            <span class="[ glyphicon glyphicon-chevron-down ]"></span>
                        </span>

                            <ul class="dropdown-menu" role="menu">
                                <?php if ($user->id == Yii::$app->user->identity['id']): ?>
                                    <li role="presentation"><a role="menuitem" tabindex="-1"
                                                               href="<?= Url::to(['/uzer/delete', 'id' => $item->id]) ?>">Удалить</a>
                                    </li>
                                <?php endif; ?>

                                <?php if ($item->user_id == Yii::$app->user->getId()): ?>
                                    <li role="presentation">
                                        <a role="menuitem" tabindex="-1"
                                           href="<?= Url::to(['/uzer/update', 'id' => $item->id, 'view' => 'view']) ?>">
                                            Изменить</a>
                                    </li>
                                <?php endif; ?>

                                <?php if ($item->user_id != Yii::$app->user->getId() /*&& Yii::$app->user->identity['role_id'] < 6*/): ?>

                                    <a href="<?= Url::to(['/uzer/report-comment/',
                                        'user_id' => $item->user_id, 'post_id' =>
                                            $item->id, 'type' => 'Оскорбление', 'route' => 'view']) ?>">
                                        <span class="label label-info">Пожаловаться</span>
                                    </a>
                                <?php endif; ?>
                            </ul>
                        </div>


                        <div class="media-body">
                            <p class="text-left text-post post-font"><?= $item->content; ?></p>
                            <p class="text-right text-post">By <?= $user->name . " " . $user->surname ?></p>
                            <ul class="list-inline list-unstyled">
                                <li>
                                    <span><i class="glyphicon glyphicon-calendar"></i>
                                        <?php
                                        $today = date("Y-m-d H:i:s");
                                        $todayDate = new DateTime($today);
                                        $postDate = new DateTime($item->date);
                                        $interval = $postDate->diff($todayDate, true); ?>
                                        <span class="text-post">
                                            <?= $interval->format('%a дней, %h часов'); ?>
                                        </span>
                                    </span></li>
                                <li>|</li>
                                <li>
                                    <input type="hidden" id="id_user" value="<?= Yii::$app->user->getId(); ?>"/>
                                    <div class="one_news">
                                        <?php
                                        $countLikes = count(\app\models\VotesNewsUser::find()
                                            ->where(['id_news' => $item->id])
                                            ->all());
                                        ?>
                                        <span class="glyphicon glyphicon-heart text-post"
                                              id="like">Like (<b><?= $countLikes ?></b>)</span>
                                        <input type="hidden" id="id_news" value="<?= $item->id; ?>"/>
                                    </div>
                                </li>
                                <li>|</li>
                                <a href="#comments<?= $item->id ?>" data-toggle="collapse">
                                <span><i class="glyphicon glyphicon-comment"></i>

                                    <?php
                                    $comments = Comment::find()->where(['post_id' => $item->id])->all();
                                    if (isset($comments) && $comments != null)
                                        echo count($comments);
                                    else
                                        echo 0;
                                    ?>
                                    <span class="text-post">
                                        комментариев
                                    </span>
                                </span>
                                    <i class="fa fa-caret-down"></i>
                                </a>

                                <div class="collapse" id="comments<?= $item->id ?>">
                                    <?php foreach ($comments

                                                   as $itemComment): ?>
                                        <?php
                                        if (isset($itemComment) && $itemComment != null)
                                            $picture = Image::findOne(['profile_id' => $itemComment->user_id]);
                                        if (isset($picture) && $picture->avatar != null) {
                                            $imgPic = $picture->avatar;
                                        } else {
                                            $imgPic = 'user_default.png';
                                        }
                                        if (isset($itemComment) && $itemComment != null)
                                            $uzer = User::findOne(['id' => $itemComment->user_id]);
                                        ?>
                                        <hr>
                                        <div class="row">
                                            <div class="col-sm-1" style="margin-left: 5px;">
                                                <div class="thumbnail">
                                                    <?= Html::img("@web/images/usr/{$imgPic}", ['class' => 'img-responsive user-photo']) ?>
                                                </div><!-- /thumbnail -->
                                            </div><!-- /col-sm-1 -->

                                            <div class="col-sm-5">
                                                <div class="panel panel-default">
                                                    <div class="panel-heading">
                                                        <strong><?= $uzer->name . " " . $uzer->surname ?></strong>
                                                        <?php
                                                        $today = date("Y-m-d H:i:s");
                                                        $todayDate = new DateTime($today);
                                                        $commentDate = new DateTime($itemComment->date);
                                                        $interval = $commentDate->diff($todayDate, true);
                                                        ?>
                                                        <span class="text-muted"><?= $interval->format('прокомментировано %a дней, %h часов назад'); ?></span>
                                                        <!--actions-->
                                                        <div class="btn-group show-on-hover">
                                                                <span class="dropdown-toggle" type="button"
                                                                      data-toggle="dropdown">
                                                                    <span class="[ glyphicon glyphicon-chevron-down ]"></span>
                                                                </span>

                                                            <ul class="dropdown-menu" role="menu">

                                                                <?php if ((Yii::$app->user->identity['role_id'] >= 5 &&
                                                                        $itemComment->user_id != Yii::$app->user->getId() && $user->role_id < 5) ||
                                                                    $uzer->id == Yii::$app->user->identity['id']): ?>
                                                                    <li role="presentation">

                                                                        <?= Html::a('Удалить', ['delete-comment-news', 'id' => $itemComment->id], [
                                                                            'class' => 'btn btn-danger',
                                                                            'data' => [
                                                                                'confirm' => 'Вы уверены, что хотите удалить эту комментарий?',
                                                                                'method' => 'post',
                                                                            ],
                                                                        ]) ?>
                                                                    </li>
                                                                <?php endif; ?>


                                                                <?php if ($itemComment->user_id != Yii::$app->user->getId()): ?>
                                                                    <li role="presentation">

                                                                        <a href="<?= Url::to(['/uzer/report-comment/',
                                                                            'user_id' => $itemComment->user_id, 'comment_id' =>
                                                                                $itemComment->id, 'type' => 'Оскорбление']) ?>"
                                                                           class="btn btn-info">
                                                                            Пожаловаться
                                                                        </a>
                                                                    </li>

                                                                <?php endif; ?>


                                                            </ul>
                                                        </div>
                                                        <!--actions-->
                                                    </div>
                                                    <div class="panel-body-comment text-post comment-font">
                                                        <?= $itemComment->text ?>
                                                    </div><!-- /panel-body -->
                                                </div><!-- /panel panel-default -->
                                            </div><!-- /col-sm-5 -->

                                        </div><!-- /row -->

                                    <?php endforeach; ?>

                                    <?php $commentForm = ActiveForm::begin(); ?>
                                    <div class="well">
                                        <h4>Что у тебя на уме?</h4>

                                        <?= $commentForm->field($comment, 'text')->input('text',
                                            ['id' => 'userComment', 'class' => 'form-control input-sm chat-input comment',
                                                'placeholder' => 'Напиши своё сообщение здесь',]) ?>

                                        <span class="input-group-btn" onclick="addComment()">
                                        <?= Html::a(' Добавить комментарий', ['view', 'post_id' => $item->id, 'user_id' => $user->id], [
                                            'class' => 'btn btn-primary btn-sm glyphicon glyphicon-comment',
                                            'data' => [
                                                'method' => 'post',
                                            ],
                                        ]) ?>


                                        <?php
                                        $commentForm = ActiveForm::end(); ?>

                                    </div>
                                    <!--                            <li>|</li>-->
                            </ul>
                        </div>
                    </div>
                </div>
            <?php endforeach; ?>
        <?php endif; ?>
    </div>
</div>