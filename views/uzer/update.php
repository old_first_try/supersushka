<?php

use app\models\Comment;
use app\models\Follow;
use app\models\Post;
use app\models\PostUpdateForm;
use app\modules\user\models\Image;
use app\modules\user\models\ProfileUpdateForm;
use app\modules\user\models\User;
use app\modules\user\models\UserQuestion;
use yii\bootstrap\Modal;
use yii\data\Pagination;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\Url;
use yii\widgets\LinkPager;

?>

<div class="user_container">

    <div class="list-group-item update-post" align="center">

        <?php

        $postForm = ActiveForm::begin();

        echo $postForm->field($modelPost, 'content')->textarea(['placeholder' => 'Что ты хочешь добавить?', 'rows' => 5,
            'style' => 'width:90%;border-radius:15px;'])->label('Изменить пост');

        echo Html::submitButton('Сохранить', [
            'class' => 'btn btn-success bun',
            'style' => 'margin-top:20px;',
            'data' => [
                'method' => 'post',
            ],
        ]);

        if (isset($view)):
            switch ($view):
                case  'view':
                    echo Html::a('Назад', Url::to('/uzer/view'), ['class' => 'btn btn-warning bun',
                        'style' => 'margin-top:20px;',]);
                    break;
                case  'news':
                    echo Html::a('Назад', Url::to('/uzer/news'), ['class' => 'btn btn-warning bun',
                        'style' => 'margin-top:20px;',]);
                    break;
                case  'followers':
                    echo Html::a('Назад', Url::to('/uzer/followers'), ['class' => 'btn btn-warning bun',
                        'style' => 'margin-top:20px;',]);
                    break;
            endswitch;
        endif;
        if (!isset($view))
            echo Html::a('Назад', Url::to('/uzer/view'),['class' => 'btn btn-warning bun',
                'style' => 'margin-top:20px;',]);
        $postForm = ActiveForm::end();

        ?>
    </div>

</div>
