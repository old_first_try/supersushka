<?php
/**
 * supersushka - results.php
 *
 * Initial version by: Tom
 * Initial created on: 05.10.2017 5:31
 */

use app\models\Mark;
use app\modules\user\models\Task;
use app\modules\user\models\User;
use app\modules\user\models\Voting;
use yii\helpers\Html;
use yii\helpers\Url;

$marks = Mark::find()->where(['user_id' => Yii::$app->user->getId()])->all();
//$iUserVoting = Voting::findOne(['user_id' => Yii::$app->user->getId()]);

?>

<div class="user_container well" style="border-radius: 10px;">

    <div class="row" style="min-height: 650px;">

        <h4 align="center" id="title" class="title"><?= Html::encode("Результаты заданий:") ?></h4>
        <?php if (Yii::$app->user->identity['role_id'] == 4): ?>
            <div class="list-group-item border-green border_radius_margin">
                <h3 align="center" class="text-post">
                    <?= Html::encode("Можете не волноваться!") ?>
                    <br>
                    <?= Html::encode("У Вас ПОЛНЫЙ иммунитет - 
            поэтому задания вас не касаются!") ?>
                </h3>
            </div>
        <?php endif; ?>


        <p align="center">
            <strong>
                **<?= Html::img('@web/images/exclamation_point.png', ['width' => 40, 'height' => 40,
                    'style' => 'margin-right:10px;']) ?> - обязательное задание!
            </strong>
            <br>
            <strong>
                **<?= Html::img('@web/images/happy.svg', ['width' => 40, 'height' => 40,
                    'style' => 'margin-right:15px;']) ?> - задание выполнено!
            </strong>
            <br>
            <strong>
                **<?= Html::img('@web/images/sad.svg', ['width' => 40, 'height' => 40,
                    'style' => 'margin-right:15px;']) ?> - задание не выполнено!
            </strong>

        </p>

        <?php if (isset($marks)): ?>
            <?php foreach ($marks
                           as $mark):
                $task = Task::findOne(['id' => $mark->task_id]); ?>
                <!--            border-radius: 15px;-->
                <div class="list-group-item task-results" style="height: 59px;">

                    <?= Html::encode($task->title) ?>
                    <?php
                    $originalDate = strtotime($task->date);
                    $Month_r = array(
                        "01" => "января",
                        "02" => "февраля",
                        "03" => "марта",
                        "04" => "апреля",
                        "05" => "мая",
                        "06" => "июня",
                        "07" => "июля",
                        "08" => "августа",
                        "09" => "сентября",
                        "10" => "октября",
                        "11" => "ноября",
                        "12" => "декабря");
                    $now_month = date('m', $originalDate); // месяц на eng
                    $rus_month = $Month_r[$now_month];

                    $newDate = date("- d ", $originalDate) . $rus_month
                        . date(" Y ", $originalDate);
                    echo Html::encode($newDate) ?>

                    <?php
                        if ($task->status) {
                            echo Html::img('@web/images/exclamation_point.png', ['width' => 40, 'height' => 40, 'float' => 'none',
                                'align' => 'right']);
                        }
                        if ($mark->status == 1): ?>
                            <?= Html::img('@web/images/happy.svg', ['width' => 40, 'height' => 40,
                                'float' => 'none', 'align' => 'right']) ?>
                        <?php endif; ?>

                    <?php if ($mark->status == 0): ?>

                        <?= Html::img('@web/images/sad.svg', ['width' => 40, 'height' => 40, 'float' => 'none',
                            'align' => 'right']) ?>
                        <?php if ($voting->changeChance && ($voting->task_id == $mark->task_id)): ?>
                            <strong style="color: red;font-size: 18px;">
                                <?= Html::encode("Можете не волноваться! Здесь Вы использовали иммунитет!") ?>
                            </strong>
                            <?= Html::img('@web/images/happy.svg', ['width' => 40, 'height' => 40, 'align' => 'right',
                                'style' => 'position:absolute']) ?>

                        <?php endif; ?>
                    <?php endif; ?>
                </div>
            <?php endforeach; ?>

        <?php endif; ?>

        <?php
        if ($marks == null): ?>
            <div class="list-group-item flash text-post">
                У вас пока что нет заданий!
            </div>
        <?php endif; ?>

        <div class="list-group-item flash text-post border_margin">
            Результаты голосования:
        </div>


        <?php //TODO: Voting button?>
        <div class="faq-create" align='center' style='position: absolute;float: inherit;'>
            <?php $patch = \app\modules\user\models\Patches::findOne(['status' => 1]);
            if (isset($patch) && $patch->voting_level == 1):
                echo Html::a('Голосование 1 тур итоги', ['/message/voting'],
                    ['class' => 'btn btn-warning',
                        'style' => 'font-size: 15px;margin-left: auto;
    margin-right: auto;'
                    ]);
            elseif (isset($patch) && $patch->voting_level == 2):
                echo Html::a('Голосование 2 тур итоги', ['/message/voting-second'],
                    ['class' => 'btn btn-danger',
                        'style' => 'font-size: 15px;margin-left: auto;
    margin-right: auto;'
                    ]);
            elseif (isset($patch) && $patch->voting_level == 3):
                echo Html::a('Голосование 3 тур итоги', ['/message/voting-third'], [
                    'class' => 'btn btn-success',
                    'style' => 'font-size: 15px;margin-left: auto;
    margin-right: auto;'
                ]);
            endif;
            ?>
        </div>
    </div>
</div>


