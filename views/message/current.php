<?php
/**
 * supersushka - current.php
 *
 * Initial version by: программирование
 * Initial created on: 12.11.2017 16:35
 */

use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;


?>

<div class="user_container well" style="border-radius: 10px;">
    <h3 align="center"
        style="font-family: 'Times New Roman',serif;" id="title" class="title text-post">
        Ваши сообщения
    </h3>

    <?php
    if (!isset($messages) || $messages == null): ?>
        <div class="list-group-item border_radius_margin">
            У вас пока что нет сообщений.
        </div>
    <?php endif; ?>

    <?php if (Yii::$app->session->getFlash('error')): ?>
        <div class="alert alert-danger alert-dismissible flash" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span
                        aria-hidden="true">&times;</span></button>
            <strong>Ошибка!</strong> Попробуйте снова!
        </div>
    <?php endif; ?>
    <div class="well-sm panel-default">

        <div class=" list-group-item panel-body border_radius">
            <?php if (isset($messages) && $messages != null): ?>
                <?php foreach ($messages

                               as $sms): ?>
                    <div class="date">
                        <?php
                        $today = date("Y-m-d H:i:s");
                        $todayDate = new DateTime($today);
                        $smsDate = new DateTime($sms->data);
                        $interval = $smsDate->diff($todayDate, true);
                        ?>
                        <?php if ($interval->format('%a') == 0): ?>
                            <span class="bold">Сегодня, </span>
                        <?php endif; ?>
                        <?php
                        if ($interval->format('%a') == 0 && $interval->format('%h') == 0 && $interval->format('%i') == 0)
                            echo $interval->format(' Только что');
                        if ($interval->format('%a') != 0 && $interval->format('%h') != 0 && $interval->format('%i') != 0)
                            echo $interval->format(' %a дней, %h часов, %i минут назад');

                        elseif ($interval->format('%a') == 0 && $interval->format('%h') == 0)
                            echo $interval->format(' %i минут назад');
                        elseif ($interval->format('%h') == 0 && $interval->format('%i') == 0)
                            echo $interval->format(' %a дней назад');
                        elseif ($interval->format('%a') == 0 && $interval->format('%i') == 0)
                            echo $interval->format(' %h часов назад');
                        elseif ($interval->format('%a') == 0)
                            echo $interval->format(' %h часов, %i минут назад');
                        elseif ($interval->format('%h') == 0)
                            echo $interval->format(' %a дней, %i минут назад');
                        elseif ($interval->format('%i') == 0)
                            echo $interval->format(' %a дней, %h часов назад');
                        else echo $sms->data;
                        ?>
                    </div>
                    <?php if ($sms->admin_id == Yii::$app->user->identity['parent_id']): ?>
                        <div class="row">

                            <div class="message-user pull-right">

                                <?= $sms->text ?>
                            </div>

                        </div>
                    <?php endif; ?>

                    <div class="pull-right">

                        <br>
                        <?php
                        if ($sms->path_first != null || $sms->path_second != null &&
                            $sms->path_third != null && $sms->path_fourth != null) {
                            echo newerton\fancybox\FancyBox::widget([
                                'target' => 'a[rel=fancybox]',
                                'helpers' => true,
                                'mouse' => true,
                                'config' => [
                                    'maxWidth' => '100%',
                                    'maxHeight' => '100%',
                                    'playSpeed' => 7000,
                                    'padding' => 0,
                                    'fitToView' => false,
                                    'width' => '70%',
                                    'height' => '70%',
                                    'autoSize' => false,
                                    'closeClick' => false,
                                    'openEffect' => 'elastic',
                                    'closeEffect' => 'elastic',
                                    'prevEffect' => 'elastic',
                                    'nextEffect' => 'elastic',
                                    'closeBtn' => false,
                                    'openOpacity' => true,
                                    'helpers' => [
                                        'title' => ['type' => 'float'],
                                        'buttons' => [],
                                        'thumbs' => ['width' => 68, 'height' => 50],
                                        'overlay' => [
                                            'css' => [
                                                'background' => 'rgba(0, 0, 0, 0.8)'
                                            ]
                                        ]
                                    ],
                                ]
                            ]);
                        }
                        if ($sms->path_first != null) {
                            echo Html::a(Html::img("@web/images/photos/{$sms->path_first}", ['width' => 150, 'height' => 100,
                                'class' => 'photos-message-user radius-img']), "@web/images/photos/{$sms->path_first}", ['rel' => 'fancybox']);
                        }
                        if ($sms->path_second != null) {
                            echo Html::a(Html::img("@web/images/photos/{$sms->path_second}", ['width' => 150, 'height' => 100,
                                'class' => 'photos-message-user radius-img']), "@web/images/photos/{$sms->path_second}", ['rel' => 'fancybox']);
                        }
                        if ($sms->path_third != null) {
                            echo Html::a(Html::img("@web/images/photos/{$sms->path_third}", ['width' => 150, 'height' => 100,
                                'class' => 'photos-message-user radius-img']), "@web/images/photos/{$sms->path_third}", ['rel' => 'fancybox']);
                        }
                        if ($sms->path_fourth != null) {
                            echo Html::a(Html::img("@web/images/photos/{$sms->path_fourth}", ['width' => 150, 'height' => 100,
                                'class' => 'photos-message-user radius-img']), "@web/images/photos/{$sms->path_fourth}", ['rel' => 'fancybox']);
                        }
                        ?>
                    </div>

                    <?php if ($sms->admin_id == 0): ?>
                        <div class="row">
                            <div class="message message-in pull-left">

                                <?= $sms->text ?>
                            </div>

                        </div>

                    <?php endif; ?>
                <?php endforeach; ?>
            <?php endif; ?>
            <div class="row"></div>
        </div>
        <div class="list-group-item border_radius_margin">
            <?php
            $messageForm = ActiveForm::begin();
            echo $messageForm->field($newMessage, 'text')->textarea(['placeholder' =>
                'Как продвигается твоё задание?', 'id' => 'user_message',]);
            ?>
            <div class="row" style="float: none">
                <?php
                $itemsTask = array();
                $tasks = \app\modules\user\models\Task::find()->all();
                foreach ($tasks as $task) {
                    $itemsTask[$task->id] = $task->title;
                }
                $paramsTask = [
                    'prompt' => 'Выберите задание...'
                ];

                echo $messageForm->field($newMessage, 'imageFiles[]')->fileInput(['multiple' => true, 'accept' => 'image/*']);

                echo $messageForm->field($newMessage, 'voting')->checkbox();

                echo $messageForm->field($newMessage, 'task_id', [
                    'options' => [
                        'id' => 'task_id'],
                ])->dropDownList($itemsTask, $paramsTask)->label('');

                echo Html::submitButton('Отправить', ['class' => 'btn btn-success fa fa-share']);
                ?>
            </div>
            <?php
            $messageForm = ActiveForm::end();
            ?>
        </div>
    </div>

</div>
<script type="text/javascript">
    var csrfToken = "<?=Yii::$app->request->getCsrfToken()?>";
</script>
