<?php
/**
 * supersushka - user-list.php
 *
 * Initial version by: Tom
 * Initial created on: 16.09.2017 22:01
 */

use app\models\Message;
use app\modules\user\models\User;
use lesha724\youtubewidget\Youtube;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use yii\widgets\LinkPager;

$this->title = $task->title;
$imgUser = \app\modules\user\models\Image::findOne(['id' => Yii::$app->user->getId()]);
$imgAdmin = \app\modules\user\models\Image::findOne(['id' => $admin->id]);
if (!isset($messageUser))
    $messageUser = new Message();
?>

<div class="user_container well post-font" style="border-radius: 10px;">

    <h3 align="center" class="text-post"><?= Html::encode($this->title) ?></h3>

    <div class="list-group-item border_radius text-post">
        <?php if (isset($task->videoDo) && $task->videoDo != null): ?>

            <div class="video" align="center">

                <?php
                try {
                    echo \cics\widgets\VideoEmbed::widget([
                        'url' => "{$task->videoDo}", 'show_errors' => true,
                        'container_id' => 'yourCustomId',
                        'container_class' => 'video-container',
                    ]);
                } catch (Exception $e) {
                } ?>
            </div>
        <?php endif; ?>

        <?= $task->content ?>

        <?php if (isset($task->videoPosle) && $task->videoPosle != null): ?>

            <div class="vid" align="center">
                <?php
                try {
                    echo \cics\widgets\VideoEmbed::widget([
                        'url' => "{$task->videoPosle}", 'show_errors' => true,
                        'container_id' => 'yourCustomId',
                        'container_class' => 'video-container',
                    ]);
                } catch (Exception $e) {
                } ?>
            </div>
        <?php endif; ?>
        <hr>
    </div>
    <?php $messages = \app\models\Message::findAll(['user_id' => Yii::$app->user->getId(), 'task_id' => $task->id]);
    ?>
    <div class="list-group-item border_radius_margin panel-default">
        <div class="panel-heading text-center">
            <div class="row current-task border_radius">
                <div class="col-md-2">
                    <div class="icon"><span class="glyphicon glyphicon-chevron-left"></span></div>
                </div>
                <div class="col-md-offset-3 col-md-2">
                    <a href="<?= Url::to(['/uzer/view', 'user_id' => $admin->id]) ?>">
                        <div id="contact">
                            <span class="glyphicon" aria-hidden="true">

                                <?= Html::img("@web/images/usr/{$imgAdmin->avatar}", ['class' => 'img-message-user', 'aria-hidden' => 'true']) ?>
                            </span>
                            <strong style="margin-left: auto;margin-right: auto;">
                                <?= $admin->name ?>
                            </strong>
                        </div>
                    </a>
                </div>
            </div>
        </div>

        <div class="list-group-item border_radius panel-body">
            <?php if (isset($messages) && $messages != null): ?>
                <?php foreach ($messages

                               as $sms): ?>
                    <div class="date">
                        <?php
                        $today = date("Y-m-d H:i:s");
                        $todayDate = new DateTime($today);
                        $smsDate = new DateTime($sms->data);
                        $interval = $smsDate->diff($todayDate, true);
                        ?>
                        <?php if ($interval->format('%a') == 0): ?>
                            <span class="bold">Сегодня, </span>
                        <?php endif; ?>
                        <?php
                        if ($interval->format('%a') == 0 && $interval->format('%h') == 0 && $interval->format('%i') == 0)
                            echo $interval->format(' Только что');
                        if ($interval->format('%a') != 0 && $interval->format('%h') != 0 && $interval->format('%i') != 0)
                            echo $interval->format(' %a дней, %h часов, %i минут назад');

                        elseif ($interval->format('%a') == 0 && $interval->format('%h') == 0)
                            echo $interval->format(' %i минут назад');
                        elseif ($interval->format('%h') == 0 && $interval->format('%i') == 0)
                            echo $interval->format(' %a дней назад');
                        elseif ($interval->format('%a') == 0 && $interval->format('%i') == 0)
                            echo $interval->format(' %h часов назад');
                        elseif ($interval->format('%a') == 0)
                            echo $interval->format(' %h часов, %i минут назад');
                        elseif ($interval->format('%h') == 0)
                            echo $interval->format(' %a дней, %i минут назад');
                        elseif ($interval->format('%i') == 0)
                            echo $interval->format(' %a дней, %h часов назад');
                        else echo $sms->data;
                        ?>
                    </div>
                    <?php if ($sms->admin_id == Yii::$app->user->identity['parent_id']): ?>
                        <div class="row">

                            <div class="message-user pull-right">

                                <?= $sms->text ?>
                            </div>

                        </div>
                    <?php endif; ?>

                    <div class="pull-right">

                        <br>
                        <?php
                        if ($sms->path_first != null || $sms->path_second != null &&
                            $sms->path_third != null && $sms->path_fourth != null) {
                            try {
                                echo newerton\fancybox\FancyBox::widget([
                                    'target' => 'a[rel=fancybox]',
                                    'helpers' => true,
                                    'mouse' => true,
                                    'config' => [
                                        'maxWidth' => '100%',
                                        'maxHeight' => '100%',
                                        'playSpeed' => 7000,
                                        'padding' => 0,
                                        'fitToView' => false,
                                        'width' => '70%',
                                        'height' => '70%',
                                        'autoSize' => false,
                                        'closeClick' => false,
                                        'openEffect' => 'elastic',
                                        'closeEffect' => 'elastic',
                                        'prevEffect' => 'elastic',
                                        'nextEffect' => 'elastic',
                                        'closeBtn' => false,
                                        'openOpacity' => true,
                                        'helpers' => [
                                            'title' => ['type' => 'float'],
                                            'buttons' => [],
                                            'thumbs' => ['width' => 68, 'height' => 50],
                                            'overlay' => [
                                                'css' => [
                                                    'background' => 'rgba(0, 0, 0, 0.8)'
                                                ]
                                            ]
                                        ],
                                    ]
                                ]);
                            } catch (Exception $e) {
                            }
                        }
                        if ($sms->path_first != null) {
                            echo Html::a(Html::img("@web/images/photos/{$sms->path_first}", ['width' => 150, 'height' => 100,
                                'class' => 'photos-message-user radius-img']), "@web/images/photos/{$sms->path_first}", ['rel' => 'fancybox']);
                        }
                        if ($sms->path_second != null) {
                            echo Html::a(Html::img("@web/images/photos/{$sms->path_second}", ['width' => 150, 'height' => 100,
                                'class' => 'photos-message-user radius-img']), "@web/images/photos/{$sms->path_second}", ['rel' => 'fancybox']);
                        }
                        if ($sms->path_third != null) {
                            echo Html::a(Html::img("@web/images/photos/{$sms->path_third}", ['width' => 150, 'height' => 100,
                                'class' => 'photos-message-user radius-img']), "@web/images/photos/{$sms->path_third}", ['rel' => 'fancybox']);
                        }
                        if ($sms->path_fourth != null) {
                            echo Html::a(Html::img("@web/images/photos/{$sms->path_fourth}", ['width' => 150, 'height' => 100,
                                'class' => 'photos-message-user radius-img']), "@web/images/photos/{$sms->path_fourth}", ['rel' => 'fancybox']);
                        }
                        ?>
                    </div>

                    <?php if ($sms->admin_id == 0): ?>
                        <div class="row">
                            <div class="message message-in pull-left">

                                <?= $sms->text ?>
                            </div>

                        </div>

                    <?php endif; ?>
                <?php endforeach; ?>
            <?php endif; ?>
            <div class="row"></div>
        </div>
        <div class="panel-footer list-group-item border_radius_margin border-green "
             style="margin-bottom: 10px;background: #FFF;color: #000444;">
            <h3 align="center"> Форма ответа:</h3>

            <?php
            $form = \yii\widgets\ActiveForm::begin();

            echo $form->field($messageUser, 'text')->textarea(['placeholder' =>
                'Как продвигается твоё задание?', 'id' => 'message_user',]); ?>
            <?php
            echo $form->field($messageUser, 'imageFiles[]')->fileInput(['multiple' => true, 'accept' => 'image/*'])
                ->label('Нажмите для загрузки фото');

            echo $form->field($messageUser, 'voting')->checkbox();
            echo Html::submitButton('Отправить', ['class' => 'btn btn-success radius'])
            ?>
            <?php $form = \yii\widgets\ActiveForm::end(); ?>

            <!--demo-->
            <!--            <form id="uploadForm" method="post">-->
            <!--                <label>Upload Image File:</label><br/>-->
            <!--                <div id="targetLayer">No Image</div>-->
            <!--                <div id="uploadFormLayer">-->
            <!--                    <input name="userImage" type="file" class="inputFile"/>-->
            <!--                    <input type="submit" id="send_message" value="Submit" class="btnSubmit"/>-->
            <!--                </div>-->
            <!--                <!--                                <button id="send_message" class="btn btn-default" type="button">-->
            <!--            </form>-->
            <!--demo-->
        </div>
    </div>

    <?php
    //TODO:Flash message
    if (Yii::$app->session->getFlash('successImmune')):
        ?>
        <div class="alert alert-success alert-dismissible flash" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span
                        aria-hidden="true">&times;</span></button>
            <strong>Сделано!</strong> Иммунитет защитил Вас! Но только один раз!)
        </div>
    <?php endif; ?>

    <?php if (Yii::$app->session->getFlash('errorImmune')):
        ?>
        <div class="alert alert-danger alert-dismissible flash" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span
                        aria-hidden="true">&times;</span></button>
            <strong>Ошибка!</strong> У Вас больше нет иммунитета! Если вы не согласны - обратитесь в обратную
            связь!)
        </div>
    <?php endif; ?>

    <?php if (Yii::$app->user->identity['role_id'] == 3 && $task->status == 1): ?>
        <div class="list-group-item border_radius_margin">
            <?php
            $chanceForm = \yii\widgets\ActiveForm::begin();

            echo $chanceForm->field($voting, 'changeChance')->checkbox();

            ?>
            <p align="center">
                <?= Html::submitButton('Отправить', [
                    'class' => 'btn green',
                    'style' => 'margin-left: auto;
    margin-right: auto;font-size: 14px;margin-top: 10px;',
                    'data' => [
                        'confirm' => 'Вы уверены, что хотите использовать иимунитет? Это можно сделать только один раз!',
                    ],
                ]); ?>
            </p>

            <?php $chanceForm = \yii\widgets\ActiveForm::end(); ?>
        </div>
    <?php endif; ?>

</div>
<script type="text/javascript">
    var task_id_user = "<?=$task->id?>";
    var tokenCsrf_user = "<?=Yii::$app->request->getCsrfToken()?>";
</script>
