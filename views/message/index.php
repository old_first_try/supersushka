<?php

use app\modules\user\models\Answer;
use yii\captcha\Captcha;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\Url;

$marks = Answer::find()->where(['user_id' => Yii::$app->user->getId()])->all();
//debug($userQuestion);
?>
<!--file for viewing all user questions cause i won little css using-->
<div class="main_admin_view_all">
    <p align="center"
       style="font-family: 'Times New Roman',serif;font-size: 24px;color: #000;width: 750px;margin-left: auto;margin-right: auto;margin-top: 25px;">
        Здесь выводятся ответы на вопросы в форме
        <a href="<?= Url::to('/user/help/feedback') ?>">
            <strong>
                обратной связи
            </strong>.
        </a>
    </p>

    <?php if (isset($marks)): ?>
        <?php foreach ($marks

                       as $answer): ?>
            <li class="list-group-item " style="margin-left: auto;
    margin-right: auto;width:550px;font-size: 17px;font-family: Cambria,serif;margin-top: 25px;color:#000">
                <strong>Имя: </strong><?= Html::encode($answer->name) ?>
                <br>
                <hr>
                <strong>Тема вопроса: </strong><?= Html::encode($answer->theme) ?>
                <hr>
                <strong>Ответ: </strong>
                <br>
                <?= Html::encode($answer->text) ?>
                <br>

                <?= Html::a('Закрыть вопрос', Url::to(['/message/delete', 'id' => $answer->id]),
                    ['style' => 'color:red']) ?>
            </li>
        <?php endforeach; ?>

    <?php endif; ?>

    <?php
    if (isset($marks)): ?>
        <li class="list-group-item " style="margin-left: auto;
    margin-right: auto;width:550px;font-size: 17px;font-family: Cambria,serif;margin-top: 25px;color:#000">
            У вас больше нет сообщений!
        </li>
    <?php endif; ?>

</div>
