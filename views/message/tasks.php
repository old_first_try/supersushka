<?php

use app\modules\user\models\Task;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\Url;
use yii\widgets\LinkPager;

?>

<script>
    var user_week = "<?=$week?>";
    var tasks_tokenCsrf = "<?=Yii::$app->request->getCsrfToken()?>";
    var tasks_admin_id = "<?=Yii::$app->user->identity['parent_id']?>";
</script>

<div class="user_container well post-font" style="border-radius: 10px;">

    <h1 align="center" id="title" class="title"><?= Html::encode('#СуперСушка17') ?></h1>
    <?php if (isset($week) && $week != null): ?>
        <?php if ($week == 1): ?>
            <ul class="nav nav-pills week">
                <h3 align="left" id="title">Задания распределяются по неделям:</h3>

                <li role="presentation" class="active list-group-item "><a
                            href="<?= Url::to(['/message/tasks/', 'week' => 1]) ?>">1</a></li>
                <li role="presentation" class=" list-group-item "><a
                            href="<?= Url::to(['/message/tasks/', 'week' => 2]) ?>">2</a></li>
                <li role="presentation" class=" list-group-item "><a
                            href="<?= Url::to(['/message/tasks/', 'week' => 3]) ?>">3</a></li>
                <li role="presentation" class="list-group-item "><a
                            href="<?= Url::to(['/message/tasks/', 'week' => 4]) ?>">4</a></li>

            </ul>
        <?php endif; ?>
        <?php if ($week == 2): ?>
            <ul class="nav nav-pills week">
                <h3 align="left"><strong>Задания распределяются по неделям:</strong></h3>

                <li role="presentation" class="list-group-item "><a
                            href="<?= Url::to(['/message/tasks/', 'week' => 1]) ?>">1</a></li>
                <li role="presentation" class="active list-group-item "><a
                            href="<?= Url::to(['/message/tasks/', 'week' => 2]) ?>">2</a></li>
                <li role="presentation" class="list-group-item "><a
                            href="<?= Url::to(['/message/tasks/', 'week' => 3]) ?>">3</a></li>
                <li role="presentation" class="list-group-item "><a
                            href="<?= Url::to(['/message/tasks/', 'week' => 4]) ?>">4</a></li>

            </ul>
        <?php endif; ?>

        <?php if ($week == 3): ?>
            <ul class="nav nav-pills week">
                <h3 align="left"><strong>Задания распределяются по неделям:</strong></h3>

                <li role="presentation" class="list-group-item "><a
                            href="<?= Url::to(['/message/tasks/', 'week' => 1]) ?>">1</a></li>
                <li role="presentation" class=" list-group-item "><a
                            href="<?= Url::to(['/message/tasks/', 'week' => 2]) ?>">2</a></li>
                <li role="presentation" class="active list-group-item "><a
                            href="<?= Url::to(['/message/tasks/', 'week' => 3]) ?>">3</a></li>
                <li role="presentation" class="list-group-item "><a
                            href="<?= Url::to(['/message/tasks/', 'week' => 4]) ?>">4</a></li>

            </ul>
        <?php endif; ?>

        <?php if ($week == 4): ?>
            <ul class="nav nav-pills week">
                <h3 align="left"><strong>Задания распределяются по неделям:</strong></h3>

                <li role="presentation" class="list-group-item "><a
                            href="<?= Url::to(['/message/tasks/', 'week' => 1]) ?>">1</a></li>
                <li role="presentation" class=" list-group-item "><a
                            href="<?= Url::to(['/message/tasks/', 'week' => 2]) ?>">2</a></li>
                <li role="presentation" class=" list-group-item "><a
                            href="<?= Url::to(['/message/tasks/', 'week' => 3]) ?>">3</a></li>
                <li role="presentation" class="active list-group-item "><a
                            href="<?= Url::to(['/message/tasks/', 'week' => 4]) ?>">4</a></li>

            </ul>
        <?php endif; ?>
    <?php endif; ?>
    <?php if (!isset($week) || $week == null): ?>
        <ul class="nav nav-pills week">
            <h3 align="left"><strong>Задания распределяются по неделям:</strong></h3>

            <li role="presentation" class="active list-group-item "><a
                        href="<?= Url::to(['/message/tasks/', 'week' => 1]) ?>">1</a></li>
            <li role="presentation" class=" list-group-item "><a
                        href="<?= Url::to(['/message/tasks/', 'week' => 2]) ?>">2</a></li>
            <li role="presentation" class="list-group-item "><a
                        href="<?= Url::to(['/message/tasks/', 'week' => 3]) ?>">3</a></li>
            <li role="presentation" class="list-group-item "><a
                        href="<?= Url::to(['/message/tasks/', 'week' => 4]) ?>">4</a></li>
        </ul>
        <?php
        $tasks = Task::find()->where(['week' => 1])->all();
    endif;
    ?>

    <p align="center">
        <strong>
            **<?= Html::img('@web/images/exclamation_point.png', ['width' => 40, 'height' => 40,
                'style' => 'margin-right:10px;']) ?> - обязательное задание!
        </strong>
        <br>
        <strong>
            **<?= Html::img('@web/images/happy.svg', ['width' => 40, 'height' => 40,
                'style' => 'margin-right:15px;']) ?> - задание выполнено!
        </strong>
        <br>
        <strong>
            **<?= Html::img('@web/images/sad.svg', ['width' => 40, 'height' => 40,
                'style' => 'margin-right:15px;']) ?> - задание не выполнено!
        </strong>

    </p>

    <?php if($tasks==null):?>
        <h4 align='center' id='title' class='title'>
            Пока что на этой неделе нет заданий.
        </h4>
    <?php endif;?>
    <div id="content" >

    </div>

</div>