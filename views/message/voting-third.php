<?php
/**
 * supersushka - voting-third.php
 *
 * Initial version by: Tom
 * Initial created on: 18.10.2017 22:35
 */

use app\modules\user\models\User;
use app\modules\user\models\Voting;
use newerton\fancybox\FancyBox;
use yii\helpers\Html;

$chance = 0;
$votings = Voting::find()->where(['>', "chance", $chance])->all();

$n = count($votings);
$temp = 0;
for ($i = 0; $i < $n;
     $i++) {
    for ($j = 1; $j < ($n - $i);
         $j++) {
        if ($votings[$j - 1]->balls < $votings[$j]->balls) {
            //swap elements
            $temp = $votings[$j - 1];
            $votings[$j - 1] = $votings[$j];
            $votings[$j] = $temp;
        }
    }
}

?>

<div class="user_container well" style="border-radius: 15px;">
    <?php $patch = \app\modules\user\models\Patches::findOne(['status' => 1]);
    if (isset($patch) && $patch->voting_level == 3) : ?>
    <?php if (Yii::$app->user->identity['role_id'] >= 5): ?>
        <h1 align="center" id="title">Голосование ещё идёт - текущие результаты 2 тура: </h1>
    <?php elseif (Yii::$app->user->identity['role_id'] < 5): ?>
        <h1 align="center" id="title">Для Вас 3 тур голосования окончен. Результаты: </h1>
    <?php endif; ?>
    <?php
    $i = 1;
    foreach ($votings

    as $voting) :
    $user = User::findOne(['id' => $voting->user_id]);
    if ($i == 1): ?>
    <div class="list-group-item border_radius_margin voting border-green">
        <?php elseif ($i == 2): ?>
        <div class="list-group-item border_radius_margin voting border-blue">
            <?php elseif ($i == 3): ?>
            <div class="list-group-item border_radius_margin voting border-red">
                <?php else: ?>


                <div class="list-group-item border_radius_margin voting">
                    <?php endif ?>
                    <span style="margin-left:auto;margin-right:auto">
                    <?= Html::encode($user->name) . " " . Html::encode($user->surname) . " : " ?>

                    <?php if ($voting->balls == 0): ?>
                        <?= Html::encode('0 баллов') ?>
                    <?php elseif ($voting->balls != 0): ?>
                        <?= Html::encode($voting->balls . ' баллов') ?>
                    <?php endif; ?>
                </span>
                    <br>
                    <?php
                    $i++;
                    try {
                        echo FancyBox::widget(['target' => 'a[rel=fancybox]',
                            'helpers' => true,
                            'mouse' => true,
                            'config' => ['maxWidth' => '100%',
                                'maxHeight' => '100%',
                                'playSpeed' => 700,
                                'padding' => 0,
                                'fitToView' => false,
                                'width' => '70%',
                                'height' => '70%',
                                'autoSize' => false,
                                'closeClick' => false,
                                'openEffect' => 'elastic',
                                'closeEffect' => 'elastic',
                                'prevEffect' => 'elastic',
                                'nextEffect' => 'elastic',
                                'closeBtn' => false,
                                'openOpacity' => true,
                                'helpers' => ['title' => ['type' => 'float'],
                                    'buttons' => [],
                                    'thumbs' => ['width' => 68, 'height' => 50],
                                    'overlay' => ['css' => ['background' => 'rgba(0, 0, 0, 0.8)']]],]]);
                    } catch (Exception $e) {
                        debug($e);
                    } ?>

                    <?php
                    //                    $defaultImage = "@web / images / usr / user_default . png";
                    if (($user->role_id == 4 && $voting->photoDo == null) || $voting->photoDo == null)
                        echo Html::a(Html::img("@web/images/usr/user_default.png", ['class' => 'voting-img-left']),
                            "@web/images/usr/user_default.png", ['rel' => 'fancybox']);
                    else
                        echo Html::a(Html::img("@web/images/photos/
                        {$voting->photoDo}", ['class' => 'voting-img-left']),
                            "@web/images/photos/{$voting->photoDo}", ['rel' => 'fancybox']);

                    if (($user->role_id == 4 && $voting->photoPosle == null) || $voting->photoPosle == null)
                        echo Html::a(Html::img("@web/images/usr/user_default.png", ['class' => 'voting-img-right']),
                            "@web/images/usruser_default.png", ['rel' => 'fancybox']);
                    else
                        echo Html::a(Html::img("@web/images/photos/{$voting->photoPosle}", ['class' => 'voting-img-right'
                        ]), "@web/images/photos/{$voting->photoPosle}", ['rel' => 'fancybox']);
                    ?>
                </div>
                <?php endforeach; ?>
                <?php endif; ?>

                <?php
                if (isset($patch) && $patch->voting_level != 3): ?>
                    <h3 align="center" id="title">О третьем туре будет объявлено заранее</h3>
                <?php endif; ?>

                <?php if (Yii::$app->user->identity['role_id'] < 5): ?>
                    <p align="center">
                        <?= Html::a('Назад', ['results'], ['class' => 'btn btn-warning radius']) ?>
                    </p>
                <?php endif; ?>
            </div>