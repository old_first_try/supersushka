<?php

/**
 * @var string $content
 * @var \yii\web\View $this
 */

use app\assets\AppAsset;
use app\modules\user\models\Image;
use app\modules\user\models\ProfileOLD;
use app\modules\user\models\Login;
use yii\helpers\Html;
use \yii\helpers\Url;
use yiister\gentelella\assets\Asset;

AppAsset::register($this);
?>
<?php $this->beginPage(); ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
    <meta charset="<?= Yii::$app->charset ?>"/>
    <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
    <meta name="viewport" content="width=device-width, initial-scale=1"/>
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    <link rel="shortcut icon" href="<?php echo Yii::$app->request->baseUrl; ?>/web/favicon.ico" type="image/x-icon">

</head>

<body>
<?php $this->beginBody(); ?>

<!-- top navigation -->
<div class="top_nav">

    <nav class="navbar navbar-default navbar-fixed-top top-sidebar" role="navigation" style="background-color: #ffdb53;
    min-height: 88px;margin-left: -22%">
        <div class="container-fluid">

            <a class="brand" href="http://s-1.sypersushka.ru">
                <span style="color: rgb(255,30,32);font-style: italic;font-family: Impact,serif;
    font-size: 44px;text-shadow: #cccccc 0 1px 0, #c9c9c9 0 2px 0, #bbbbbb 0px 3px 0px, #b9b9b9 0px 4px 0px,
                                    #aaaaaa 0px 5px 0px, rgba(0, 0, 0, 0.0980392) 0px 6px 1px,
                                     rgba(0, 0, 0, 0.0980392) 0px 0px 5px, rgba(0, 0, 0, 0.298039) 0px 1px 3px,
                                      rgba(0, 0, 0, 0.14902) 0px 3px 5px, rgba(0, 0, 0, 0.2) 0px 5px 10px,
                                       rgba(0, 0, 0, 0.2) 0px 10px 10px, rgba(0, 0, 0, 0.0980392) 0px 20px 20px;
                                           cursor: pointer;    text-align: center;">
                    #Я с т р о й н а я
                </span>
                <?= Html::img('@web/images/20x1080.png', ['width' => 30, 'height' => 73,
                    'cursor' => 'pointer',
                    'style' => 'margin-bottom:10px;margin-left: 5px;']) ?>

            </a>


            <a class="brand-season" href="http://s-1.sypersushka.ru">
                <span style=";color: rgb(255,30,32);font-style: italic;font-family: Impact,serif;
font-size: 44px;text-shadow: #cccccc 0 1px 0, #c9c9c9 0 2px 0, #bbbbbb 0px 3px 0px, #b9b9b9 0px 4px 0px,
                                #aaaaaa 0px 5px 0px, rgba(0, 0, 0, 0.0980392) 0px 6px 1px,
                                 rgba(0, 0, 0, 0.0980392) 0px 0px 5px, rgba(0, 0, 0, 0.298039) 0px 1px 3px,
                                  rgba(0, 0, 0, 0.14902) 0px 3px 5px, rgba(0, 0, 0, 0.2) 0px 5px 10px,
                                   rgba(0, 0, 0, 0.2) 0px 10px 10px, rgba(0, 0, 0, 0.0980392) 0px 20px 20px;
                                       cursor: pointer;    text-align: center;">
                    <?= Html::encode("1 СЕЗОН  ") ?>
                </span>
            </a>
        </div>
    </nav>
</div>
<!-- /top navigation -->

<!-- page content -->


<?= $content ?>
<!-- /page content -->

<?php $this->endBody(); ?>
</body>
</html>
<?php $this->endPage(); ?>
