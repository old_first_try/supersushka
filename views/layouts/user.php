<?php
/**
 * supersushka - user.php
 *
 * Initial version by: программирование
 * Initial created on: 16.11.2017 9:36
 */

use app\assets\AppAsset;
use app\components\Notification;
use app\models\Message;
use app\modules\user\models\Answer;
use app\modules\user\models\Image;
use app\modules\user\models\User;
use app\modules\user\models\UserQuestion;
use machour\yii2\notifications\widgets\NotificationsWidget;
use yii\bootstrap\Modal;
use yii\helpers\Html;
use \yii\helpers\Url;

if (!Yii::$app->user->isGuest) {
    $id = Yii::$app->user->identity->getId();
    $image = Image::findOne(['profile_id' => $id]);;
}
AppAsset::register($this);
$this->registerLinkTag(['rel' => 'icon', 'type' => 'image/svg', 'href' => Url::to(['@web/images/20x1080.png'])]);
?>
<?php $this->beginPage(); ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
    <meta charset="<?= Yii::$app->charset ?>"/>
    <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
    <meta name="viewport" content="width=device-width, initial-scale=1"/>
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>


<body class="nav-md">
<?php $this->beginBody(); ?>
<div class="container body" style="width: 100%;padding:0;">

    <a id="goTop">
        <?= Html::button('Наверх', ['class' => 'btn btn-default']) ?>
    </a>
    <!-- top navigation -->
    <nav class="navbar navbar-default" style="background:#ffdb53;position: relative; ">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
            <button type="button" data-target="#navbarCollapse" data-toggle="collapse" class="navbar-toggle">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
        </div>
        <!-- Collection of nav links, forms, and other content for toggling -->
        <div id="navbarCollapse" class="collapse navbar-collapse">
            <ul class="nav navbar-nav logo">
                <li>
                    <span style="color: rgb(255,30,32);font-style: italic;font-family: Impact,serif;
    font-size: 35px;text-shadow: #cccccc 0 1px 0, #c9c9c9 0 2px 0, #bbbbbb 0px 3px 0px, #b9b9b9 0px 4px 0px,
                                    #aaaaaa 0px 5px 0px, rgba(0, 0, 0, 0.0980392) 0px 6px 1px,
                                     rgba(0, 0, 0, 0.0980392) 0px 0px 5px, rgba(0, 0, 0, 0.298039) 0px 1px 3px,
                                      rgba(0, 0, 0, 0.14902) 0px 3px 5px, rgba(0, 0, 0, 0.2) 0px 5px 10px,
                                       rgba(0, 0, 0, 0.2) 0px 10px 10px, rgba(0, 0, 0, 0.0980392) 0px 20px 20px;
                                           cursor: pointer;    text-align: center;">
                    <a style="color:  rgb(255,30,32);" href="<?= Url::to(['/uzer/view']) ?>">
                        #Я с т р о й н а я

                    </a>
                    </span>
                </li>

                    <?= Html::img('@web/images/20x1080.png', [/*'width' => 20, 'height' => 45,*/
                'cursor' => 'pointer',
                'style' => 'margin-top:0.2%;margin-bottom:0.2%;height:70px;']) ?>

            </ul>
            <ul class="nav navbar-nav">
                <li>
                    <a href="http://s-1.sypersushka.ru" aria-expanded="false" class="logo-text">
                        1 Сезон
                    </a>
                </li>

                <li>
                    <a href="<?= Url::to('/user/faq') ?>" aria-expanded="false" class="logo-text">
                        FAQ
                        <span
                                class="badge bg-green"><?= count(\app\modules\user\models\Faq::find()->all()) ?></span>
                    </a>
                </li>

            </ul>

            <ul class="nav navbar-nav navbar-right logo-right">

                <!--Notifications-->
                <?php
                if (Yii::$app->user->identity['role_id'] < 5) {
                    $notifies = \app\models\Notification::find()
                        ->where(['admin_id'=> 0])
                        ->andWhere(['seen' => 0])
                        ->asArray()
                        ->all();
                } elseif (Yii::$app->user->identity['role_id'] > 4) {
                    $notifies = \app\models\Notification::find()
                        ->where(['>','admin_id' , 0])
                        ->andWhere(['seen' => 0])
                        ->asArray()
                        ->all();
                }
                $count = count($notifies);
                //admin
                ?>
                <li role="presentation" class="dropdown" style="margin-right: 0;">
                    <a href="javascript:;" class="dropdown-toggle info-number logo-text" data-toggle="dropdown"
                       aria-expanded="false">
                        <i class="fa fa-bell-o"></i>
                        <span class="label label-warning notifications-icon-count" id="count_not">
                            <span id="count" style="margin-right: 7px;">
                                0
                            </span>

                        </span>
                    </a>
                    <ul id="menu1" class="dropdown-menu list-unstyled msg_list border_radius" role="menu"
                        data-id="<?= $count ?>">
                        <span class="text-center border_radius_margin text-nav" id="new-notify">
                            <div id="count-notify" onclick="notifyAll()">
<!--                                count-notify-->
                            </div>
                        </span>

                        <script>
                            user_id_not = "<?=Yii::$app->user->getId()?>";
                        </script>

                        <div id="user-notify">
                            <!--                            user-notify-->
                        </div>
                    </ul>
                </li>
                <!--Notifications-->

                <li>
                    <a href="#" class="user-profile dropdown-toggle" data-toggle="dropdown"
                       aria-expanded="false">
                        <?= Yii::$app->user->identity['name'] . " " . Yii::$app->user->identity['surname'] ?>
                        <?php if (isset($image)): ?>
                            <?= Html::img("@web/images/usr/{$image->avatar}", ['width' => 30, 'height' => 30]) ?>
                        <?php endif; ?>
                        <span class=" fa fa-angle-down"></span>
                    </a>

                    <ul class="dropdown-menu dropdown-usermenu pull-right" style="margin-right: 5%;">
                        <li><a href="<?= Url::to('/user/help/profile') ?>">
                                Мои настройки
                            </a>
                        </li>
                        <li><a href="<?= Url::to('/user/auth/logout') ?>"><i
                                        class="fa fa-sign-out pull-right"></i>
                                (Выход)</a>
                        </li>
                    </ul>
                </li>

            </ul>
            <!--Notifications-->


        </div>
    </nav>
    <!-- top navigation -->


    <div class="row" style="margin-right: 0px;margin-left: 0px;">
        <?php if (Yii::$app->user->identity['role_id'] < 5): ?>
            <div class="col-sm-2 user-menu">
                <div class="sidebar-nav">
                    <div class="navbar navbar-default" role="navigation" style="background: inherit;">
                        <div class="navbar-collapse collapse sidebar-navbar-collapse">
                            <ul class="nav navbar-nav default well " id="menu" style="border-radius: 15px;padding: 5px;">
                                <?php
                                $patch = \app\modules\user\models\Patches::findOne(['status' => 1]);
                                if (isset($patch) && $patch->voting_level == 2) :?>
                                    <li class="active">
                                        <a href="<?= Url::to('/uzer-voting/second-voting') ?>">
                                            <?= Html::img('@web/images/voting.svg', ['width' => 25, 'height' => 25]) ?>
                                            Голосование 2 тур!
                                        </a>
                                    </li>
                                <?php elseif (isset($patch) && $patch->voting_level == 3) : ?>
                                    <li class="active">
                                        <a href="<?= Url::to('/uzer-voting/third-voting-inner') ?>">
                                            <?= Html::img('@web/images/voting.svg', ['width' => 25, 'height' => 25]) ?>
                                            Голосование 3 тур!
                                        </a>
                                    </li>
                                <?php elseif (isset($patch) && $patch->voting_level == 1) : ?>
                                    <li class="active">
                                        <a href="<?= Url::to('/uzer-voting/first-voting') ?>">
                                            <?= Html::img('@web/images/voting.svg', ['width' => 25, 'height' => 25]) ?>
                                            Голосование 1 тур!
                                        </a>
                                    </li>
                                <?php endif; ?>

                                <?php if (Yii::$app->controller->route == 'uzer/view'): ?>
                            <li class="active">
                            <?php else: ?>
                                <li>
                                    <?php endif; ?>
                                    <a href="<?= Url::to('/uzer/view') ?>">
                                        <?= Html::img('@web/images/user.svg', ['width' => 25, 'height' => 25]) ?>
                                        Моя Страница
                                    </a>
                                </li>
                                <?php if (Yii::$app->controller->route == 'uzer/news'): ?>
                            <li class="active">
                            <?php else: ?>
                                <li>
                                    <?php endif; ?>
                                    <a href="<?= Url::to('/uzer/news') ?>">
                                        <?= Html::img('@web/images/text-lines.svg', ['width' => 25, 'height' => 25]) ?>
                                        Новостная Лента
                                    </a>
                                </li>
                                <?php if (Yii::$app->controller->route == 'user/video/user'): ?>
                            <li class="active">
                            <?php else: ?>
                                <li>
                                    <?php endif; ?>
                                    <a href="<?= Url::to('/user/video/user') ?>">
                                        <?= Html::img('@web/images/video.svg', ['width' => 25, 'height' => 25]) ?>
                                        Видеотека
                                        <span class="badge bg-green"><?= count(\app\modules\user\models\VideoSection::find()
                                                ->all()) ?>
                                            </span>
                                    </a>
                                </li>
                                <?php if (Yii::$app->controller->route == 'message/current-task' || Yii::$app->controller->route == 'message/tasks'): ?>
                            <li class="active">
                            <?php else: ?>
                                <li>
                                    <?php endif; ?>
                                    <a href="<?= Url::to('/message/tasks') ?>">
                                        <?= Html::img('@web/images/list.svg', ['width' => 25, 'height' => 25]) ?>
                                        Мои Задания
                                    </a>
                                </li>
                                <?php if (Yii::$app->controller->route == 'message/results'): ?>
                            <li class="active">
                            <?php else: ?>
                                <li>
                                    <?php endif; ?>
                                    <a href="<?= Url::to('/message/results') ?>">
                                        <?= Html::img('@web/images/result.svg', ['width' => 25, 'height' => 25]) ?>
                                        Результаты
                                        <span class="badge bg-green"><?= count(\app\models\Mark::find()
                                                ->where(['user_id' => Yii::$app->user->getId()])
                                                ->all()) ?>
                                            </span>
                                    </a>
                                </li>
                                <?php if (Yii::$app->controller->route == 'user/help/feedback'): ?>
                                <li class="active">
                                    <?php else: ?>
                                <li>
                                    <?php endif; ?>
                                    <a href="<?= Url::to('/user/help/feedback') ?>">
                                        <?= Html::img('@web/images/feedback.svg', ['width' => 25, 'height' => 25]) ?>
                                        Обратная связь
                                    </a>
                                </li>
                            </ul>
                        </div>
                    </div><!--/.nav-collapse -->
                </div>
            </div>
        <?php elseif (Yii::$app->user->identity['role_id'] >= 5): ?>
            <div class="col-sm-2 user-menu">
                <div class="sidebar-nav">
                    <div class="navbar navbar-default" role="navigation" style="background: inherit;">
                        <div class="navbar-collapse collapse sidebar-navbar-collapse">
                            <ul class="nav navbar-nav default well " id="menu" style="border-radius: 10px;">
                                <?php if (Yii::$app->controller->route == 'uzer/view'): ?>
                            <li class="active">
                            <?php else: ?>
                                <li>
                                    <?php endif; ?>
                                    <a href="<?= Url::to('/uzer/view') ?>">
                                        <?= Html::img('@web/images/admin.svg', ['width' => 25, 'height' => 25,
                                            'class' => 'margin_li']) ?>
                                        Страница Админа
                                    </a>
                                </li>
                                <?php if (Yii::$app->controller->route == 'uzer/news'): ?>
                            <li class="active">
                            <?php else: ?>
                                <li>
                                    <?php endif; ?>
                                    <a href="<?= Url::to('/uzer/news') ?>">
                                        <?= Html::img('@web/images/text-lines.svg', ['width' => 25, 'height' => 25,
                                            'class' => 'margin_li']) ?>
                                        Новостная лента
                                    </a>
                                </li>
                                <?php if (Yii::$app->controller->route == 'user/user/'): ?>
                            <li class="active">
                            <?php else: ?>
                                <li>
                                    <?php endif; ?>
                                    <a href="<?= Url::to('/user/user/') ?>">
                                        <?= Html::img('@web/images/users.svg', ['width' => 25, 'height' => 25,
                                            'class' => 'margin_li']) ?>
                                        Все пользователи
                                        <span class="badge bg-green">
                                                <?= count(\app\modules\user\models\User::find()->all()) ?>
                                            </span>
                                    </a>
                                </li>
                                <?php if (Yii::$app->controller->route == 'user/admin/voting'): ?>
                            <li class="active">
                            <?php else: ?>
                                <li>
                                    <?php endif; ?>
                                    <a href="<?= Url::to('/user/admin/voting') ?>">
                                        <?= Html::img('@web/images/voting.svg', ['width' => 25, 'height' => 25,
                                            'class' => 'margin_li']) ?>
                                        Голосование
                                    </a>
                                </li>

                                <?php if (Yii::$app->controller->route == 'user/task'): ?>
                            <li class="active">
                            <?php else: ?>
                                <li>
                                    <?php endif; ?>
                                    <a href="<?= Url::to('/user/task/') ?>">
                                        <?= Html::img('@web/images/list.svg', ['width' => 25, 'height' => 25,
                                            'class' => 'margin_li']) ?>
                                        Все Задания
                                        <span class="badge bg-green">
                                                <?= count(\app\modules\user\models\Task::find()->all()) ?>
                                            </span>
                                    </a>
                                </li>
                                <?php if (Yii::$app->controller->route == 'user/admin/message-user'): ?>
                            <li class="active">
                            <?php else: ?>
                                <li>
                                    <?php endif; ?>
                                    <a href="<?= Url::to('/user/admin/message-user') ?>">
                                        <?= Html::img('@web/images/report.svg', ['width' => 25, 'height' => 25,
                                            'class' => 'margin_li']) ?>
                                        Отчёты участников
                                        <span class="badge bg-green">
                                               <?= count(\app\models\Message::find()->
                                                where(['>', 'admin_id', '0'])
                                                    ->andWhere(['is_readed' => 0])
                                                    ->all()) ?>
                                            </span>
                                    </a>
                                </li>
                                <?php if (Yii::$app->controller->route == 'user/video/'): ?>
                                <li class="active">
                                    <?php else: ?>
                                <li>
                                    <?php endif; ?>
                                    <a href="<?= Url::to('/user/video/') ?>">
                                        <?= Html::img('@web/images/video.svg', ['width' => 25, 'height' => 25,
                                            'class' => 'margin_li']) ?>
                                        Видеотека
                                    </a>
                                </li>
                                <!--                                    <li>-->
                                <!--                                        <a href=" Url::to(['/user/admin/user-list']) ?>">-->
                                <!--                                            Html::img('@web/images/user-list.svg', ['width' => 25, 'height' => 25,-->
                                <!--                                                'class' => 'margin_li']) -->
                                <!--                                            Список участников-->
                                <!--                                            <span class="badge bg-green">-->
                                <!--                                            count(\app\modules\user\models\User::find()->-->
                                <!--                                                where(['<', 'role_id', '5'])->all()) ?>-->
                                <!--                                            </span>-->
                                <!--                                        </a>-->
                                <!--                                    </li>-->

                                <!-- <li>
                                    <a href="http://s-1.sypersushka.ru">
                                         Html::img('@web/images/landing-page.svg', ['width' => 25, 'height' => 25,
                                            'class' => 'margin_ul']) 
                                        Лендинг
                                    </a>
                                </li> -->
                            </ul>
                        </div>
                    </div><!--/.nav-collapse -->
                </div>
            </div>
        <?php endif; ?>
        <?= $content ?>
        <footer class="footer-admin " style="border-radius: 15px;">
            <div class="container" align="center">
                © Ястройная 2017.
                <br>
                <br>
                Мы в cоциальных сетях:
                <br>
                <br>
                <a href="https://instagram.com/1supersushka"
                   aria-expanded="false">
                    <?= Html::img('@web/images/instagram.svg',
                        ['class' => 'social',
                            'onmouseover' => "this.src='../images/instagram-logo.svg ';",
                            "onmouseout" => "this.src='/../images/instagram.svg';"]); ?>
                </a>

                <a href="https://vk.com/sypersushka"
                   aria-expanded="false">
                    <?= Html::img('@web/images/vk.svg',
                        ['class' => 'social',
                            'onmouseover' => "this.src='../images/vk-logo.svg';",
                            "onmouseout" => "this.src='/../images/vk.svg';"]); ?>
                </a>
            </div>
        </footer>
    </div>


</div>
<?php $this->endBody(); ?>
</body>
</html>
<?php $this->endPage(); ?>
<script type="text/javascript">
    var user_id_notify = "<?=Yii::$app->user->getId()?>";
    var tokenCsrf_notify = "<?=Yii::$app->request->getCsrfToken()?>";
</script>

