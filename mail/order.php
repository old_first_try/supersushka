<?php
/**
 * supersushka - order.php
 *
 * Initial version by: Tom
 * Initial created on: 04.10.2017 21:05
 */

use yii\helpers\Html;

?>

<div class="task_container">

    Здравстуйте, <?= Html::encode($name) ?>! Поздравляем вас с успешной регистрацией в сервисе
    <strong>СУПЕРСУШКА</strong>.
    <br>
    <br>
    <br>
    <strong>Вы выбрали пакет :
        <?php switch ($role_id):
            case 1 :
                echo 'Пользователь - Пробный';
                break;
            case 2:
                echo 'Пользователь - Участник';
                break;
            case 3:
                echo 'Пользователь - Иммунитет';
                break;
            case 4:
                echo 'Пользователь - ПОЛНЫЙ иммунитет';
                break;
        endswitch;
        ?>
    </strong>.
    <br>
    <br>
    <br>
    <strong>Ваш логин: <?= Html::encode($login) ?></strong>
    <br>
    <br>
    <br>
    <strong>Ваш пароль: <?= Html::encode($password) ?></strong>
    <br>
    <br>
    <br>
    Вы можете войти в свой аккаунт по адресу:
    <a href="https://sypersushka.ru">
        sypersushka.ru
    </a>
</div>
