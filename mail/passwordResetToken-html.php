<?php

use yii\helpers\Html;

$resetLink = Yii::$app->urlManager->createAbsoluteUrl(['/user/auth/reset-password', 'token' => $user->password_reset_token]);
?>

<div class="password-reset">
    <p>Привет <?= Html::encode($user->name) ?>,</p>
    <p>Следуй приведенной ниже ссылке, чтобы сбросить пароль:</p>
    <p><?= Html::a(Html::encode($resetLink), $resetLink) ?></p>
    <p>Если вы не запрашивали восстановление пароля, проигнорируйте это письмо.</p>
</div>