<?php
$resetLink = Yii::$app->urlManager->createAbsoluteUrl(['/user/help/password-change', 'token' => $user->password_reset_token]);
?>

Привет <?= $user->name ?>,
Следуй приведенной ниже ссылке, чтобы сбросить пароль:

<?= $resetLink ?>
Если вы не запрашивали восстановление пароля, проигнорируйте это письмо.
