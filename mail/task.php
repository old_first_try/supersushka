<?php
/**
 * supersushka - order.php
 *
 * Initial version by: Tom
 * Initial created on: 04.10.2017 21:05
 */

use yii\helpers\Html;

?>

<div class="task_container">

    Здравстуйте, <?= Html::encode($name) ?>! Вам пришло ОБЯЗАТЕЛЬНОЕ ЗАДАНИЕ от
    <strong>СУПЕРСУШКИ</strong>.
    <br>
    <br>
    <br>
    Поспешите разобраться с ним в течение заданного времени!
    Вы можете войти в свой аккаунт по адресу:
    <a href="https://sypersushka.ru">
        sypersushka.ru
    </a>
</div>
