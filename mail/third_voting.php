<?php
/**
 * supersushka - third_voting.php
 *
 * Initial version by: Tom
 * Initial created on: 23.10.2017 7:39
 */

use yii\helpers\Html;

?>

<div class="task_container">

    Здравстуйте, <?= Html::encode($voting_name) ?>! Спасибо, что проголосовали за <?= Html::encode($user->name) ?>
    <br>
    <br>
    <br>

    Вы можете узнать о предстоящих конкурсах игры <strong>СУПЕРСУШКА</strong> в наших группах
    <br>
    <br>
    Вконтакте:
    <a href="https://vk.com/sypersushka">
        vk.com/sypersushka
    </a>

    <br>
    <br>
    Инстаграм:
    <a href="https://www.instagram.com/1supersushka/">
        instagram.com/1supersushka
    </a>

    <br>
    <br>
    <?= Html::encode(' или на сайте:') ?>
    <a href="http://s-1.sypersushka.ru">
        s-1.sypersushka.ru
    </a>
</div>
