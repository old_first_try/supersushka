<?php
/**
 * Created by PhpStorm.
 * User: ayder
 * Date: 8/19/17
 * Time: 5:36 PM
 */

namespace app\modules\user\controllers;


use app\modules\user\models\Login;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use yii\web\Controller;

/**
 * Class AppUserController
 * @package app\modules\user\controllers
 * main app controller, content global for
 * all other controller functions and const's
 * to comfort i'll call all 'registered users' as 'users'
 * and all 'unregistered users' as 'un_users'
 */
class AppUserController extends Controller
{

    protected function setMeta($title = null, $keywords = null, $description = null)
    {
        session_write_close() ;
        $this->view->title = $title;
        $this->view->registerMetaTag(['name' => 'keywords', 'content' => "$keywords"]);
        $this->view->registerMetaTag(['name' => 'description', 'content' => "$description"]);

    }

    /**
     * allow login and sign up un_users
     * and log out for users
     * @return array
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['login', 'logout', 'signup'],
                'rules' => [
                    [
                        'actions' => ['login', 'error'],
                        'allow' => true, // Разрешаем доступ к login и error для всех
                    ],
                    [
                        'allow' => true,
                        'actions' => ['login'],
                        'roles' => ['?'],
                    ],
                    [
                        'allow' => true,
                        'actions' => ['logout'],
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post', 'get'],
                ],
            ],
        ];
    }


}