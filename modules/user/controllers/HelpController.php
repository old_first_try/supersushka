<?php
/**
 * Created by PhpStorm.
 * User: ayder
 * Date: 8/19/17
 * Time: 6:25 PM
 */

namespace app\modules\user\controllers;


use app\models\ContactForm;
use app\models\Notification;
use app\models\Report;
use app\modules\user\models\CaptchaForm;
use app\modules\user\models\Image;
use app\modules\user\models\Login;
use app\modules\user\models\PasswordChangeForm;
use app\modules\user\models\ProfileOLD;
use app\modules\user\models\ProfileUpdateForm;
use app\modules\user\models\User;
use app\modules\user\models\UserQuestion;
use Yii;
use yii\helpers\Url;
use yii\web\HttpException;
use yii\web\NotFoundHttpException;
use yii\web\UploadedFile;

/**
 * Class HelpController
 * Answer on FAQ, FeedBack and UserProfile actions
 */
class HelpController extends AppUserController
{
    public $layout = 'user';

    public function actions()
    {
        return [
            // ...
            'captcha' => [
                'class' => 'app\components\MathCaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? '42' : null,
            ],
        ];
    }

    //action for sending query to admins
    public function actionFeedback()
    {
        $this->setMeta('Суперсушка');
        if (Yii::$app->user->isGuest) {
            return Yii::$app->response->redirect(['/user/user/auth_error']);
        }
        if (!Yii::$app->user->isGuest) {
            $model = new CaptchaForm();
            //change
            if ($model->load(Yii::$app->request->post()) && $model->questionToDB()) {
                //notification
                $question = UserQuestion::find()
                    ->where(['email' => $model->email])
                    ->andWhere(['theme' => $model->subject])
                    ->one();

                $notification = new Notification();
                //task id
                $notification->type = 'question';
                $notification->task_id = $model->getQuestion()->id;
                //user id
                $admin = User::findOne(['id' => Yii::$app->user->identity['parent_id']]);
                $notification->user_id = Yii::$app->user->getId();
                $notification->admin_id = $admin->id;
                $notification->seen = 0;
                //date
                date_default_timezone_set('Etc/GMT-3');
                $notification->created_at = date('Y-m-d H:i:s');
                $notification->flashed = 0;
                //user id
                $notification->admin_name = $admin->name;
                if ($notification->validate())
                    $notification->save();
                //notification
                Yii::$app->session->setFlash('contactFormSubmitted');

                return $this->refresh();
            }
            return $this->render('feedback', [
                'model' => $model,
            ]);
        }
        return new HttpException(401);
    }

    //action for sending e-mail to USERS
//    public function actionFeedback()
//    {
//        $this->setMeta('Суперсушка');
//        if (Yii::$app->user->isGuest) {
//            return Yii::$app->response->redirect(['/user/user/auth_error']);
//        }
//        if (!Yii::$app->user->isGuest) {
//            $model = new CaptchaForm();
//            //change
//            if ($model->load(Yii::$app->request->post()) && $model->contact(Yii::$app->params['adminEmail'])) {
//                Yii::$app->session->setFlash('contactFormSubmitted');
//
//                return $this->refresh();
//            }
//            return $this->render('feedback', [
//                'model' => $model,
//            ]);
//        }
//        return new HttpException(401);
//    }

    /**
     * show user profile info
     * icons for changing info
     * @return $this|string|HttpException
     */
    public function actionProfile()
    {
        $this->setMeta('Суперсушка');
        if (Yii::$app->user->isGuest) {
            return Yii::$app->response->redirect(['/user/user/auth_error']);
        }
        if (!Yii::$app->user->isGuest) {
            $user = $this->findUserModel();
            $model = new ProfileUpdateForm($user);

            if ($model->load(Yii::$app->request->post())) {
                if ($model->update()) {
                    Yii::$app->session->setFlash('successProfileUpdate');
                    return $this->refresh();
                } else {
                    //$er = 'Error!@';
                    Yii::$app->session->setFlash('errorProfileUpdate');
                    return $this->refresh();
                }
            }
            return $this->render('profile', [
                'model' => $model,
            ]);
        }
        return new HttpException(401);
    }


    /**
     * password changing for user
     */
    public function actionPasswordChange()
    {
        $this->setMeta('Суперсушка');
        if (Yii::$app->user->isGuest) {
            return Yii::$app->response->redirect(['/user/user/auth_error']);
        }
        if (!Yii::$app->user->isGuest) {
            $user = $this->findUserModel();
            $model = new PasswordChangeForm($user);

            if ($model->load(Yii::$app->request->post())) {
                if ($model->changePassword())
                    Yii::$app->session->setFlash('successPasswordUpdate');
                else
                    Yii::$app->session->setFlash('errorPasswordUpdate');
                return $this->redirect(['profile']);
            } else {
                //$er = 'Error!@';
                return $this->render('password-change', [
                    'model' => $model,
                ]);
            }
        }

        return new HttpException(401);
    }

    /**
     * @return User ProfileOLD the loaded model
     */
    private function findUserModel()
    {
        return User::findOne(Yii::$app->user->identity->getId());
    }

    /**
     * @return Image the loaded model
     */
    private function findImageModel()
    {
        return Image::findOne(['profile_id' => Yii::$app->user->getId()]);
    }

    /**
     * @return $this|string|HttpException|\yii\web\Response
     */
    public function actionAvatar()
    {
        $this->setMeta('Суперсушка');
        if (Yii::$app->user->isGuest) {
            return Yii::$app->response->redirect(['/user/user/auth_error']);
        }

        if (!Yii::$app->user->isGuest) {
            $model = $this->findImageModel();
            if (!isset($model) && $model != null) {
                $model = new Image();
//                if ($model->avatar == null)
//                    $model->avatar = "user_default.png";
                $model->profile_id = Yii::$app->user->getId();
                $model->save();
            }

            if (Yii::$app->request->isPost || Yii::$app->request->isGet) {
                $model->file = UploadedFile::getInstance($model, 'file');

                if ($model->file) {
                    if ($model->validate()) {
                        if ($model->file) {
                            if ($model->avatar != 'user_default.png' && $model->avatar != null) {
                                unlink('images/usr/' . $model->avatar);
                                $model->avatar = 'user' . Yii::$app->user->getId() . Yii::$app->getSecurity()->generateRandomString(7) . '.' . $model->file->extension;
                            }
                            $model->avatar = 'user' . Yii::$app->user->identity->getId() . Yii::$app->getSecurity()->generateRandomString(7) . '.' . $model->file->extension;
                        }
                        $model->file->saveAs('images/usr/' . $model->avatar);
                        $model->update();
                        //return $this->redirect(['profile']);
                        Yii::$app->session->setFlash('successAvatarUpdate');
                        return $this->refresh();
                    } else {
                        Yii::$app->session->setFlash('errorAvatarUpdate');
                        return $this->refresh();
                    }
                }
            }

            return $this->render('avatar', ['model' => $model]);
        }

        return new HttpException(401);
    }
}