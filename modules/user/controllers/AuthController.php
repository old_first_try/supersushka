<?php

namespace app\modules\user\controllers;

use app\models\ResetPasswordForm;
use app\modules\user\models\Image;
use app\modules\user\models\LoginForm;
use app\modules\user\models\Post;
use app\modules\user\models\RecoverForm;
use yii\base\InvalidParamException;
use yii\web\BadRequestHttpException;
use yii\web\HttpException;
use Yii;
use yii\web\Response;

/**
 * Default controller for the `user` module
 * content actions for user page, login, logout,
 * error and recover form
 */
class AuthController extends AppUserController
{
    /**
     * 'Access Denied' error for un_users
     * 'Not Found' error for registered users
     * @return string
     */
    public function actionError()
    {
        $this->layout = 'login';
        $this->setMeta('Суперсушка');
        if (Yii::$app->user->isGuest) {
            return $this->render('auth_error');
        }

        return $this->render('error');
    }

    /**
     * Login action
     * @return Response|string
     */
    public function actionLogin()
    {
        $this->layout = 'login';
        $this->setMeta('Вход');
        if (!Yii::$app->user->isGuest)
            return $this->redirect('/uzer/view');
//            return $this->goHome();
        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->redirect('/uzer/view');
        }
        return $this->render('login', [
            'model' => $model,
        ]);
    }

    public
    function actionLogout()
    {
        $this->setMeta('Суперсушка');
        if (Yii::$app->user->isGuest) {
            return $this->redirect('/user/auth/error');
        }
        if (!Yii::$app->user->isGuest) {

            $this->setMeta('Суперсушка');
            Yii::$app->user->logout();

            //return $this->goHome();

            return Yii::$app->response->redirect(['/user/auth/login']);
        }
        return new HttpException(401);
    }


    /**
     * recover access if user forget password
     * @return string
     */
    public
    function actionRecover()
    {
        $this->setMeta('Суперсушка');
        $this->layout = "login";
        $model = new RecoverForm();
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            if ($model->sendEmail()) {
                Yii::$app->session->setFlash('success', 'Проверь свою почту для дальнейших инструкций.');
                return $this->goHome();
            } else {
                Yii::$app->session->setFlash('error', 'Извини, мы не можем сбросить пароль для заявленной электронной почты.');
            }
        }

        return $this->render('recover', compact('model'));
    }

    /**
     * Resets password.
     *
     * @param string $token
     * @return mixed
     * @throws BadRequestHttpException
     */
    public function actionResetPassword($token)
    {
        $this->setMeta('Суперсушка');
        $this->layout = 'login';
        try {
            $model = new ResetPasswordForm($token);
        } catch (InvalidParamException $e) {
            throw new BadRequestHttpException($e->getMessage());
        }

        if ($model->load(Yii::$app->request->post()) && $model->validate() && $model->resetPassword()) {
            Yii::$app->session->setFlash('successPasswordUpdate', 'New password was saved.');
            return $this->goHome();
        }

        return $this->render('reset-password', compact('model'));
    }
}
