<?php
/**
 * supersushka - DeleteController.php
 *
 * Initial version by: программирование
 * Initial created on: 12.11.2017 15:51
 */

namespace app\modules\user\controllers;


use app\models\Message;
use Yii;

class DeleteController extends AppUserController
{
    public function actionMessage($id)
    {
        if (isset($id) && $id != null) {
            $message = Message::findOne(['id' => $id]);
            if (isset($message->path_first) && $message->path_first != null)
                unlink('images/photos/' . $message->path_first);
            if (isset($message->path_second) && $message->path_second != null)
                unlink('images/photos/' . $message->path_second);
            if (isset($message->path_third) && $message->path_third != null)
                unlink('images/photos/' . $message->path_third);
            if (isset($message->path_fourth) && $message->path_fourth != null)
                unlink('images/photos/' . $message->path_fourth);
            if (Message::deleteAll(['in', 'id', $id])) {
                Yii::$app->session->setFlash('success');
                $this->refresh();
            } else {
                Yii::$app->session->setFlash('error');
                $this->refresh();
            }
        }

    }

}