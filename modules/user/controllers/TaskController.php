<?php

namespace app\modules\user\controllers;

use app\models\Mark;
use app\models\Message;
use app\modules\user\models\VideoSection;
use Yii;
use app\modules\user\models\Task;
use app\modules\user\models\TaskSearch;
use yii\web\Controller;
use yii\web\HttpException;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\UploadedFile;

/**
 * TaskController implements the CRUD actions for Task model.
 */
class TaskController extends AppUserController
{
    public $layout = 'user';

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Task models.
     * @return mixed
     */
    public function actionIndex()
    {
        $this->setMeta('Суперсушка');
        if (Yii::$app->user->isGuest) {
            return $this->redirect('/user/auth/error');
        }
        if (!Yii::$app->user->isGuest) {

            if (Yii::$app->user->identity['role_id'] >= 5) {

                $searchModel = new TaskSearch();
                $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

                return $this->render('index', [
                    'searchModel' => $searchModel,
                    'dataProvider' => $dataProvider,
                ]);
            }
            if (Yii::$app->user->identity['role_id'] < 5) {
                return $this->redirect('/user/auth/logout');
            }
        }
        return new HttpException(401);
    }

    /**
     * Displays a single Task model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        $this->setMeta('Суперсушка');
        if (Yii::$app->user->isGuest) {
            return $this->redirect('/user/auth/error');
        }
        if (!Yii::$app->user->isGuest) {

            if (Yii::$app->user->identity['role_id'] >= 5) {

                return $this->render('view', [
                    'model' => $this->findModel($id),
                ]);
            }
            if (Yii::$app->user->identity['role_id'] < 5) {
                return $this->redirect('/user/auth/logout');
            }
        }
        return new HttpException(401);
    }

    /**
     * Creates a new Task model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $this->setMeta('Суперсушка');
        if (Yii::$app->user->isGuest) {
            return $this->redirect('/user/auth/logout');
        }
        if (!Yii::$app->user->isGuest) {

            if (Yii::$app->user->identity['role_id'] >= 5) {

                $model = new Task();
                if ($model->load(Yii::$app->request->post()) && $model->save()) {
                    if ($model->status)
                        $model->senMails();
                    return $this->redirect(['view', 'id' => $model->id]);
                } else {
                    return $this->render('create', [
                        'model' => $model,
                    ]);
                }
            }
            if (Yii::$app->user->identity['role_id'] < 5) {
                return $this->redirect('/user/auth/logout');
            }
        }
        return new HttpException(401);
    }

    /**
     * Updates an existing Task model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $this->setMeta('Суперсушка');
        if (Yii::$app->user->isGuest) {
            return $this->redirect('/user/auth/error');
        }
        if (!Yii::$app->user->isGuest) {
            if (Yii::$app->user->identity['role_id'] >= 5) {

                $model = $this->findModel($id);

                if ($model->load(Yii::$app->request->post()) && $model->save()) {
                    return $this->redirect(['view', 'id' => $model->id]);
                } else {
                    return $this->render('update', [
                        'model' => $model,
                    ]);
                }
            }
            if (Yii::$app->user->identity['role_id'] < 5) {
                return $this->redirect('/user/auth/logout');
            }
        }
        return new HttpException(401);
    }

    /**
     * Deletes an existing Task model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->setMeta('Суперсушка');
        if (Yii::$app->user->isGuest) {
            return $this->redirect('/user/auth/error');
        }
        if (!Yii::$app->user->isGuest) {
            if (Yii::$app->user->identity['role_id'] >= 5) {

                $this->findModel($id)->delete();
                Mark::deleteAll(['in', 'task_id', $id]);
                Message::deleteAll(['in', 'task_id', $id]);
                return $this->redirect(['index']);
            }
            if (Yii::$app->user->identity['role_id'] < 5) {
                return $this->redirect('/user/auth/logout');
            }
        }
        return new HttpException(401);
    }

    /**
     * Finds the Task model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Task the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Task::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
