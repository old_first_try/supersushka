<?php
/**
 * Created by PhpStorm.
 * User: Tom
 * Date: 22.08.2017
 * Time: 13:17
 */

namespace app\modules\user\controllers;

use app\controllers\AppController;
use app\models\Mark;
use app\models\Message;
use app\models\Notification;
use app\models\VotingIndexArrayChunk;
use app\modules\user\models\Answer;
use app\modules\user\models\CaptchaForm;
use app\modules\user\models\ChosenParticipant;
use app\modules\user\models\FinalVoting;
use app\modules\user\models\Patches;
use app\modules\user\models\Task;
use app\modules\user\models\User;
use app\modules\user\models\UserQuestion;
use app\modules\user\models\Voting;
use Yii;
use yii\data\Pagination;
use yii\db\StaleObjectException;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\HttpException;


class AdminController extends AppUserController
{
    public $layout = 'user';

    //action for display all questions and go to answer window
    public function actionQuestions()
    {
        $this->setMeta('Суперсушка');
        if (Yii::$app->user->isGuest) {
            return $this->redirect('/user/auth/error');
        }
        if (!Yii::$app->user->isGuest) {
            if (Yii::$app->user->identity['role_id'] >= 5) {
                $query = UserQuestion::find()->where(['status' => 1]);
                $pages = new Pagination(['totalCount' => $query->count(), 'pageSize' => 7]);

                $questions = $query->offset($pages->offset)
                    ->limit($pages->limit)
                    //жадная загрузка данных
                    ->with()
                    //
                    ->all();
                return $this->render('questions', compact('pages', 'questions'));
            }
            if (Yii::$app->user->identity['role_id'] < 5) {
                return $this->redirect('/user/auth/logout');
            }

        }
        return new HttpException(401);
    }

    //action for answering on user questions
    public function actionAnswer($id)
    {
        $this->setMeta('Суперсушка');

        if (Yii::$app->user->isGuest) {
            return Yii::$app->response->redirect(['/user/user/auth_error']);
        }
        if (!Yii::$app->user->isGuest) {
            if (Yii::$app->user->identity['role_id'] >= 5) {

                $userQuestion = UserQuestion::find()->where(['id' => $id])->one();
                //answering
                $answer = new Answer();
                //find user
                $_user = User::find()->where(['id' => Yii::$app->user->getId()])->one();
                //find theme
                if ($answer->load(Yii::$app->request->post())) {
                    if ($answer->answerToDB($_user, $answer, $userQuestion)) {
                        //notification
                        $notification = new Notification();
                        $notification->type = 'answer';
                        $notification->task_id = $answer->id;
                        //user id
                        $user = User::findOne(['login' => $userQuestion->email]);
                        $notification->user_id = $user->id;
                        $notification->admin_id = 0;
                        $notification->seen = 0;
                        //date
                        date_default_timezone_set('Etc/GMT-3');
                        $notification->created_at = date('Y-m-d H:i:s');
                        $notification->flashed = 0;
                        //user id
                        $notification->admin_name = $_user->name;
                        if ($notification->validate())
                            $notification->save();
                        //notification
                        Yii::$app->session->setFlash('answerFormSubmitted');
                        $userQuestion->status = 0;
                        try {
                            $userQuestion->update();
                        } catch (StaleObjectException $e) {
                        } catch (\Exception $e) {
                        } catch (\Throwable $e) {
                        }
                        return $this->redirect('/uzer/view');
                    } else {
                        Yii::$app->session->setFlash('error');
                        return $this->redirect('/uzer/view');
                    }
                }
                return $this->render('answer', compact('userQuestion', 'answer'));
            }
            if (Yii::$app->user->identity['role_id'] < 5) {
                return $this->redirect('/user/auth/logout');
            }
        }
        return new HttpException(401);
    }

    public function actionUserTasks($user_id)
    {
        $this->setMeta('Суперсушка');

        if (Yii::$app->user->isGuest) {
            return Yii::$app->response->redirect(['/user/user/auth_error']);
        }

        if (!Yii::$app->user->isGuest) {
            if (Yii::$app->user->identity['role_id'] >= 5) {
                $user = User::findOne(['id' => $user_id]);
                return $this->render("user-tasks", compact('user'));
            }

            if (Yii::$app->user->identity['role_id'] < 5) {
                return $this->redirect('/user/auth/logout');
            }
        }
        return new HttpException(401);

    }

    public function actionUserList()
    {
        $this->setMeta('Суперсушка');

        if (Yii::$app->user->isGuest) {
            return Yii::$app->response->redirect(['/user/user/auth_error']);
        }

        if (!Yii::$app->user->isGuest) {
            if (Yii::$app->user->identity['role_id'] >= 5) {
                return $this->render("user-list");
            }

            if (Yii::$app->user->identity['role_id'] < 5) {
                return $this->redirect('/user/auth/logout');
            }
        }
        return new HttpException(401);
    }


    public function actionMessage()
    {
        if (Yii::$app->request->isAjax) {
            $data = Yii::$app->request->post();
            $message = explode(":", $data['message']);
            if ($message != null || $message != '') {
                $task_id = explode(":", $data['task_id']);
                $user_id = explode(":", $data['user_id']);
                $message = $message[0];
                $task_id = $task_id [0];
                $user_id = $user_id [0];
                //my code here
                $search = $message . " " . $task_id . " " . $user_id;
                $newMessage = new Message();
                $newMessage->text = Html::encode($message);
                $newMessage->task_id = $task_id;
                $newMessage->user_id = $user_id;
                $newMessage->admin_id = Yii::$app->user->identity['parent_id'];

                \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
                if ($newMessage->validate()) {
                    $newMessage->save();
                    //notification
                    $notification = new Notification();
                    //task id
                    $notification->task_id = $task_id;
                    $notification->type = 'messageUser';
                    //user id
                    $notification->user_id = $user_id;
                    $notification->admin_id = 0;
                    $notification->seen = 0;
                    //date
                    date_default_timezone_set('Etc/GMT-3');
                    $notification->created_at = date('Y-m-d H:i:s');
                    $notification->flashed = 0;
                    //user id
                    $notification->admin_name = Yii::$app->user->identity['name'];
                    if ($notification->validate())
                        $notification->save();
                    //notification
                    return $message;
                }
            }
            if ($data['message'] == null || $data['message'] == '')
                return 'error!! empty message -' . $message;
        }
    }

    public function actionUserTask($task_id = false, $user_id = false)
    {
        $this->setMeta('Суперсушка');

        if (Yii::$app->user->isGuest) {
            return Yii::$app->response->redirect(['/user/auth/logout']);
        }

        if (!Yii::$app->user->isGuest) {
            if (Yii::$app->user->identity['role_id'] > 4) {

                $task = Task::findOne(['id' => $task_id]);
                $user = User::findOne(['id' => $user_id]);
                $message = new Message();
                $mark = new Mark();
                if ($message->load(Yii::$app->request->post())) {
                    if ($message->messageAdminToDB($message, $task_id, $user_id)) {

                        //notification
                        $notification = new Notification();
                        $notification->key = 'message';
                        //task id
                        $notification->task_id = $task->id;
                        $notification->type = 'success';
                        //user id
                        $notification->user_id = Yii::$app->user->getId();
                        $notification->seen = false;
                        //date
                        date_default_timezone_set('Etc/GMT-3');
                        $notification->created_at = date('Y-m-d H:i:s');
                        //user id
                        $notification->admin_name = Yii::$app->user->identity['name'];
                        $notification->save();
                        //notification
                        return $this->refresh();
                    }
                }


                $mark = Mark::findOne(['user_id' => $user_id, 'task_id' => $task_id]);
                if (isset($mark)) {
                    if ($mark->load(Yii::$app->request->post())) {
                        if ($mark->doMarkUpdate($mark)) {
                            Yii::$app->session->setFlash('successMark');
                            return $this->refresh();
                        } else {
                            Yii::$app->session->setFlash('errorMark');
                            return $this->refresh();
                        }
                    }
                }
                if (!isset($mark)) {
                    $mark = new Mark();
                    $mark->user_id = $user_id;
                    $mark->task_id = $task_id;
                    if ($mark->load(Yii::$app->request->post())) {
                        if ($mark->doMark($mark)) {
                            Yii::$app->session->setFlash('successMark');
                            return $this->refresh();
                        } else {
                            Yii::$app->session->setFlash('errorMark');
                            return $this->refresh();
                        }
                    }
                }
                return $this->render("user-task", compact('task', 'user', 'message', 'mark'));
            }
            if (Yii::$app->user->identity['role_id'] < 5) {
                return $this->redirect('/user/auth/logout');
            }
        }
        return new HttpException(401);
    }

    public function actionVoting($status = false)
    {
        $this->setMeta('Суперсушка');
        if (Yii::$app->user->isGuest) {
            $this->setMeta(\Titles::$AUTH_ERROR, \Titles::$PROJECT_NAME, \Titles::$PROJECT_NAME);
            return $this->redirect('/user/auth/login');
        }
        if (!Yii::$app->user->isGuest) {
            if (Yii::$app->user->identity['role_id'] >= 5) {
                $this->setMeta(\Titles::$FIRST_VOTING, \Titles::$PROJECT_NAME, \Titles::$PROJECT_NAME);
                //finding all voting records
                $votings = Voting::find()
                    ->where(['>=', 'chance', 1])
                    ->all();
                if (isset($status) && ($status != null || $status == 1)) {
                    //prepare voting records to
                    \Titles::$FIRST_VOTING;
                    //setting id each voting record to 1, 2, 3, ... up to end of grub
//                $i = 1;//deprecated - look at the UzerVotingController - first voting action
                    foreach ($votings as $voting) {
//                    $voting->id = $i;$i = $i + 1;
                        $voting->chance = 1;
                        $voting->status = 0;
                        $voting->balls = 0;
                        try {
                            $voting->update();
                        } catch (\Exception $e) {
                        } catch (\Throwable $e) {
                        }
                    }
                    //COUNT IN UZER_VOTING CONTROLLER for n = 20 for first and second voting, & =10 for third voting
                    //deleting old patches if exist
                    $oldPatches = Patches::find()->all();
                    if (isset($oldPatches) && $oldPatches != null) {
                        foreach ($oldPatches as $oldPatch) {
                            try {
                                $oldPatch->delete();
                            } catch (StaleObjectException $e) {
                            } catch (\Exception $e) {
                            } catch (\Throwable $e) {
                            }
                        }
                    }
                    //creation new patches for first voting

                    $patch = new Patches();
                    $patch->indexArrayChunk = 0;
                    $patch->voting_level = 1;
                    $patch->status = 1;
                    if ($patch->validate()) {
                        $patch->save();
//                            echo "works <br/>";//testing
                    }
                    Yii::$app->session->setFlash('successVoting');
                    return $this->render('voting', compact('votings'));
                }
                return $this->render('voting', compact('votings'));
            }
        }
        return new HttpException(401);
    }

    /**
     * @param bool $status
     * @return string|HttpException|\yii\web\Response
     */
    public
    function actionVotingSecond($status = false)
    {
        $this->setMeta('Суперсушка');
        if (Yii::$app->user->isGuest) {
            return $this->redirect('/user/auth/login');
        }
        if (!Yii::$app->user->isGuest) {
            if (Yii::$app->user->identity['role_id'] >= 5) {
                $this->setMeta(\Titles::$SECOND_VOTING, \Titles::$PROJECT_NAME, \Titles::$PROJECT_NAME);
                //finding all voting records
                $votings = Voting::find()
                    ->all();

                //prepare voting records to
                \Titles::$SECOND_VOTING;
                //array for second voting
                $secondVotings = array();
                //votings sort
                $n = count($votings);
                for ($i = 0; $i < $n;
                     $i++) {
                    for ($j = 1; $j < ($n - $i);
                         $j++) {
                        if ($votings[$j - 1]->balls < $votings[$j]->balls) {
                            //swap elements
                            $temp = $votings[$j - 1];
                            $votings[$j - 1] = $votings[$j];
                            $votings[$j] = $temp;
                        }
                    }
                }

                if ($status != null && $status == 1) {
                    //array for second voting
                    //отбор половины пользователей
                    $count = 0;
                    $countHalf = sizeof($votings) / 2;
//                    $votings = Voting::find()
//                        ->all();
                    foreach ($votings as $voting) {
                        $voting->status = 0;
                        if ($count <= $countHalf) {
                            $voting->balls /= 2;
                            echo "surprize mz!";
                            array_push($secondVotings, $voting);
                        }
                        try {
                            $voting->update();
                        } catch (StaleObjectException $e) {
                        } catch (\Exception $e) {
                        } catch (\Throwable $e) {
                        }
                        $count++;
                    }
                    //COUNT IN UZER_VOTING CONTROLLER for n = 20 for first and second voting, & =10 for third voting
                    //deleting old patches if exist
                    $oldPatches = Patches::find()->all();
                    if (isset($oldPatches) && $oldPatches != null) {
                        foreach ($oldPatches as $oldPatch) {
                            try {
                                $oldPatch->delete();
                            } catch (StaleObjectException $e) {
                            } catch (\Exception $e) {
                            } catch (\Throwable $e) {
                            }
                        }
                    }
                    //creation new patches forn first voting
                    $patch = new Patches();
                    $patch->indexArrayChunk = 0;
                    $patch->voting_level = 2;
                    $patch->status = 1;
                    if ($patch->validate()) {
                        $patch->save();
//                        echo "works <br/>";//testing
                    }
                    Yii::$app->session->setFlash('successSecondVoting');
                    return $this->render('voting-second', compact('secondVotings'));
                }
                return $this->render('voting-second', compact('votings'));
            }
        }
        return new HttpException(401);
    }

    public
    function actionVotingThird($status = false)
    {
        if (Yii::$app->user->isGuest) {
            $this->setMeta(\Titles::$AUTH_ERROR, \Titles::$PROJECT_NAME, \Titles::$PROJECT_NAME);
            return $this->redirect('/user/auth/login');
        }
        if (!Yii::$app->user->isGuest) {
            if (Yii::$app->user->identity['role_id'] >= 5) {
                $this->setMeta(\Titles::$THIRD_VOTING, \Titles::$PROJECT_NAME, \Titles::$PROJECT_NAME);
                //finding all voting records
                //trash
                $votings = Voting::find()
                    ->all();
                foreach ($votings as $voting) {
                    if ($voting->chance != 1)
                        $voting->chance = 0;
                    $voting->status = 0;
                    try {
                        $voting->update();
                    } catch (StaleObjectException $e) {
                    } catch (\Exception $e) {
                    } catch (\Throwable $e) {
                    }
                }
                //trash
                $votings = Voting::find()
                    ->where(['>', 'chance', 0])
                    ->all();

                //prepare voting records to
                \Titles::$THIRD_VOTING_INNER;
                //array for third voting
                //votings sort
                $n = count($votings);
                for ($i = 0; $i < $n;
                     $i++) {
                    for ($j = 1; $j < ($n - $i);
                         $j++) {
                        if ($votings[$j - 1]->balls < $votings[$j]->balls) {
                            //swap elements
                            $temp = $votings[$j - 1];
                            $votings[$j - 1] = $votings[$j];
                            $votings[$j] = $temp;
                        }
                    }
                }


                $thirdVotings = array();
                if ($status != null && $status == 1) {

                    //array for third voting
                    //отбор 10 пользователей
                    $count = 0;
                    foreach ($votings as $voting) {
                        if ($count == 10 || empty($votings))
                            break;
                        if ($voting->balls > 0) {
                            $voting->chance = 1;
                            $voting->balls = 0;
                            array_push($thirdVotings, $voting);
//                            echo "surprize mz!";
                            $count++;
                        }elseif ($voting->balls == 0){
                            $voting->chance = 0;
                        }
                        try {
                            $voting->update();
                        } catch (StaleObjectException $e) {
                        } catch (\Exception $e) {
                        } catch (\Throwable $e) {
                        }
                    }

//                  //COUNT IN UZER_VOTING CONTROLLER for n = 20 for first and second voting,
// & =10 for third voting
                    //deleting old patches if exist
                    $oldPatches = Patches::find()->all();
                    if (isset($oldPatches) && $oldPatches != null) {
                        foreach ($oldPatches as $oldPatch) {
                            try {
                                $oldPatch->delete();
                            } catch (StaleObjectException $e) {
                            } catch (\Exception $e) {
                            } catch (\Throwable $e) {
                            }
                        }
                    }
                    //creation new patches forn first voting
                    $patch = new Patches();
                    $patch->indexArrayChunk = 0;
                    $patch->voting_level = 3;
                    $patch->status = 1;
                    if ($patch->validate()) {
                        $patch->save();
//                        echo "works <br/>";//testing
                    }
                    Yii::$app->session->setFlash('successThirdVoting');
                    return $this->render('voting-third', compact('thirdVotings'));
                }
//                $votingShow = Voting::find()
//                    ->where(['>', 'balls', 0])
//                    ->all();
//                $n = count($votingShow);
//                for ($i = 0; $i < $n;
//                     $i++) {
//                    for ($j = 1; $j < ($n - $i);
//                         $j++) {
//                        if ($votingShow [$j - 1]->balls < $votingShow[$j]->balls) {
//                            //swap elements
//                            $temp = $votingShow[$j - 1];
//                            $votingShow[$j - 1] = $votingShow[$j];
//                            $votingShow[$j] = $temp;
//                        }
//                    }
//                }
                return $this->render('voting-third', compact('votings'));
            }
        }
        return new HttpException(401);
    }

    public
    function actionDelvoting($user_id, $level = false)
    {
        $this->setMeta('Суперсушка');
        if (Yii::$app->user->isGuest) {
            return $this->redirect('/user/auth/login');
        }
        if (!Yii::$app->user->isGuest) {
            if (Yii::$app->user->identity['role_id'] >= 5) {
                //TODO:second voting
                if (isset($level) && $level == 2) {
                    if (Voting::deleteAll(['in', 'id', $user_id])) {
                        Yii::$app->session->setFlash('successDelete');
                        $this->redirect('/user/admin/voting-second');
                    }
                } else {
                    Yii::$app->session->setFlash('errorDelete');
                    $this->redirect('/user/admin/voting-second');
                }
                //TODO:third voting
                if (isset($level) && $level == 3) {
                    if (Voting::deleteAll(['in', 'id', $user_id])) {
                        Yii::$app->session->setFlash('successDelete');
                        $this->redirect('/user/admin/voting-third');
                    }
                } else {
                    Yii::$app->session->setFlash('errorDelete');
                    $this->redirect('/user/admin/voting-third');
                }

                if (isset($user_id)) {
                    if (Voting::deleteAll(['in', 'id', $user_id])) {
                        Yii::$app->session->setFlash('successDelete');
                        $this->redirect('/user/admin/voting');
                    }
                } else {
                    Yii::$app->session->setFlash('errorDelete');
                    $this->redirect('/user/admin/voting');
                }
//       return $this->redirect('/user/admin/voting');
            }
        }
        return new HttpException(401);
    }

    public
    function actionStop($flag, $view)
    {
        if (Yii::$app->user->identity['role_id'] >= 5) {
            Patches::deleteAll();
            Yii::$app->session->setFlash('successStop');
            if ($view == 'first')
                return $this->redirect('/user/admin/voting');
            elseif ($view == 'second')
                return $this->redirect('/user/admin/voting-second');
            elseif ($view == 'third')
                return $this->redirect('/user/admin/voting-third');
        } elseif (Yii::$app->user->identity['role_id'] < 5) {
            return $this->redirect('/user/auth/logout');
        }
    }

    public
    function actionMessageUser($task_id = false, $user_id = false)
    {
        $this->setMeta('Суперсушка', 'Суперсушка', 'Суперсушка');

//        $newMessage = new Message();
//        $mark = new Mark();
//        if (Yii::$app->request->isAjax) {
//            $data = Yii::$app->request->post();
//            $message = explode(":", $data['message']);
//            if ($message != null || $message != '') {
//                $task_id = explode(":", $data['task_id']);
//                $user_id = explode(":", $data['user_id']);
//                //is_readed to message
//                $user_message_id = explode(":", $data['user_message_id']);
//                $userMessage = Message::findOne(['id' => $user_message_id]);
//                $userMessage->is_readed = 1;
//                try {
//                    $userMessage->update();
//                } catch (StaleObjectException $e) {
//                } catch (\Exception $e) {
//                }
//                //
//                $message = $message[0];
//                $task_id = $task_id [0];
//                $user_id = $user_id [0];
//                //my code here
//                $search = $message . " " . $task_id . " " . $user_id;
//                $newMessage = new Message();
//                $newMessage->text = Html::encode($message);
//                $newMessage->task_id = $task_id;
//                $newMessage->user_id = $user_id;
//                $newMessage->admin_id = 0;
//                Yii::$app->session->setFlash('success');
//
//
//                \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
//                if ($newMessage->validate()) {
//                    $newMessage->save();
//                    //notification
//                    $notification = new Notification();
//                    //task id
//                    $notification->task_id = $task_id;
//                    $notification->type = 'messageUser';
//                    //user id
//                    $notification->user_id = $user_id;
//                    $notification->admin_id = 0;
//                    $notification->seen = 0;
//                    //date
//                    date_default_timezone_set('Etc/GMT-3');
//                    $notification->created_at = date('Y-m-d H:i:s');
//                    $notification->flashed = 0;
//                    //user id
//                    $notification->admin_name = Yii::$app->user->identity['name'];
//                    if ($notification->validate())
//                        $notification->save();
//                    //notification
//                    return $this->refresh();
//
//                }
//            }
//            if ($data['message'] == null || $data['message'] == '') {
//                Yii::$app->session->setFlash('error');
//                return 'error!! empty message -' . $message;
//            }
//
//        }


        return $this->render('message', compact('tasks', 'newMessage', 'user'));
    }

    public
    function actionMessageAnswer($task_id, $user_id, $message_id = false)
    {
        if (Yii::$app->user->identity['role_id'] > 4) {

            $this->setMeta('Суперсушка', 'Суперсушка', 'Суперсушка');
            //Ajax part


            //!ajax part
            $task = Task::findOne(['id' => $task_id]);
            $user = User::findOne(['id' => $user_id]);

            $messageUser = Message::findOne(['id' => $message_id]);
            $newMessage = new Message();

            if ($newMessage->load(Yii::$app->request->post())) {
                $messageUser->is_readed = 1;
                try {
                    $messageUser->update();
                } catch (StaleObjectException $e) {
                } catch (\Exception $e) {
                } catch (\Throwable $e) {
                }
                $newMessage->task_id = $task_id;
                $newMessage->user_id = $user_id;
                $newMessage->admin_id = 0;
                Yii::$app->session->setFlash('success');

                if ($newMessage->validate()) {
                    $newMessage->save();
                    //notification
                    $notification = new Notification();
                    //task id
                    $notification->task_id = $task_id;
                    $notification->type = 'messageUser';
                    //user id
                    $notification->user_id = $user_id;
                    $notification->admin_id = 0;
                    $notification->seen = 0;
                    //date
                    date_default_timezone_set('Etc/GMT-3');
                    $notification->created_at = date('Y-m-d H:i:s');
                    $notification->flashed = 0;
                    //user id
                    $notification->admin_name = Yii::$app->user->identity['name'];
                    if ($notification->validate())
                        $notification->save();
                    //notification
                }

                Yii::$app->session->setFlash('success');
                return $this->redirect('/user/admin/message-user');
            }

            $mark = Mark::findOne(['user_id' => $user_id, 'task_id' => $task_id]);
            if (isset($mark)) {
                if ($mark->load(Yii::$app->request->post())) {
                    if ($mark->doMarkUpdate($mark)) {
                        Yii::$app->session->setFlash('success');
                        return $this->redirect('/user/admin/message-user');
                    } else {
                        Yii::$app->session->setFlash('error');
                        return $this->redirect('/user/admin/message-user');
                    }
                }
            }
            if (!isset($mark)) {
                $mark = new Mark();
                $mark->user_id = $user_id;
                $mark->task_id = $task_id;
                if ($mark->load(Yii::$app->request->post())) {
                    if ($mark->doMark($mark)) {
                        Yii::$app->session->setFlash('success');
                        return $this->redirect('/user/admin/message-user');
                    } else {
                        Yii::$app->session->setFlash('error');
                        return $this->redirect('/user/admin/message-user');
                    }
                }
            }

            return $this->render('message-answer', compact('newMessage',
                'mark', 'task', 'user', 'messageUser'));

        } elseif (Yii::$app->user->identity['role_id'] < 5) {
            return $this->redirect('/user/auth/logout');
        }
        return new HttpException(404);

    }


    /**
     * @return string
     * @throws \Exception
     * @throws \Throwable
     * @throws \yii\db\StaleObjectException
     */
    public
    function actionAjaxDeleteNotify()
    {
        if (Yii::$app->request->isAjax) {
            $id = $_POST['data'];
            $notify = Notification::findOne(['id' => $id]);
            $notify->seen = 1;
            $notify->update();

            return 'success';
//            if ($notify->delete()) {
//                return "удалено";
//            } else
//                return "ошибка передачи id: " . $id;
        }

    }

    public
    function actionAjaxDrawNotify()
    {
        if (Yii::$app->request->isAjax) {
            if (Yii::$app->user->identity['role_id'] < 5) {
                $notifies = \app\models\Notification::find()
                    ->where(['admin_id' => 0])
                    ->andWhere(['seen' => 0])
                    ->asArray()
                    ->all();
            } elseif (Yii::$app->user->identity['role_id'] > 4) {
                $notifies = \app\models\Notification::find()
                    ->where(['>', 'admin_id', 0])
                    ->andWhere(['seen' => 0])
                    ->asArray()
                    ->all();
            }
//            $notifies = Notification::find()
//                ->where(['user_id' => Yii::$app->user->getId()])
//                ->andWhere(['seen' => 0])
//                ->asArray()
//                ->all();
            if ($notifies != null)
                $response = json_encode($notifies);
            elseif ($notifies == null)
                $response = json_encode('empty');
            return $response;
        }

    }
}