<?php

namespace app\modules\user\controllers;

use app\models\Comment;
use app\models\Follow;
use app\models\Mark;
use app\models\Post;
use app\modules\user\models\Image;
use app\modules\user\models\UserUpdate;
use app\modules\user\models\Voting;
use Yii;
use app\modules\user\models\User;
use app\modules\user\models\SearchUser;
use yii\db\StaleObjectException;
use yii\web\Controller;
use yii\web\HttpException;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * UserController implements the CRUD actions for User model.
 */
class UserController extends AppUserController
{
    public $layout = 'user';

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all User models.
     * @return mixed
     */
    public function actionIndex()
    {
        $this->setMeta('Суперсушка');
        if (Yii::$app->user->isGuest) {
            return $this->redirect('/user/auth/error');
        }

        if (!Yii::$app->user->isGuest) {
            if (Yii::$app->user->identity['role_id'] >= 5) {

                $searchModel = new SearchUser();
                $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

                return $this->render('index', [
                    'searchModel' => $searchModel,
                    'dataProvider' => $dataProvider,
                ]);
            }
            if (Yii::$app->user->identity['role_id'] < 5) {
                return $this->redirect('/user/auth/logout');
            }
        }
        return new HttpException(401);
    }

    /**
     * Displays a single User model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        $this->setMeta('Суперсушка');
        if (Yii::$app->user->isGuest) {
            return $this->redirect('/user/auth/error');
        }

        if (!Yii::$app->user->isGuest) {
            if (Yii::$app->user->identity['role_id'] >= 5) {

                return $this->render('view', [
                    'model' => $this->findModel($id),
                ]);
            }
            if (Yii::$app->user->identity['role_id'] < 5) {
                return $this->redirect('/user/auth/logout');
            }
        }
        return new HttpException(401);
    }

    /**
     * Creates a new User model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $this->setMeta('Суперсушка');
        if (Yii::$app->user->isGuest) {
            return $this->redirect('/user/auth/error');
        }
        if (!Yii::$app->user->isGuest) {
            if (Yii::$app->user->identity['role_id'] >= 5) {

                $model = new User();
                $voting = new Voting();
                if ($model->load(Yii::$app->request->post()) && $model->create($model)) {
                    $this->imageRecord($model);
                    if ($this->votingRecord($model, $voting))
                        return $this->redirect(['view', 'id' => $model->id]);
                    return $this->redirect(['view', 'id' => $model->id]);
                } else {
                    return $this->render('create', [
                        'model' => $model,
                    ]);
                }
            }
            if (Yii::$app->user->identity['role_id'] < 5) {
                return $this->redirect('/user/auth/logout');
            }
        }
        return new HttpException(401);
    }

    /**
     * Updates an existing User model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $this->setMeta('Суперсушка');
        if (Yii::$app->user->isGuest) {
            return $this->redirect('/user/auth/error');
        }
        if (!Yii::$app->user->isGuest) {
            if (Yii::$app->user->identity['role_id'] >= 5) {
                try {
                    $user = $this->findModel($id);
                    $model = new UserUpdate($user);
                    $voting = $this->findVotingModel($id);
                } catch (NotFoundHttpException $e) {
                }
                if ($model->load(Yii::$app->request->post()) && $model->update()) {
                    $this->votingUpdate($model->role_id, $voting);
                    return $this->redirect(['view', 'id' => $model->id]);
                } else {
                    return $this->render('update', [
                        'model' => $model,
                    ]);
                }
            }
            if (Yii::$app->user->identity['role_id'] < 5) {
                return $this->redirect('/user/auth/logout');
            }
        }
        return new HttpException(401);
    }

    /**
     * Deletes an existing User model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->setMeta('Суперсушка');
        if (Yii::$app->user->isGuest) {
            return $this->redirect('/user/auth/error');
        }
        if (!Yii::$app->user->isGuest) {
            if (Yii::$app->user->identity['role_id'] >= 5) {

                try {
                    $this->findModel($id)->delete();
                    $this->findVotingModel($id)->delete();
                    $this->findImageModel($id)->delete();
                } catch (StaleObjectException $e) {
                } catch (NotFoundHttpException $e) {
                } catch (\Exception $e) {
                } catch (\Throwable $e) {
                }
                $this->deleteEtcData($id);

                return $this->redirect('/user/user/index');
            }
            if (Yii::$app->user->identity['role_id'] < 5) {
                return $this->redirect('/user/auth/logout');
            }
        }
        return new HttpException(401);
    }

    /**
     * Finds the User model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return User the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = User::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    /**
     * @param $id
     * @return null|static
     * @throws NotFoundHttpException
     */
    protected function findVotingModel($id)
    {
        if (($voting = Voting::findOne(['user_id' => $id])) !== null) {
            return $voting;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    /**
     * Finds the Image model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Image the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findImageModel($id)
    {
        if (($model = Image::findOne(['profile_id' => $id])) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    /**
     * @param $id
     */
    private function deleteEtcData($id)
    {
        $oldComments = Comment::findAll(['user_id' => $id]);
        foreach ($oldComments as $oldComment) {
            $oldComment->delete();
        }
        $oldFollows = Follow::findAll(['follower_id' => $id]);
        foreach ($oldFollows as $oldFollow) {
            $oldFollow->delete();
        }
        $oldMarks = Mark::findAll(['user_id' => $id]);
        foreach ($oldMarks as $oldMark) {
            $oldMark->delete();
        }
        $oldPosts = Post::findAll(['user_id' => $id]);
        foreach ($oldPosts as $oldPost) {
            $oldPost->delete();
        }
    }

    /**
     * @param User $model
     * @param Voting $voting
     * @return bool
     */
    private function votingRecord(User $model, Voting $voting)
    {
        $voting->user_id = $model->id;
        $voting->status = 0;
        switch ($model->role_id):
            case 1:
                $voting->chance = 0;
                break;
            case 2:
                $voting->chance = 1;
                break;
            case 3:
                $voting->chance = 2;
                break;
            case 4:
                $voting->chance = 3;
                break;
        endswitch;
        $voting->save();
        return true;
    }

    private function votingUpdate($role_id, Voting $voting)
    {
        switch ($role_id):
            case 1:
                $voting->chance = 0;
                break;
            case 2:
                $voting->chance = 1;
                break;
            case 3:
                $voting->chance = 2;
                break;
            case 4:
                $voting->chance = 3;
                break;
        endswitch;
        $voting->update();
        return true;
    }

    /**
     * @param $model
     */
    private function imageRecord($model)
    {
        $image = new Image();
        $image->profile_id = $model->id;
        $image->save();
    }
}
