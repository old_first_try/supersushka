<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\modules\user\models\SearchUser */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Список пользователей';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="admin_container well" style="border-radius: 15px;">

    <h3 align="center" class="text-post">
        <?= Html::encode($this->title) ?>
    </h3>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p align="center">
        <?= Html::a('Создать  пользователя', ['create'], ['class' => 'btn btn-success radius']) ?>
    </p>
    <div class="list-group-item border_radius user-index-all border-green">
        <?= GridView::widget([
            'options' => [
                'class' => 'gridView text-post',
            ],
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'columns' => [
                ['class' => 'yii\grid\SerialColumn'],

//                    'id',
                [
                    'attribute' => 'login',
                    'headerOptions' => ['style' => 'width:30%'],
                    'contentOptions' => [
                        'style' => 'background-color:#fff;'
                    ]
                ],
//                'password',
//                'auth_key',
//                    'role_id',
                [
                    'label' => 'Статус',
                    'contentOptions' => [
                        'style' => 'background-color:#fff;'
                    ],
                    'format' => 'raw',
                    'value' => function ($model) {
                        switch ($model->role_id):
                            case 1:
                                return 'Участник - Пробный';
                                break;
                            case 2:
                                return 'Участник- Участник';
                                break;
                            case 3:
                                return 'Участник - Иммунитет';
                                break;
                            case 4:
                                return 'Участник - Полный Иммунитет';
                                break;
                            case 5:
                                return 'Админ';
                                break;
                            case 6:
                                return 'Главный Админ';
                                break;
                        endswitch;

                    },
                ],
                // 'parent_id',
                [
                    'attribute' => 'name',
                    'headerOptions' => ['style' => 'width:15%'],
                    'contentOptions' => [
                        'style' => 'background-color:#fff;'
                    ]
                ],
                [
                    'attribute' => 'surname',
                    'headerOptions' => ['style' => 'width:15%;'],
                    'contentOptions' => [
                        'style' => 'background-color:#fff;'
                    ]
                ],
//                'phone',
                // 'instagram',
                // 'vk',
                // 'country',
                // 'city',
                // 'birthday',
                // 'sex',
                // 'users',

                ['class' => 'yii\grid\ActionColumn'],
            ],
        ]); ?>
    </div>

</div>
