<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\modules\user\models\User */

$this->title = 'Изменить данные участника: ';
$this->params['breadcrumbs'][] = ['label' => 'Users', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="admin_container well" style="border-radius: 15px;">

    <h3 align="center" class="text-post">
        <?= Html::encode($this->title)?>
    </h3>
    <h3 align="center" id="title">
        <?= Html::encode($model->name) ?>
    </h3>

    <div class="list-group-item border-green " style="border-radius: 15px;">
        <?php $form = ActiveForm::begin(); ?>

        <?php
        $items = [
            '1' => 'Пользователь - Пробный',

            '2' => 'Пользователь - Участник',
            '3' => 'Пользователь - Иммунитет',
            '4' => 'Пользователь - ПОЛНЫЙ иммунитет',
            '5' => 'Администратор',
            '6' => 'Главный Администратор'
        ];
        $params = [
            'prompt' => 'Выберите статус...'
        ];
        echo $form->field($model, 'role_id')->dropDownList($items, $params);
        ?>

        <?php
        //    $admins = \app\modules\user\models\User::find()->where(['role_id' => 5])->all();
        //    $fio = $admins[0]->name . " " . $admins[0]->surname;
        //    $adds = [$admins[0]->id => "{$fio }"];
        //    for ($i = 1; $i < sizeof($admins); ++$i) {
        //        $fio = $admins[$i]->name . " " . $admins[$i]->surname;
        //        $adds[$admins[$i]->id] = $fio;
        //    }

        $admins = \app\modules\user\models\User::find()->where(['role_id' => 6])->all();
        $fio = $admins[0]->name . " " . $admins[0]->surname;
        $mAdds = [$admins[0]->id => "{$fio }"];
        for ($i = 1; $i < sizeof($admins); ++$i) {
            $fio = $admins[$i]->name . " " . $admins[$i]->surname;
            $mAdds[$admins[$i]->id] = $fio;
        }
        $param = [
            'prompt' => 'Выберите админа по заданиям...'
        ];
        echo $form->field($model, 'parent_id')->dropDownList($mAdds, $param);
        ?>

        <!--    --><?php //$form->field($model, 'name')->textInput(['maxlength' => true, 'placeholder' => 'имя']) ?>

        <!--    --><?php //$form->field($model, 'surname')->textInput(['maxlength' => true, 'placeholder' => 'фамилия']) ?>

        <!--    --><?php // $form->field($model, 'phone')->textInput(['maxlength' => true, 'placeholder' => 'телефон']) ?>


        <?php
        $itemsCountry = [
            'Россия' => 'Россия',
        ];
        $paramsCountry = [
            'prompt' => 'Выберите страну...'
        ];
        //    echo $form->field($model, 'country')->dropDownList($itemsCountry, $paramsCountry);
        ?>


        <?php
        $itemsSex = [
            'Женский' => 'Женский',
            'Мужской' => 'Мужской',
        ];
        $paramsSex = [
            'prompt' => 'Выберите пол...'
        ];
        //    echo $form->field($model, 'sex')->dropDownList($itemsSex, $paramsSex);
        ?>


        <div class="form-group">
            <?= Html::submitButton('Изменить', ['class' => 'btn btn-success bun']) ?>
            <?= Html::a('Ко всем пользователям', ['index'], [
                'class' => 'btn btn-default bun', 'style' => 'margin-top:10px;'
            ]) ?>
        </div>

        <?php ActiveForm::end(); ?>
    </div>

</div>
