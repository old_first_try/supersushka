<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\modules\user\models\User */

$this->title = "Участник ";
$admin = \app\modules\user\models\User::findOne(['parent_id' => $model->parent_id]);
$fio = $admin->name . " " . $admin->surname;
$this->params['breadcrumbs'][] = ['label' => 'Users', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="admin_container well" style="border-radius: 15px;">

    <div class="user-view">
        <h3 align="center" class="text-post">
            <?= Html::encode($this->title) ?>
        </h3>
        <h3 align="center" id="title">
            <?= Html::encode($model->name . " " . $model->surname) ?>
        </h3>

        <div class="list-group-item border_radius border-green text-post user-index" style="border-radius: 15px;">

            <p align="center">
                <?= Html::a('Изменить', ['update', 'id' => $model->id], ['class' => 'btn btn-info bun']) ?>
                <?= Html::a('Все пользователи', ['index', 'id' => $model->id], ['class' => 'btn btn-default bun']) ?>
                <?= Html::a('Удалить', ['delete', 'id' => $model->id], [
                    'class' => 'btn btn-danger bun',
                    'data' => [
                        'confirm' => 'Вы уверены , что хотите удалить этого пользователя?',
                        'method' => 'post',
                    ],
                ]) ?>
            </p>
            <?= DetailView::widget([
                'model' => $model,
                'options' => [
                    'class' => 'gridView text-post',
                ],

                'attributes' => [//                    'id',
//                    'login',
//                    'password',
                    //                    'auth_key',
                    'name',
                    'surname',
                    [
                        'label' => 'E-mail',
                        'format' => 'raw',
                        'value' => function ($model) {
                            return $model->login;

                        },
                    ],
                    'phone',
                    ['label' => 'Админ по заданиям',
                        'value' => \app\modules\user\models\User::fio($model->parent_id),//                        'filter' => Lookup::items('SubjectType'),
                    ],
                    ['label' => 'Пакет участника',
                        'value' => \app\modules\user\models\User::Chance($model->id),//                        'filter' => Lookup::items('SubjectType'),
                    ],
//                    'role_id',
                    //                    "parent_id"=>$fio,
                    //
                    //                    'parent_name',

                    [
                        'label' => 'Instagram',
                        'contentOptions' => [
                            'style' => 'background-color:#fff;'
                        ],
                        'format' => 'raw',
                        'value' => function ($model) {
                            switch ($model->instagram):
                                case null:
                                    return 'Не указано';
                                    break;
                            endswitch;
                            return $model->instagram;

                        },
                    ],
//                    'instagram',
//                    'vk',
                    [
                        'label' => 'VK',
                        'contentOptions' => [
                            'style' => 'background-color:#fff;'
                        ],
                        'format' => 'raw',
                        'value' => function ($model) {
                            switch ($model->vk):
                                case null:
                                    return 'Не указано';
                                    break;
                            endswitch;
                            return $model->vk;

                        },
                    ],
//                    'country',
                    [
                        'label' => 'Страна',
                        'contentOptions' => [
                            'style' => 'background-color:#fff;'
                        ],
                        'format' => 'raw',
                        'value' => function ($model) {
                            switch ($model->country):
                                case null:
                                    return 'Не указано';
                                    break;
                            endswitch;
                            return $model->country;

                        },
                    ],
//                    'city',
                    [
                        'label' => 'Город',
                        'contentOptions' => [
                            'style' => 'background-color:#fff;'
                        ],
                        'format' => 'raw',
                        'value' => function ($model) {
                            switch ($model->city):
                                case null:
                                    return 'Не указано';
                                    break;
                            endswitch;
                            return $model->city;

                        },
                    ],
//                    'birthday',
                    [
                        'label' => 'Дата рождения',
                        'contentOptions' => [
                            'style' => 'background-color:#fff;'
                        ],
                        'format' => 'raw',
                        'value' => function ($model) {
                            switch ($model->birthday):
                                case null:
                                    return 'Не указано';
                                    break;
                            endswitch;
                            return $model->birthday;

                        },
                    ],
//                    'sex',
                    [
                        'label' => 'Пол',
                        'contentOptions' => [
                            'style' => 'background-color:#fff;'
                        ],
                        'format' => 'raw',
                        'value' => function ($model) {
                            switch ($model->sex):
                                case null:
                                    return 'Не указано';
                                    break;
                            endswitch;
                            return $model->sex;

                        },
                    ],
//                    'users',
                ],]) ?>
        </div>
    </div>
</div>
