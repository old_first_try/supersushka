<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\modules\user\models\User */

$this->title = 'Создание пользователя';
$this->params['breadcrumbs'][] = ['label' => 'Users', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="admin_container well" style="border-radius: 15px;">

    <h3 align="center" class="text-post">
        <?= Html::encode($this->title) ?>
    </h3>

    <div class="list-group-item border_radius user-index-all border-green text-post">
        <?= $this->render('_form', [
            'model' => $model,
        ]) ?>
    </div>

</div>
