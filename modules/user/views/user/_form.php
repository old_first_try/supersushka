<?php

use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\modules\user\models\User */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="user-form ">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'login')->textInput(['maxlength' => true, 'placeholder' => 'E-mail данный при покупке пакета']) ?>

    <h4 id="title"> Пароль генерируется автоматически</h4>
    <!--    --><?php //$form->field($model, 'password')->passwordInput(['maxlength' => true, 'placeholder' => 'пароль']) ?>

    <?php
    //    $form->field($model, 'auth_key')->textInput(['maxlength' => true])
    ?>

    <?= $form->field($model, 'name')->textInput(['maxlength' => true, 'placeholder' => 'Как зовут пользователя?']) ?>

    <?= $form->field($model, 'surname')->textInput(['maxlength' => true, 'placeholder' => 'Какая у него фамилия?']) ?>

    <?= $form->field($model, 'phone')->textInput(['maxlength' => true, 'placeholder' => 'Есть телефонный номер?']) ?>

    <?php
    $items = [
        '1' => 'Пользователь - Пробный',
        '2' => 'Пользователь - Участник',
        '3' => 'Пользователь - Иммунитет',
        '4' => 'Пользователь - ПОЛНЫЙ иммунитет',
        '5' => 'Администратор',
        '6' => 'Главный Администратор'
    ];
    $params = [
        'prompt' => 'Выберите статус...'
    ];
    echo $form->field($model, 'role_id')->dropDownList($items, $params);
    ?>

    <?php
    //    $admins = \app\modules\user\models\User::find()->where(['role_id' => 5])->all();
    //    $fio = $admins[0]->name . " " . $admins[0]->surname;
    //    $adds = [$admins[0]->id => "{$fio }"];
    //    for ($i = 1; $i < sizeof($admins); ++$i) {
    //        $fio = $admins[$i]->name . " " . $admins[$i]->surname;
    //        $adds[$admins[$i]->id] = $fio;
    //    }

    $admins = \app\modules\user\models\User::find()->where(['role_id' => 6])->all();
    $fio = $admins[0]->name . " " . $admins[0]->surname;
    $mAdds = [$admins[0]->id => "{$fio }"];
    for ($i = 1; $i < sizeof($admins); ++$i) {
        $fio = $admins[$i]->name . " " . $admins[$i]->surname;
        $mAdds[$admins[$i]->id] = $fio;
    }
    $param = [
        'prompt' => 'Выберите админа по заданиям...'
    ];
    echo $form->field($model, 'parent_id')->dropDownList($mAdds, $param);
    ?>

    <?php
    $itemsCountry = [
        'Россия' => 'Россия',
    ];
    $paramsCountry = [
        'prompt' => 'Выберите страну...'
    ];
    echo $form->field($model, 'country')->dropDownList($itemsCountry, $paramsCountry);
    ?>

    <?php //$form->field($model, 'country')->textInput(['maxlength' => true, 'placeholder' => 'страна']) ?>


    <?php
    $itemsSex = [
        'Женский' => 'Женский',
        'Мужской' => 'Мужской',
    ];
    $paramsSex = [
        'prompt' => 'Выберите пол...'
    ];
    echo $form->field($model, 'sex')->dropDownList($itemsSex, $paramsSex);
    ?>


    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Создать' : 'Изменить', ['class' => $model->isNewRecord ? 'btn btn-success bun' : 'btn btn-info bun']) ?>
        <?= Html::a('Ко всем пользователям', ['index'], [
            'class' => 'btn btn-default bun', 'style' => 'margin-top:10px;'
        ]) ?>
    </div>

    <?php ActiveForm::end(); ?>
</div>
