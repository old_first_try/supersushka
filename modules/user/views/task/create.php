    <?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\modules\user\models\Task */

$this->title = 'Новое задание';
$this->params['breadcrumbs'][] = ['label' => 'Tasks', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="task_container well" style="border-radius: 15px;">

    <h1 align="center" class="text-post">
        <?= Html::encode($this->title) ?>
    </h1>

    <div class="list-group-item border-green border_radius user-index-all" style=" margin-top:10px;">
        <?= $this->render('_form', [
            'model' => $model,
        ]) ?>

        <?= Html::a('Назад', ['index'], ['class' => 'btn btn-warning radius',
            'style' => 'margin-left:15px;',]) ?>
    </div>

</div>
