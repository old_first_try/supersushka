<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\modules\user\models\Task */

$this->title = 'Изменить Задание: ';
$this->params['breadcrumbs'][] = ['label' => 'Tasks', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->title, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Обновить';
?>
<div class="task_container well" style="border-radius: 15px;">

    <div class="task-update list-group-item border-green border_radius_margin post-font">

        <h3 align="center" class="text-post">
            <?= Html::encode($this->title) ?>
        </h3>
        <h3 align="center" id="title">
            <?= Html::encode($model->title) ?>
        </h3>

        <?= $this->render('_form', [
            'model' => $model,
        ]) ?>

        <?= Html::a('Назад', ['index'], ['class' => 'btn btn-warning radius',
            'style' => 'margin-left:15px;',]) ?>

    </div>

</div>
