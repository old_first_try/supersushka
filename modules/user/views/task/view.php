<?php

use lesha724\youtubewidget\Youtube;
use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\modules\user\models\Task */

$this->title = "Задание: ";
$this->params['breadcrumbs'][] = ['label' => 'Tasks', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="task_container well" style="border-radius: 15px;">

    <div class="task-view">


        <h3 align="center" class="text-post">
            <?= Html::encode($this->title) ?>
        </h3>
        <h3 align="center" id="title">
            <?= Html::encode($model->title) ?>
        </h3>

        <p>
            <?= Html::a('Изменить', ['update', 'id' => $model->id], ['class' => 'btn btn-info radius']) ?>
            <?= Html::a('Просмотреть все задания', ['index', 'id' => $model->id], ['class' => 'btn btn-default radius']) ?>
            <?= Html::a('Удалить', ['delete', 'id' => $model->id], [
                'class' => 'btn btn-danger radius',
                'data' => [
                    'confirm' => 'Вы уверены, что хотите удалить это задание?',
                    'method' => 'post',
                ],
            ]) ?>
        </p>
        <div class="list-group-item border-green border_radius_margin post-font text-post">

            <?php
            try {
                echo DetailView::widget([
                    'model' => $model,
                    'options' => [
                        'class' => 'gridView',
                    ],
                    'attributes' => [
                        //                    'id',
                        [
                            'label' => 'Статус задания',
                            'format' => 'raw',
                            'contentOptions' => [
                                'style' => 'background-color:#fff;'
                            ],
                            'value' => function ($model) {
                                if ($model->status == 0) {
                                    return "Не обязательное";
                                }
                                if ($model->status == 1) {
                                    $string = "<p style='color: red;'>Обязательное</p>";
                                    return $string;

                                }
                            },
                        ],
                        [
                            'attribute' => 'title',
                            'contentOptions' => [
                                'style' => 'background-color:#fff;'
                            ]
                        ],
                        [
                            'label' => 'Дата',
                            'headerOptions' => ['style' => 'width:18%'],
                            'format' => 'raw',
                            'contentOptions' => [
                                'style' => 'background-color:#fff;'
                            ],
                            'value' => function ($model) {
                                setlocale(LC_ALL, 'ru_RU.UTF-8');
                                $Month_r = array(
                                    "01" => "января",
                                    "02" => "февраля",
                                    "03" => "марта",
                                    "04" => "апреля",
                                    "05" => "мая",
                                    "06" => "июня",
                                    "07" => "июля",
                                    "08" => "августа",
                                    "09" => "сентября",
                                    "10" => "октября",
                                    "11" => "ноября",
                                    "12" => "декабря");
                                $date = strtotime($model->date);
                                $begin_month = date('m', strtotime($model->date)); // месяц на eng
                                $rus_month_begin = $Month_r[$begin_month];

                                $url = $model->week . " неделя - " . date('d ', $date) . $rus_month_begin .
                                    date(' Y', $date);

                                return $url;
                            },
                        ],
                        //                    'date',
                        [
                            'label' => 'Содержание задания',
                            'format' => 'raw',
                            'headerOptions' => ['style' => 'width:18%'],
                            'contentOptions' => [
                                'style' => 'background-color:#fff;'
                            ],
                            'value' => function ($model) {
                                return $model->content;

                            },
                        ],
                        [
                            'label' => 'Начальный ролик',
                            'format' => 'raw',
                            'contentOptions' => [
                                'style' => 'background-color:#fff;'
                            ],
                            'value' => function ($model) {
                                if ($model->videoDo == null)
                                    return Html::a(Html::img("@web/images/youtube-grey.png", ['alt' => 'yii']));
                                if ($model->videoDo != null) {
                                    $video = \cics\widgets\VideoEmbed::widget([
                                        'url' => "{$model->videoDo}", 'show_errors' => true,
                                        'container_id' => 'yourCustomId',
                                        'container_class' => 'video-container',
                                    ]);
                                    $url =
                                        " <div class=\"video\" align=\"center\">
                                    $video
                                 </div>";
                                    return $url;
                                }
                            }
                        ],
                        [
                            'label' => 'Конечный ролик',
                            'format' => 'raw',
                            'contentOptions' => [
                                'style' => 'background-color:#fff;'
                            ],
                            'value' => function ($model) {
                                if ($model->videoPosle == null)
                                    return Html::a(Html::img("@web/images/youtube-grey.png", ['alt' => 'yii']));
                                if ($model->videoPosle != null)
                                    $video = \cics\widgets\VideoEmbed::widget([
                                        'url' => "{$model->videoPosle}", 'show_errors' => true,
                                        'container_id' => 'yourCustomId',
                                        'container_class' => 'video-container',
                                    ]);
                                $url =
                                    " <div class=\"video\" align=\"center\">
                                    $video
                                 </div>";
                                return $url;
                            }
                        ],
                        //                    'week',
                    ],
                ]);
            } catch (Exception $e) {
            } ?>

        </div>

    </div>

</div>
