<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\modules\user\models\TaskSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Список заданий первого сезона';
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="task_container well" style="border-radius: 15px;">

    <h3 align="center" class="text-post">
        <?= Html::encode($this->title) ?>
    </h3>
    <p align="center">
        <?= Html::a('Создать задание', ['create'], ['class' => 'btn btn-success radius']) ?>
    </p>
    <?php //echo $this->render('_search', ['model' => $searchModel]); ?>

    <div class="list-group-item border-green border_radius user-index-all" style=" margin-top:10px;">
        <?php
        try {
            echo GridView::widget([
                'options' => [
                    'class' => 'gridView text-post',
                ],
                'dataProvider' => $dataProvider,
                'filterModel' => $searchModel,
                'columns' => [
                    ['class' => 'yii\grid\SerialColumn'],

                    //                'id',
                    [
                        'attribute' => 'title',
                        'contentOptions' => [
                            'style' => 'background-color:#fff;'
                        ]
                    ],
                    //                'title',
                    //                'date',
                    //                [
                    //                    'label' => 'Дата',
                    //                    'format' => 'raw',
                    //                    'value' => function ($model) {
                    //                        $originalDate = $model->date;
                    //
                    //                        $newDate = date("d-m-Y", strtotime($originalDate));
                    //                        return $newDate;
                    //                    },
                    //                ],
                    [
                        'label' => 'Дата',
                        'headerOptions' => ['style' => 'width:18%'],
                        'format' => 'raw',
                        'contentOptions' => [
                            'style' => 'background-color:#fff;'
                        ],
                        'value' => function ($model) {
                            setlocale(LC_ALL, 'ru_RU.UTF-8');
                            $Month_r = array(
                                "01" => "января",
                                "02" => "февраля",
                                "03" => "марта",
                                "04" => "апреля",
                                "05" => "мая",
                                "06" => "июня",
                                "07" => "июля",
                                "08" => "августа",
                                "09" => "сентября",
                                "10" => "октября",
                                "11" => "ноября",
                                "12" => "декабря");
                            $date = strtotime($model->date);
                            $begin_month = date('m', strtotime($model->date)); // месяц на eng
                            $rus_month_begin = $Month_r[$begin_month];
                            $weekDate = strtotime("+7 day", $date);
                            $end_month = date('m', $weekDate); // месяц на eng
                            $rus_month_end = $Month_r[$end_month];

                            $url = $model->week . " неделя " . date('d ', strtotime($model->date)) . $rus_month_begin
                                . " - " . date(' d ', $weekDate) . $rus_month_end . date(' Y', $weekDate);

                            return $url;
                        },
                    ],
                    //                'content:html',
                    //                'videoDo',
                    [
                        'label' => 'Начальный ролик ',
                        'format' => 'raw',
                        'contentOptions' => [
                            'style' => 'background-color:#fff;'
                        ],
                        'value' => function ($model) {
                            if ($model->videoDo == null)
                                return Html::a(Html::img("@web/images/youtube-grey.png", ['alt' => 'yii']));
                            if ($model->videoDo != null) {
                                $img = Html::img("@web/images/youtube.png", ['alt' => 'yii']);
                                $url =
                                    "<a href='{$model->videoDo}'>
                                    $img
                                </a>";
                                return $url;
                            }
                        }
                    ],
                    [
                        'label' => 'Конечный ролик',
                        'format' => 'raw',
                        'contentOptions' => [
                            'style' => 'background-color:#fff;'
                        ],
                        'value' => function ($model) {
                            if ($model->videoPosle == null)
                                return Html::a(Html::img("@web/images/youtube-grey.png", ['alt' => 'yii']));
                            if ($model->videoPosle != null)
                                $img = Html::img("@web/images/youtube.png", ['alt' => 'yii']);
                            $url =
                                "<a href='{$model->videoPosle}'>
                                    $img
                                </a>";
                            return $url;
                        }
                    ],
                    //                'status',
                    [
                        'label' => 'Статус задания',
                        'headerOptions' => ['style' => 'width:16%'],
                        'contentOptions' => [
                            'style' => 'background-color:#fff;'
                        ],
                        'format' => 'raw',
                        'value' => function ($model) {
                            if ($model->status == 0)
                                return "Необязательное";
                            if ($model->status == 1)
                                return "Обязательное";

                        },
                    ],
                    //                'week',

                    ['class' => 'yii\grid\ActionColumn'],
                ],
            ]);
        } catch (Exception $e) {
        } ?>
    </div>

</div>
