<?php

use mihaildev\ckeditor\CKEditor;
use mihaildev\elfinder\ElFinder;
use mihaildev\elfinder\InputFile;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

mihaildev\elfinder\Assets::noConflict($this);

/* @var $this yii\web\View */
/* @var $model app\modules\user\models\Task */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="task_form">
    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'title')->textInput(['maxlength' => true,
        'placeholder' => 'Название нового задания', 'autofocus' => 'autofocus',]) ?>

    <?php
    $itemsWeek = [
        '1' => 1,
        '2' => 2,
        '3' => 3,
        '4' => 4,
    ];
    $paramsWeek = [
        'prompt' => 'Выберите неделю...'
    ];
    echo $form->field($model, 'week')->dropDownList($itemsWeek, $paramsWeek);
    ?>


    <?= $form->field($model, 'date')->input('date')->label('Дата публикации задания - по умолчанию берётся текущая') ?>

    <?= $form->field($model, 'videoDo')->textInput(['placeholder' => 'Youtube видео в начале задания']); ?>

    <?= $form->field($model, 'content', ['inputOptions' =>
        ['height' => '500px']])->widget(CKEditor::className(), [
        'editorOptions' => ElFinder::ckeditorOptions('elfinder',
            [/* Some CKEditor Options */]),
    ]); ?>

    <?= $form->field($model, 'videoPosle')->textInput(['placeholder' => 'Youtube видео в конце задания'])->label('Вставьте ссылку на видео, если хотите'); ?>

    <p style="color: #000;">
        <?= $form->field($model, 'status')->checkbox(['0', '1']) ?>
    </p>

    <div class="form-group" style="margin-left:10px;">
        <?= Html::submitButton($model->isNewRecord ? 'Создать' : 'Изменить', ['class' => $model->isNewRecord ? 'btn btn-success radius' : 'btn btn-success radius']) ?>


    </div>

    <?php ActiveForm::end(); ?>

</div>

