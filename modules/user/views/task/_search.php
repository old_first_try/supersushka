<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\modules\user\models\TaskSearch */
/* @var $form yii\widgets\ActiveForm */
?>


<?php $form = ActiveForm::begin([
    'action' => ['index'],
    'method' => 'get',
]); ?>

<?php
//    $form->field($model, 'id');
//    echo $form->field($model, 'videoDo');
//    echo $form->field($model, 'videoPosle');
//    echo $form->field($model, 'content');
//    echo $form->field($model, 'date');
?>

<?= $form->field($model, 'title', ['inputOptions' =>
    ['autofocus' => 'autofocus', 'class' => 'form-control', 'tabindex' => '1']]) ?>

<?= $form->field($model, 'week') ?>

<?php echo $form->field($model, 'status') ?>

<div class="form-group">
    <?= Html::submitButton('Поиск', ['class' => 'btn btn-primary']) ?>
    <?= Html::resetButton('Отмена', ['class' => 'btn btn-default']) ?>
</div>

<?php ActiveForm::end(); ?>


