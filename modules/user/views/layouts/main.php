<?php

/**
 * @var string $content
 * @var \yii\web\View $this
 */

use app\assets\AppAsset;
use app\modules\user\models\Answer;
use app\modules\user\models\Image;
use app\modules\user\models\UserQuestion;
use yii\bootstrap\Modal;
use yii\helpers\Html;
use \yii\helpers\Url;
use yiister\gentelella\assets\Asset;

if (!Yii::$app->user->isGuest) {
    $id = Yii::$app->user->identity->getId();
    $image = Image::findOne(['profile_id' => $id]);
}
AppAsset::register($this);
$this->registerLinkTag(['rel' => 'icon', 'type' => 'image/png', 'href' => Url::to(['@web/images/cup.svg'])]);
?>
<?php $this->beginPage(); ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>

    <meta charset="<?= Yii::$app->charset ?>"/>
    <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
    <meta name="viewport" content="width=device-width, initial-scale=1"/>
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    <link rel="shortcut icon" href="<?php echo Yii::$app->request->baseUrl; ?>/web/favicon.ico" type="image/x-icon">

</head>

<body class="nav-md">
<?php $this->beginBody(); ?>
<div class="container body" style="width: 100%;">

    <!-- top navigation -->
    <nav class="navbar navbar-default" style="background:#ffbb11;position: relative; ">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
            <button type="button" data-target="#navbarCollapse" data-toggle="collapse" class="navbar-toggle">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a href="#" class="navbar-brand">
                <?= Html::img('@web/images/cup.svg', ['width' => 40, 'height' => 40]) ?>
            </a>
        </div>
        <!-- Collection of nav links, forms, and other content for toggling -->
        <div id="navbarCollapse" class="collapse navbar-collapse" style="    color: #111111;">
            <ul class="nav navbar-nav">
                <li>
                    <a href="<?= Url::to('/uzer/view') ?>">
                                                <span class="glyphicon glyphicon-user" aria-hidden="true" style="font-size: 16px;
                                                 color: rgba(28,28,28,0.81);"></span>
                        Моя Страница
                    </a>
                </li>
                <li>
                    <a href="<?= Url::to('/uzer/news') ?>">
                                                 <span class="glyphicon glyphicon-th-list" aria-hidden="true" style="font-size: 16px;
                                                 color: rgba(28,28,28,0.81);"></span>
                        Новостная Лента
                    </a>

                </li>

                <?php if (Yii::$app->user->identity['role_id'] < 5): ?>
                    <li class="dropdown">
                        <a data-toggle="dropdown" class="dropdown-toggle" href="#">Мои Задания <b class="caret"></b></a>
                        <ul class="dropdown-menu">
                            <li>
                                <a href="<?= Url::to(['/message/current', 'id' => Yii::$app->user->getId()]) ?>">
                                    <span class="glyphicon glyphicon-envelope" aria-hidden="true"></span>
                                    Сообщения
                                    <span class="badge bg-green"><?= count(\app\models\Message::find()
                                            ->where(['user_id' => Yii::$app->user->getId()])
                                            ->where(['admin_id'=>0])
                                            ->all()) ?>
                                            </span>
                                </a>
                            </li>

                            <li>
                                <a href="<?= Url::to('/user/video/user') ?>">
                                    <span class="glyphicon glyphicon-film" aria-hidden="true"></span>
                                    Видеотека
                                    <span class="badge bg-green"><?= count(\app\modules\user\models\VideoSection::find()
                                            ->all()) ?>
                                            </span>
                                </a>
                            </li>
                            <li>
                                <a href="<?= Url::to('/message/tasks') ?>">
                                    <span class="glyphicon glyphicon-fire" aria-hidden="true"></span>
                                    <strong>Текущие</strong>
                                    <span class="badge bg-green"><?= count(\app\modules\user\models\Task::find()
                                            ->all()) ?>
                                            </span>
                                </a>
                            </li>
                            <li class="divider"></li>
                            <li>
                                <a href="<?= Url::to('/message/results') ?>">
                                    <span class=" glyphicon glyphicon-check" aria-hidden="true" style="font-size: 16px;
                                                 color: rgba(28,28,28,0.81);"></span>
                                    Завершённые
                                    <span class="badge bg-green"><?= count(\app\models\Mark::find()
                                            ->where(['user_id' => Yii::$app->user->getId()])
                                            ->all()) ?>
                                            </span>
                                </a>
                            </li>
                        </ul>
                    </li>

                    <li class="dropdown">
                        <a data-toggle="dropdown" class="dropdown-toggle" href="#">Обратная связь <b class="caret"></b></a>
                        <ul class="dropdown-menu" style="width: 190%">
                            <li>
                                <a href="<?= Url::to('/user/help/feedback') ?>">
                                    <span class="glyphicon glyphicon-edit" aria-hidden="true"></span>
                                    Заполнить форму
                                </a>
                            </li>
                            <li>
                                <a href="<?= Url::to('/user/faq') ?>">
                                    <span class="glyphicon glyphicon-link" aria-hidden="true"></span>
                                    Вопросы-ответы к игре
                                    <span class="badge bg-green"><?= count(\app\modules\user\models\Faq::find()->all()) ?>
                                            </span>
                                </a>
                            </li>
                        </ul>
                    </li>
                    <li>
                                 <span style="color: rgb(255,30,32);font-style: italic;font-family: Impact,serif;
    font-size: 40px;text-shadow: #cccccc 0 1px 0, #c9c9c9 0 2px 0, #bbbbbb 0px 3px 0px, #b9b9b9 0px 4px 0px,
                                    #aaaaaa 0px 5px 0px, rgba(0, 0, 0, 0.0980392) 0px 6px 1px,
                                     rgba(0, 0, 0, 0.0980392) 0px 0px 5px, rgba(0, 0, 0, 0.298039) 0px 1px 3px,
                                      rgba(0, 0, 0, 0.14902) 0px 3px 5px, rgba(0, 0, 0, 0.2) 0px 5px 10px,
                                       rgba(0, 0, 0, 0.2) 0px 10px 10px, rgba(0, 0, 0, 0.0980392) 0px 20px 20px;
                                           cursor: pointer;    text-align: center;"># С У П Е Р С У Ш К А</span>

                    </li>

                <?php elseif (Yii::$app->user->identity['role_id'] >= 5): ?>
                    <li class="dropdown">
                        <a data-toggle="dropdown" class="dropdown-toggle" href="#">Задания <b class="caret"></b></a>
                        <ul class="dropdown-menu">
                            <li>
                                <a href="<?= Url::to('/user/task/') ?>">
                                    <span class="glyphicon glyphicon-tasks" aria-hidden="true"></span>
                                    Все Задания
                                    <span class="badge bg-green"><?= count(\app\modules\user\models\Task::find()->all()) ?>
                                    </span>
                                </a>
                            </li>

                        </ul>
                    </li>

                    <li class="dropdown ">
                        <a data-toggle="dropdown" class="dropdown-toggle" href="#">Пользователи<b class="caret"></b></a>
                        <ul class="dropdown-menu">
                            <li>
                                <a class="list-group-item" style="color: #000;"
                                   href="<?= Url::to('/user/user/index') ?>">
                                    Все пользователи
                                    <span class="badge bg-green"><?= count(\app\modules\user\models\User::find()->all()) ?>
                                        </span>
                                </a>
                            </li>
                            <li>
                                <a class="list-group-item" style="color: #000;"
                                   href="<?= Url::to('/user/admin/voting') ?>">Голосование
                                </a>
                            </li>
                            <li>
                                <a class="list-group-item" href="<?= Url::to('/user/faq/') ?>">
                                    Вопросы-ответы к игре
                                </a>
                            </li>
                        </ul>
                    </li>
                    <li>
                        <a href="<?= Url::to('/user/video/') ?>">
                            <span class="glyphicon glyphicon-film" aria-hidden="true"></span>
                            Видеотека
                        </a>
                    </li>
                    <li>
                        <a href="<?= Url::to('/user/admin/message-user') ?>">
                            <span class="glyphicon glyphicon-th-list" aria-hidden="true" style="font-size: 16px;
                                                 color: rgba(28,28,28,0.81);"></span>
                            Отчёты участников
                            <span class="badge bg-green"><?= count(\app\models\Message::find()
                                    ->where(['>', 'admin_id', '0'])
                                    ->all()) ?>
                                        </span>
                        </a>
                    </li>

                <?php endif; ?>

            </ul>

            <ul class="nav navbar-nav navbar-right">
                <?php if (Yii::$app->user->identity['role_id'] < 5): ?>
                    <?php
                    $answers = Answer::find()->where(['user_id' => Yii::$app->user->getId()])->all();
                    if (isset($answers) && $answers != null):
                        ?>
                        <li role="presentation" class="dropdown" style="margin-right: 0;">
                            <a href="javascript:;" class="dropdown-toggle info-number" data-toggle="dropdown"
                               aria-expanded="false">
                                Уведомления

                                <span class="badge bg-green"><?= sizeof($answers) ?></span>
                            </a>
                            <ul id="menu1" class="dropdown-menu list-unstyled msg_list" role="menu">
                                <?php
                                foreach ($answers

                                         as $answer):
                                    $image = Image::findOne(['profile_id' => $answer->user_id]); ?>
                                    <li>
                                        <a href="<?= Url::to('/user/help/feedback') ?>">
                      <span class="image">
                                        <?= Html::img("@web/images/usr/{$image->avatar}", ['width' => 35, 'height' => 35]) ?>
                                    </span>
                                            <span>
                                        <span><strong><?= Html::encode($answer->name) ?></strong></span>
                                            <br>
                                        <span style="font-size: 16px;"><?= Html::encode($answer->theme) ?></span>
                                                <!--                      <span class="time">3 mins ago</span>-->
                      </span>
                                            <span class="message"
                                                  style="font-size: 14px;">
                                         <?= Html::encode($answer->text) ?>
                                    </span>
                                        </a>
                                    </li>

                                    <li>
                                        <div class="text-center">
                                            <a href="<?= Url::to('/user/help/feedback') ?>">
                                                <strong>Ответить</strong>
                                                <i class="fa fa-angle-right"></i>
                                            </a>
                                        </div>
                                    </li>
                                <?php endforeach; ?>
                            </ul>
                        </li>
                    <?php endif; ?>

                    <?php if (!isset($answers) || $answers == null): ?>
                        <li role="presentation" class="dropdown" style="margin-right: 0;">
                            <a href="javascript:;" class="dropdown-toggle info-number" data-toggle="dropdown"
                               aria-expanded="false">
                                Уведомления
                                <span class="badge bg-green">0</span>
                            </a>
                        </li>
                    <?php endif; ?>

                <?php endif; ?>

                <?php if (Yii::$app->user->identity['role_id'] >= 5): ?>
                    <?php
                    $questions = UserQuestion::find()->all();
                    if (isset($questions) && $questions != null):
                        ?>
                        <li role="presentation" class="dropdown" style="margin-right: 10px;">
                            <a href="javascript:;" class="dropdown-toggle info-number" data-toggle="dropdown"
                               aria-expanded="false">
                                <span class="badge bg-green"><?= sizeof($questions) ?></span>
                                Уведомления

                            </a>
                            <ul id="menu1" class="dropdown-menu list-unstyled msg_list" role="menu">
                                <?php
                                foreach ($questions

                                         as $question):
                                    ?>
                                    <li>
                                        <a href="<?= Url::to('/user/help/feedback') ?>">
                                                <span>
                                        <span><strong><?= Html::encode($question->name) ?></strong></span>
                                            <br>
                                        <span style="font-size: 16px;"><?= Html::encode($question->theme) ?></span>
                                                    <!--                      <span class="time">3 mins ago</span>-->
                      </span>
                                            <span class="message"
                                                  style="font-size: 14px;">
                                         <?= Html::encode($question->text) ?>
                                    </span>
                                        </a>
                                    </li>

                                    <li>
                                        <div class="text-center">
                                            <a href="<?= Url::to('/user/admin/questions') ?>">
                                                <strong>Ответить</strong>
                                                <i class="fa fa-angle-right"></i>
                                            </a>
                                        </div>
                                    </li>
                                <?php endforeach; ?>
                            </ul>
                        </li>
                    <?php endif; ?>

                    <?php if (!isset($questions) || $questions == null): ?>
                        <li role="presentation" class="dropdown" style="margin-right: 10px;">
                            <a href="javascript:;" class="dropdown-toggle info-number" data-toggle="dropdown"
                               aria-expanded="false">
                                <span class="badge bg-green">0</span>
                                Уведомления

                            </a>
                        </li>
                    <?php endif; ?>
                <?php endif; ?>

                <li>
                    <a href="#" class="user-profile dropdown-toggle" data-toggle="dropdown"
                       aria-expanded="false">
                        Мои настройки
                        <span class=" fa fa-angle-down"></span>
                    </a>

                    <ul class="dropdown-menu dropdown-usermenu pull-right" style="margin-right: 5%;">
                        <li><a href="<?= Url::to('/user/help/profile') ?>">
                                <?php if (isset($image)): ?>
                                    <?= Html::img("@web/images/usr/{$image->avatar}", ['width' => 50, 'height' => 50]) ?>
                                <?php endif; ?>
                                <?= Yii::$app->user->identity['name'] . " " . Yii::$app->user->identity['surname'] ?>
                            </a>
                        </li>
                        <li><a href="<?= Url::to('/user/auth/logout') ?>"><i
                                        class="fa fa-sign-out pull-right"></i>
                                (Выход)</a>
                        </li>
                    </ul>
                </li>
            </ul>
        </div>
    </nav>
    <!-- top navigation -->


    <?= $content ?>


</div>
<?php $this->endBody(); ?>
</body>
</html>
<?php $this->endPage(); ?>
