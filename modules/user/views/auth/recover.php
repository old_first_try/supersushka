<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;

?>
<?php $form = ActiveForm::begin([
        'options' => ['id' => 'login'],
    ]
); ?>
<div class="login_container" align="center">
    <h1 align="center"><?= Html::encode('Восстановить доступ') ?></h1>
    <br>

    <div class="col-md-12" style="float: inherit;">
        <?= $form->field($model, 'email')->input('email', ['placeholder' => 'Введите Ваш логин '])
            ->label('E-mail:') ?>
        <br>
    </div>
    <div align="center">
        <?= Html::submitButton('Восстановить', ['class' => 'btn btn-success', 'style' => "font-size: 15px;
         top:7px;", 'width' => '300px']); ?>
        <br>
        <a href="<?= Url::to('/user/auth/login') ?>"> <?= Html::button('Назад', ['class' => 'btn btn-warning',
                'style' => "font-size: 15px;margin-top:0px;"]); ?></a>
    </div>

</div>
<?php ActiveForm::end(); ?>

