<?php

/**
 * @var $this yii\web\View
 * @var $name string
 * @var $message string
 * @var $exception \yii\web\HttpException
 */

use yii\helpers\Html;
use yii\helpers\Url;
use \yii\web\HttpException;

$exception = new HttpException(401);
$message = "";
$textColor = $exception->statusCode === 404 ? "text-yellow" : "text-red";

?>

<div class="auth_error">
    <div align="center" class="text-center text-center" style="color: #000" >
        <h1><?= Html::encode('Ошибка авторизации!') ?></h1>
        <h1 class="error-number"><?= $exception->statusCode ?></h1>
        <h2><?= nl2br(Html::encode($message)) ?></h2>
        <h4><strong>Вы не авторизованы.</strong>
            <br>
            <br>
            Перейдите <a href="<?= Url::to('/user/auth/login') ?>"><strong><u>сюда</u></strong></a> для входа.</h4>

    </div>

</div>
