<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

$this->title = 'Сброс пароля';
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="site-reset-password" >
    <h1 align="center" id="title"><?= Html::encode($this->title) ?></h1>
    <h4 align="center" id="title">Напиши свой новый пароль:</h4>
    <div class="row" align="center" style="margin-right: auto;margin-left: auto">
        <div class="col-lg-5" style="float: none;">

            <?php $form = ActiveForm::begin(['id' => 'reset-password-form']); ?>

            <div class="col-md-12" style="float: inherit;">

                <?= $form->field($model, 'password')->passwordInput(['autofocus' => true, 'style' =>
                    'margin-left:auto;margin-right:auto;'])->label('Новый пароль:') ?>
                <?= Html::checkbox('reveal-password', false, ['id' => 'reveal-password']) ?> <?= Html::label('Показать пароль', 'reveal-password') ?>
            </div>

            <div class="form-group" align="center" style="margin-top: 5px;">
                <?= Html::submitButton('Сохранить', ['class' => 'btn btn-success']) ?>
            </div>
            <?php ActiveForm::end(); ?>

        </div>
    </div>


</div>