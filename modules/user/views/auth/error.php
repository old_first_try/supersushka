<?php
/**
 * Created by PhpStorm.
 * User: Tom
 * Date: 29.08.2017
 * Time: 20:08
 */

/**
 * @var $this yii\web\View
 * @var $name string
 * @var $message string
 * @var $exception \yii\web\HttpException
 */

use yii\helpers\Html;
use yii\helpers\Url;
use \yii\web\HttpException;

$exception = new HttpException(404);
$message = "";
$textColor = $exception->statusCode === 404 ? "text-yellow" : "text-red";

?>

<div class="default_error" align="center">
    <div class="well" align="center">

        <h1 class="error-number"><?= $exception->statusCode ?></h1>
        <h2><?= nl2br(Html::encode($message)) ?></h2>
        <p style="font-size: 22px;">
            Данная ошибка произошла пока Веб сервер обрабатывал ваш запрос.
        </p>
        <p style="font-size: 22px;">
            Пожалуйста сообщите нам если вы думаете, что это серверная ошибка. Спасибо.
        </p>

    </div>

</div>
