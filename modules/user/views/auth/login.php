<?php

use yii\helpers\Url;
use yii\widgets\ActiveForm;
use yii\helpers\Html;

?>
<?php //debug($users); ?>


<div class="login_container">

    <h1 align="center" id="title"><?= Html::encode('Вход') ?></h1>

    <?php if (Yii::$app->session->getFlash('Логин/пароль введены неверно.')): ?>
        <div class="alert alert-danger alert-dismissible flash-login border_radius" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span
                        aria-hidden="true">&times;</span></button>
            Логин/пароль введены неверно.
        </div>
    <?php endif; ?>

    <?php if (Yii::$app->session->getFlash('success')): ?>
        <div class="alert alert-success alert-dismissible flash-login border_radius" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span
                        aria-hidden="true">&times;</span></button>
            Проверь свою почту для дальнейших инструкций.
        </div>
    <?php endif; ?>

    <?php if (Yii::$app->session->getFlash('successPasswordUpdate')): ?>
        <div class="alert alert-success alert-dismissible flash-loginborder_radius" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span
                        aria-hidden="true">&times;</span></button>
            Новый пароль был сохранён.
        </div>
    <?php endif; ?>

    <?php if (Yii::$app->session->getFlash('error')): ?>
        <div class="alert alert-danger alert-dismissible flash-login border_radius" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span
                        aria-hidden="true">&times;</span></button>
            Извини, мы не можем сбросить пароль для заявленной электронной почты.
        </div>
    <?php endif; ?>

    <?php $form = ActiveForm::begin([
        'options' => ['id' => 'login'],
    ]); ?>
    <?= $form->field($model, 'login')->input('email', ['placeholder' => 'Напишите свой логин'])->label('Логин'); ?>
    <?= $form->field($model, 'password')->passwordInput(['placeholder' => 'Напишите свой пароль'])->label('Пароль') ?>

    <?= Html::checkbox('reveal-password', false, ['id' => 'reveal-password']) ?> <?= Html::label('Показать пароль', 'reveal-password') ?>
    <?= $form->field($model, 'rememberMe')->checkbox() ?>

    <div align="center" style="position: relative;margin: auto;top: 10px;">
        <?= Html::submitButton('Вход', ['class' => 'btn btn-success radius ', 'style' =>
            'margin-left:auto:margin-right:auto;']) ?>
    </div>
    <a style="position: relative;margin: auto;color: #216eff;font-size: 14px;top: 10px;" target="_blank"
       href="<?= Url::to('https://s-1.sypersushka.ru') ?>">
        <div align="center">Регистрация</div>
    </a>
    <a style="position: relative;margin: auto;color: #216eff;font-size: 14px;top: 15px;"
       href="<?= Url::to('/user/auth/recover') ?>">
        <div align="center">Забыли свой пароль?</div>
    </a>

    <?php $form = ActiveForm::end(); ?>

</div>
