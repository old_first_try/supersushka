<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */

/* @var $model app\models\ */

use app\modules\user\models\Answer;
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\captcha\Captcha;
use yii\helpers\Url;

$this->params['breadcrumbs'][] = $this->title;

$marks = Answer::find()->where(['user_id' => Yii::$app->user->getId()])
    ->andWhere(['is_readed' => 0])
    ->all();
//debug($userQuestion);
?>

<div class="user_container well" style="border-radius: 10px;">

    <h1 align="center" id="title" class="title">
        <?= Html::encode('Форма для обратной связи') ?>
    </h1>
    <div class="site-contact list-group-item border_radius" align="center">

        <?php if (Yii::$app->session->hasFlash('contactFormSubmitted')): ?>

            <div class="alert alert-success alert-dismissible flash-login border_radius" role="alert"
                 style="width: 50%">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span
                            aria-hidden="true">&times;</span></button>
                Спасибо за то что связались с нами!
                <br>
                Мы ответим вам так скоро, насколько это возможно.
            </div>
        <?php else: ?>

            <p id="beauty-in-label">
                Пожалуйста заполните предложенную
                <br>
                форму чтобы связаться с нами. Спасибо!
            </p>

            <div class="row">
                <div class="col-lg-5" style="float: none">

                    <?php $form = ActiveForm::begin(['id' => 'contact-form']); ?>

                    <?= $form->field($model, 'name')->textInput(['autofocus' => true, 'placeholder' => 'Как вас зовут?']) ?>
                    <br>
                    <br>
                    <br>
                    <div class="alert alert-success radius"
                         style="font-size: large;width: 100%;height: 75%">
                        Ваш логин указанный при регистрации:
                        <?= Yii::$app->user->identity['login'] ?>
                    </div>
                    <?= $form->field($model, 'subject')->input('text', ['placeholder' => 'Что вас интересует?']) ?>

                    <?= $form->field($model, 'body')->textarea(['rows' => 1, 'placeholder' => 'Описание вашего вопроса']) ?>

                    <?= $form->field($model, 'verifyCode')->widget(Captcha::className(), [
                        'captchaAction' => 'help/captcha',
                        'template' => '<div class="row"><div class="col-lg-3" style="margin-right: 35px;">{image}</div><div class="col-lg-6" >{input}</div></div>',
                    ])->label('это детский пример - посчитайте и напишите ответ)') ?>
                    <div class="form-group">
                        <?= Html::submitButton('Отправить', ['class' => 'btn btn-success', 'name' => 'contact-button']) ?>
                    </div>

                    <?php ActiveForm::end(); ?>

                </div>
            </div>

        <?php endif; ?>
    </div>

    <div class="user-questions-answer list-group-item border_radius_margin text-post post-font">

        <p>
            Ответы на Ваши вопросы читайте ниже:
        </p>
        <?php if (isset($marks)): ?>
            <?php foreach ($marks

                           as $answer): ?>
                <div class="list-group-item border_radius border-green text-post post-font">
                    <p style="margin-bottom: 0;">
                        <strong>Имя: </strong><?= Html::encode($answer->name) ?>
                    </p>

                    <hr>

                    <p style="margin-bottom: 0;">
                        <strong>Тема вопроса: </strong><?= Html::encode($answer->theme) ?>
                    </p>

                    <hr>

                    <p>
                        <strong>Ответ: </strong>
                    </p>

                    <hr>

                    <p>
                        <strong style="font-size: 21px;"> <?= Html::encode($answer->text) ?></strong>
                    </p>

                    <hr>

                    <p>
                        <?= Html::a('Закрыть вопрос', Url::to(['/message/delete', 'id' => $answer->id]), [
                            'class' => 'btn btn-info bun '
                        ]) ?>
                    </p>

                    <hr>
                </div>
            <?php endforeach; ?>

        <?php endif; ?>

        <?php
        if (isset($marks) && $marks == null): ?>
            <div class="list-group-item border-green border_radius post-font text-post" style="width: 35%;
            margin-left: auto;margin-right: auto;text-align: center; font-size: 16px;">
                <h5 id="title">У вас пока что нет сообщений.</h5>
            </div>
        <?php endif; ?>
    </div>

</div>
