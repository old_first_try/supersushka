<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;

?>
<div class="user_container well" style="border-radius: 10px;min-height: 500px;">
    <h3 align="center" class="text-post" >
        <?= Html::encode('Изменить фото') ?>
    </h3>

    <nav id="icon-text-tab-bar " class="mdc-tab-bar mdc-tab-bar--icons-with-text myTabImage">
        <a class="mdc-tab mdc-tab--with-icon-and-text" href="<?= Url::to('/user/help/profile') ?>">
            <i class="material-icons mdc-tab__icon" aria-hidden="true">settings</i>
            <span class="mdc-tab__icon-text">Ваш Профиль</span>
        </a>
        <a class="mdc-tab mdc-tab--with-icon-and-text" href="<?= Url::to('/user/help/password-change') ?>"
           style="margin-left: 14px;">
            <i class="material-icons mdc-tab__icon" aria-hidden="true">security</i>
            <span class="mdc-tab__icon-text">Пароль</span>
        </a>
        <span class="mdc-tab-bar__indicator"></span>
    </nav>

    <?php if (Yii::$app->session->getFlash('successAvatarUpdate')): ?>
        <div class="alert alert-success alert-dismissible flash border_radius" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span
                        aria-hidden="true">&times;</span></button>
            Ваш аватар изменён.
        </div>
    <?php endif; ?>

    <?php if (Yii::$app->session->getFlash('errorAvatarUpdate')): ?>
        <div class="alert alert-danger alert-dismissible flash border_radius" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span
                        aria-hidden="true">&times;</span></button>
            Ошибка! Попробуйте снова или обратитесь в тех-поддержку.
        </div>
    <?php endif; ?>

    <div class="change_photo list-group-item border-green border_radius post-font" align="center"
         style="position: relative;margin-left: auto;margin-right: auto;margin-bottom: 45px;">

        <?= Html::img("@web/images/usr/{$model->avatar}", ['alt' => 'Avatar', 'class' => 'img-thumbnail', 'width' => 425, 'height' => 300]) ?>

        <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data',
            'prompt' => 'Please Select']]) ?>

        <?= $form->field($model, 'file')->fileInput(['input' => '#my-input']) ?>

        <?= Html::submitButton('Сохранить', ['class' => 'btn btn-success']) ?>

        <?php ActiveForm::end() ?>
    </div>
</div>
