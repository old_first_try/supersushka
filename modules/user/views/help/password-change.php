<?php

use app\modules\user\models\User;
use yii\bootstrap\ActiveForm;
use yii\helpers\Html;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model app\modules\user\models\PasswordChangeForm */
$login = User::findOne(Yii::$app->user->identity->getId())->login;
?>
<div class="user_container well" style="border-radius: 10px;">


    <h3 align="center" class="text-post">
        <?= Html::encode('Изменить пароль')?>
    </h3>

    <nav id="icon-text-tab-bar " class="mdc-tab-bar mdc-tab-bar--icons-with-text myTabContent">
        <a class="mdc-tab mdc-tab--with-icon-and-text" href="<?=Url::to('/user/help/profile')?>" >
            <i class="material-icons mdc-tab__icon" aria-hidden="true">settings</i>
            <span class="mdc-tab__icon-text">Ваш Профиль</span>
        </a>
        <a class="mdc-tab mdc-tab--with-icon-and-text" href="<?=Url::to('/user/help/avatar')?>" style="margin-left: 14px;">
            <i class="material-icons mdc-tab__icon" aria-hidden="true">image</i>
            <span class="mdc-tab__icon-text">Аватар</span>
        </a>
        <span class="mdc-tab-bar__indicator"></span>
    </nav>

    <?php if (Yii::$app->session->getFlash('errorPasswordUpdate')): ?>
        <div class="alert alert-danger alert-dismissible flash border_radius" role="alert" >
            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span
                        aria-hidden="true">&times;</span></button>
            <strong>Ошибка!</strong> Попробуйте снова или обратитесь в тех-поддержку.
        </div>
    <?php endif; ?>

    <div class="user-form" align="center" >

        <?php $form = ActiveForm::begin(); ?>

        <div class="row" >
            <div class="col-sm-4" style="float: none;">
                <?= $form->field($model, 'currentPassword')->passwordInput(['maxlength' => true]) ?>
            </div>
            <br>
            <div class="col-sm-4" style="float: none;">
                <?= $form->field($model, 'newPassword')->passwordInput(['maxlength' => true]) ?>
            </div>
            <br>
            <div class="col-sm-4" style="float: none;">
                <?= $form->field($model, 'newPasswordRepeat')->passwordInput(['maxlength' => true]) ?>

            </div>
        </div>

        <div class="form-group">
            <?= Html::submitButton(Yii::t('app', 'Сохранить'), ['class' => 'btn btn-success']) ?>
        </div>

        <?php ActiveForm::end(); ?>
    </div>

</div>
