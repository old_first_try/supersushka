<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\modules\user\models\VideoSection */

$this->title = 'Изменить Видео Секцию: ';
$this->params['breadcrumbs'][] = ['label' => 'Видео Секции', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->title, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Изменить';
?>
<div class="admin_container well" style="border-radius: 10px;">

    <h3 align="center" class="text-post">
        <?= Html::encode($this->title) ?>
    </h3>
    <h3 align="center" id="title">
        <?= Html::encode($model->title) ?>
    </h3>

    <div class="list-group-item border-green border_radius_margin post-font text-post">

        <?= $this->render('_form', [
            'model' => $model,
        ]) ?>
    </div>
</div>
