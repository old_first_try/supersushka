<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

?>

<div class="video-section-form">


        <?php
        $modelForm = ActiveForm::begin();
        echo $modelForm->field($model, 'title')->input('text', ['maxlength' => true,
            'placeholder' => 'Название новой секции', 'autofocus' => 'autofocus']);

        echo $modelForm->field($model, 'title1')->input('text');
        echo $modelForm->field($model, 'video1')->input('text');

        echo $modelForm->field($model, 'title2')->input('text');
        echo $modelForm->field($model, 'video2')->input('text');

        echo $modelForm->field($model, 'title3')->input('text');
        echo $modelForm->field($model, 'video3')->input('text');

        echo $modelForm->field($model, 'title4')->input('text');
        echo $modelForm->field($model, 'video4')->input('text');

        echo $modelForm->field($model, 'title5')->input('text');
        echo $modelForm->field($model, 'video5')->input('text');

        echo $modelForm->field($model, 'title6')->input('text');
        echo $modelForm->field($model, 'video6')->input('text');

        echo $modelForm->field($model, 'title7')->input('text');
        echo $modelForm->field($model, 'video7')->input('text');

        echo $modelForm->field($model, 'date')->input('date');
        ?>

        <div class="form-group">
            <?= Html::submitButton($model->isNewRecord ? 'Создать секцию' : 'Сохранить изменения', ['class' => $model->isNewRecord ? 'btn btn-success radius' : 'btn btn-primary bun']) ?>
            <?= Html::a('Все секции', ['index', 'id' => $model->id], ['class' => 'btn btn-warning bun']) ?>
        </div>

        <?php $modelForm = ActiveForm::end(); ?>


</div>
