<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\modules\user\models\VideoSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Все видео с проекта - здесь:';
$ask = 'Здесь вы можете добавлять/удалять как разделы, так и видео в них.';
?>

<div class="admin_container well" style="border-radius: 10px;">

    <h3 align="center" class="text-post">
        <?= Html::encode($this->title) ?>
    </h3>
    <h5 align="center" id="title">
        <?= Html::encode($ask) ?>
    </h5>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <div class="list-group-item border-green border_radius_margin post-comment text-post">

        <p>
            <?= Html::a('Создать ВидеоСекцию', ['create'], ['class' => 'btn btn-success radius']) ?>
        </p>
        <?php
        try {
            echo GridView::widget([
                'options' => [
                    'class' => 'gridView text-post',
                ],
                'dataProvider' => $dataProvider,
                'filterModel' => $searchModel,
                'columns' => [
                    ['class' => 'yii\grid\SerialColumn'],

                    //                'id',
                    [
                        'attribute' => 'title',
                        'contentOptions' => [
                            'style' => 'background-color:#fff;'
                        ]
                    ],
                    //                'date',
                    [
                        'label' => 'Дата',
                        'format' => 'raw',
                        'contentOptions' => [
                            'style' => 'background-color:#fff;'
                        ],
                        'value' => function ($model) {
                            $originalDate = $model->date;
                            $newDate = date("d-m-Y", strtotime($originalDate));
                            return $newDate;
                        },
                    ],
                    //                'video1',
                    [
                        'label' => 'Первый ролик',
                        'format' => 'raw',
                        'contentOptions' => [
                            'style' => 'background-color:#fff;'
                        ],
                        'value' => function ($model) {
                            if ($model->video1 == null)
                                return Html::a(Html::img("@web/images/youtube-grey.png", ['alt' => 'yii']));
                            if ($model->video1 != null) {
                                $img = Html::img("@web/images/youtube.png", ['alt' => 'yii']);
                                $url =
                                    "<a href='{$model->video1}'>
                                    $img
                                </a>";
                                return $url;
                            }
                        }
                    ],
                    //                'video2',
                    [
                        'label' => 'Второй ролик',
                        'format' => 'raw',
                        'contentOptions' => [
                            'style' => 'background-color:#fff;'
                        ],
                        'value' => function ($model) {
                            if ($model->video2 == null)
                                return Html::a(Html::img("@web/images/youtube-grey.png", ['alt' => 'yii']));
                            if ($model->video2 != null) {
                                $img = Html::img("@web/images/youtube.png", ['alt' => 'yii',]);
                                $url =
                                    "<a href='{$model->video2}'>
                                    $img
                                </a>";
                                return $url;
                            }
                        }
                    ],
                    // 'video3',
                    // 'video4',
                    // 'video5',
                    // 'video6',
                    // 'video7',

                    ['class' => 'yii\grid\ActionColumn'],
                ],
            ]);
        } catch (Exception $e) {
        } ?>

    </div>

</div>
