<?php

use lesha724\youtubewidget\Youtube;
use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\modules\user\models\VideoSection */

$this->title = 'Видео Секция:';
$this->params['breadcrumbs'][] = ['label' => 'Video Sections', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="admin_container well" style="border-radius: 10px;">

    <h3 align="center" class="text-post">
        <?= Html::encode($this->title) ?>
    </h3>
    <h3 align="center" id="title">
        <?= Html::encode($model->title) ?>
    </h3>

    <div class="list-group-item border-green border_radius_margin post-font text-post">

        <p>
            <?= Html::a('Изменить', ['update', 'id' => $model->id], ['class' => 'btn btn-primary radius']) ?>
            <?= Html::a('Удалить', ['delete', 'id' => $model->id], [
                'class' => 'btn btn-danger radius',
                'data' => [
                    'confirm' => 'Вы уверены, что хотите удалить эту секцию?',
                    'method' => 'post',
                ],
            ]) ?>

            <?= Html::a('Все секции', ['index', 'id' => $model->id], ['class' => 'btn btn-primary radius']) ?>
        </p>

        <?php try {
            echo DetailView::widget([
                'options' => [
                    'class' => 'gridView text-post',
                ],
                'model' => $model,
                'attributes' => [
                    [
                        'label' => 'Дата',
                        'format' => 'raw',
                        'contentOptions' => [
                            'style' => 'background-color:#fff;'
                        ],
                        'value' => function ($model) {
                            $originalDate = $model->date;
                            $newDate = date("d-m-Y", strtotime($originalDate));
                            return $newDate;
                        },
                    ],
                    //                'video1',
                    [
                        'label' => $model->title1,
                        'format' => 'raw',
                        'contentOptions' => [
                            'style' => 'background-color:#fff;'
                        ],
                        'value' => function ($model) {
                            if ($model->video1 == null)
                                return Html::a(Html::img("@web/images/youtube-grey.png", ['alt' => 'yii']));
                            if ($model->video1 != null)
                                $video = \cics\widgets\VideoEmbed::widget([
                                    'url' => "{$model->video1}", 'show_errors' => true,
                                    'container_class' => 'video-container',
                                    ]);
                            $url =
                                " <div class=\"video\" align=\"center\">
                                    $video
                                 </div>";
                            return $url;
                        }
                    ],
                    //                'video2',
                    [
                        'label' => $model->title2,
                        'format' => 'raw',
                        'contentOptions' => [
                            'style' => 'background-color:#fff;'
                        ],
                        'value' => function ($model) {
                            if ($model->video2 == null)
                                return Html::a(Html::img("@web/images/youtube-grey.png", ['alt' => 'yii']));
                            if ($model->video2 != null)
                                $video = \cics\widgets\VideoEmbed::widget([
                                    'url' => "{$model->video2}", 'show_errors' => true,
                                    'container_id' => 'yourCustomId',
                                    'container_class' => 'video-container',
                                ]);
                            $url =
                                " <div class=\"video\" align=\"center\">
                                    $video
                                 </div>";
                            return $url;
                        }
                    ],
                    //                'video3',
                    [
                        'label' => $model->title3,
                        'format' => 'raw',
                        'contentOptions' => [
                            'style' => 'background-color:#fff;'
                        ],
                        'value' => function ($model) {
                            if ($model->video3 == null)
                                return Html::a(Html::img("@web/images/youtube-grey.png", ['alt' => 'yii']));
                            if ($model->video3 != null)
                                $video = \cics\widgets\VideoEmbed::widget([
                                    'url' => "{$model->video3}", 'show_errors' => true,
                                    'container_id' => 'yourCustomId',
                                    'container_class' => 'video-container',
                                ]);
                            $url =
                                " <div class=\"video\" align=\"center\">
                                    $video
                                 </div>";
                            return $url;
                        }
                    ],
                    //                'video4',
                    [
                        'label' => $model->title4,
                        'format' => 'raw',
                        'contentOptions' => [
                            'style' => 'background-color:#fff;'
                        ],
                        'value' => function ($model) {
                            if ($model->video4 == null)
                                return Html::a(Html::img("@web/images/youtube-grey.png", ['alt' => 'yii']));
                            if ($model->video4 != null)
                                $video = \cics\widgets\VideoEmbed::widget([
                                    'url' => "{$model->video4}", 'show_errors' => true,
                                    'container_id' => 'yourCustomId',
                                    'container_class' => 'video-container',
                                ]);
                            $url =
                                " <div class=\"video\" align=\"center\">
                                    $video
                                 </div>";
                            return $url;
                        }
                    ],
                    //                'video5',
                    [
                        'label' => $model->title5,
                        'format' => 'raw',
                        'contentOptions' => [
                            'style' => 'background-color:#fff;'
                        ],
                        'value' => function ($model) {
                            if ($model->video5 == null)
                                return Html::a(Html::img("@web/images/youtube-grey.png", ['alt' => 'yii']));
                            if ($model->video5 != null)
                                $video = \cics\widgets\VideoEmbed::widget([
                                    'url' => "{$model->video5}", 'show_errors' => true,
                                    'container_id' => 'yourCustomId',
                                    'container_class' => 'video-container',
                                ]);
                            $url =
                                " <div class=\"video\" align=\"center\">
                                    $video
                                 </div>";
                            return $url;
                        }
                    ],
                    //                'video6',
                    [
                        'label' => $model->title6,
                        'format' => 'raw',
                        'contentOptions' => [
                            'style' => 'background-color:#fff;'
                        ],
                        'value' => function ($model) {
                            if ($model->video6 == null)
                                return Html::a(Html::img("@web/images/youtube-grey.png", ['alt' => 'yii']));
                            if ($model->video6 != null)
                                $video = \cics\widgets\VideoEmbed::widget([
                                    'url' => "{$model->video6}", 'show_errors' => true,
                                    'container_id' => 'yourCustomId',
                                    'container_class' => 'video-container',
                                ]);
                            $url =
                                " <div class=\"video\" align=\"center\">
                                    $video
                                 </div>";
                            return $url;
                        }
                    ],
                    //                'video7',
                    [
                        'label' => $model->title7,
                        'format' => 'raw',
                        'contentOptions' => [
                            'style' => 'background-color:#fff;'
                        ],
                        'value' => function ($model) {
                            if ($model->video7 == null)
                                return Html::a(Html::img("@web/images/youtube-grey.png", ['alt' => 'yii']));
                            if ($model->video7 != null)
                                $video = \cics\widgets\VideoEmbed::widget([
                                    'url' => "{$model->video7}", 'show_errors' => true,
                                    'container_id' => 'yourCustomId',
                                    'container_class' => 'video-container',
                                ]);
                            $url =
                                " <div class=\"video\" align=\"center\">
                                    $video
                                 </div>";
                            return $url;
                        }
                    ],
                ],
            ]);
        } catch (Exception $e) {
        } ?>
    </div>
</div>
