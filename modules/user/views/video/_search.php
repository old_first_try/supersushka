<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\modules\user\models\VideoSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="video-section-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'title') ?>

    <?= $form->field($model, 'date') ?>

    <?= $form->field($model, 'title1') ?>
    <?= $form->field($model, 'video1') ?>

    <?= $form->field($model, 'title2') ?>
    <?= $form->field($model, 'video2') ?>

    <?php // echo $form->field($model, 'video3') ?>

    <?php // echo $form->field($model, 'video4') ?>

    <?php // echo $form->field($model, 'video5') ?>

    <?php // echo $form->field($model, 'video6') ?>

    <?php // echo $form->field($model, 'video7') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
