<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\modules\user\models\VideoSection */

$this->title = 'Создание видео секции';
$this->params['breadcrumbs'][] = ['label' => 'Video Sections', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="user_container">

    <h3 align="center" id="title" class="title">
        <?= Html::encode($this->title) ?>
    </h3>

    <div class="well">

        <?= $this->render('_form', [
            'model' => $model,
        ]) ?>
    </div>

</div>
