<?php
/**
 * supersushka - user.php
 *
 * Initial version by: Tom
 * Initial created on: 09.11.2017 19:13
 */

use lesha724\youtubewidget\Youtube;
use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\modules\user\models\VideoSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Не знаешь как делать берпи или другое упражнение? Тебе сюда.';
?>

<div class="user_container well" style="border-radius: 10px;">

    <h3 align="center" id="title" class="title">
        <?= Html::encode($this->title) ?>
    </h3>

    <?php if (isset($sections) && $sections != null): ?>
        <?php foreach ($sections

                       as $section) : ?>
            <div class="list-group-item border_radius_margin">
                <h4 id="title">
                    <?= Html::encode($section->title) ?>
                </h4>
                <div class="row">
                    <div class="col-sm-3">
                        <?php if ($section->video1 != null): ?>
                            <?=$section->title1?>
                            <?php
                            try {
                                echo \cics\widgets\VideoEmbed::widget([
                                    'url' => "{$section->video1}", 'show_errors' => true,
                                    'container_id' => 'yourCustomId',
                                    'container_class' => 'video-container',
                                ]);
                            } catch (Exception $e) {
                            } ?>
                        <?php endif; ?>

                        <?=$section->title2?>
                        <?php if ($section->video2 != null): ?>
                            <?php
                            try {
                                echo \cics\widgets\VideoEmbed::widget([
                                    'url' => "{$section->video2}", 'show_errors' => true,
                                    'container_id' => 'yourCustomId',
                                    'container_class' => 'video-container',
                                ]);
                            } catch (Exception $e) {
                            } ?>
                        <?php endif; ?>
                    </div>
                    <div class="col-sm-3">
                        <?=$section->title3?>
                        <?php if ($section->video3 != null): ?>
                            <?php try {
                                echo \cics\widgets\VideoEmbed::widget([
                                    'url' => "{$section->video3}", 'show_errors' => true,
                                    'container_id' => 'yourCustomId',
                                    'container_class' => 'video-container',
                                ]);
                            } catch (Exception $e) {
                            } ?>
                        <?php endif; ?>

                        <?=$section->title4?>
                        <?php if ($section->video4 != null): ?>
                            <?php try {
                                echo \cics\widgets\VideoEmbed::widget([
                                    'url' => "{$section->video4}", 'show_errors' => true,
                                    'container_id' => 'yourCustomId',
                                    'container_class' => 'video-container',
                                ]);
                            } catch (Exception $e) {
                            } ?>
                        <?php endif; ?>
                    </div>
                    <div class="col-sm-3">
                        <?=$section->title5?>
                        <?php if ($section->video5 != null): ?>
                            <?php try {
                                echo \cics\widgets\VideoEmbed::widget([
                                    'url' => "{$section->video5}", 'show_errors' => true,
                                    'container_id' => 'yourCustomId',
                                    'container_class' => 'video-container',
                                ]);
                            } catch (Exception $e) {
                            } ?>
                        <?php endif; ?>

                        <?=$section->title6?>
                        <?php if ($section->video6 != null): ?>
                            <?php try {
                                echo \cics\widgets\VideoEmbed::widget([
                                    'url' => "{$section->video6}", 'show_errors' => true,
                                    'container_id' => 'yourCustomId',
                                    'container_class' => 'video-container',
                                ]);
                            } catch (Exception $e) {
                            } ?>
                        <?php endif; ?>
                    </div>
                    <div class="col-sm-3">
                        <?=$section->title7?>
                        <?php if ($section->video7 != null): ?>
                            <?php try {
                                echo \cics\widgets\VideoEmbed::widget([
                                    'url' => "{$section->video7}", 'show_errors' => true,
                                    'container_id' => 'yourCustomId',
                                    'container_class' => 'video-container',
                                ]);
                            } catch (Exception $e) {
                            } ?>
                        <?php endif; ?>
                    </div>
                </div>
            </div>
        <?php endforeach; ?>
    <?php elseif (isset($sections) && $sections == null): ?>
        <h3 align="center" id="title" class="title">
            <?= Html::encode('Пока что админы не загрузили видео. Подождите - ещё немного.') ?>
        </h3>
    <?php endif; ?>

</div>
