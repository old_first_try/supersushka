<?php
/**
 * supersushka - voting-second.php
 *
 * Initial version by: Tom
 * Initial created on: 18.10.2017 21:21
 */

use app\modules\user\models\Task;
use app\modules\user\models\User;
use yii\helpers\Html;
use yii\helpers\Url;

$this->title = 'Голосование - 2 тур. Баллы урезаны в четыре раза.';
if (!isset($secondVotings))
    $secondVotings = $votings;
?>

<div class="voting_second_container well" style="border-radius: 15px;">


    <ul class="nav nav-tabs nav-justified news">
        <li><a href="<?= Url::to('/user/admin/voting') ?>"><strong>Первый тур голосования</strong></a></li>
        <li class="active"><a href="#"><strong>Второй тур голосования</strong></a></li>
        <li><a href="<?= Url::to('/user/admin/voting-third') ?>"><strong>Третий тур голосования</strong></a></li>
    </ul>

    <p align="center">
        <?= Html::a('Снять голосование', ['stop', 'flag' => true, 'view' => 'second'], [
            'class' => 'btn btn-danger radius',
            'data' => [
                'confirm' => 'Вы уверены, что хотите отменить голосование?',
                'method' => 'post',
            ],
        ]) ?>
        <?php
        echo Html::a('Результаты второго тура голосования будут здесь)', ['/message/voting-second'], [
                'class' => 'btn btn-success',
                'style' => 'font-size:16px;',
            ]);
        ?>
    </p>

    <?php if (Yii::$app->session->hasFlash('successStop')): ?>
        <div class="alert alert-success alert-dismissible flash" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span
                        aria-hidden="true">&times;</span></button>
            Второй тур голосования остановлен.
        </div>
    <?php endif; ?>

    <?php if (!Yii::$app->session->hasFlash('successSecondVoting')): ?>
        <h3 align="center" id="title" class="title" STYLE="margin-top: 30px;">Результаты первого тура голосования:</h3>

    <?php elseif (Yii::$app->session->hasFlash('successSecondVoting')): ?>
        <h3 align="center" id="title" class="title" STYLE="margin-top: 30px;"><?= Html::encode($this->title) ?></h3>
        <div class="alert alert-success alert-dismissible flash" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span
                        aria-hidden="true">&times;</span></button>
            <strong>Сделано!</strong> Второй тур голосования запущен!
        </div>

    <?php endif; ?>

    <?php if (Yii::$app->session->hasFlash('successDelete')): ?>
        <div class="alert alert-success alert-dismissible flash" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span
                        aria-hidden="true">&times;</span></button>
            <strong>Сделано!</strong> Пользователь исключён из голосования.
        </div>

    <?php endif; ?>

    <?php if (Yii::$app->session->hasFlash('errorDelete')): ?>
        <div class="alert alert-danger alert-dismissible flash" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span
                        aria-hidden="true">&times;</span></button>
            <strong>Ошибка!</strong> Попробуйте снова. Если не выходит - обратитесь в службу поддержки.
        </div>
    <?php endif; ?>

    <?php
    foreach ($secondVotings
             as $voting) :
        $user = User::findOne(['id' => $voting->user_id]); ?>

        <div class="list-group-item border_radius_margin post-font voting">
            <a href="<?= Url::to(['/uzer/view', 'user_id' => $user->id]) ?>" target="_blank"
               style="margin-left: auto;margin-right: auto">
                <?= Html::encode($user->name) . " " . Html::encode($user->surname) ?>
                <?php if ($voting->balls == 0): ?>
                    <?= Html::encode('0 баллов') ?>
                <?php endif; ?>
                <?php if ($voting->balls != 0): ?>
                    <?= Html::encode($voting->balls . ' баллов') ?>
                <?php endif; ?>
            </a>
            <br>
            <?php

            try {
                echo newerton\fancybox\FancyBox::widget([
                    'target' => 'a[rel=fancybox]',
                    'helpers' => true,
                    'mouse' => true,
                    'config' => [
                        'maxWidth' => '100%',
                        'maxHeight' => '100%',
                        'playSpeed' => 7000,
                        'padding' => 0,
                        'fitToView' => false,
                        'width' => '70%',
                        'height' => '70%',
                        'autoSize' => false,
                        'closeClick' => false,
                        'openEffect' => 'elastic',
                        'closeEffect' => 'elastic',
                        'prevEffect' => 'elastic',
                        'nextEffect' => 'elastic',
                        'closeBtn' => false,
                        'openOpacity' => true,
                        'helpers' => [
                            'title' => ['type' => 'float'],
                            'buttons' => [],
                            'thumbs' => ['width' => 68, 'height' => 50],
                            'overlay' => [
                                'css' => [
                                    'background' => 'rgba(0, 0, 0, 0.8)'
                                ]
                            ]
                        ],
                    ]
                ]);
            } catch (Exception $e) {
            }

            if (($user->role_id == 4 && $voting->photoDo == null) || $voting->photoDo == null)
                echo Html::a(Html::img("@web/images/usr/user_default.png", ['class' => 'voting-img-left']),
                    "@web/images/usr/user_default.png", ['rel' => 'fancybox']);
            else
                echo Html::a(Html::img("@web/images/photos/{$voting->photoDo}", ['class' => 'voting-img-left']),
                    "@web/images/photos/{$voting->photoDo}", ['rel' => 'fancybox']);

            if (($user->role_id == 4 && $voting->photoPosle == null) || $voting->photoPosle == null)
                echo Html::a(Html::img("@web/images/usr/user_default.png", ['class' => 'voting-img-right']),
                    "@web/images/usr/user_default.png", ['rel' => 'fancybox']);
            else
                echo Html::a(Html::img("@web/images/photos/{$voting->photoPosle}", ['class' => 'voting-img-right'
                ]), "@web/images/photos/{$voting->photoPosle}", ['rel' => 'fancybox']);

            echo Html::a('Удалить из голосования', ['delvoting', 'user_id' => $voting->id, 'level' => 2], [
                'class' => 'btn btn-danger',
                'style' => "margin-left: 150px;margin-top:10px;font-size:18px;",
                'data' => [
                    'confirm' => 'Вы уверены, что хотите убрать пользователя из голосования?',
                    'method' => 'post',
                ],
            ]);
            ?>
        </div>
    <?php endforeach; ?>

    <?php
    echo "<p align='center'>" .
        Html::a('Начать второй тур голосования', ['voting-second', 'status' => 1], [
            'class' => 'btn btn-success',
            'style' => 'font-size:16px;',
            'data' => [
                'confirm' => 'Вы уверены, что хотите начать второй тур голосования?',
                'method' => 'post',
            ],
        ])
        . "</p>";
    ?>

</div>

