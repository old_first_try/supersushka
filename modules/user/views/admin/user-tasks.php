<?php
/**
 * supersushka - user-tasks.php
 *
 * Initial version by: Tom
 * Initial created on: 27.09.2017 12:02
 */

use app\modules\user\models\Task;
use yii\helpers\Html;
use yii\helpers\Url;

$this->title = 'Задания';

?>

<div class="task_container well" style="border-radius: 15px;">


    <h1 align="center" id="title" class="title"><?= Html::encode('Задания') ?></h1>

    <?php
    $tasks = Task::find()->where(['status' => 1])->all();
    foreach ($tasks

             as $task) : ?>

        <li class="list-group-item border-green border_radius_margin post-font">
            <a href="<?= Url::to(['/user/admin/user-task', 'task_id' => $task->id, 'user_id' => $user->id]) ?>">
                <?= Html::encode($task->title) . " " . Html::encode($task->date) ?>
                <?= "Неделя: " . Html::encode($task->week) ?>
            </a>
        </li>
    <?php endforeach; ?>
</div>
