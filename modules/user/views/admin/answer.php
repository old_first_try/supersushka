<?php
/**
 * supersushka - answer.php
 *
 * Initial version by: Tom
 * Initial created on: 16.09.2017 20:41
 */

use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use yii\widgets\LinkPager;

//debug($userQuestion);
?>
<!--file for viewing all user questions cause i won little css using-->
<div class="user_question_container well" style="border-radius:15px;">

    <a class="flash btn btn-info " href="<?= Url::to('/uzer/view') ?>">
        Вернуться ко всем вопросам
    </a>

    <?php if (Yii::$app->session->getFlash('answerFormSubmitted')): ?>
        <div class="alert alert-success alert-dismissible flash border_radius" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span
                        aria-hidden="true">&times;</span></button>
            Ваш ответ отправлен.
        </div>
    <?php endif; ?>

    <?php if (Yii::$app->session->getFlash('error')): ?>
        <div class="alert alert-danger alert-dismissible flash border_radius" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span
                        aria-hidden="true">&times;</span></button>
            Ошибка! Попробуйте снова или обратитесь в тех-поддержку.
        </div>
    <?php endif; ?>

    <?php if (isset($userQuestion)): ?>
    <div class="list-group-item border_radius border-green text-post post-font">
        <strong>Имя: </strong><?= Html::encode($userQuestion->name) ?>
        <hr>
        <strong>E-mail: </strong><?= Html::encode($userQuestion->email) ?>

        <hr>
        <strong>Содержание: </strong>
        <hr>
        <?= Html::encode($userQuestion->text) ?>
        <hr>
        <?php $form = ActiveForm::begin(); ?>
        <?= $form->field($answer, 'text')->textarea(['placeholder' => 'Что вы думаете по этому поводу?'])->label('Ваш ответ:') ?>
        <?= Html::submitButton('Отправить', ['class' => 'btn btn-success']) ?>
        <?php $form = ActiveForm::end(); ?>
        <?php endif; ?>
    </div>

</div>