<?php
/**
 * supersushka - questions.php
 *
 * Initial version by: Tom
 * Initial created on: 16.09.2017 19:36
 */

use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\LinkPager;


?>
<!--file for viewing all user questions cause i won little css using-->
<div class="main_admin_view_all">

    <h1 align="center" id="title"><?= Html::encode('Здесь выводятся вопросы от пользователей') ?></h1>

    <?php if (isset($questions)): ?>
        <?php
        foreach ($questions as $question) :?>

            <a href="<?= Url::to(['/user/admin/answer', 'id' => $question->id]) ?>" >

                <div class="well" style="font-size: 16px;width: 75%;margin-right: auto;margin-left: auto;">
                    <p style="margin-bottom: 0;">
                        <strong>
                            Имя: <?= Html::encode($question->name); ?>
                        </strong>
                    </p>

                    <br>

                    <p>
                        <strong>
                            Тема вопроса: <?= Html::encode($question->theme); ?>
                        </strong>
                    </p>
                </div>
            </a>
        <?php endforeach;
        echo LinkPager::widget([
            'pagination' => $pages,
        ]); ?>
    <?php endif; ?>

    <?php if (isset($questions)): ?>
        <h2 align="center" id="title"><strong>Больше нет вопросов.</strong></h2>
    <?php endif; ?>

</div>