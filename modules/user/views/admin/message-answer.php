<?php
/**
 * Created by PhpStorm.
 * User: deka
 * Date: 12/3/17
 * Time: 8:15 PM
 */

use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;

?>
<div class="user_container well" style="border-radius: 15px;">

    <div class="list-group-item border-green border_radius post-font" align="center">

        <h3 align="center" class="text-post">
            Форма ответа:
        </h3>
        <h3 align="center" class="text-post">
            <?= $user->name . " " . $user->surname ?>
            <br>
            Сообщение участника:
        </h3>
        <br>
        <h3 align="center" class="text-post">
            <?= $messageUser->text ?>
        </h3>

        <?php
        if ($messageUser->path_first != null || $messageUser->path_second != null &&
            $messageUser->path_third != null && $messageUser->path_fourth != null) {
            try {
                echo newerton\fancybox\FancyBox::widget([
                    'target' => 'a[rel=fancybox]',
                    'helpers' => true,
                    'mouse' => true,
                    'config' => [
                        'maxWidth' => '100%',
                        'maxHeight' => '100%',
                        'playSpeed' => 7000,
                        'padding' => 0,
                        'fitToView' => false,
                        'width' => '70%',
                        'height' => '70%',
                        'autoSize' => false,
                        'closeClick' => false,
                        'openEffect' => 'elastic',
                        'closeEffect' => 'elastic',
                        'prevEffect' => 'elastic',
                        'nextEffect' => 'elastic',
                        'closeBtn' => false,
                        'openOpacity' => true,
                        'helpers' => [
                            'title' => ['type' => 'float'],
                            'buttons' => [],
                            'thumbs' => ['width' => 68, 'height' => 50],
                            'overlay' => [
                                'css' => [
                                    'background' => 'rgba(0, 0, 0, 0.8)'
                                ]
                            ]
                        ],
                    ]
                ]);
            } catch (Exception $e) {
            }
        }
        if ($messageUser->path_first != null) {
            echo Html::a(Html::img("@web/images/photos/{$messageUser->path_first}", ['width' => 150, 'height' => 100,
                'class' => 'photos-message-user radius-img']), "@web/images/photos/{$messageUser->path_first}", ['rel' => 'fancybox']);
        }
        if ($messageUser->path_second != null) {
            echo Html::a(Html::img("@web/images/photos/{$messageUser->path_second}", ['width' => 150, 'height' => 100,
                'class' => 'photos-message-user radius-img']), "@web/images/photos/{$messageUser->path_second}", ['rel' => 'fancybox']);
        }
        if ($messageUser->path_third != null) {
            echo Html::a(Html::img("@web/images/photos/{$messageUser->path_third}", ['width' => 150, 'height' => 100,
                'class' => 'photos-message-user radius-img']), "@web/images/photos/{$messageUser->path_third}", ['rel' => 'fancybox']);
        }
        if ($messageUser->path_fourth != null) {
            echo Html::a(Html::img("@web/images/photos/{$messageUser->path_fourth}", ['width' => 150, 'height' => 100,
                'class' => 'photos-message-user radius-img']), "@web/images/photos/{$messageUser->path_fourth}", ['rel' => 'fancybox']);
        }
        ?>

        <h3 data-label="Статус Задания">
            <?php
            if ($task->status == 0)
                echo "Необязательное";
            if ($task->status == 1)
                echo "Обязательное";
            ?>
        </h3>

        <h3 data-label="Состояние">
            <?php if ($mark->status)
                echo 'Принято';
            elseif (!$mark->status)
                echo 'Не принято'; ?>
        </h3>

        <h3 data-label="Ответить" style="width: 100px">

        </h3>

        <div class="list-group=item" align="center">

            <?php
            $messageForm = ActiveForm::begin();
            echo $messageForm->field($newMessage, 'text')->textarea(['placeholder' =>
                'Напишите сообщение', 'id' => 'admin-message']);

            echo Html::submitButton('Отправить', ['class' => 'btn btn-success radius']);
            $messageForm = ActiveForm::end(); ?>

            <script type="text/javascript">
                var admin_message_user_id = "<?=$user->id?>";
                var admin_message_task_id = "<?=$task->id?>";
                var admin_message_tokenCsrf = "<?=Yii::$app->request->getCsrfToken()?>";
                var user_message_id = "<?=$messageUser->id?>";
            </script>

            <div class="well">

                <?php
                $markForm = ActiveForm::begin();

                $itemsMark = [
                    '0' => 'Не засчитать задание',
                    '' => '',
                    '1' => 'Засчитать задание',
                ];
                $paramsMark = [
                    'prompt' => 'Выберите оценку...'
                ];

                echo $markForm->field($mark, 'status')->dropDownList($itemsMark, $paramsMark);


                echo Html::a('Отправить', ['message-answer',
                    'task_id' => $task->id,
                    'user_id' => $user->id
                ], [
                    'class' => 'btn btn-success',
                    'data' => [
//                                    'confirm' => 'Вы уверены, что хотите удалить это задание?',
                        'method' => 'post',
                    ],
                ]);

                $markForm = ActiveForm::end();
                ?>
            </div>

        </div>
    </div>
</div>
