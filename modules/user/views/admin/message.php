<?php
/**
 * supersushka - message.php
 *
 * Initial version by: Tom
 * Initial created on: 09.11.2017 22:01
 */

use app\models\Mark;
use app\models\Message;
use app\modules\user\models\Task;
use app\modules\user\models\User;
use lesha724\youtubewidget\Youtube;
use yii\bootstrap\Modal;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use yii\widgets\DetailView;

/* @var $this yii\web\View */

$this->title = 'Здесь выводится текущее состояние всех отчётов';

?>
<div class="task_container well" style="border-radius: 15px;">

    <h3 align="center" id="title">
        <?= Html::encode($this->title) ?>
    </h3>

    <?php
    //TODO:Flash message
    if (Yii::$app->session->getFlash('success')):
        ?>
        <div class="alert alert-success alert-dismissible flash" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span
                        aria-hidden="true">&times;</span></button>
            Действие выполнено.
        </div>
    <?php endif; ?>

    <?php if (Yii::$app->session->getFlash('error')):
        ?>
        <div class="alert alert-danger alert-dismissible flash" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span
                        aria-hidden="true">&times;</span></button>
            Ошибка. Попробуйте снова или обратитесь в службу поддержки.
        </div>
    <?php endif; ?>

    <div class="col-md-5" align="center" style="float: none;height: 75px;">
        <a href="<?= Url::to('/user/admin/user-list') ?>">
            <button class="btn btn-success">
                <span class="glyphicon glyphicon-send">
                    К переписке
                </span>
            </button>
        </a>
    </div>

    <?php
    $messages = Message::find()->where(['>', 'admin_id', '0'])
        ->andWhere(['is_readed' => 0])
        ->all();
    if (isset($messages) && $messages != null):?>
        <div class="list-group-item admins-reports border-green">
            <table>
                <caption class="attention flash">
                    <h3 align="center">
                        Ответы участников
                    </h3>
                </caption>
                <thead>
                <tr>
                    <th scope="col">Дата отправки</th>
                    <th scope="col" style="width: 180px;padding: 0;">Содержание</th>
                    <th scope="col">Статус Задания</th>
                    <th scope="col">Состояние</th>
                    <th scope="col" style="width: 175px;padding: 0;">Ответ</th>

                </tr>
                </thead>
                <tbody>
                <?php foreach ($messages as $message) :
                    $user = User::findOne(['id' => $message->user_id]);
                    $task = Task::findOne(['id' => $message->task_id]);
                    if (isset($task)) :
                        $mark = \app\models\Mark::find()
                            ->where(['user_id' => $user->id])
                            ->andWhere(['task_id' => $task->id])->one();
                        if (!isset($mark)) {
                            $mark = new Mark();
                            $mark->user_id = $user->id;
                            $mark->task_id = $task->id;
                            $mark->status = 0;
                            $mark->save();
                        }

                        $originalDate = strtotime($message->data);
                        $Month_r = array(
                            "01" => "января",
                            "02" => "февраля",
                            "03" => "марта",
                            "04" => "апреля",
                            "05" => "мая",
                            "06" => "июня",
                            "07" => "июля",
                            "08" => "августа",
                            "09" => "сентября",
                            "10" => "октября",
                            "11" => "ноября",
                            "12" => "декабря");
                        $now_month = date('m', $originalDate); // месяц на eng
                        $rus_month = $Month_r[$now_month];
//                        echo $rus_month;
                        $newDate = date("d ", $originalDate) . $rus_month
                            . date(" Y ", $originalDate) . date(" H:i:s", $originalDate);
                        ?>
                        <!--foreach-->
                        <tr>
                            <td data-label="Дата отправки"><?= $newDate ?></td>
                            <td data-label="Ответ">
                                <strong>
                                    <?= $user->name . " " . $user->surname ?>
                                </strong>
                                <br>
                                <?= $message->text ?>
                            </td>
                            <td data-label="Статус Задания"><?php
                                if ($task->status == 0)
                                    echo "Необязательное";
                                if ($task->status == 1)
                                    echo "Обязательное";
                                ?></td>
                            <td data-label="Состояние">
                                <a href="<?= Url::to(['/user/admin/user-task', 'task_id' => $task->id, 'user_id' => $user->id]) ?>">

                                    <?php if ($mark->status)
                                        echo 'Принято';
                                    elseif (!$mark->status)
                                        echo 'Не принято'; ?>
                                </a>
                            </td>

                            <td data-label="Ответить" style="width: 100px">
                                <a href="<?= Url::to(["/user/admin/message-answer", 'task_id'=>$task->id,'user_id'=>$user->id,
                                    'message_id'=>$message->id]) ?>">

                                    <?= Html::button('Написать', [
                                        'class' => 'btn btn-primary view-answer',
                                        'style' => 'font-size:14px;'

                                    ]);
                                    ?>
                                </a>


                            </td>
                        </tr>
                        <!--foreach-->
                    <?php endif; ?>
                <?php endforeach; ?>
                </tbody>
            </table>
        </div>
    <?php endif; ?>



    <?php if (isset($messages) && $messages == null): ?>
        <div class="well ">
            <p align="center"
               style="font-size: 18px; ">
                У вас пока что нет отчётов.
            </p>
        </div>
    <?php endif; ?>

</div>
