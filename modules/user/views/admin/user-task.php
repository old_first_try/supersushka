<?php
/**
 * supersushka - user-list.php
 *
 * Initial version by: Tom
 * Initial created on: 16.09.2017 22:01
 */

use app\models\Message;
use app\modules\user\models\User;
use lesha724\youtubewidget\Youtube;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\LinkPager;

$this->title = "Задание: " . $task->title;
$imgUser = \app\modules\user\models\Image::findOne(['id' => $user->id]);
$imgAdmin = \app\modules\user\models\Image::findOne(['id' => Yii::$app->user->getId()]);

$messages = \app\models\Message::findAll(['user_id' => $user->id, 'task_id' => $task->id]);
?>

<div class="task_container well" style="border-radius: 15px;">

    <h1 align="center" id="title"><?= Html::encode($this->title) ?></h1>
    <h1 align="center" id="title"><?= Html::encode("Пользователь: " . $user->name . " " . $user->surname) ?></h1>
    <h4 align="center" id="title"><?= Html::encode("Содержание") ?></h4>

    <div class="list-group-item border-green border_radius_margin post-font">
        <?php if (isset($task->videoDo) && $task->videoDo != null): ?>

            <div class="video" align="center">

                <?php
                try {
                    echo \cics\widgets\VideoEmbed::widget([
                        'url' => "{$task->videoDo}", 'show_errors' => true,
                        'container_id' => 'yourCustomId',
                        'container_class' => 'video-container',
                    ]);
                } catch (Exception $e) {
                } ?>
            </div>
        <?php endif; ?>

        <?= $task->content ?>

        <?php if (isset($task->videoPosle) && $task->videoPosle != null): ?>
            <div class="video" align="center">
                <?php
                try {
                    echo \cics\widgets\VideoEmbed::widget([
                        'url' => "{$task->videoPosle}", 'show_errors' => true,
                        'container_id' => 'yourCustomId',
                        'container_class' => 'video-container',
                    ]);
                } catch (Exception $e) {
                } ?>
            </div>

        <?php endif; ?>
        <hr>
    </div>


    <div class="list-group-item border-green border_radius_margin post-font panel-default">
        <div class="panel-heading text-center">
            <div class="row" style="height: 75px ;">
                <div class="col-md-2">
                    <div class="icon"><span class="glyphicon glyphicon-chevron-left"></span></div>
                </div>
                <div class="col-md-offset-3 col-md-2">
                    <a href="<?= Url::to(['/uzer/view', 'user_id' => $user->id]) ?>">
                        <div id="contact">
                            <span class="glyphicon" aria-hidden="true">

                                <?= Html::img("@web/images/usr/{$imgUser->avatar}", ['class' => 'img-message-user', 'aria-hidden' => 'true']) ?>
                            </span>
                            <strong style="margin-left: auto;margin-right: auto;">
                                <?= $user->name ?>
                            </strong>
                        </div>
                    </a>
                </div>
            </div>
        </div>

        <div class="panel-body">
            <?php if (isset($messages) && $messages != null): ?>
                <?php foreach ($messages

                               as $sms): ?>
                    <div class="date">
                        <?php
                        $today = date("Y-m-d H:i:s");
                        $todayDate = new DateTime($today);
                        $smsDate = new DateTime($sms->data);
                        $interval = $smsDate->diff($todayDate, true);
                        ?>
                        <?php if ($interval->format('%a') == 0): ?>
                            <span class="bold">Сегодня, </span>
                        <?php endif; ?>
                        <?php
                        if ($interval->format('%a') == 0 && $interval->format('%h') == 0 && $interval->format('%i') == 0)
                            echo $interval->format(' Только что');
                        elseif ($interval->format('%a') != 0 && $interval->format('%h') != 0 && $interval->format('%i') != 0)
                            echo $interval->format(' %a дней, %h часов, %i минут назад');

                        elseif ($interval->format('%a') == 0 && $interval->format('%h') == 0)
                            echo $interval->format(' %i минут назад');
                        elseif ($interval->format('%h') == 0 && $interval->format('%i') == 0)
                            echo $interval->format(' %a дней назад');
                        elseif ($interval->format('%a') == 0 && $interval->format('%i') == 0)
                            echo $interval->format(' %h часов назад');
                        elseif ($interval->format('%a') == 0)
                            echo $interval->format(' %h часов, %i минут назад');
                        elseif ($interval->format('%h') == 0)
                            echo $interval->format(' %a дней, %i минут назад');
                        elseif ($interval->format('%i') == 0)
                            echo $interval->format(' %a дней, %h часов назад');
                        else echo $sms->data;
                        ?>
                    </div>
                    <?php if ($sms->admin_id == $user->parent_id): ?>
                        <div class="row">
                            <div class="message message-in pull-left">

                                <?= $sms->text ?>
                            </div>

                        </div>
                    <?php endif; ?>

                    <br>

                    <?php
                    if ($sms->path_first != null || $sms->path_second != null &&
                        $sms->path_third != null && $sms->path_fourth != null) {
                        try {
                            echo newerton\fancybox\FancyBox::widget([
                                'target' => 'a[rel=fancybox]',
                                'helpers' => true,
                                'mouse' => true,
                                'config' => [
                                    'maxWidth' => '100%',
                                    'maxHeight' => '100%',
                                    'playSpeed' => 7000,
                                    'padding' => 0,
                                    'fitToView' => false,
                                    'width' => '70%',
                                    'height' => '70%',
                                    'autoSize' => false,
                                    'closeClick' => false,
                                    'openEffect' => 'elastic',
                                    'closeEffect' => 'elastic',
                                    'prevEffect' => 'elastic',
                                    'nextEffect' => 'elastic',
                                    'closeBtn' => false,
                                    'openOpacity' => true,
                                    'helpers' => [
                                        'title' => ['type' => 'float'],
                                        'buttons' => [],
                                        'thumbs' => ['width' => 68, 'height' => 50],
                                        'overlay' => [
                                            'css' => [
                                                'background' => 'rgba(0, 0, 0, 0.8)'
                                            ]
                                        ]
                                    ],
                                ]
                            ]);
                        } catch (Exception $e) {
                        }
                    }
                    if ($sms->path_first != null) {
                        echo Html::a(Html::img("@web/images/photos/{$sms->path_first}", ['width' => 150, 'height' => 100,
                            'class' => 'photos-message-user radius-img']), "@web/images/photos/{$sms->path_first}", ['rel' => 'fancybox']);
                    }
                    if ($sms->path_second != null) {
                        echo Html::a(Html::img("@web/images/photos/{$sms->path_second}", ['width' => 150, 'height' => 100,
                            'class' => 'photos-message-user radius-img']), "@web/images/photos/{$sms->path_second}", ['rel' => 'fancybox']);
                    }
                    if ($sms->path_third != null) {
                        echo Html::a(Html::img("@web/images/photos/{$sms->path_third}", ['width' => 150, 'height' => 100,
                            'class' => 'photos-message-user radius-img']), "@web/images/photos/{$sms->path_third}", ['rel' => 'fancybox']);
                    }
                    if ($sms->path_fourth != null) {
                        echo Html::a(Html::img("@web/images/photos/{$sms->path_fourth}", ['width' => 150, 'height' => 100,
                            'class' => 'photos-message-user radius-img']), "@web/images/photos/{$sms->path_fourth}", ['rel' => 'fancybox']);
                    }
                    ?>


                    <?php if ($sms->admin_id == 0): ?>
                        <div class="row">
                            <div class="message message-out pull-right">
                                <?= $sms->text ?>
                            </div>

                        </div>

                    <?php endif; ?>
                <?php endforeach; ?>
            <?php endif; ?>
            <div class="row"></div>
        </div>
        <div class="panel-footer list-group-item border-green border_radius_margin post-font" style="margin-bottom: 10px;">
            <form>
                <div class="input-group">
                    <?php
                    $form = \yii\widgets\ActiveForm::begin();
                        echo $form->field($message, 'text')->textarea(['placeholder' =>
                            'Узнайте как идут дела по заданию', 'id' => 'message_text',]); ?>
                    <span class="input-group-btn">
                        <button id="envoi" class="btn btn-default" type="button">
                            <span class="glyphicon glyphicon-send"></span>
                        </button>
                    </span>

                    <?php $form = \yii\widgets\ActiveForm::end(); ?>
                </div>
            </form>
        </div>


        <?php if (Yii::$app->session->getFlash('successMark')): ?>
            <div class="alert alert-success alert-dismissible flash" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span
                            aria-hidden="true">&times;</span></button>
                Действие выполнено.
            </div>
        <?php endif; ?>

        <?php if (Yii::$app->session->getFlash('errorMark')): ?>
            <div class="alert alert-danger alert-dismissible flash" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span
                            aria-hidden="true">&times;</span></button>
                Ошибка! Попробуйте снова или обратитесь в тех-поддержку.
            </div>
        <?php endif; ?>


        <div class="list-group-item border-green border_radius_margin post-font">

            <?php
            $mrk = \yii\widgets\ActiveForm::begin();

            $itemsMark = [
                '0' => 'Не засчитать задание',
                '1' => 'Засчитать задание',
            ];
            $paramsMark = [
                'prompt' => 'Выберите оценку...'
            ];

            echo $mrk->field($mark, 'status')->dropDownList($itemsMark, $paramsMark);;

            ?>

            <button type="submit" class="btn btn-success"><i class="fa fa-share"></i> Отправить
            </button>
            <?php $mrk = \yii\widgets\ActiveForm::end(); ?>
        </div>
    </div>

</div>
<script type="text/javascript">
    var user_id = "<?=$user->id?>";
    var task_id = "<?=$task->id?>";
    var tokenCsrf = "<?=Yii::$app->request->getCsrfToken()?>";
</script>
