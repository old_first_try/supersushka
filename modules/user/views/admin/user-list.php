<?php
/**
 * supersushka - user-list.php
 *
 * Initial version by: Tom
 * Initial created on: 16.09.2017 22:01
 */

use app\modules\user\models\ProfileOLD;
use app\modules\user\models\User;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\LinkPager;

$this->title = 'Список участников';
//TODO: Add Pagination
?>

<div class="task_container well" style="border-radius: 15px;">

    <h1 align="center" id="title"><?= Html::encode($this->title) ?></h1>

    <ul class="list-group-item border-green border_radius post-font">
        <?php
        $profiles = User::find()->where(['<', 'role_id', '5'])->all();
        if (isset($profiles)):?>

            <?php foreach ($profiles as $user) : ?>
                <a href="<?= Url::to(['/user/admin/user-tasks', 'user_id' => $user->id]) ?>">

                    <div class="well" style="border-radius: 15px;">
                        <?= $user->name . " " . $user->surname ?>
                    </div>
                </a>
            <?php endforeach; ?>

        <?php elseif (isset($profiles) && $profiles == null): ?>
            <div class="list-group-item border-green border_radius">
                <p align="center"
                   style="font-size: 18px; ">
                    У вас пока что нет подопечных.
                </p>
            </div>
        <?php endif; ?>
    </ul>

</div>