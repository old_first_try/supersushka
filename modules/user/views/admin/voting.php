<?php
/**
 * supersushka - user-tasks.php
 *
 * Initial version by: Tom
 * Initial created on: 27.09.2017 12:02
 */

use app\modules\user\models\Task;
use app\modules\user\models\User;
use yii\helpers\Html;
use yii\helpers\Url;
//debug(array_chunk($votings, 10, TRUE));

?>

<div class="voting_second_container well" style="border-radius: 15px;">

    <ul class="nav nav-tabs nav-justified news">
        <li class="active"><a href="#"><strong>Первый тур голосования</strong></a></li>
        <li><a href="<?= Url::to('/user/admin/voting-second') ?>"><strong>Второй тур голосования</strong></a></li>
        <li><a href="<?= Url::to('/user/admin/voting-third') ?>"><strong>Третий тур голосования</strong></a></li>
    </ul>

    <p align="center">
        <?= Html::a('Снять голосование', ['stop', 'flag' => true, 'view' => 'first'], [
            'class' => 'btn btn-danger radius',
            'data' => [
                'confirm' => 'Вы уверены, что хотите отменить голосование?',
                'method' => 'post',
            ],
        ]) ?>

        <?php
        echo Html::a('Результаты первого тура голосования будут здесь)', ['/message/voting'], [
            'class' => 'btn btn-success',
            'style' => 'font-size:16px;',
        ])
        ?>
    </p>

    <?php if (Yii::$app->session->hasFlash('successStop')): ?>
        <div class="alert alert-success alert-dismissible flash border_radius" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span
                        aria-hidden="true">&times;</span></button>
            Первый тур голосования остановлен.
        </div>
    <?php endif; ?>

    <h1 align="center" id="title" class="title" STYLE="margin-top: 30px;"><?= Html::encode($this->title) ?></h1>


    <?php if (Yii::$app->session->hasFlash('successVoting')): ?>
        <div class="alert alert-success alert-dismissible flash border_radius" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span
                        aria-hidden="true">&times;</span></button>
            <strong>Сделано!</strong> Первый тур голосования запущен.
        </div>


    <?php endif; ?>

    <?php if (Yii::$app->session->hasFlash('successDelete')): ?>
        <div class="alert alert-success alert-dismissible flash border_radius" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span
                        aria-hidden="true">&times;</span></button>
            <strong>Сделано!</strong> Пользователь исключён из голосования.
        </div>

    <?php endif; ?>

    <?php if (Yii::$app->session->hasFlash('errorDelete')): ?>
        <div class="alert alert-danger alert-dismissible flash border_radius" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span
                        aria-hidden="true">&times;</span></button>
            <strong>Ошибка!</strong> Попробуйте снова. Если не выходит - обратитесь в службу поддержки.
        </div>
    <?php endif; ?>

    <?php
    foreach ($votings
             as $voting) :
        $user = User::findOne(['id' => $voting->user_id]) ?>

        <div class="list-group-item border_radius_margin post-font voting">
            <a href="<?= Url::to(['/uzer/view', 'user_id' => $user->id]) ?>" target="_blank"
               style="margin-left: auto;margin-right: auto">
                <?= Html::encode($user->name) . " " . Html::encode($user->surname) ?>
            </a>
            <br>
            <?php

            try {
                echo newerton\fancybox\FancyBox::widget([
                    'target' => 'a[rel=fancybox]',
                    'helpers' => true,
                    'mouse' => true,
                    'config' => [
                        'maxWidth' => '100%',
                        'maxHeight' => '100%',
                        'playSpeed' => 7000,
                        'padding' => 0,
                        'fitToView' => false,
                        'width' => '70%',
                        'height' => '70%',
                        'autoSize' => false,
                        'closeClick' => false,
                        'openEffect' => 'elastic',
                        'closeEffect' => 'elastic',
                        'prevEffect' => 'elastic',
                        'nextEffect' => 'elastic',
                        'closeBtn' => false,
                        'openOpacity' => true,
                        'helpers' => [
                            'title' => ['type' => 'float'],
                            'buttons' => [],
                            'thumbs' => ['width' => 68, 'height' => 50],
                            'overlay' => [
                                'css' => [
                                    'background' => 'rgba(0, 0, 0, 0.8)'
                                ]
                            ]
                        ],
                    ]
                ]);
            } catch (Exception $e) {
            }

            if (($user->role_id == 4 && $voting->photoDo == null) || $voting->photoDo == null)
                echo Html::a(Html::img("@web/images/usr/user_default.png", ['class' => 'voting-img-left']),
                    "@web/images/usr/user_default.png", ['rel' => 'fancybox']);
            else
                echo Html::a(Html::img("@web/images/photos/{$voting->photoDo}", ['class' => 'voting-img-left']),
                    "@web/images/photos/{$voting->photoDo}", ['rel' => 'fancybox']);

            if (($user->role_id == 4 && $voting->photoPosle == null) || $voting->photoPosle == null)
                echo Html::a(Html::img("@web/images/usr/user_default.png", ['class' => 'voting-img-right']),
                    "@web/images/usr/user_default.png", ['rel' => 'fancybox']);
            else
                echo Html::a(Html::img("@web/images/photos/{$voting->photoPosle}", ['class' => 'voting-img-right']),
                    "@web/images/photos/{$voting->photoPosle}", ['rel' => 'fancybox']);
            ?>
            <p align="center">

                <?= Html::a('Удалить из голосования', ['delvoting', 'user_id' => $voting->id], [
                    'class' => 'btn btn-danger',
                    'style' => "font-size:16px;",
                    'data' => [
                        'confirm' => 'Вы уверены, что хотите убрать пользователя из голосования?',
                        'method' => 'post',
                    ],
                ]); ?>
            </p>
        </div>
    <?php endforeach; ?>

    <p align="center">
        <?= Html::a('Начать первый тур голосования', ['/user/admin/voting', 'status' => 1], [
            'class' => 'btn btn-success',
            'style' => 'font-size:16px;',
            'data' => [
                'confirm' => 'Вы уверены, что хотите начать первый тур голосования?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

</div>
