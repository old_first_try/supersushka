<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\modules\user\models\Faq */

$this->title = $model->title;
$this->params['breadcrumbs'][] = ['label' => 'Faqs', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="user_container well" style="border-radius: 15px;">

    <h3 align="center" class="text-post">
        <?= Html::encode("Вопрос: ") ?>
    </h3>
    <h3 align="center" id="title">
        <?= Html::encode($this->title) ?>
    </h3>

    <div class="list-group-item border-green border_radius post-font ">
        <p>
            <?= Html::a('Обновить', ['update', 'id' => $model->id], ['class' => 'btn btn-primary radius']) ?>
            <?= Html::a('Ко всем вопросам', ['index'], ['class' => 'btn btn-default radius']) ?>
            <?= Html::a('Удалить', ['delete', 'id' => $model->id], [
                'class' => 'btn btn-danger radius',
                'data' => [
                    'confirm' => 'Вы уверены, что хотите удалить этот вопрос?',
                    'method' => 'post',
                ],
            ]) ?>
        </p>
        <?= DetailView::widget([
            'model' => $model,
            'attributes' => [
                'id',
                'title',
                'text:html',
            ],
        ]) ?>

    </div>

</div>
