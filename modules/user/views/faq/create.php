<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\modules\user\models\Faq */

$this->title = 'Создать вопрос';
$this->params['breadcrumbs'][] = ['label' => 'Faqs', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="user_container well" style="border-radius: 15px;">

    <div class="list-group-item border-green border_radius post-font">

        <h3 align="center" class="text-post">
            <?= Html::encode($this->title)?>
        </h3>

        <?= $this->render('_form', [
            'model' => $model,
        ]) ?>

    </div>

</div>
