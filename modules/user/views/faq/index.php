<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $searchModel app\modules\user\models\SearchFaq */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Вопросы и ответы';
$this->params['breadcrumbs'][] = $this->title;
$faqs = \app\modules\user\models\Faq::find()->all();
?>
<div class="user_container well " style="border-radius: 15px;">
    <h3 align="center" class="text-post">
        <?= Html::encode($this->title) ?>
    </h3>
    <?php if (Yii::$app->user->identity['role_id'] >= 5): ?>
        <p align="center">
            <?= Html::a('Создать вопрос', ['create'], ['class' => 'btn btn-primary radius',
                'style' => 'font-size:16px;']) ?>
        </p>
    <?php endif; ?>


    <div class="row text-post margin" style="position: relative;">

        <?php if (isset($faqs)): ?>
            <?php foreach ($faqs as $faq): ?>
                <div class="list-group-item border_radius_margin post-font">
                    <?php if (Yii::$app->user->identity['role_id'] >= 5): ?>
                        <?= Html::a('Обновить', ['update', 'id' => $faq->id], ['class' => 'btn btn-primary radius_small']) ?>
                        <?= Html::a('Удалить', ['delete', 'id' => $faq->id], [
                            'class' => 'btn btn-danger radius_small',
                            'data' => [
                                'confirm' => 'Вы уверены, что хотите удалить этот вопрос?',
                                'method' => 'post',
                            ],
                        ]) ?>
                    <?php endif; ?>
                    <?=
                    \yiister\gentelella\widgets\Accordion::widget(
                        [
                            'items' => [
                                [
                                    'title' => "<p style='font-size: 20px' class='text-post'><strong>{$faq->title}</strong></p>",
                                    'content' => "{$faq->text}",
                                ],
                            ],
                        ]
                    );
                    ?>
                </div>
            <?php endforeach; ?>
        <?php endif; ?>
    </div>

    <?php if (!isset($faqs)): ?>
        <div class="list-group-item border-green" style="margin-top: 15px;">
            <strong>Здесь пока что нет вопросов. Скоро главные админы их создадут!</strong>
        </div>
    <?php endif; ?>

</div>
