<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use mihaildev\ckeditor\CKEditor;
use mihaildev\elfinder\ElFinder;

/* @var $this yii\web\View */
/* @var $model app\modules\user\models\Faq */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="faq-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'title')->textInput(['maxlength' => true, 'placeholder' => 'название вопроса']) ?>

    <?= $form->field($model, 'text')->widget(CKEditor::className(), [
        'editorOptions' => ElFinder::ckeditorOptions('elfinder',
            [/* Some CKEditor Options */
                'clientOptions' => ['height' => 500],
            ]),
    ]); ?>


    <div class="form-group" style="margin-top: 15px;">
        <?= Html::submitButton($model->isNewRecord ? 'Создать' : 'Изменить', ['class' => $model->isNewRecord ? 'btn btn-primary' : 'btn btn-warning',
            'style' => 'font-size:18px;']) ?>

        <?= Html::a('Все вопросы', ['index'], ['class' => 'btn btn-info',
            'style' => 'font-size:16px;margin-top:15px;']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
