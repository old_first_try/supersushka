<?php

namespace app\modules\user\models;

use Yii;
use yii\helpers\Html;
use yii\web\UploadedFile;

/**
 * This is the model class for table "video".
 *
 * @property integer $id
 * @property string $title
 * @property string $date
 * @property string $video1
 * @property string $video2
 * @property string $video3
 * @property string $video4
 * @property string $video5
 * @property string $video6
 * @property string $video7
 * @property string $title1
 * @property string $title2
 * @property string $title3
 * @property string $title4
 * @property string $title5
 * @property string $title6
 * @property string $title7
 */
class VideoSection extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'videosection';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['date'], 'safe'],
            [['date'], 'date', 'format' => 'php:Y-m-d'],
            [['date'], 'default', 'value' => date('Y-m-d')],

            [['title'], 'string', 'max' => 50],

            [['title', 'video1', 'title1'], 'required', 'message' => 'Это поле обязательно'],

            [['video1', 'video2', 'video3', 'video4', 'video5', 'video6', 'video7'], 'default', 'value' => null],
            [['title1', 'title2', 'title3', 'title4', 'title5', 'title6', 'title7'], 'default', 'value' => null],

            [['title', 'video1', 'video2', 'video3', 'video4', 'video5', 'video6', 'video7'], 'trim'],
            [['title1', 'title2', 'title3', 'title4', 'title5', 'title6', 'title7'], 'trim'],

            [['video1', 'video2', 'video3', 'video4', 'video5', 'video6', 'video7'], 'string', 'max' => 255],
            [['title1', 'title2', 'title3', 'title4', 'title5', 'title6', 'title7'], 'string', 'max' => 255],

        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Название секции',
            'date' => 'Дата загрузки секции',
            'video1' => 'Первый ролик',
            'video2' => 'Второй ролик',
            'video3' => 'Третий ролик',
            'video4' => 'Четвёртый ролик',
            'video5' => 'Пятый ролик',
            'video6' => 'Шестой ролик',
            'video7' => 'Седьмой ролик',
            'title1' => 'Заголовок 1',
            'title2' => 'Заголовок 2',
            'title3' => 'Заголовок 3',
            'title4' => 'Заголовок 4',
            'title5' => 'Заголовок 5',
            'title6' => 'Заголовок 6',
            'title7' => 'Заголовок 7',

        ];
    }

    public function sectionAdmin(VideoSection $newSection)
    {
        if ($this->validate()) {
            $newSection->title = Html::encode($this->title);
            //TODO: logic
            $newSection->save();
            return true;
        }
        return false;
    }
}
