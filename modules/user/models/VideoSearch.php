<?php

namespace app\modules\user\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\modules\user\models\VideoSection;

/**
 * VideoSearch represents the model behind the search form about `app\modules\user\models\VideoSection`.
 */
class VideoSearch extends VideoSection
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id'], 'integer'],
            [['title', 'date', 'video1', 'video2', 'video3', 'video4', 'video5', 'video6', 'video7'], 'safe'],
            [['title1', 'title2', 'title3', 'title4', 'title5', 'title6', 'title7'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = VideoSection::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'date' => $this->date,
        ]);

        $query->andFilterWhere(['like', 'title', $this->title])
            ->andFilterWhere(['like', 'video1', $this->video1])
            ->andFilterWhere(['like', 'video2', $this->video2])
            ->andFilterWhere(['like', 'video3', $this->video3])
            ->andFilterWhere(['like', 'video4', $this->video4])
            ->andFilterWhere(['like', 'video5', $this->video5])
            ->andFilterWhere(['like', 'video6', $this->video6])
            ->andFilterWhere(['like', 'video7', $this->video7])
            ->andFilterWhere(['like', 'title1', $this->video7])
            ->andFilterWhere(['like', 'title2', $this->video7])
            ->andFilterWhere(['like', 'title3', $this->video7])
            ->andFilterWhere(['like', 'title4', $this->video7])
            ->andFilterWhere(['like', 'title5', $this->video7])
            ->andFilterWhere(['like', 'title6', $this->video7])
            ->andFilterWhere(['like', 'title7', $this->video7]);

        return $dataProvider;
    }
}
