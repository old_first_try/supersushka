<?php

namespace app\modules\user\models;

use Yii;

/**
 * This is the model class for table "patches".
 *
 * @property integer $id
 * @property integer $indexArrayChunk
 * @property integer $status
 * @property integer $voting_level
 */
class Patches extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'patches';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [[ 'indexArrayChunk', 'status', 'voting_level'], 'integer'],
            [['indexArrayChunk', 'status', 'voting_level'], 'required'],
            [['indexArrayChunk' ], 'default', 'value' => 0],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'indexArrayChunk' => 'Index Array Chunk',
            'status' => 'Status',
            'voting_level' => 'Voting Level',
        ];
    }
}
