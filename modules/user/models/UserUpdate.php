<?php
/**
 * supersushka - UserUpdate.php
 *
 * Initial version by: Tom
 * Initial created on: 23.10.2017 6:18
 */

namespace app\modules\user\models;


use yii\base\Model;

class UserUpdate extends Model
{
    public $id;
    public $role_id;
    public $parent_id;
    public $name;
    public $surname;
    public $phone;
    public $country;
    public $sex;

    /**
     * @var User
     */
    private $_user;

    public function __construct(User $user, $config = [])
    {
        $this->_user = $user;
        $this->id = $user->id;
        $this->role_id = $user->role_id;
        $this->parent_id = $user->parent_id;
        $this->name = $user->name;
        $this->surname = $user->surname;
        $this->phone = $user->phone;
        $this->country = $user->country;
        $this->sex = $user->sex;
        parent::__construct($config);
    }

    public function rules()
    {
        return [
            [['role_id', 'parent_id', 'name', 'surname',
                'phone', 'country'], 'trim'],

            [['id', 'role_id', 'parent_id'], 'integer'],

            [['parent_id'], 'default', 'value' => 0],

            ['role_id', 'required'],
            ['parent_id', 'required'],

            ['name', 'required'],
            ['name', 'string', 'min' => 3],

            ['surname', 'required'],
            ['surname', 'string', 'min' => 3],

            ['phone', 'required'],
            ['phone', 'udokmeci\yii2PhoneValidator\PhoneValidator'],

//
            [['country'], 'required'],
            ['country', 'string', 'min' => 4, 'max' => 50],

            ['sex', 'required'],
            ['sex', 'string', 'min' => 5, 'max' => 7],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'role_id' => 'Роль',
            'parent_id' => 'Админ по заданиям',
            'name' => 'Имя',
            'surname' => 'Фамилия',
            'phone' => 'Телефон',
            'country' => 'Страна',
            'sex' => 'Пол',
        ];
    }

    public function update()
    {
        if ($this->validate()) {
            $user = $this->_user;
            $user->role_id = $this->role_id;
            $user->parent_id = $this->parent_id;
            $user->name = $this->name;
            $user->surname = $this->surname;
            $user->phone = $this->phone;
            $user->country = $this->country;
            $user->sex = $this->sex;
            return $user->update();
        } else {
            return false;
        }
    }
}