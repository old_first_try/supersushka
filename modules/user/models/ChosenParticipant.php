<?php
    /**
     * Created by PhpStorm.
     * User: Айдер
     * Date: 04.01.2018
     * Time: 17:40
     */

    namespace app\modules\user\models;


    use yii\base\Model;

    class ChosenParticipant extends Model
    {
        private $name;
        private $surname;

        public function rules()
        {
            return [
                // username and password are both required
                [['name', 'surname'], 'required', 'message' => 'Это поле обязательно'],
                [['name', 'surname'], 'string', 'max' => 50],
                [['name', 'surname'], 'trim'],
            ];
        }

        public function attributeLabels()
        {
            return [
                'name' => 'Имя',
                'surname' => 'Фамилия',
            ];
        }

    }