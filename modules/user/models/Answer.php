<?php

namespace app\modules\user\models;

use Yii;

/**
 * This is the model class for table "answer".
 *
 * @property integer $id
 * @property string $name
 * @property string $theme
 * @property string $text
 * @property string $email
 * @property integer $user_id
 * @property boolean $is_readed
 * @property string $date
 */
class Answer extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'answer';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['text'], 'string'],
            [['text'], 'required', 'message' => 'Это поле обязательно'],

            ['is_readed', 'default', 'value' => false],
            [['user_id'], 'integer'],
            [['name', 'theme'], 'string', 'max' => 50],
            [['email'], 'string', 'max' => 255],

            [['date'], 'date', 'format' => 'php:Y-m-d H:i:s'],
            [['date'], 'default', 'value' => date('Y-m-d H:i:s')],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Имя',
            'theme' => 'Тема',
            'text' => 'Содержание',
            'email' => 'E-mail',
            'user_id' => 'User ID',
            'is_readed' => 'Прочитан или нет',
            'date' => 'Дата',
        ];
    }


    public function answerToDB(User $_user, Answer $answer, UserQuestion $userQuestion)
    {
        if ($this->validate()) {
            $answer->name = $_user->name;
            $answer->email = $userQuestion->email;
            $answer->theme = $userQuestion->theme;
            $answer->text = $this->text;
            if (!isset(User::find()->where(['login' => $userQuestion->email])->one()->id))
                return false;
            $answer->user_id = User::find()->where(['login' => $userQuestion->email])->one()->id;
            $answer->save();
            return true;
        }
        return false;
    }

}
