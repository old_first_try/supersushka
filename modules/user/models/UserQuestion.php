<?php

namespace app\modules\user\models;

use Yii;

/**
 * This is the model class for table "user_question".
 *
 * @property integer $id
 * @property string $name
 * @property string $email
 * @property string $theme
 * @property string $text
 * @property integer $status
 * @property string $date
 */
class UserQuestion extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'user_question';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['text'], 'string'],
            [['status'], 'integer'],
            [['name', 'email', 'theme'], 'string', 'max' => 50],

            [['date'], 'date', 'format' => 'php:Y-m-d H:i:s'],
            [['date'], 'default', 'value' => date('Y-m-d H:i:s')],

        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Имя',
            'email' => 'E-mail',
            'theme' => 'Тема',
            'text' => 'Содержание',
            'status' => 'Статус',
        ];
    }
}