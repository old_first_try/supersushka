<?php

namespace app\modules\user\models;

use Yii;

/**
 * This is the model class for table "task".
 *
 * @property integer $id
 * @property string $title
 * @property string $date
 * @property string $content
 * @property integer $week
 * @property integer $status
 * @property string $videoDo
 * @property string $videoPosle
 */
class Task extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'task';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [

            [['date'], 'date', 'format' => 'php:Y-m-d'],
            [['date'], 'default', 'value' => date('Y-m-d')],

            [['title', 'content', 'week'], 'required', 'message'=>'Это поле обязательно'],
            [['title', 'videoDo', 'videoPosle'],
                'string', 'max' => 255],
            [[ 'videoDo', 'videoPosle'], 'default', 'value' => null],
            [['videoDo', 'videoPosle'], 'trim'],

            [['content'], 'string'],
            [['status'], 'integer'],
            [['status'], 'default', 'value' => 0],


        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Заголовок',
            'date' => 'Дата',
            'content' => 'Напишите содержание задания',
            'videoDo' => 'Вставьте ссылку на задание 1',
            'videoPosle' => 'Вставьте ссылку на второе видео, если требуется',
            'status' => 'Поставьте галочку, если задание обязательное',
            'week' => 'Неделя',
        ];
    }

    public function senMails()
    {
        $begin = 2;
        $end = 4;
        $users = User::find()->where(['and', "role_id>=$begin", "role_id<=$end"])->all();
        foreach ($users as $user) {

            $login = $user->login;
            $name = $user->name;
            Yii::$app->mailer->compose('task', compact('login', 'name'))
                ->setFrom([Yii::$app->params['supportEmail'] => 'СУПЕРСУШКА'])
                ->setTo($login)
                ->setSubject('Обязательное задание!')
                ->send();
        }
    }

    /*
 * Matches each symbol of PHP date format standard
 * with jQuery equivalent codeword
 * @author Tristan Jahier
 */
    function dateformat_PHP_to_jQueryUI($php_format)
    {
        $SYMBOLS_MATCHING = array(
            // Day
            'd' => 'dd',
            'D' => 'D',
            'j' => 'd',
            'l' => 'DD',
            'N' => '',
            'S' => '',
            'w' => '',
            'z' => 'o',
            // Week
            'W' => '',
            // Month
            'F' => 'MM',
            'm' => 'mm',
            'M' => 'M',
            'n' => 'm',
            't' => '',
            // Year
            'L' => '',
            'o' => '',
            'Y' => 'yy',
            'y' => 'y',
            // Time
            'a' => '',
            'A' => '',
            'B' => '',
            'g' => '',
            'G' => '',
            'h' => '',
            'H' => '',
            'i' => '',
            's' => '',
            'u' => ''
        );
        $jqueryui_format = "";
        $escaping = false;
        for($i = 0; $i < strlen($php_format); $i++)
        {
            $char = $php_format[$i];
            if($char === '\\') // PHP date format escaping character
            {
                $i++;
                if($escaping) $jqueryui_format .= $php_format[$i];
                else $jqueryui_format .= '\'' . $php_format[$i];
                $escaping = true;
            }
            else
            {
                if($escaping) { $jqueryui_format .= "'"; $escaping = false; }
                if(isset($SYMBOLS_MATCHING[$char]))
                    $jqueryui_format .= $SYMBOLS_MATCHING[$char];
                else
                    $jqueryui_format .= $char;
            }
        }
        return $jqueryui_format;
    }
}
