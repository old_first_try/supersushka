<?php

namespace app\modules\user\models;

use Yii;

/**
 * This is the model class for table "voting".
 *
 * @property integer $id
 * @property string $photoDo
 * @property string $photoPosle
 * @property integer $user_id
 * @property integer $balls
 * @property integer $status
 * @property integer $chance
 * @property integer $changeChance
 * @property integer $task_id
 */
class Voting extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'voting';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [

            [['user_id', 'balls', 'status', 'chance', 'changeChance', 'task_id'], 'integer'],
            [['photoDo', 'photoPosle'], 'string', 'max' => 255],
//            [['status'], 'required'],
//            [['status'], 'default', 'value' => 0],

        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'photoDo' => 'Photo Do',
            'photoPosle' => 'Photo Posle',
            'user_id' => 'User ID',
            'balls' => 'Баллы',
            'status' => 'Выбрать участника',
            'chance' => 'Chance',
            'changeChance' => 'Воспользоваться иммунитетом',
            'task_id' => 'Task ID',
        ];
    }
}
