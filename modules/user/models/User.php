<?php

namespace app\modules\user\models;

use Yii;
use yii\web\IdentityInterface;

/**
 * This is the model class for table "user".
 *
 * @property integer $id
 * @property string $login
 * @property string $password
 * @property string $password_reset_token
 * @property string $auth_key
 * @property integer $role_id
 * @property integer $parent_id
 * @property string $name
 * @property string $surname
 * @property string $phone
 * @property string $instagram
 * @property string $vk
 * @property string $country
 * @property string $city
 * @property string $birthday
 * @property string $sex
 * @property integer $users
 * @property string $parent_name
 */
class User extends \yii\db\ActiveRecord implements IdentityInterface
{

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'user';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['login', 'password', 'role_id', 'parent_id', 'name', 'surname', 'instagram',
                'vk', 'country', 'city', 'sex', 'users'], 'trim'],

            [['login', 'password', 'auth_key', 'password_reset_token'], 'string', 'max' => 255],
            ['login', 'email', 'message' => 'Введите правильный e-mail адрес'],

            [['password_reset_token'], 'unique'],

//            ['login', 'checkEmail'],

            [['login'/*, 'password'*/], 'required', 'message' => 'Это поле обязательно!'],

            ['password', 'string', 'min' => 6, 'message' => 'Пароль должен быть не меньше 6 символов!'],

            [['id', 'role_id', 'parent_id', 'users'], 'integer'],
            [['parent_id'], 'default', 'value' => 0],
            [['users'], 'default', 'value' => 0],

            [['name', 'surname', 'phone'], 'required', 'message' => 'Это поле обязательно!'],
            ['phone', 'udokmeci\yii2PhoneValidator\PhoneValidator'],

            [['instagram', 'vk', 'country'], 'string', 'max' => 50],
            ['city', 'string', 'max' => 100],

        ];
    }

    public function checkEmail($attribute)
    {
        $newUserEmail = $this->$attribute;
        $checkUser = User::findOne(['login' => $newUserEmail]);
        //user CHECK
        if (isset($checkUser)) {
            $this->addError($attribute, "Пользователь с таким e-mail уже есть в системе!");
        }

    }


    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'login' => 'E-mail',
            'password' => 'Пароль',
            'password_reset_token' => 'Password Reset Token',
            'role_id' => 'Роль',
            'parent_id' => 'Админ по заданиям',
            'parent_name' => 'Админ по заданиям',
            'name' => 'Имя',
            'surname' => 'Фамилия',
            'phone' => 'Телефон',
            'instagram' => 'Instagram',
            'vk' => 'Vk',
            'country' => 'Страна',
            'city' => 'Город',
            'birthday' => 'Дата рождения',
            'sex' => 'Пол',
            'users' => 'Количество подопечных (для администратора)',
        ];
    }

    /**
     * Finds an identity by the given ID.
     * @param string|int $id the ID to be looked for
     * @return IdentityInterface the identity object that matches the given ID.
     * Null should be returned if such an identity cannot be found
     * or the identity is not in an active state (disabled, deleted, etc.)
     */
    public static function findIdentity($id)
    {
        return static::findOne($id);
    }

    /**
     * Finds an identity by the given token.
     * @param mixed $token the token to be looked for
     * @param mixed $type the type of the token. The value of this parameter depends on the implementation.
     * For example, [[\yii\filters\auth\HttpBearerAuth]] will set this parameter to be `yii\filters\auth\HttpBearerAuth`.
     * @return IdentityInterface the identity object that matches the given token.
     * Null should be returned if such an identity cannot be found
     * or the identity is not in an active state (disabled, deleted, etc.)
     */
    public static function findIdentityByAccessToken($token, $type = null)
    {
//        return static::findOne(['access_token' => $token]);
    }

    /**
     * Returns an ID that can uniquely identify a user identity.
     * @return string|int an ID that uniquely identifies a user identity.
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Returns a key that can be used to check the validity of a given identity ID.
     *
     * The key should be unique for each individual user, and should be persistent
     * so that it can be used to check the validity of the user identity.
     *
     * The space of such keys should be big enough to defeat potential identity attacks.
     *
     * This is required if [[User::enableAutoLogin]] is enabled.
     * @return string a key that is used to check the validity of a given identity ID.
     * @see validateAuthKey()
     */
    public function getAuthKey()
    {
        return $this->auth_key;
    }

    /**
     * Validates the given auth key.
     *
     * This is required if [[User::enableAutoLogin]] is enabled.
     * @param string $authKey the given auth key
     * @return bool whether the given auth key is valid.
     * @see getAuthKey()
     */
    public function validateAuthKey($authKey)
    {
        return $this->auth_key === $authKey;
    }

    public static function findByLogin($login)
    {
        return static::findOne(['login' => $login]);
    }

    public function validatePassword($password)
    {
//        return $this->password === $password;
        return \Yii::$app->security->validatePassword($password, $this->password);
    }

    public function generateAuthKey()
    {
        $this->auth_key = \Yii::$app->security->generateRandomString();
    }

    public function setPassword($newPassword)
    {
        $this->password = Yii::$app->getSecurity()->generatePasswordHash($newPassword);
    }

    public function create(User $model)
    {
        if ($model->role_id == 4) {
            $newPassword = 7777777;
        }
        if ($model->role_id != 4) {
            $newPassword = Yii::$app->getSecurity()->generateRandomString(7);
        }

        if ($this->validate()) {
            $this->sendMail($newPassword);
            $this->password = Yii::$app->getSecurity()->generatePasswordHash($newPassword);
//            $admin = \app\modules\user\models\User::find()->where(['parent_id' => $this->parent_id])->one();
//            $fio = $admin->name . " " . $admin->surname;
//            $this->parent_name = $fio;
            $this->save();
            return true;
        } else
            return false;
    }

    public function sendMail($newPassword)
    {
        $login = $this->login;
        $password = $newPassword;
        $name = $this->name;
        $role_id = $this->role_id;
        Yii::$app->mailer->compose('order', compact('login', 'password', 'name', 'role_id'))
            ->setFrom([Yii::$app->params['supportEmail'] => 'СУПЕРСУШКА'])
            ->setTo($login)
            ->setSubject('Регистрация на sypersushka.ru')
            ->send();
    }

    public static function fio($parent_id)
    {
        if ($parent_id == 0)
            return "Это не участник игры!";
        $admin = \app\modules\user\models\User::find()->where(['id' => $parent_id])->one();
        $fio = $admin->name . " " . $admin->surname;
        return $fio;
    }

    public static function Chance($user_id)
    {
        $user = User::findOne(['id' => $user_id]);
        switch ($user->role_id):
            case 1:
                return 'Пользователь - Пробный';
                break;
            case 2:
                return 'Пользователь - Участник';
                break;
            case 3:
                return 'Пользователь - Иммунитет';
                break;
            case 4:
                return 'Пользователь - ПОЛНЫЙ иммунитет';
                break;
        endswitch;
        return false;
    }

    //Password Reset Token
    public static function findByPasswordResetToken($token)
    {

        if (!static::isPasswordResetTokenValid($token)) {
            return null;
        }

        return static::findOne([
            'password_reset_token' => $token,
        ]);
    }

    public static function isPasswordResetTokenValid($token)
    {

        if (empty($token)) {
            return false;
        }

        $timestamp = (int)substr($token, strrpos($token, '_') + 1);
        $expire = Yii::$app->params['user.passwordResetTokenExpire'];
        return $timestamp + $expire >= time();
    }

    public function generatePasswordResetToken()
    {
        $this->password_reset_token = Yii::$app->security->generateRandomString() . '_' . time();
    }

    public function removePasswordResetToken()
    {
        $this->password_reset_token = null;
    }

}
