<?php
/**
 * Created by PhpStorm.
 * User: ayder
 * Date: 8/19/17
 * Time: 6:07 PM
 */

namespace app\modules\user\models;


use Yii;
use yii\base\Model;

class RecoverForm extends Model
{
    public $email;

    public function rules()
    {
        return [
            ['email', 'trim'],
            ['email', 'required',  'message' => 'Это поле обязательно'],
            ['email', 'email', 'message' => 'Введите корректный E-mail адрес'],

            ['email', 'checkEmail'],
        ];
    }

    public function checkEmail($attribute)
    {
        $voting_login = $this->$attribute;
        $checkUser = User::findOne(['login' => $voting_login]);
        //user CHECK
        if (!isset($checkUser)) {
            $this->addError($attribute, "Пользователя с таким e-mail нет в системе! Попробуйте снова $)");
        }
    }

    public function attributeLabels()
    {
        return [
            'email' => 'E-mail',
        ];
    }

//    public function sendMail($login)
//    {
//        $sendLogin = $login;
//        $users = User::find()->where(['and', "role_id>=$begin", "role_id<=$end"])->all();
//        foreach ($users as $user) {
//
//            $login = $user->login;
//            $name = $user->name;
//            Yii::$app->mailer->compose('task', compact('login', 'name'))
//                ->setFrom('info@sypersushka.ru')
//                ->setTo($login)
//                ->setSubject('Обязательное задание!')
//                ->send();
//        }
//    }

    /**
     * Sends an email with a link, for resetting the password.
     *
     * @return bool whether the email was send
     */
    public function sendEmail()
    {
        /* @var $user User */
        $user = User::findOne([
            'login' => $this->email,
        ]);

        if (!$user) {
            return false;
        }

        if (!User::isPasswordResetTokenValid($user->password_reset_token)) {
            $user->generatePasswordResetToken();
            if (!$user->save()) {
                return false;
            }
        }

        return Yii::$app
            ->mailer
            ->compose(
                ['html' => 'passwordResetToken-html', 'text' => 'passwordResetToken-text'],
                ['user' => $user]
            )
            ->setFrom([Yii::$app->params['supportEmail'] => 'СУПЕРСУШКА'])
            ->setTo($this->email)
            ->setSubject('Сброс пароля для ' . Yii::$app->name)
            ->send();
    }

}