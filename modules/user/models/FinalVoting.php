<?php

namespace app\modules\user\models;

use Yii;

/**
 * This is the model class for table "finalvoting".
 *
 * @property integer $id
 * @property string $name
 * @property string $user_email
 * @property string $voting_login
 */
class FinalVoting extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'finalvoting';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'user_email', 'voting_login'], 'string', 'max' => 255],
            [['name', 'user_email', 'voting_login'], 'required'],
            [['user_email', 'voting_login'], 'email'],
            [['name', 'user_email', 'voting_login'], 'trim'],
//            [['name'], 'checkAgain'],
            [['user_email'], 'checkAgain'],
            [['voting_login'], 'checkEmail'],

        ];
    }

    public function checkEmail($attribute)
    {
        $voting_login = $this->$attribute;
        $checkUser = User::findOne(['login' => $voting_login]);
        //user CHECK
        if (!isset($checkUser)) {
            Yii::$app->session->setFlash("Пользователя с таким e-mail нет в системе! Попробуйте снова $)");
            $this->addError($attribute, "Пользователя с таким e-mail нет в системе! Попробуйте снова $)");
        }
        //voting check
        if (isset($checkUser)) {
            $voting = Voting::findOne(['user_id' => $checkUser->id]);
            if (!isset($voting)) {

                Yii::$app->session->setFlash("Пользователь с таким e-mail не участвует в 3 туре голосования!");
                $this->addError($attribute, "Пользователь с таким e-mail не участвует в 3 туре голосования! Попробуйте снова $)");
            }
        }


        if (isset($voting) && $voting->balls > 950) {
            Yii::$app->session->setFlash("Этот участик уже набрал 50 голосов! 
            Проголосуйте за другого участника, если хотите $)");
            $this->addError($attribute, "Этот участик уже набрал 50 голосов! 
            Проголосуйте за другого участника, если хотите $)");
        }
    }

    public function checkAgain($attribute)
    {
        $user_mail = $this->$attribute;
        $name = $this->$attribute;
        $finalVoting = FinalVoting::findOne(['user_email' => $user_mail]);
        $finalVotingName = FinalVoting::findOne(['name' => $name]);
        if (isset($finalVoting) || isset($finalVotingName)) {
            Yii::$app->session->setFlash("Вы уже голосовали!");
            $this->addError($attribute, "Вы уже голосовали!");
        }
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Имя',
            'user_email' => 'Ваш E-mail',
            'voting_login' => 'E-mail участника сервиса "СУПЕРСУШКА", за которого Вы голосуете',
        ];
    }

    public function sendMails()
    {

        $voting_name = $this->name;
        $voting_email = $this->user_email;
        $user = User::findOne(['login' => $this->voting_login]);
        Yii::$app->mailer->compose('third_voting', compact('voting_name', 'voting_email', 'user'))
            ->setFrom([Yii::$app->params['supportEmail'] => 'СУПЕРСУШКА'])
            ->setTo($voting_email)
            ->setSubject('Финальное голосование!')
            ->send();
    }
}
