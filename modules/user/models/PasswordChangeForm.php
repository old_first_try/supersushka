<?php
/**
 * Created by PhpStorm.
 * User: Tom
 * Date: 26.08.2017
 * Time: 20:28
 */

namespace app\modules\user\models;


use Yii;
use yii\base\Model;
use yii\helpers\Html;


class PasswordChangeForm extends Model
{
    public $currentPassword;
    public $newPassword;
    public $newPasswordRepeat;

    private $_user;

    /**
     * @param User $user
     * @param array $config
     */
    public function __construct(User $user, $config = [])
    {
        $this->_user = $user;
//        $this->login = $user->login;
        parent::__construct($config);
    }

    public function rules()
    {
        return [
            [['currentPassword', 'newPassword', 'newPasswordRepeat',/*'newLogin', 'newLoginRepeat'*/], 'required',
                'message' => 'Это поле обязательно'],
            [['currentPassword', 'newPassword', 'newPasswordRepeat', /*'login', 'newLogin'*/], 'trim'],
            ['currentPassword', 'currentPassword'],
            ['newPassword', 'string', 'min' => 6],
            ['newPasswordRepeat', 'compare', 'compareAttribute' => 'newPassword'],
            /*['newLoginRepeat', 'compare', 'compareAttribute' => 'newLogin'],

            [['login', 'newLogin', 'newLoginRepeat'], 'email', 'message' => 'Введите корректный e-mail!']*/
        ];
    }

    public function attributeLabels()
    {
        return [
            'newPassword' => 'Новый пароль',
            'newPasswordRepeat' => 'Повторите новый пароль',
            'currentPassword' => 'Старый пароль',
           /* 'login' => 'E-mail',
            'newLogin' => 'Новый E-mail',
            'newLoginRepeat' => 'Повторите новый E-mail'*/
        ];
    }

    /**
     * @param string $attribute
     * @param array $params
     */
    public function currentPassword($attribute, $params)
    {
        if (!$this->hasErrors()) {
            if (!$this->_user->validatePassword($this->$attribute)) {
                $this->addError($attribute, Yii::t('app', 'Ошибка! Неправильный текущий пароль!'));
            }
        }
    }


    /**
     * @return boolean
     */
    public function changePassword()
    {
        if ($this->validate()) {
            $user = $this->_user;
            $user->setPassword($this->newPassword);
//            $user->login = $this->login;
            return $user->save();
        } else {
            return false;
        }
    }

}