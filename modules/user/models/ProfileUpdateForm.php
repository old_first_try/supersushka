<?php
/**
 * Created by PhpStorm.
 * User: Tom
 * Date: 26.08.2017
 * Time: 19:23
 */

namespace app\modules\user\models;


use Yii;
use yii\base\Model;
use yii\helpers\Html;

class ProfileUpdateForm extends Model
{
    public $name;
    public $surname;
    public $phone;
    public $instagram;
    public $vk;
    public $country;
    public $city;
    public $birthday;
    public $sex;

    /**
     * @var User
     */
    private $_user;

    public function __construct(User $user, $config = [])
    {
        $this->_user = $user;
        $this->name = $user->name;
        $this->surname = $user->surname;
        $this->phone = $user->phone;
        $this->instagram = $user->instagram;
        $this->vk = $user->vk;
        $this->country = $user->country;
        $this->city = $user->city;
        $this->birthday = $user->birthday;
        $this->sex = $user->sex;
        parent::__construct($config);
    }

    public function rules()
    {
        return [
            [['name', 'surname',
                'phone', 'instagram', 'vk',
                'city', 'country'], 'trim'],

            ['name', 'required'],
            ['name', 'string', 'min'=>3],

            ['surname', 'required'],
            ['surname', 'string', 'min'=>3],

            ['phone', 'required'],
            ['phone', 'udokmeci\yii2PhoneValidator\PhoneValidator'],

//            ['instagram', 'required'],
            ['instagram', 'string', 'min'=>5],

//            ['vk', 'required'],
            ['vk', 'string', 'min'=>5],

            [['country'], 'required'],
            ['country', 'string','min'=>4, 'max'=>50],

            ['city', 'required'],
            ['city', 'string', 'min'=>4,'max'=>50],

            ['birthday', 'required'],

            ['sex', 'required'],
            ['sex', 'string', 'min'=>5,'max'=>7],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'login' => 'E-mail',
            'name' => 'Имя',
            'surname' => 'Фамилия',
            'phone' => 'Телефон',
            'instagram' => 'Instagram',
            'vk' => 'Vk',
            'country' => 'Страна',
            'city' => 'Город',
            'birthday' => 'Дата рождения',
            'sex' => 'Пол',
        ];
    }

    public function update()
    {
        if ($this->validate()) {
            $user = $this->_user;
            $user->name = Html::encode($this->name);
            $user->surname = Html::encode($this->surname);
            $user->phone = Html::encode($this->phone);
            $user->instagram = Html::encode($this->instagram);
            $user->vk = Html::encode($this->vk);
            $user->country = $this->country;
            $user->city = $this->city;
            $user->birthday = $this->birthday;
            $user->sex = $this->sex;
            return $user->save();
        } else {
            return false;
        }
    }
}