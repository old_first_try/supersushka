<?php

namespace app\modules\user\models;

use Yii;
use yii\web\UploadedFile;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "image".
 *
 * @property integer $id
 * @property string $avatar
 * @property string $filename
 * @property integer $profile_id
 */
class Image extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'image';
    }

    /**
     * @var UploadedFile file attribute
     */
    public $file;


    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['profile_id'], 'required'],
            [['profile_id'], 'integer'],
            [['avatar', 'filename'], 'string', 'max' => 50],
            [['file'], 'file'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'avatar' => 'Avatar',
            'filename' => 'Filename',
            'profile_id' => 'ProfileOLD ID',
            'file' => ' Нажмите сюда, чтобы поменять фото',
        ];
    }

}
