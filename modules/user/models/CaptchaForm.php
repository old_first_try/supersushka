<?php
/**
 * supersushka - CaptchaForm.php
 *
 * Initial version by: Tom
 * Initial created on: 16.09.2017 15:43
 */

namespace app\modules\user\models;

use Yii;
use yii\base\Model;
use yii\helpers\Html;


/**
 * CaptchaForm is the model behind the contact form.
 */
class CaptchaForm extends Model
{
    public $name;
    public $email;
    public $subject;
    public $body;
    public $verifyCode;
    //notify
    public $question;


    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            // name, email, subject and body are required
            [['name', 'subject', 'body', 'verifyCode'], 'required'],
            [['name', 'subject', 'body', 'verifyCode'], 'trim'],
            // email has to be a valid email address
            [['name', 'email', 'subject'], 'string', 'max' => 50, 'message' => 'Сократите тему до 50 символов'],
            [['body'], 'string'],

            // verifyCode needs to be entered correctly
            ['verifyCode', 'captcha', 'captchaAction' => '/user/help/captcha'],
        ];
    }

    /**
     * @return array customized attribute labels
     */
    public function attributeLabels()
    {
        return [
            'verifyCode' => 'Введите результат примера:',
            'name' => 'Имя',
            'email' => 'E-mail',
            'subject' => 'Тема вопроса',
            'body' => 'Содержание',
        ];
    }

    /**
     * Sends an email to the specified email address using the information collected by this model.
     * @param string $email the target email address
     * @return bool whether the model passes validation
     */
    public function contact($email)
    {
        if ($this->validate()) {
            Yii::$app->mailer->compose()
                ->setTo($email)
                ->setFrom([$this->email => $this->name])
                ->setSubject($this->subject)
                ->setTextBody($this->body)
                ->send();

            return true;
        }
        return false;
    }

    /**
     * Saves an user question to database using the information collected by this model.
     * @return bool whether the model passes validation
     */
    public function questionToDB()
    {
        if ($this->validate()) {
            $question = new UserQuestion();
            $question->name = Html::encode($this->name);
            $question->email = Yii::$app->user->identity['login'];
            $question->theme = Html::encode($this->subject);
            $question->text = Html::encode($this->body);
            $question->status = 1;
            $this->question = $question;
            if ($question->validate()) {
                $question->save();
                return true;
            }
        }
        return false;
    }

    public function getQuestion(){
        return $this->question;
    }

}
