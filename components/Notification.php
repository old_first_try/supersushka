<?php
/**
 * Created by PhpStorm.
 * User: deka
 * Date: 11/24/17
 * Time: 8:49 PM
 */

namespace app\components;

use app\modules\user\models\User;
use Yii;
use app\modules\user\models\Task;
use app\models\Message;
use machour\yii2\notifications\models\Notification as BaseNotification;


class Notification extends BaseNotification
{

    /**
     * A new message notification
     */
    const KEY_NEW_MESSAGE = 'new_message';
    /**
     * A meeting reminder notification
     */
    const KEY_TASK_REMINDER = 'task_reminder';
    /**
     * No disk space left !
     */
    const KEY_NO_DISK_SPACE = 'no_disk_space';

    /**
     * @var array Holds all usable notifications
     */
    public static $keys = [
        self::KEY_NEW_MESSAGE,
        self::KEY_TASK_REMINDER,
        self::KEY_NO_DISK_SPACE,
    ];

    /**
     * @inheritdoc
     */
    public function getTitle()
    {
        switch ($this->key) {
            case self::KEY_TASK_REMINDER:
                return Yii::t('app', 'Task reminder');

            case self::KEY_NEW_MESSAGE:
                return Yii::t('app', 'You got a new message');

            case self::KEY_NO_DISK_SPACE:
                return Yii::t('app', 'No disk space left');
        }
    }

    /**
     * @inheritdoc
     */
    public function getDescription()
    {
        switch ($this->key) {
            case self::KEY_TASK_REMINDER:
                $task = Task::findOne($this->key_id);
                return Yii::t('app', 'You are task named {title}', [
                    'title' => $task->title
                ]);

            case self::KEY_NEW_MESSAGE:
                $message = Message::findOne($this->key_id);
                $user = User::findOne($this->key_id);
                return Yii::t('app', '{admin} sent you a message', [
                    'admin' => $user->name
                ]);

            case self::KEY_NO_DISK_SPACE:
                // We don't have a key_id here
                return 'Please buy more space immediately';
        }
    }

    /**
     * @inheritdoc
     */
    public function getRoute()
    {
        switch ($this->key) {
            case self::KEY_TASK_REMINDER:
                return ['/message/current_task', 'task_id' => $this->key_id];

            case self::KEY_NEW_MESSAGE:
                return ['message/read', 'id' => $this->key_id];

            case self::KEY_NO_DISK_SPACE:
                return 'https://aws.amazon.com/';
        };
    }

}