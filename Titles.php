<?php
/**
 * Created by PhpStorm.
 * User: deka
 * Date: 12/6/17
 * Time: 7:30 PM
 */

class Titles
{
    //common titles
    public static $PROJECT_NAME = 'Суперсушка';
    public static $USER_SETTINGS = 'Кабинет пользователя';
    public static $USER_PSSWD = 'Смена пароля';
    public static $USER_AVATAR = 'Смена аватарки';
    //user titles
    public static $USER_PAGE = 'Страница участника';
    public static $NEWS = 'Новостная лента';
    public static $NEWS_FOLLOWS = 'Новости подписок';
    public static $SEARCH = 'Поиск';
    public static $MY_TASKS = 'Мои задания';
    public static $VIDEO_SECTION = 'Видеотека';
    public static $FEEDBACK = 'Обратная связь';
    public static $FAQ = 'Вопросы и ответы';

    //admin titles
    public static $ADMIN_PAGE = 'Страница админа';
    public static $ALL_USERS = 'Все пользователи';
    public static $ALL_TASKS = 'Все задания';
    public static $MESSAGES = 'Отчёты участников';
    public static $QUESTIONS_AND_REPORTS = 'Вопросы и жалобы';
    public static $VOTING = 'Голосование';
    public static $FIRST_VOTING = 'Первый тур голосования';
    public static $SECOND_VOTING = 'Второй тур голосования';
    public static $THIRD_VOTING = 'Третий тур голосования';
    public static $THIRD_VOTING_INNER = 'Третий тур голосования - внутренняя часть';
    public static $THIRD_VOTING_OUTPUT = 'Третий тур голосования - внешняя часть';
    public static $LANDING = 'Ссылка на лендинг';

    //errors
    public static $AUTH_ERROR = 'Ошибка авторизации';
    public static $SITE_ERROR = 'Ошибка сайта';
    public static $SERVER_ERROR = 'Ошибка сервера';

}