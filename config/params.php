<?php

return [
    'adminEmail' => 'ayderumerov.com@gmail.com',
    'supportEmail' => 'info@sypersushka.ru',
    'user.passwordResetTokenExpire' => 3600,
];
